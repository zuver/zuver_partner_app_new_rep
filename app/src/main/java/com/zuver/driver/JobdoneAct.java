package com.zuver.driver;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.zuver.driver.data.CommonData;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.service.APIService_Volley_JSON;
import com.zuver.driver.service.NonActivity;
import com.zuver.driver.utils.FontHelper;
import com.zuver.driver.utils.SessionSave;

import org.json.JSONException;
import org.json.JSONObject;

public class JobdoneAct extends MainActivity
{
    // Class members declarations
    private TextView fareTxt, txtPassName, back;
    private ImageView txtPassImage;
    private TextView referalTxt;
    private TextView HeadTitle;
    private Button back_main;
    private String j_fare;
    private String j_referal;
    private double m_fare = 0.0;
    private String message;
    private String ratingBarValue = "0";
    NonActivity nonactiityobj = new NonActivity();

    RatingBar ratingBar;
    private DisplayImageOptions options;

    // Set the layout to activity.
    @Override
    public int setLayout()
    {
        setLocale();
        return R.layout.paypopup_lay;
    }

    // Initialize the views on layout
    @Override
    public void Initialize()
    {
        ratingBar = (RatingBar) findViewById(R.id.ratingBar1);
        Bundle bun = getIntent().getExtras();
        SessionSave.saveSession("status", "F", getApplicationContext());
        nonactiityobj.startServicefromNonActivity(JobdoneAct.this);
        FontHelper.applyFont(this, findViewById(R.id.inner_content), "DroidSans.ttf");
        CommonData.current_act = "JobdoneAct";

        if (bun != null)
        {
            message = bun.getString("message");
            fareTxt = (TextView) findViewById(R.id.fareTxt);
            txtPassName = (TextView) findViewById(R.id.txt_pass_name);
            referalTxt = (TextView) findViewById(R.id.jobreferralTxt);
            HeadTitle = (TextView) findViewById(R.id.headerTxt);
            back = (TextView) findViewById(R.id.backup);
            back.setVisibility(View.GONE);
            txtPassImage = (ImageView) findViewById(R.id.img_pass_image);
            back_main = (Button) findViewById(R.id.back_main);
            HeadTitle.setText("" + getResources().getString(R.string.submit_rating));
            CommonData.km_calc = 0;
            SessionSave.saveSession("speedwaiting", "", JobdoneAct.this);
            SessionSave.saveSession("speedwaitingTime1", "", JobdoneAct.this);
            SessionSave.saveSession("speedwaitingTime2", "", JobdoneAct.this);

            CommonData.currentspeed = "";
            SessionSave.saveSession("drop_location", "", JobdoneAct.this);
            SessionSave.saveSession("waitingHr", "", JobdoneAct.this);

            options = new DisplayImageOptions.Builder().showImageOnLoading(null) // resource
                    // or
                    // drawable
                    .showImageForEmptyUri(null) // resource or drawable
                    .showImageOnFail(null) // resource or drawable
                    .resetViewBeforeLoading(false) // default
                    .delayBeforeLoading(1000).cacheInMemory(true) // default
                    .cacheOnDisc(true) // default
                    .imageScaleType(ImageScaleType.EXACTLY_STRETCHED) // default
                    .bitmapConfig(Bitmap.Config.RGB_565) // default
                    .displayer(new SimpleBitmapDisplayer()) // default
                    .handler(new Handler()) // default
                    // .showImageOnLoading(R.drawable.ic_stub)
                    .build();

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).defaultDisplayImageOptions(options).build();
            ImageLoader.getInstance().init(config);
            // Show the job completion notification with fare,id and payment type.
            txtPassName.setText("" + SessionSave.getSession("passenger_name", JobdoneAct.this));


        }

        String Image_path = SessionSave.getSession("p_image", JobdoneAct.this);

        if (Image_path != null && Image_path.length() > 0)
        {
            Log.e("Profile Image", "" + SessionSave.getSession("p_image", JobdoneAct.this));
            ImageLoader.getInstance().displayImage(Image_path, txtPassImage, options);
        }

        ratingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener()
        {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser)
            {
                ratingBarValue = String.valueOf(ratingBar.getRating());
            }
        });


        back.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                SessionSave.saveSession("trip_id", "", JobdoneAct.this);
                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                finish();
            }
        });

        // This for close this activity and move to dashboard activity.
        back_main.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                if (ratingBarValue.equalsIgnoreCase("0"))
                {
                    alert_view(JobdoneAct.this, "Message", "Please provide rating", "OK", "");
                }
                else
                {
                    SessionSave.saveSession("trip_id", "", JobdoneAct.this);
                    final String url = "update_ratings_comments";
                    new RatingAPI(url, ratingBarValue);
                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    finish();
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        ShowToast(JobdoneAct.this, "Please rate the passenger");
    }
    // This is used for getting passenger rating from driver application.

    private class RatingAPI implements APIResult
    {
        private RatingAPI(final String url, String rating)
        {
            try
            {
                JSONObject j = new JSONObject();
                j.put("pass_id", "" + MainActivity.mMyStatus.getpassengerId());
                j.put("ratings", "" + rating);
                j.put("comments", "");
                j.put("user_type", "D");
                new APIService_Volley_JSON(JobdoneAct.this, this, j, false).execute(url);
            }
            catch (Exception e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        @Override
        public void getResult(final boolean isSuccess, final String result)
        {
            try
            {
                if (isSuccess)
                {
                    final JSONObject json = new JSONObject(result);

                    if (json.getInt("status") == 1)
                    {
                        alert_view(JobdoneAct.this, "" + getResources().getString(R.string.message), "" + json.getString("message"), "" + getResources().getString(R.string.ok), "");
                    }
                    else if (json.getInt("status") == -1)
                    {
                        //User has been logged out for any reason
                        askForLogin();
                    }
                    else
                    {
                        alert_view(JobdoneAct.this, "" + getResources().getString(R.string.message), "" + json.getString("message"), "" + getResources().getString(R.string.ok), "");
                    }
                }
                else
                {
                    alert_view(JobdoneAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
                }
            }
            catch (final JSONException e)
            {
                e.printStackTrace();
            }
        }
    }
}
