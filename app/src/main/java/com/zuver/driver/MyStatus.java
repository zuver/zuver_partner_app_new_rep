package com.zuver.driver;

import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.zuver.driver.data.CommonData;
import com.zuver.driver.data.MapWrapperLayout;
import com.zuver.driver.utils.FontHelper;
import com.zuver.driver.utils.SessionSave;

/**
 * @author developer
 */
public class MyStatus extends MainActivity implements ConnectionCallbacks,
        OnConnectionFailedListener, LocationListener {
    // Class members declarations.
    public static GoogleMap googleMap;
    private Marker c_marker;
    private TextView slider;
    public static MyStatus mystatus;
    private TextView currentLocation1, headerTxt;
    private TextView curlocation, tvCancel;
    private int mapstatus;
    private MapWrapperLayout mapWrapperLayout;
    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 30000; // 30 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 0; // 10 meters
    private Location mLastLocation;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;

    private ImageView imgMenuIcon;

    LatLng coordinate;
    View dashMenu;

    LinearLayout driver_profile, driver_ongoing, my_drive, dashboard,
            parentSlideLay;
    ImageView driver_image, ongoing_img, mydriv_img, dash_img, img_edit,
            menu_img;
    TextView txt_driver_profile_name, txt_driver_profile_number, txt_ongoing,
            txt_mydrive, txt_dash;

    LinearLayout layHome;
    TextView txtHome;
    ImageView imgHome;

    private DisplayImageOptions options;

    @Override
    public int setLayout() {
        mapstatus = 0;
        setLocale();
        return R.layout.mystatus_lay;
    }

    // Initialize the views on layout
    @Override
    public void Initialize() {
        CommonData.sContext = this;
        mapstatus = 0;
        CommonData.current_act = "MyStatus";
        FontHelper.applyFont(this, findViewById(R.id.mystatus_layout),
                "DroidSans.ttf");

        currentLocation1 = (TextView) findViewById(R.id.currentlocation);
        curlocation = (TextView) findViewById(R.id.curlocation);
        slider = (TextView) findViewById(R.id.backup);
        slider.setVisibility(View.GONE);
//		slider.setBackgroundDrawable(getResources().getDrawable(R.drawable.menu_icon));
//		slider.setBackground(getResources().getDrawable(R.drawable.menu_icon));

        headerTxt = (TextView) findViewById(R.id.headerTxt);
        headerTxt.setText("" + getResources().getString(R.string.home));

        parentSlideLay = (LinearLayout) findViewById(R.id.driver_profile_parent);

        tvCancel = (TextView) findViewById(R.id.tvCancel);

        dashMenu = (View) findViewById(R.id.lay_dash_menu);

        driver_profile = (LinearLayout) findViewById(R.id.driver_profile_lay);
        driver_ongoing = (LinearLayout) findViewById(R.id.ongoing_lay);
        my_drive = (LinearLayout) findViewById(R.id.mydrive_lay);
        dashboard = (LinearLayout) findViewById(R.id.dashboard_lay);
        parentSlideLay = (LinearLayout) findViewById(R.id.driver_profile_parent);

        ongoing_img = (ImageView) findViewById(R.id.img_ongoing);
        mydriv_img = (ImageView) findViewById(R.id.img_mydrive);
        dash_img = (ImageView) findViewById(R.id.dash_img);

        txt_driver_profile_name = (TextView) findViewById(R.id.txt_driver_profile_name);
        txt_driver_profile_number = (TextView) findViewById(R.id.txt_driver_profile_number);
        txt_ongoing = (TextView) findViewById(R.id.txt_ongoing);
        txt_mydrive = (TextView) findViewById(R.id.txt_mydrive);
        txt_dash = (TextView) findViewById(R.id.txt_dash);

        layHome = (LinearLayout) findViewById(R.id.home_lay);
        imgHome = (ImageView) findViewById(R.id.home_img);
        txtHome = (TextView) findViewById(R.id.txt_home);

        txt_driver_profile_name.setText(SessionSave.getSession("Name",
                MyStatus.this));
        txt_driver_profile_number.setText(SessionSave.getSession("Phone",
                MyStatus.this));

        imgMenuIcon = (ImageView) findViewById(R.id.img_menu_icon);
        imgMenuIcon.setVisibility(View.VISIBLE);

        Log.e("My Status: ", "My Status Trip ID: " + SessionSave.getSession("trip_id", MyStatus.this));

        if (SessionSave.getSession("trip_id", MyStatus.this).equals("")) {

            Intent intent = new Intent(MyStatus.this, MyStatus.class);
            startActivity(intent);

        } else {

            Intent intent = new Intent(MyStatus.this, OngoingAct.class);
            startActivity(intent);

        }

        options = new DisplayImageOptions.Builder().showImageOnLoading(null) // resource
                // or
                // drawable
                .showImageForEmptyUri(null) // resource or drawable
                .showImageOnFail(null) // resource or drawable
                .resetViewBeforeLoading(false) // default
                .delayBeforeLoading(1000).cacheInMemory(true) // default
                .cacheOnDisc(true) // default
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED) // default
                .bitmapConfig(Bitmap.Config.RGB_565) // default
                .displayer(new SimpleBitmapDisplayer()) // default
                .handler(new Handler()) // default
                // .showImageOnLoading(R.drawable.ic_stub)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                this).defaultDisplayImageOptions(options).build();
        ImageLoader.getInstance().init(config);

        Log.e("Shift Status",
                "My status"
                        + SessionSave.getSession("shift_status", MyStatus.this));

        if (SessionSave.getSession("shift_status", MyStatus.this)
                .equalsIgnoreCase("IN")) {
            tvCancel.setText("" + getResources().getString(R.string.onduty));

        } else if (SessionSave.getSession("shift_status", MyStatus.this)
                .equalsIgnoreCase("OUT")) {

            tvCancel.setText("" + getResources().getString(R.string.off_duty));
        }

        if (servicesConnected()) {
            // Building the GoogleApi client
            buildGoogleApiClient();
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(UPDATE_INTERVAL);
            mLocationRequest.setFastestInterval(FATEST_INTERVAL);
            mLocationRequest
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setSmallestDisplacement(DISPLACEMENT);


        }
        try {
            GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
            final SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            googleMap = mapFrag.getMap();
            MapsInitializer.initialize(MyStatus.this);
            mapWrapperLayout = (MapWrapperLayout) findViewById(R.id.map_relative_layout);
            mapWrapperLayout.init(googleMap,
                    getPixelsFromDp(MyStatus.this, 39 + 20));
            // System.gc();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        // Close this activity.

        driver_image = (ImageView) findViewById(R.id.dash_image);
        //
        //
        String Image_path = SessionSave.getSession("driver_profileimage",
                MyStatus.this);

        if (Image_path != null && Image_path.length() > 0) {
            Log.e("Profile Image", "" + SessionSave.getSession("driver_profileimage", MyStatus.this));
            ImageLoader.getInstance().displayImage(Image_path, driver_image,
                    options);
        }

        imgMenuIcon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (parentSlideLay.isShown()) {
                    parentSlideLay.setVisibility(View.GONE);
                    dashMenu.setVisibility(View.GONE);
                } else {
                    parentSlideLay.setVisibility(View.VISIBLE);
                    dashMenu.setVisibility(View.VISIBLE);
                }
            }
        });

        slider.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (parentSlideLay.isShown()) {
                    parentSlideLay.setVisibility(View.GONE);
                    dashMenu.setVisibility(View.GONE);
                } else {
                    parentSlideLay.setVisibility(View.VISIBLE);
                    dashMenu.setVisibility(View.VISIBLE);
                }

            }
        });
        curlocation.setTypeface(null, Typeface.BOLD);
    }

    // Get the google map pixels from xml density independent pixel.
    public static int getPixelsFromDp(final Context context, final float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnectionFailed(final ConnectionResult arg0) {
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        tvCancel = (TextView) findViewById(R.id.tvCancel);

        Log.e("Shift Status",
                "My status"
                        + SessionSave.getSession("shift_status", MyStatus.this));

        if (SessionSave.getSession("shift_status", MyStatus.this)
                .equalsIgnoreCase("IN")) {
            tvCancel.setText("" + getResources().getString(R.string.onduty));

        } else if (SessionSave.getSession("shift_status", MyStatus.this)
                .equalsIgnoreCase("OUT")) {

            tvCancel.setText("" + getResources().getString(R.string.off_duty));
        }
    }

    /*
     * Convert the lat and lng into address value,set the marker with given
     * lat/lng. Set the current location to text view.
     */
    private void getCurrentAddress(double lat, double lon) {
        Geocoder geocoder;
        List<Address> addresses = null;
        String address = "";
        String city = "";
        String country = "";
        geocoder = new Geocoder(this, Locale.UK);
        try {
            addresses = geocoder.getFromLocation(lat, lon, 1);
            address = addresses.get(0).getAddressLine(0);
            city = addresses.get(0).getAddressLine(1);
            country = addresses.get(0).getAddressLine(2);
            LatLng coordinate = new LatLng(lat, lon);
            if (c_marker != null) {
                c_marker.remove();
            }
            c_marker = googleMap
                    .addMarker(new MarkerOptions()
                            .position(coordinate)
                            .title(""
                                    + getResources().getString(
                                    R.string.you_are_here))
                            .icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.mystatus)));
            currentLocation1.setText("" + address + "\n" + city + "\n"
                    + country);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Starting the location updates
     */
    protected void startLocationUpdates() {
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }


    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
//		LocationServices.FusedLocationApi.removeLocationUpdates(
//				mGoogleApiClient, this);
//		
        try {
            if (mGoogleApiClient != null) {
                if (mGoogleApiClient.isConnected()) {
                    LocationServices.FusedLocationApi.removeLocationUpdates(
                            mGoogleApiClient, this);
                    mGoogleApiClient.disconnect();
                }
            }

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    // When connect with location client the following function get the current
    // lat/lng and update the UI.
    @Override
    public void onConnected(Bundle connectionHint) {
        try {
            startLocationUpdates();
            mLastLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);
            if (servicesConnected()) {
                if (isOnline()) {
                    if (mLastLocation != null) {
                        coordinate = new LatLng(mLastLocation.getLatitude(),
                                mLastLocation.getLongitude());
                        getCurrentAddress(mLastLocation.getLatitude(),
                                mLastLocation.getLongitude());
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                coordinate, 16));
                    }
                }

//				if (mGoogleApiClient != null) {
//					mGoogleApiClient.connect();
//				}	
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Method for check the google service available or not.
    private boolean servicesConnected() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        } else {
            return false;
        }
    }

    // Function get the current lat/lng and update the UI onlocationchange.
    @Override
    public void onLocationChanged(Location location) {
        try {
            if (mapstatus == 1) {
                getCurrentAddress(location.getLatitude(),
                        location.getLongitude());
                mapstatus = 0;
            } else {
                mapstatus = 1;
                coordinate = new LatLng(location.getLatitude(),
                        location.getLongitude());
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                        coordinate, 16));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Accessing the all activities from the slide menu.
    public void clickMethod(View v) {
        Intent i;
        switch (v.getId()) {

            case R.id.driver_profile_lay:
                showLoading(MyStatus.this);
                driver_profile.setBackgroundResource(R.color.lightgrey);
                i = new Intent(this, MeAct.class);
                startActivity(i);
                finish();
                break;
            case R.id.ongoing_lay:

                parentSlideLay.setVisibility(View.GONE);
                break;
            case R.id.mydrive_lay:
                showLoading(MyStatus.this);
                my_drive.setBackgroundResource(R.color.gray);
                txt_mydrive.setTextColor(getResources().getColor(R.color.white));
                mydriv_img.setImageResource(R.drawable.mydriver_focus);

                i = new Intent(this, JobsAct.class);
                startActivity(i);
                finish();

                // logout(DashAct.this);
                break;
            case R.id.dashboard_lay:
                showLoading(MyStatus.this);
                dashboard.setBackgroundResource(R.color.gray);
                txt_dash.setTextColor(getResources().getColor(R.color.white));
                dash_img.setImageResource(R.drawable.dashboard_focus);
                // dashboard.setBackgroundColor(getResources().getColor(R.id.li))
                i = new Intent(this, HomeActivity.class);
                startActivity(i);
                finish();
                break;

            case R.id.home_lay:
                showLoading(MyStatus.this);
                layHome.setBackgroundResource(R.color.gray);
                txtHome.setTextColor(getResources().getColor(R.color.white));
                imgHome.setImageResource(R.drawable.home_focus);
                // dashboard.setBackgroundColor(getResources().getColor(R.id.li))
                i = new Intent(this, DashAct.class);
                startActivity(i);
                finish();
                break;

            case R.id.lin_menu:

                // ShowToast(JobsAct.this, "Slide Layout");
                if (parentSlideLay.isShown()) {
                    parentSlideLay.setVisibility(View.GONE);
                    dashMenu.setVisibility(View.GONE);
                } else {
                    parentSlideLay.setVisibility(View.VISIBLE);
                    dashMenu.setVisibility(View.VISIBLE);
                }

                break;

            default:
                break;
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

    @Override
    protected void onDestroy() {
        stopLocationUpdates();
        googleMap = null;
        super.onDestroy();
    }
}