package com.zuver.driver;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;

import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.service.APIService_Volley_JSON;
import com.zuver.driver.service.NonActivity;
import com.zuver.driver.utils.FontHelper;
import com.zuver.driver.utils.SessionSave;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * This class is calls when the driver missed the request.
 *
 * @author developer
 */
public class PickupMissedNotification extends MainActivity
{
    private String message, edtMessage;
    private Button btnSubmit;
    private CheckBox chckPickReason1, chckPickReason2, chckPickReason3,
            chckPickReason4, chckPickReason5;
    NonActivity nonactiityobj = new NonActivity();

    private TextView txtTripType, txtStartTime, slideImg, headerTxt;

    private EditText edtComments;

    Bundle bundle = new Bundle();

    boolean bookCommentFlag = false;

    @Override
    public int setLayout() {
        // TODO Auto-generated method stub
        setLocale();
        return R.layout.pickup_missed_notifi_lay;

    }

    @Override
    public void Initialize()
    {
        // TODO Auto-generated method stub

        bundle = getIntent().getExtras();
//		JSONObject obj = bundle.toString();

//		Log.e("Pick Up Missed Notify Bundle", ""+bundle+":"+getIntent().getStringExtra("trip_details"));
        Log.e("Params: ", "Params: " + bundle.getString("trip_type") + " : " + bundle.getString("pickup_time") + ":" + bundle.toString());

        btnSubmit = (Button) findViewById(R.id.btn_sumbit);

        chckPickReason1 = (CheckBox) findViewById(R.id.chk_pickup_reason1);
        chckPickReason2 = (CheckBox) findViewById(R.id.chk_pickup_reason2);
        chckPickReason3 = (CheckBox) findViewById(R.id.chk_pickup_reason3);
        chckPickReason4 = (CheckBox) findViewById(R.id.chk_pickup_reason4);
        chckPickReason5 = (CheckBox) findViewById(R.id.chk_pickup_reason5);
        slideImg = (TextView) findViewById(R.id.backup);
        headerTxt = (TextView) findViewById(R.id.headerTxt);
        headerTxt.setText(getResources().getString(R.string.pickup_missed_title));

        slideImg.setVisibility(View.GONE);

        txtTripType = (TextView) findViewById(R.id.txt_trip_type);
        txtStartTime = (TextView) findViewById(R.id.txt_start_time);

        edtComments = (EditText) findViewById(R.id.edt_comments);

        nonactiityobj.stopServicefromNonActivity(PickupMissedNotification.this);

        chckPickReason1
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        // TODO Auto-generated method stub
                        edtComments.setText("");
                        Log.i("Check Box", "Message: " + getResources().getString(
                                R.string.missed_notifiy1));
                    }
                });


        txtTripType.setText("" + bundle.getString("trip_type"));
        txtStartTime.setText("" + bundle.getString("pickup_time"));
        chckPickReason2
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        // TODO Auto-generated method stub
                        edtComments.setText("");

                        Log.i("Check Box", "Message: " + getResources().getString(
                                R.string.missed_notifiy2));


                    }
                });

        chckPickReason3
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        // TODO Auto-generated method stub
                        edtComments.setText("");
                        Log.i("Check Box", "Message: " + getResources().getString(
                                R.string.missed_notifiy3));


                    }
                });

        chckPickReason5
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        // TODO Auto-generated method stub
//				message = "";
//				message = getResources().getString(
//						R.string.missed_notifiy5);
                        edtComments.setText("");
                        Log.i("Check Box", "Message: " + getResources().getString(
                                R.string.missed_notifiy5));


                    }
                });

        chckPickReason4
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        // TODO Auto-generated method stub
//						message = "";
//						message = getResources().getString(
//								R.string.missed_notifiy4);
                        edtComments.setEnabled(true);
                        if (isChecked) {
                            bookCommentFlag = true;
                        } else {
                            bookCommentFlag = false;
                        }


                    }
                });

        btnSubmit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (chckPickReason1.isChecked()) {
                    message = getResources().getString(
                            R.string.missed_notifiy1);
                } else if (chckPickReason2.isChecked()) {
                    message = getResources().getString(
                            R.string.missed_notifiy2);
                } else if (chckPickReason3.isChecked()) {
                    message = getResources().getString(
                            R.string.missed_notifiy3);
                } else if (chckPickReason5.isChecked()) {
                    message = getResources().getString(
                            R.string.missed_notifiy5);
                } else if (chckPickReason4.isChecked()) {
                    message = edtComments.getText().toString();
                }


                edtMessage = edtComments.getText().toString();


                Log.e("asdf", "sadf" + message);

                if (bookCommentFlag) {
                    if (edtMessage.equals("")) {
//						message = "";
                        alert_view(PickupMissedNotification.this, getResources().getString(R.string.message), getResources().getString(R.string.comment_fields), getResources().getString(R.string.ok), null);
                        return;
                    }

                } else {
                    if (message == null || message.equals("")) {
                        alert_view(PickupMissedNotification.this, getResources().getString(R.string.message), "Please Pick the reason from the list", "OK", "");
                        return;
                    }
                }
                sendTripRejectReason();
            }
        });

    }

    /**
     * This method is used for submitting the user data while missing
     * the request.
     */
    public void sendTripRejectReason() {

        if (message == null || message.equals("")) {
            alert_view(PickupMissedNotification.this, getResources().getString(R.string.message), "Please Pick the reason from the list", "OK", "");
            return;
        }

        JSONObject j = new JSONObject();
        try {

            String trip_id = MainActivity.mMyStatus.gettripId();
            Log.i("Karthik",
                    "karthik: "
                            + trip_id
                            + ":"
                            + SessionSave.getSession("trip_id",
                            PickupMissedNotification.this));

            j.put("trip_id", trip_id);
            j.put("driver_id",
                    SessionSave.getSession("Id", PickupMissedNotification.this));
            j.put("reason", "" + message);
            j.put("reject_type", "0");
            final String Url = "reject_trip_reason";
            new TripReject(Url, j);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private class TripReject implements APIResult {
        String msg;

        public TripReject(final String url, JSONObject data) {
            // TODO Auto-generated constructor stub
            new APIService_Volley_JSON(PickupMissedNotification.this, this,
                    data, false).execute(url);
        }

        @Override
        public void getResult(final boolean isSuccess, final String result) {
            // TODO Auto-generated method stub
            showLog("Reject:" + result);
            try {
                if (isSuccess) {
                    nonactiityobj.startServicefromNonActivity(PickupMissedNotification.this);
                    final JSONObject json = new JSONObject(result);
                    if (json.getInt("status") == 1) {
                        alert_view_pickup(PickupMissedNotification.this, "" + getResources().getString(R.string.message), "" + json.getString("message"), "" + getResources().getString(R.string.ok), "");
                        SessionSave.saveSession("trip_id", "", PickupMissedNotification.this);
                        JSONObject jsonDriver = json.getJSONObject("driver_statistics");
                        SessionSave.saveSession("driver_statistics", "" + jsonDriver, PickupMissedNotification.this);
                    }else if (json.getInt("status") == -1){
                        //User has been logged out for any reason
                        askForLogin();
                    }
                } else
                    alert_view(
                            PickupMissedNotification.this,
                            "" + getResources().getString(R.string.message),
                            ""
                                    + getResources().getString(
                                    R.string.check_net_connection), ""
                                    + getResources().getString(R.string.ok), "");
            } catch (final JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void alert_view_pickup(Context mContext, String title, String message, String success_txt, String failure_txt) {
        final View view = View.inflate(mContext, R.layout.alert_view, null);
        alertDialog = new Dialog(mContext, R.style.NewDialog);
        alertDialog.setContentView(view);
        alertDialog.setCancelable(true);
        FontHelper.applyFont(mContext, alertDialog.findViewById(R.id.alert_id), "DroidSans.ttf");
        alertDialog.show();
        final TextView title_text = (TextView) alertDialog.findViewById(R.id.title_text);
        final TextView message_text = (TextView) alertDialog.findViewById(R.id.message_text);
        final Button button_success = (Button) alertDialog.findViewById(R.id.button_success);
        title_text.setText(title);
        message_text.setText(message);
        button_success.setText(success_txt);
        button_success.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
                showLoading(PickupMissedNotification.this);
                Intent intent = new Intent(PickupMissedNotification.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void OnChecked(View v) {

        switch (v.getId()) {
            case R.id.chk_pickup_reason1:
                chckPickReason1.setChecked(true);
                chckPickReason2.setChecked(false);
                chckPickReason3.setChecked(false);
                chckPickReason4.setChecked(false);
                chckPickReason5.setChecked(false);
                edtComments.setEnabled(false);
                hide_keyboard(PickupMissedNotification.this);
                break;
            case R.id.chk_pickup_reason2:
                chckPickReason1.setChecked(false);
                chckPickReason2.setChecked(true);
                chckPickReason3.setChecked(false);
                chckPickReason4.setChecked(false);
                chckPickReason5.setChecked(false);
                edtComments.setEnabled(false);
                hide_keyboard(PickupMissedNotification.this);
                break;
            case R.id.chk_pickup_reason3:
                chckPickReason1.setChecked(false);
                chckPickReason2.setChecked(false);
                chckPickReason3.setChecked(true);
                chckPickReason4.setChecked(false);
                chckPickReason5.setChecked(false);
                edtComments.setEnabled(false);
                hide_keyboard(PickupMissedNotification.this);
                break;
            case R.id.chk_pickup_reason5:
                chckPickReason1.setChecked(false);
                chckPickReason2.setChecked(false);
                chckPickReason3.setChecked(false);
                chckPickReason4.setChecked(false);
                chckPickReason5.setChecked(true);
                edtComments.setEnabled(false);
                hide_keyboard(PickupMissedNotification.this);
                break;
            case R.id.chk_pickup_reason4:
                chckPickReason1.setChecked(false);
                chckPickReason2.setChecked(false);
                chckPickReason3.setChecked(false);
                chckPickReason4.setChecked(true);
                chckPickReason5.setChecked(false);
                edtComments.setEnabled(true);
                break;

            default:
                break;
        }
    }


    private void hide_keyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            // Find the currently focused view, so we can grab the correct window
            // token from it.
            View view = activity.getCurrentFocus();
            // If no view currently has focus, create a new one, just so we can grab
            // a window token from it
            if (view == null) {
                view = new View(activity);
            }
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

}
