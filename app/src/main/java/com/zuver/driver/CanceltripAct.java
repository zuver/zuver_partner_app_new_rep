package com.zuver.driver;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.TextView;

import com.zuver.driver.service.NonActivity;
import com.zuver.driver.utils.SessionSave;


/**
 * This is the common class for this application is used
 * for cancelling the trip in two different places.
 *
 * @author developer
 */
public class CanceltripAct extends MainActivity implements OnClickListener
{
    private TextView m_message;
    private String cancel_msg;
    private Button m_cancel;
    NonActivity nonactiityobj = new NonActivity();

    @Override
    public int setLayout()
    {
        // TODO Auto-generated method stub
        return R.layout.canceltrip_lay;
    }

    @Override
    public void Initialize()
    {
        // TODO Auto-generated method stub
        unlockScreen();
        Bundle bun = getIntent().getExtras();

        if (bun != null)
        {
            m_message = (TextView) findViewById(R.id.message);
            m_cancel = (Button) findViewById(R.id.button1);
            cancel_msg = bun.getString("message");
            m_message.setText(cancel_msg);
            SessionSave.saveSession("trip_id", "", CanceltripAct.this);
            setonclickListener();
        }
    }

    private void setonclickListener() {
        m_cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if (v == m_cancel)
        {
            MainActivity.mMyStatus.setStatus("F");
            SessionSave.saveSession("status", "F", getApplicationContext());
            MainActivity.mMyStatus.settripId("");
            SessionSave.saveSession("trip_id", "", getApplicationContext());
            MainActivity.mMyStatus.setOnstatus("On");
            MainActivity.mMyStatus.setOnPassengerImage("");
            MainActivity.mMyStatus.setOnpassengerName("");
            MainActivity.mMyStatus.setOndropLocation("");
            MainActivity.mMyStatus.setPassengerOndropLocation("");
            MainActivity.mMyStatus.setOnpickupLatitude("");
            MainActivity.mMyStatus.setOnpickupLongitude("");
            MainActivity.mMyStatus.setOndropLatitude("");
            MainActivity.mMyStatus.setOndropLongitude("");
            MainActivity.mMyStatus.setOndriverLatitude("");
            MainActivity.mMyStatus.setOndriverLongitude("");
            Intent in = new Intent(CanceltripAct.this, HomeActivity.class);
            in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET | Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
//					| Intent.FLAG_ACTIVITY_NO_ANIMATION
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(in);
            finish();
        }
    }

    @Override
    public void onBackPressed()
    {
        MainActivity.mMyStatus.setStatus("F");
        SessionSave.saveSession("status", "F", getApplicationContext());
        MainActivity.mMyStatus.settripId("");
        SessionSave.saveSession("trip_id", "", getApplicationContext());
        MainActivity.mMyStatus.setOnstatus("On");
        MainActivity.mMyStatus.setOnPassengerImage("");
        MainActivity.mMyStatus.setOnpassengerName("");
        MainActivity.mMyStatus.setOndropLocation("");
        MainActivity.mMyStatus.setPassengerOndropLocation("");
        MainActivity.mMyStatus.setOnpickupLatitude("");
        MainActivity.mMyStatus.setOnpickupLongitude("");
        MainActivity.mMyStatus.setOndropLatitude("");
        MainActivity.mMyStatus.setOndropLongitude("");
        MainActivity.mMyStatus.setOndriverLatitude("");
        MainActivity.mMyStatus.setOndriverLongitude("");

        SessionSave.saveSession("trip_id", "", CanceltripAct.this);
        Intent in = new Intent(CanceltripAct.this, HomeActivity.class);
        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET | Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
                | Intent.FLAG_ACTIVITY_NO_ANIMATION
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(in);
        finish();
        OngoingAct.activity.finish();
        super.onBackPressed();
    }

    // This method is to check and open the notification view in front even the mobile screen off.
    private void unlockScreen()
    {
        Window window = this.getWindow();
        window.addFlags(LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.addFlags(LayoutParams.FLAG_TURN_SCREEN_ON);
    }
}