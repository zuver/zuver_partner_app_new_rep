package com.zuver.driver;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.zuver.driver.adapter.JobsListAdapter;
import com.zuver.driver.data.CommonData;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.service.APIService_Volley_JSON;
import com.zuver.driver.service.NonActivity;
import com.zuver.driver.utils.FontHelper;
import com.zuver.driver.utils.SessionSave;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class JobsAct extends MainActivity implements OnClickListener
{
    // Class members declarations.
    private TextView btnPendingBooking, btnAllRides;
    private TextView btnPastBooking;
    private TextView emptyView;
    private ImageView btnRefresh;
    private ListView list;
    private int limit = 10;
    private JSONArray cancelledJsonArray;
    private JSONArray pastJsonArray, rateCardJsonArray;
    private LinearLayout back;
    private View viewAllRides, viewUpComing, viewCompleted;
    private boolean IsShowingCancelledTrip = false;
    NonActivity nonactiityobj = new NonActivity();
    View dashMenu;

    LinearLayout driver_profile, driver_ongoing, my_drive, dashboard, parentSlideLay;
    ImageView driver_image, ongoing_img, mydriv_img, dash_img, img_edit, menu_img;
    TextView txt_driver_profile_name, txt_driver_profile_number, txt_ongoing, txt_mydrive, txt_dash;

    LinearLayout layHome;
    TextView txtHome;
    ImageView imgHome;

    private DisplayImageOptions options;
    private Date todayDate;
    private SimpleDateFormat dateFormatter;

    // Set the layout to activity.
    @Override
    public int setLayout()
    {
        setLocale();
        return R.layout.jobs_layout;
    }

    // Initialize the views on layout
    @Override
    public void Initialize()
    {
        try
        {
            CommonData.sContext = this;
            CommonData.current_act = "JobsAct";
            FontHelper.applyFont(this, findViewById(R.id.jos_layout), "DroidSans.ttf");
            btnPendingBooking = (TextView) findViewById(R.id.btnPendingBooking);
            btnAllRides = (TextView) findViewById(R.id.btnAllRides);
            btnPastBooking = (TextView) findViewById(R.id.btnPastBooking);
            btnRefresh = (ImageView) findViewById(R.id.refresh_btn);
            emptyView = (TextView) findViewById(R.id.txtempty);
            back = (LinearLayout) findViewById(R.id.slideImg);
            list = (ListView) findViewById(R.id.history_listView1);

            viewAllRides = (View) findViewById(R.id.view_allrides);
            viewUpComing = (View) findViewById(R.id.view_upcoming);
            viewCompleted = (View) findViewById(R.id.view_completed);
            dashMenu = (View) findViewById(R.id.lay_dash_menu);

            driver_profile = (LinearLayout) findViewById(R.id.driver_profile_lay);
            driver_ongoing = (LinearLayout) findViewById(R.id.ongoing_lay);
            my_drive = (LinearLayout) findViewById(R.id.mydrive_lay);
            dashboard = (LinearLayout) findViewById(R.id.dashboard_lay);
            parentSlideLay = (LinearLayout) findViewById(R.id.driver_profile_parent);

            ongoing_img = (ImageView) findViewById(R.id.img_ongoing);
            mydriv_img = (ImageView) findViewById(R.id.img_mydrive);
            dash_img = (ImageView) findViewById(R.id.dash_img);

            layHome = (LinearLayout) findViewById(R.id.home_lay);
            imgHome = (ImageView) findViewById(R.id.home_img);
            txtHome = (TextView) findViewById(R.id.txt_home);

            driver_image = (ImageView) findViewById(R.id.dash_image);
            String Image_path = SessionSave.getSession("driver_profileimage", JobsAct.this);

            options = new DisplayImageOptions.Builder().showImageOnLoading(null) // resource
                    // or
                    // drawable
                    .showImageForEmptyUri(null) // resource or drawable
                    .showImageOnFail(null) // resource or drawable
                    .resetViewBeforeLoading(false) // default
                    .delayBeforeLoading(1000).cacheInMemory(true) // default
                    .cacheOnDisc(true) // default
                    .imageScaleType(ImageScaleType.EXACTLY_STRETCHED) // default
                    .bitmapConfig(Bitmap.Config.RGB_565) // default
                    .displayer(new SimpleBitmapDisplayer()) // default
                    .handler(new Handler()) // default
                    // .showImageOnLoading(R.drawable.ic_stub)
                    .build();

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).defaultDisplayImageOptions(options).build();
            ImageLoader.getInstance().init(config);

            if (Image_path != null && Image_path.length() > 0)
            {
                Log.e("Profile Image", "" + SessionSave.getSession("driver_profileimage", JobsAct.this));

//				Picasso.with(JobsAct.this)
//	            .load(Image_path)
////	            .placeholder(R.drawable.ic_placeholder) // optional
//	            .error(R.drawable.menu_profile)         // optional
//	            .into(driver_image);

                ImageLoader.getInstance().displayImage(Image_path, driver_image, options);
            }

            txt_driver_profile_name = (TextView) findViewById(R.id.txt_driver_profile_name);
            txt_driver_profile_number = (TextView) findViewById(R.id.txt_driver_profile_number);
            txt_ongoing = (TextView) findViewById(R.id.txt_ongoing);
            txt_mydrive = (TextView) findViewById(R.id.txt_mydrive);
            txt_dash = (TextView) findViewById(R.id.txt_dash);

            if (!SessionSave.getSession("trip_id", JobsAct.this).equals(""))
            {
//			if(SessionSave.getSession("status", JobsAct.this).equalsIgnoreCase("A") || !SessionSave.getSession("trip_id", JobsAct.this).equals("") || SessionSave.getSession("status", JobsAct.this).equalsIgnoreCase("B")) {
                driver_ongoing.setVisibility(View.VISIBLE);
            }
            else
            {
                driver_ongoing.setVisibility(View.GONE);
            }

            btnPendingBooking.setOnClickListener(this);
            btnPastBooking.setOnClickListener(this);
            btnAllRides.setOnClickListener(this);
            btnRefresh.setOnClickListener(this);

            todayDate = new Date();
            dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Log.e("Date Format", "Date Format" + dateFormatter.format(todayDate));

            new requestingApi();
            SessionSave.saveSession("DriverAdapter", "JobsAct", JobsAct.this);

            txt_driver_profile_name.setText(SessionSave.getSession("Name", JobsAct.this));
            txt_driver_profile_number.setText(SessionSave.getSession("Phone", JobsAct.this));
            // This for move the control from list view to particular activity
            // based on the selected item from list.

            list.setOnItemClickListener(new OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
                {
                    try
                    {
                        if (!IsShowingCancelledTrip)
                        {
                            if (pastJsonArray.getJSONObject(position).getString("travel_status").equals("1"))
                            {
                                Intent intent = new Intent(JobsAct.this, TripDetails.class);
                                Bundle bundle = new Bundle();

                                bundle.putString("passenger_name", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("passenger_name") + " " + pastJsonArray.getJSONObject(position)
                                        .getString("passenger_lastname"));

                                bundle.putString("passengers_log_id", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("passengers_log_id"));

                                bundle.putString("pickup_location", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("pickup_location"));

                                bundle.putString("pickup_time", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("pickup_time"));

                                bundle.putString("wallet_payment", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("wallet_payment"));

                                bundle.putString("drop_location", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("drop_location"));

                                bundle.putString("drop_time", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("drop_time"));

                                bundle.putString("distance", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("distance"));

                                bundle.putString("trip_duration", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("trip_duration"));

                                bundle.putString("travel_status", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("travel_status"));

                                bundle.putString("trip_amt", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("amt"));

                                bundle.putString("pimage", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("profile_image"));

                                bundle.putString("paymenttype", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("payment_type"));

                                bundle.putString("parking_charge", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("parking_charge"));

                                bundle.putString("other_charge", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("other_charge"));

                                // for Bill Summary
                                bundle.putString("driver_name", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("driver_name"));

                                bundle.putString("pickup_time", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("pickup_time"));

                                bundle.putString("drop_time", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("drop_time"));

                                bundle.putString("amt", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("amt"));

                                bundle.putString("payment_mode", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("payment_mode"));

                                bundle.putString("faretype", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("faretype"));

                                bundle.putString("trip_plan", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("trip_plan"));

                                bundle.putString("convenience_charge", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("convenience_charge"));

                                bundle.putString("overtime_charge", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("overtime_charge"));

                                bundle.putString("tax", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("tax"));

                                bundle.putString("passenger_discount", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("passenger_discount"));

                                bundle.putString("tripfare", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("tripfare"));

                                bundle.putString("payment_type", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("payment_type"));

                                bundle.putString("parking_charge", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("parking_charge"));

                                bundle.putString("other_charge", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("other_charge"));

                                bundle.putString("tax_percentage", ""
                                        + pastJsonArray.getJSONObject(position)
                                        .getString("tax_percentage"));

                                /*switch (pastJsonArray.getJSONObject(position).getString("trip_plan"))
                                {
                                    case "One Way":

                                        bundle.putString("rate_card", rateCardJsonArray.getJSONArray(8).toString());

                                        break;

                                    case "Return":

                                        bundle.putString("rate_card", rateCardJsonArray.getJSONArray(0).toString());

                                        break;

                                    case "Out Station One Way":

                                        bundle.putString("rate_card", rateCardJsonArray.getJSONArray(0).toString());

                                        break;

                                    case "Out Station Return":

                                        bundle.putString("rate_card", rateCardJsonArray.getJSONArray(0).toString());

                                        break;
                                }

                                if (pastJsonArray.getJSONObject(position).getString("trip_plan").equalsIgnoreCase("One Way"))
                                {

                                }
                                else if (pastJsonArray.getJSONObject(position).getString("trip_plan").equalsIgnoreCase("One Way"))
                                {

                                }
                                else
                                if (pastJsonArray.getJSONObject(position).getString("trip_plan").equalsIgnoreCase("One Way"))
                                {

                                }

                                bundle.putString("rate_card", rateCardJsonArray.toString());*/

                                /// bundle.putString("");
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                            else if (pastJsonArray.getJSONObject(position).getString("travel_status").equals("5"))
                            {
                                SessionSave.saveSession("trip_id", "" + cancelledJsonArray.getJSONObject(position).getString("passengers_log_id"), JobsAct.this);
                                Intent i = new Intent(JobsAct.this, BillFareAct.class);
                                i.putExtra("from", "pending");
                                i.putExtra("lat", cancelledJsonArray.getJSONObject(position).getString("drop_latitude"));
                                i.putExtra("lon", cancelledJsonArray.getJSONObject(position).getString("drop_longitude"));
                                i.putExtra("distance", cancelledJsonArray.getJSONObject(position).getString("travel_status"));
                                i.putExtra("waitingHr", cancelledJsonArray.getJSONObject(position).getString("waiting_hour"));
                                i.putExtra("drop_location", cancelledJsonArray.getJSONObject(position).getString("drop_location"));
                                startActivity(i);
                            }
                            else
                            {
                                SessionSave.saveSession("trip_id", "" + cancelledJsonArray.getJSONObject(position).getString("passengers_log_id"), JobsAct.this);
                                Intent intent;
                                intent = new Intent(JobsAct.this, OngoingAct.class);
                                JobsAct.this.startActivity(intent);
                            }
                        }

                        /* else if (cancelledJsonArray.getJSONObject(position).getString("travel_status").equals("5")) {
                            SessionSave.saveSession("trip_id", "" + cancelledJsonArray.getJSONObject(position).getString("passengers_log_id"), JobsAct.this);
                            Intent i = new Intent(JobsAct.this, BillFareAct.class);
                            i.putExtra("from", "pending");
                            i.putExtra("lat", cancelledJsonArray.getJSONObject(position).getString("drop_latitude"));
                            i.putExtra("lon", cancelledJsonArray.getJSONObject(position).getString("drop_longitude"));
                            i.putExtra("distance", cancelledJsonArray.getJSONObject(position).getString("travel_status"));
                            i.putExtra("waitingHr", cancelledJsonArray.getJSONObject(position).getString("waiting_hour"));
                            i.putExtra("drop_location", cancelledJsonArray.getJSONObject(position).getString("drop_location"));
                            startActivity(i);
                        } else {
                            SessionSave.saveSession("trip_id", "" + cancelledJsonArray.getJSONObject(position).getString("passengers_log_id"), JobsAct.this);
                            Intent intent;
                            intent = new Intent(JobsAct.this, OngoingAct.class);
                            JobsAct.this.startActivity(intent);
                        }

                        */
                    }
                    catch (Exception e2)
                    {
                        e2.printStackTrace();
                    }
                }
            });
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    // This onclick method is to update the UI based on selected view(Pending
    // and past booking with local data that already stored in session)
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnPendingBooking:

                try
                {
                    btnPendingBooking.setTextColor(getResources().getColor(R.color.gray));
                    btnAllRides.setTextColor(getResources().getColor(R.color.light_gray));
                    btnPastBooking.setTextColor(getResources().getColor(R.color.light_gray));
                    btnPendingBooking.setBackgroundResource(0);
                    viewCompleted.setBackgroundResource(R.color.white);
                    viewAllRides.setBackgroundResource(R.color.white);
                    viewUpComing.setBackgroundResource(R.color.gray);
                    btnPastBooking.setBackgroundResource(0);
                    btnAllRides.setBackgroundResource(0);
                    nonactiityobj.stopServicefromNonActivity(JobsAct.this);

                    if (!cancelledJsonArray.equals(""))
                    {
                        cancelledBooking(cancelledJsonArray);
                        IsShowingCancelledTrip = true;
                    }
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                    e.printStackTrace();
                }

                break;

            case R.id.btnPastBooking:

                try
                {
                    btnPastBooking.setTextColor(getResources().getColor(R.color.gray));
                    btnPendingBooking.setTextColor(getResources().getColor(R.color.light_gray));
                    btnAllRides.setTextColor(getResources().getColor(R.color.light_gray));
                    btnAllRides.setBackgroundResource(0);
                    btnPendingBooking.setBackgroundResource(0);
                    btnPastBooking.setBackgroundResource(0);
                    viewCompleted.setBackgroundResource(R.color.gray);
                    viewUpComing.setBackgroundResource(R.color.white);
                    viewAllRides.setBackgroundResource(R.color.white);
                    nonactiityobj.stopServicefromNonActivity(JobsAct.this);

                    Log.e("Completed", "Completed");
                    //new requestingCompletedApi();
                    if (!pastJsonArray.equals(""))
                    {
                        pastBooking(pastJsonArray);
                        IsShowingCancelledTrip = false;
                    }
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                    e.printStackTrace();
                }

                break;

            case R.id.btnAllRides:

                try
                {
                    btnAllRides.setTextColor(getResources().getColor(R.color.gray));
                    btnPastBooking.setTextColor(getResources().getColor(R.color.light_gray));
                    btnPendingBooking.setTextColor(getResources().getColor(R.color.light_gray));
                    btnPastBooking.setBackgroundResource(0);
                    btnPendingBooking.setBackgroundResource(0);
                    btnAllRides.setBackgroundResource(0);
                    viewAllRides.setBackgroundResource(R.color.gray);
                    viewCompleted.setBackgroundResource(R.color.white);
                    viewUpComing.setBackgroundResource(R.color.white);
                    nonactiityobj.stopServicefromNonActivity(JobsAct.this);
                    Log.e("All Drives", "All Drives");
                    new requestingApi();

                    if (!pastJsonArray.equals(""))
                    {
                        pastBooking(pastJsonArray);
                        IsShowingCancelledTrip = false;
                    }
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                    e.printStackTrace();
                }

                break;

            case R.id.refresh_btn:

                nonactiityobj.stopServicefromNonActivity(JobsAct.this);
                new requestingApi();

                break;

            // case R.id.slideImg:
            // Intent intent = new Intent(JobsAct.this, DashAct.class);
            // startActivity(intent);
            // finish();
            // break;
        }
    }

    /*
     *  Accessing the activity class from the slide menu.
     */
    public void clickMethod(View v)
    {
        Intent i;

        switch (v.getId())
        {
            case R.id.driver_profile_lay:

                driver_profile.setBackgroundResource(R.color.lightgrey);
                showLoading(JobsAct.this);
                i = new Intent(this, MeAct.class);
                startActivity(i);
                finish();

                break;

            case R.id.ongoing_lay:

                showLoading(JobsAct.this);
                driver_ongoing.setBackgroundResource(R.color.gray);
                txt_ongoing.setTextColor(getResources().getColor(R.color.white));
                ongoing_img.setImageResource(R.drawable.ongoing_drive_focus);

                if (SessionSave.getSession("status", JobsAct.this).equals("A") || SessionSave.getSession("status", JobsAct.this).equals("B"))
                {
                    Intent intentOnGoing = new Intent(JobsAct.this, OngoingAct.class);
                    startActivity(intentOnGoing);
                    finish();
                }

                break;

            case R.id.mydrive_lay:

                my_drive.setBackgroundResource(R.color.gray);
                txt_mydrive.setTextColor(getResources().getColor(R.color.white));
                mydriv_img.setImageResource(R.drawable.mydriver_focus);

                showLoading(JobsAct.this);
                i = new Intent(this, JobsAct.class);
                startActivity(i);
                finish();

                // logout(DashAct.this);
                break;

            case R.id.dashboard_lay:

                dashboard.setBackgroundResource(R.color.gray);
                txt_dash.setTextColor(getResources().getColor(R.color.white));
                dash_img.setImageResource(R.drawable.dashboard_focus);
                // dashboard.setBackgroundColor(getResources().getColor(R.id.li))
                showLoading(JobsAct.this);
                i = new Intent(this, HomeActivity.class);
                startActivity(i);
                finish();

                break;

            case R.id.home_lay:

                layHome.setBackgroundResource(R.color.gray);
                txtHome.setTextColor(getResources().getColor(R.color.white));
                imgHome.setImageResource(R.drawable.home_focus);
                // dashboard.setBackgroundColor(getResources().getColor(R.id.li))
                // showLoading(DashAct.this);
                i = new Intent(this, DashAct.class);
                startActivity(i);
                finish();

                break;

            case R.id.lin_menu:

                /// ShowToast(JobsAct.this, "Slide Layout");
                if (parentSlideLay.isShown())
                {
                    parentSlideLay.setVisibility(View.GONE);
                    dashMenu.setVisibility(View.GONE);
                }
                else
                {
                    parentSlideLay.setVisibility(View.VISIBLE);
                    dashMenu.setVisibility(View.VISIBLE);
                }

                break;

            default:
                break;
        }
    }

    /**
     * Requesting Web service custom class and returning the response to
     * getResult() Method
     */
    private class requestingApi implements APIResult
    {
        private requestingApi()
        {
            try
            {
                JSONObject j = new JSONObject();
                j.put("driver_id", SessionSave.getSession("Id", JobsAct.this));
                j.put("start", "0");
                j.put("limit", "100");
                j.put("start_date", "");
                j.put("end_date", "");
                j.put("device_type", "1");
                j.put("start_date", "");
                j.put("end_date", "");
                Log.d("Id", "driver id stored in session is: " + SessionSave.getSession("Id", JobsAct.this));
                Log.d("Auth_Key", "auth key stored in session is: " + SessionSave.getSession("Auth_Key", JobsAct.this));
                Log.d("User_Type", "user type stored in session is: " + SessionSave.getSession("User_Type", JobsAct.this));
                String driverTripRequesting = "driver_booking_list";

                if (isOnline())
                {
                    new APIService_Volley_JSON(JobsAct.this, this, j, false).execute(driverTripRequesting);
                }
                else
                {
                    alert_view(JobsAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(
                            R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
                }
            }
            catch (Exception e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        @Override
        public void getResult(boolean isSuccess, String result)
        {
            // TODO Auto-generated method stub
            try
            {
                if (isSuccess)
                {
                    JSONObject object = new JSONObject(result);

                    if (object.getInt("status") == 1)
                    {
                        object = object.getJSONObject("detail");
                        pastJsonArray = object.getJSONArray("past_booking");
                        cancelledJsonArray = object.getJSONArray("cancelled_booking");
                        rateCardJsonArray = object.getJSONArray("driver_rate_card_details");

                        if (IsShowingCancelledTrip)
                        {
                            cancelledBooking(cancelledJsonArray);
                        }
                        else
                        {
                            pastBooking(pastJsonArray);
                        }
                    }
                    else if (object.getInt("status") == -1)
                    {
                        //User has been logged out for any reason
                        askForLogin();
                    }
                    else
                    {
                        alert_view(JobsAct.this, "" + getResources().getString(R.string.message), "" + object.getString("message"), "" + getResources().getString(R.string.ok), "");
                    }

                    //nonactiityobj.startServicefromNonActivity(JobsAct.this);
                }
                else
                {
                    // check net connection
                    alert_view(JobsAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(
                            R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }

    /**
     * @param pendingArray Array List with hash map values to ListView
     */
    private void cancelledBooking(JSONArray pendingArray)
    {
        try
        {
            int arrayLength = pendingArray.length();
            nonactiityobj.startServicefromNonActivity(JobsAct.this);

            if (arrayLength > 0)
            {
                ArrayList<HashMap<String, String>> pendingTripList = new ArrayList<HashMap<String, String>>(arrayLength);

                for (int iterator = 0; iterator < arrayLength; iterator++)
                {
                    HashMap<String, String> hashMap = new HashMap<String, String>();
                    hashMap.put("passenger_name", "" + pendingArray.getJSONObject(iterator).getString("passenger_name"));
                    hashMap.put("passengers_log_id", "" + pendingArray.getJSONObject(iterator).getString("passengers_log_id"));
                    hashMap.put("pickup_location", "" + pendingArray.getJSONObject(iterator).getString("pickup_location"));
                    hashMap.put("pickup_time", "" + pendingArray.getJSONObject(iterator).getString("pickup_time"));
                    hashMap.put("drop_location", "" + pendingArray.getJSONObject(iterator).getString("drop_location"));
                    hashMap.put("travel_status", "" + pendingArray.getJSONObject(iterator).getString("travel_status"));
                    hashMap.put("waiting_hour", "" + pendingArray.getJSONObject(iterator).getString("waiting_hour"));
                    hashMap.put("drop_latitude", "" + pendingArray.getJSONObject(iterator).getString("drop_latitude"));
                    hashMap.put("drop_longitude", "" + pendingArray.getJSONObject(iterator).getString("drop_longitude"));
                    pendingTripList.add(hashMap);
                }

                list.setVisibility(View.VISIBLE);
                emptyView.setVisibility(View.GONE);
                JobsListAdapter adapter = new JobsListAdapter(JobsAct.this, pendingTripList);
                list.setAdapter(adapter);
            }
            else
            {
                list.setVisibility(View.GONE);
                emptyView.setVisibility(View.VISIBLE);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            nonactiityobj.startServicefromNonActivity(JobsAct.this);
        }
    }

    /**
     * @param pastArray Array List with hash map values to ListView
     */
    private void pastBooking(JSONArray pastArray)
    {
        try
        {
            int arrayLength = pastArray.length();
            nonactiityobj.startServicefromNonActivity(JobsAct.this);

            if (arrayLength > 0)
            {
                ArrayList<HashMap<String, String>> pastTripList = new ArrayList<HashMap<String, String>>( arrayLength);

                for (int iterator = 0; iterator < arrayLength; iterator++)
                {
                    HashMap<String, String> hashMap = new HashMap<String, String>();
                    hashMap.put("passenger_name", "" + pastArray.getJSONObject(iterator).getString("passenger_name"));
                    hashMap.put("passengers_log_id", "" + pastArray.getJSONObject(iterator).getString("passengers_log_id"));
                    hashMap.put("pickup_location", "" + pastArray.getJSONObject(iterator).getString("pickup_location"));
                    hashMap.put("pickup_time", "" + pastArray.getJSONObject(iterator).getString("pickup_time"));
                    hashMap.put("drop_location", "" + pastArray.getJSONObject(iterator).getString("drop_location"));
                    hashMap.put("travel_status", "" + pastArray.getJSONObject(iterator).getString("travel_status"));
                    hashMap.put("waiting_hour", "" + pastArray.getJSONObject(iterator).getString("waiting_hour"));
                    hashMap.put("distance", "" + pastArray.getJSONObject(iterator).getString("distance"));
                    hashMap.put("drop_latitude", "" + pastArray.getJSONObject(iterator).getString("drop_latitude"));
                    hashMap.put("drop_longitude", "" + pastArray.getJSONObject(iterator).getString("drop_longitude"));
                    pastTripList.add(hashMap);
                }

                list.setVisibility(View.VISIBLE);
                emptyView.setVisibility(View.GONE);
                JobsListAdapter adapter = new JobsListAdapter(JobsAct.this, pastTripList);
                adapter.notifyDataSetChanged();
                list.setAdapter(adapter);
            }
            else
            {
                // no data found
                list.setVisibility(View.GONE);
                emptyView.setVisibility(View.VISIBLE);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            nonactiityobj.startServicefromNonActivity(JobsAct.this);
        }
    }

    public void onBackPressed()
    {
//		super.onBackPressed();
        startActivity(new Intent(JobsAct.this, DashAct.class));
        CommonData.Animation1(JobsAct.this);
    }
}
