package com.zuver.driver;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zuver.driver.data.CommonData;

public class TripDetails extends MainActivity
{
    // Class members declarations
    /*private ImageView imgView;
    private TextView txtPassenger, txtTripTime, txtTripPick, txtTripAmount,
            txtTripPayment, txtTripId, back;

    private String passenger_name, tripDuration;
    private TextView passnameTxt;
    private TextView droptimeTxt;
    private TextView dropplaceTxt;
    private TextView distanceTxt;
    private TextView duartionTxt;
    private double mdistance;*/

    private LinearLayout CashView, PaytmView, WalletView;

    private TextView Paytm_txt, txtDriverName, txtStartTime, txtEndTime, txtCharged,
            txtPackType, txtConCharges, txtCostCharges, txtOverTime, txtSubTotal,
            txtServiceTax, txtPromotions, txtQDMoney, txtCash, txtParkingCharges,
            txtOtherCharges, txt_service_percent, txtTripEarning, txtTripDuration,
            txtTravellingAllowance, txtTotalEarning;

    private String tripDuration;
    /// private String rate_card;
    /// JSONArray jsonArray;

    // Set the layout to activity.
    @Override
    public int setLayout()
    {
        setLocale();
        //return R.layout.trip_detailslayout;
        return R.layout.billsummary_lay;
    }

    /// Initialize the views on layout
    @Override
    public void Initialize()
    {
        CommonData.sContext = this;
        CommonData.current_act = "TripDetails";
        /// imgView = (ImageView) findViewById(R.id.driverImg);
        /// FontHelper.applyFont(this, findViewById(R.id.id_tripdetails), "DroidSans.ttf");
        TextView back = (TextView) findViewById(R.id.slideImg);

        txtDriverName = (TextView) findViewById(R.id.txt_name_value);
        txtStartTime = (TextView) findViewById(R.id.txt_start_time);
        txtEndTime = (TextView) findViewById(R.id.txt_end_time);
        txtCharged = (TextView) findViewById(R.id.txt_charged);
        txtPackType = (TextView) findViewById(R.id.txt_package_type);
        txtConCharges = (TextView) findViewById(R.id.txt_con_charge);
        txtCostCharges = (TextView) findViewById(R.id.txt_cost_flexi);
        txtOverTime = (TextView) findViewById(R.id.txt_over_time);
        txtSubTotal = (TextView) findViewById(R.id.txt_sub_total);
        txtServiceTax = (TextView) findViewById(R.id.txt_service_tax);
        txtPromotions = (TextView) findViewById(R.id.txt_promotions);
        txtTripDuration = (TextView) findViewById(R.id.txtTripDuration);
        txtQDMoney = (TextView) findViewById(R.id.txt_qd);
        txtCash = (TextView) findViewById(R.id.txt_cash);
        Paytm_txt = (TextView) findViewById(R.id.paytm_cash);
        PaytmView = (LinearLayout) findViewById(R.id.paytmview);
        CashView = (LinearLayout) findViewById(R.id.cashview);
        WalletView = (LinearLayout) findViewById(R.id.walletview);
        txtTripEarning = (TextView) findViewById(R.id.txtTripEarning);
        txtTravellingAllowance = (TextView) findViewById(R.id.txtTravellingAllowance);
        txtParkingCharges = (TextView) findViewById(R.id.txt_parking_charges);
        txtOtherCharges = (TextView) findViewById(R.id.txt_other_charges);
        txt_service_percent = (TextView) findViewById(R.id.txt_service_percent);
        txtTotalEarning = (TextView) findViewById(R.id.txtTotalEarning);

        back.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });

        getTripDetails();
    }

    /**
      * Added by Amit Jangid
      * 2017 May 09 - Tuesday - 11:31 AM
      * Getting trip details from previous activity using bundle
     */
    private void getTripDetails()
    {
        try
        {
            Bundle bundle = getIntent().getExtras();
            /*rate_card = bundle.getString("rate_card");

            try
            {
                jsonArray = new JSONArray(rate_card);
            }
            catch (Exception e)
            {
                Log.d("Exception", "while getting json array from jobs act:\n");
                e.printStackTrace();
            }*/

            txtDriverName.setText(bundle.getString("passenger_name"));
            txtStartTime.setText(date_time(bundle.getString("pickup_time")));
            txtEndTime.setText(date_time(bundle.getString("drop_time")));

            String tripType = bundle.getString("trip_plan");
            txtPackType.setText(tripType);
            txtConCharges.setText("" + getResources().getString(R.string.rs_symbol) + " " + bundle.getString("convenience_charge"));
            txtOverTime.setText("" + getResources().getString(R.string.rs_symbol) + " " + bundle.getString("overtime_charge"));
            txtServiceTax.setText("" + getResources().getString(R.string.rs_symbol) + " " + bundle.getString("tax"));
            txtPromotions.setText("" + getResources().getString(R.string.rs_symbol) + " " + bundle.getString("passenger_discount"));
            txtParkingCharges.setText("" + getResources().getString(R.string.rs_symbol) + " " + bundle.getString("parking_charge"));
            txtOtherCharges.setText("" + getResources().getString(R.string.rs_symbol) + " " + bundle.getString("other_charge"));
            txt_service_percent.setText("" + getResources().getString(R.string.service_tax1) + bundle.getString("tax_percentage") + " " + getResources().getString(R.string.service_tax2));
            txtTravellingAllowance.setText(getResources().getString(R.string.rs_symbol) + " " + "50");
            tripDuration = bundle.getString("trip_duration");
            txtTripDuration.setText(tripDuration);

            switch (tripType)
            {
                case "One Way":

                    calculateDriverEarnings(tripDuration, 150, 2, 100);
                    /// calculateDriverEarningsForOneWayTrip();

                    break;

                case "Return":

                    calculateDriverEarnings(tripDuration, 150, 2, 50);
                    /// calculateDriverEarningsForReturnTrip();

                    break;

                case "Out Station One Way":

                    calculateDriverEarnings(tripDuration, 600, 12, 200);
                    /// calculateDriverEarningsForOutstationOneWayTrip();

                    break;

                case "Out Station Return":

                    calculateDriverEarnings(tripDuration, 600, 12, 50);
                    /// calculateDriverEarningsForOutstationReturnTrip();

                    break;
            }

            Log.e("Detail Object", "Detail Object" + bundle.toString());
            double tripFare = Double.parseDouble(bundle.getString("tripfare"));
            double convCharge = Double.parseDouble(bundle.getString("convenience_charge"));

            double subT = convCharge + tripFare;
            txtCostCharges.setText("" + getResources().getString(R.string.rs_symbol) + " " + tripFare);
            txtSubTotal.setText("" + getResources().getString(R.string.rs_symbol) + " " + subT);

            if (bundle.getString("payment_mode").equalsIgnoreCase("0"))
            {
                PaytmView.setVisibility(View.GONE);
                WalletView.setVisibility(View.GONE);
                //Paytm_txt.setText("N/A");
                //txtQDMoney.setText("N/A");
                txtCharged.setText("" + getResources().getString(R.string.rs_symbol) + " " + bundle.getString("amt"));
                txtCash.setText("" + "" + getResources().getString(R.string.rs_symbol) + " " + bundle.getString("amt"));
            }
            else if (bundle.getString("payment_mode").equalsIgnoreCase("1"))
            {
                PaytmView.setVisibility(View.GONE);
                WalletView.setVisibility(View.GONE);
                //Paytm_txt.setText("N/A");
                //txtQDMoney.setText("N/A");
                txtCharged.setText("" + getResources().getString(R.string.rs_symbol) + " " + bundle.getString("amt"));
                txtCash.setText("" + "" + getResources().getString(R.string.rs_symbol) + " " + bundle.getString("amt"));
            }
            else if (bundle.getString("payment_mode").equalsIgnoreCase("2"))
            {
                CashView.setVisibility(View.GONE);
                WalletView.setVisibility(View.GONE);
                txtCharged.setText("" + getResources().getString(R.string.rs_symbol) + " " + bundle.getString("amt"));
                Paytm_txt.setText("" + "" + getResources().getString(R.string.rs_symbol) + " " + bundle.getString("amt"));
                //txtQDMoney.setText("N/A");
                //txtCash.setText("N/A");
            }
            else if (bundle.getString("payment_mode").equalsIgnoreCase("3"))
            {
                PaytmView.setVisibility(View.GONE);
                //Paytm_txt.setText("N/A");
                txtQDMoney.setText("" + "" + getResources().getString(R.string.rs_symbol) + " " + bundle.getString("wallet_payment"));

                if (!bundle.getString("amt").equalsIgnoreCase("0.00"))
                {
                    txtCharged.setText("" + getResources().getString(R.string.rs_symbol) + " " + String.valueOf(bundle.getString("amt")));
                    double tot_am = Double.parseDouble(bundle.getString("amt")) - Double.parseDouble(bundle.getString("wallet_payment"));
                    txtCash.setText("" + getResources().getString(R.string.rs_symbol) + " " + String.format("%.2f", tot_am));
                }
                else
                {
                    txtCash.setText("" + getResources().getString(R.string.rs_symbol) + " " + "0");
                }
            }
        }
        catch (Exception e)
        {
            Log.d("Exception", "int Initialize method in trip details activity:\n");
            e.printStackTrace();
        }
    }

    /**
      * Added by Amit Jangid
      * 2017 May 08 - Monday - 11:22 AM
      * Calculate Driver Earnings
     */
    private void calculateDriverEarnings(String tripDuration, int tripBaseFare, int tripBaseTimeLimit, int travellingAllowance)
    {
        try
        {
            /// separating hours, minutes and seconds from trip duration
            /// tripDuration = "5:29:59";
            /// travellingAllowance = 100;
            String[] time = tripDuration.split(":");
            int hours = Integer.parseInt(time[0]);
            int minutes = Integer.parseInt(time[1]);
            int seconds = Integer.parseInt(time[2]);

            /// Log.d("TripDetailsAct", "hours from trip are: " + hours);
            /// Log.d("TripDetailsAct", "minutes from trip are: " + minutes);
            /// Log.d("TripDetailsAct", "seconds from trip are: " + seconds);

            /*
             * if the trip duration is equal or less then 2 hours
             * then trip earnings of the driver is ₹ 150
             * and travelling allowance is ₹ 100 because it's a one way trip
             * and total earnings will be ₹ 250
             * if the trip duration is greater then 2 hours
             * then the trip earnings of the driver will be ₹ 50 extra for every hour
             * travelling allowance is ₹ 100 because it's a one way trip
             * Ex: if the trip hours is 2 hours 29 minutes 59 seconds
             * then the driver will get ₹ 25 in trip earnings
             * and if the trip hours is ₹ 2 hours 30 minutes 00 seconds
             * then also the driver will get ₹ 25 in trip earnings
             * and if the minutes go above 30
             * then the driver will get ₹ 50 in trip earnings
            */
            if (hours <= tripBaseTimeLimit)
            {
                /// trip duration is less then 2 hours
                int totalEarnings = tripBaseFare + travellingAllowance;
                txtTripEarning.setText(getResources().getString(R.string.rs_symbol) + " " + String.valueOf(tripBaseFare));
                txtTravellingAllowance.setText(getResources().getString(R.string.rs_symbol) + " " + String.valueOf(travellingAllowance));
                txtTotalEarning.setText(String.valueOf(getResources().getString(R.string.rs_symbol) + " " + totalEarnings));
            }
            else
            {
                /// trip duration is more then 2 hours
                int amt;

                /// subtracting extra hours from 2
                hours = hours - tripBaseTimeLimit;

                /// multiplying ₹ 50 to extra hours
                amt = 50 * hours;
                /// Log.d("TripDetailsAct", "hours after 2 hours are: " + hours);
                /// Log.d("TripDetailsAct", "extra amount earned in hours by driver is: " + amt);

                /*
                 * checking how much extra minutes were in trip duration
                 * if minutes is less then or equal to 29
                 * then add extra ₹ 25 to trip earnings
                 * other then the trip earnings calculated above
                 * if minutes is equal to 30 and seconds is also equal to 0
                 * then add extra ₹ 25 to trip earnings
                 * if minutes is greater then 30 and seconds is greater then 1
                 * then add extra ₹ 50 to trip earnings
                */
                if (minutes <= 29)
                {
                    amt += 25;
                    /// Log.d("TripDetailsAct", "extra amount earned in minutes & seconds by driver is: " + amt);
                }
                else if (minutes == 30 && seconds == 0)
                {
                    amt += 25;
                    /// Log.d("TripDetailsAct", "extra amount earned in minutes & seconds by driver is: " + amt);
                }
                else if (minutes >= 30 && seconds >= 1)
                {
                    amt += 50;
                    /// Log.d("TripDetailsAct", "extra amount earned in minutes & seconds by driver is: " + amt);
                }

                /// adding extra amounts to basic trip earnings
                int tripEarnings = tripBaseFare + amt;
                txtTripEarning.setText(getResources().getString(R.string.rs_symbol) + " " + String.valueOf(tripEarnings));

                /// calculating total earnings of the driver
                /// travelling allowance + total trip earnings
                int totalEarnings = travellingAllowance + tripEarnings;
                txtTravellingAllowance.setText(getResources().getString(R.string.rs_symbol) + " " + String.valueOf(travellingAllowance));
                txtTotalEarning.setText(getResources().getString(R.string.rs_symbol) + " " + String.valueOf(totalEarnings));
            }
        }
        catch (Exception e)
        {
            Log.d("Exception", "while calculating driver earnings in trip details activity");
            e.printStackTrace();
        }
    }

    /**
      * Added by Amit Jangid
      * 2017 May 08 - Monday - 11:22 AM
      * Calculate Driver Earnings For One Way Trip
     */
    private void calculateDriverEarningsForOneWayTrip() {
        try {
            /// separating hours, minutes and seconds from trip duration
            /// tripDuration = "5:29:59";
            String[] time = tripDuration.split(":");
            int hours = Integer.parseInt(time[0]);
            int minutes = Integer.parseInt(time[1]);
            int seconds = Integer.parseInt(time[2]);

            Log.d("TripDetailsAct", "hours from trip are: " + hours);
            Log.d("TripDetailsAct", "minutes from trip are: " + minutes);
            Log.d("TripDetailsAct", "seconds from trip are: " + seconds);

            /*
            * if the trip duration is equal or less then 2 hours
            * then trip earnings of the driver is ₹ 150
            * and travelling allowance is ₹ 100 because it's a one way trip
            * and total earnings will be ₹ 250
            * if the trip duration is greater then 2 hours
            * then the trip earnings of the driver will be ₹ 50 extra for every hour
            * travelling allowance is ₹ 100 because it's a one way trip
            * Ex: if the trip hours is 2 hours 29 minutes 59 seconds
            * then the driver will get ₹ 25 in trip earnings
            * and if the trip hours is ₹ 2 hours 30 minutes 00 seconds
            * then also the driver will get ₹ 25 in trip earnings
            * and if the minutes go above 30
            * then the driver will get ₹ 50 in trip earnings
            * */
            if (hours <= 2) {
                /// trip duration is less then 2 hours
                txtTripEarning.setText(getResources().getString(R.string.rs_symbol) + " " + "150");
                txtTravellingAllowance.setText(getResources().getString(R.string.rs_symbol) + " " + "100");
                txtTotalEarning.setText(String.valueOf(getResources().getString(R.string.rs_symbol) + " " + "250"));
            } else {
                /// trip duration is more then 2 hours
                int amt, travellingAllowance = 100;

                /// subtracting extra hours from 2
                hours = hours - 2;

                /// multiplying ₹ 50 to extra hours
                amt = 50 * hours;
                Log.d("TripDetailsAct", "hours after 2 hours are: " + hours);
                Log.d("TripDetailsAct", "extra amount earned in hours by driver is: " + amt);

                /*
                * checking how much extra minutes were in trip duration
                * if minutes is less then or equal to 29
                * then add extra ₹ 25 to trip earnings
                * other then the trip earnings calculated above
                * if minutes is equal to 30 and seconds is also equal to 0
                * then add extra ₹ 25 to trip earnings
                * if minutes is greater then 30 and seconds is greater then 1
                * then add extra ₹ 50 to trip earnings
                * */
                if (minutes <= 29) {
                    amt += 25;
                    Log.d("TripDetailsAct", "extra amount earned in minutes & seconds by driver is: " + amt);
                } else if (minutes == 30 && seconds == 0) {
                    amt += 25;
                    Log.d("TripDetailsAct", "extra amount earned in minutes & seconds by driver is: " + amt);
                } else if (minutes >= 30 && seconds >= 1) {
                    amt += 50;
                    Log.d("TripDetailsAct", "extra amount earned in minutes & seconds by driver is: " + amt);
                }

                /// adding extra amounts to basic trip earnings
                int tripEarnings = 150 + amt;
                txtTripEarning.setText(getResources().getString(R.string.rs_symbol) + " " + String.valueOf(tripEarnings));

                /// calculating total earnings of the driver
                /// travelling allowance + total trip earnings
                int totalEarnings = travellingAllowance + tripEarnings;
                txtTravellingAllowance.setText(getResources().getString(R.string.rs_symbol) + " " + String.valueOf(travellingAllowance));
                txtTotalEarning.setText(getResources().getString(R.string.rs_symbol) + " " + String.valueOf(totalEarnings));
            }
        } catch (Exception e) {
            Log.d("Exception", "while calculating driver earnings in trip details activity");
            e.printStackTrace();
        }
    }

    /**
      * Added by Amit Jangid
      * 2017 May 08 - Monday - 11:22 AM
      * Calculate Driver Earnings For Return Trip
     */
    private void calculateDriverEarningsForReturnTrip() {
        try {
            /// separating hours, minutes and seconds from trip duration
            /// tripDuration = "15:29:59";
            String[] time = tripDuration.split(":");
            int hours = Integer.parseInt(time[0]);
            int minutes = Integer.parseInt(time[1]);
            int seconds = Integer.parseInt(time[2]);

            Log.d("TripDetailsAct", "hours from trip are: " + hours);
            Log.d("TripDetailsAct", "minutes from trip are: " + minutes);
            Log.d("TripDetailsAct", "seconds from trip are: " + seconds);

            /*
            * if the trip duration is equal or less then 2 hours
            * then trip earnings of the driver is ₹ 150
            * and travelling allowance is ₹ 50 because it's a one way trip
            * and total earnings will be ₹ 200
            * if the trip duration is greater then 2 hours
            * then the trip earnings of the driver will be ₹ 50 extra for every hour
            * travelling allowance is ₹ 50 because it's a one way trip
            * Ex: if the trip hours is 2 hours 29 minutes 59 seconds
            * then the driver will get ₹ 25 in trip earnings
            * and if the trip hours is ₹ 2 hours 30 minutes 00 seconds
            * then also the driver will get ₹ 25 in trip earnings
            * and if the minutes go above 30
            * then the driver will get ₹ 50 in trip earnings
            * */
            if (hours <= 2) {
                /// trip duration is less then 2 hours
                txtTripEarning.setText(getResources().getString(R.string.rs_symbol) + " " + "150");
                txtTravellingAllowance.setText(getResources().getString(R.string.rs_symbol) + " " + "50");
                txtTotalEarning.setText(String.valueOf(getResources().getString(R.string.rs_symbol) + " " + "200"));
            } else {
                /// trip duration is more then 2 hours
                int amt, travellingAllowance = 50;

                /// subtracting extra hours from 2
                hours = hours - 2;

                /// multiplying ₹ 50 to extra hours
                amt = 50 * hours;
                Log.d("TripDetailsAct", "hours after 2 hours are: " + hours);
                Log.d("TripDetailsAct", "extra amount earned in hours by driver is: " + amt);

                /*
                * checking how much extra minutes were in trip duration
                * if minutes is less then or equal to 29
                * then add extra ₹ 25 to trip earnings
                * other then the trip earnings calculated above
                * if minutes is equal to 30 and seconds is also equal to 0
                * then add extra ₹ 25 to trip earnings
                * if minutes is greater then 30 and seconds is greater then 1
                * then add extra ₹ 50 to trip earnings
                * */
                if (minutes <= 29) {
                    amt += 25;
                    Log.d("TripDetailsAct", "extra amount earned in minutes & seconds by driver is: " + amt);
                } else if (minutes == 30 && seconds == 0) {
                    amt += 25;
                    Log.d("TripDetailsAct", "extra amount earned in minutes & seconds by driver is: " + amt);
                } else if (minutes >= 30 && seconds >= 1) {
                    amt += 50;
                    Log.d("TripDetailsAct", "extra amount earned in minutes & seconds by driver is: " + amt);
                }

                /// adding extra amounts to basic trip earnings
                int tripEarnings = 150 + amt;
                txtTripEarning.setText(getResources().getString(R.string.rs_symbol) + " " + String.valueOf(tripEarnings));

                /// calculating total earnings of the driver
                /// travelling allowance + total trip earnings
                int totalEarnings = travellingAllowance + tripEarnings;
                txtTravellingAllowance.setText(getResources().getString(R.string.rs_symbol) + " " + String.valueOf(travellingAllowance));
                txtTotalEarning.setText(getResources().getString(R.string.rs_symbol) + " " + String.valueOf(totalEarnings));
            }
        } catch (Exception e) {
            Log.d("Exception", "while calculating driver earnings in trip details activity");
            e.printStackTrace();
        }
    }

    /**
      * Added by Amit Jangid
      * 2017 May 08 - Monday - 11:22 AM
      * Calculate Driver Earnings For Outstation One Way Trip
     */
    private void calculateDriverEarningsForOutstationOneWayTrip() {
        try {
            /// separating hours, minutes and seconds from trip duration
            /// tripDuration = "15:29:59";
            String[] time = tripDuration.split(":");
            int hours = Integer.parseInt(time[0]);
            int minutes = Integer.parseInt(time[1]);
            int seconds = Integer.parseInt(time[2]);

            Log.d("TripDetailsAct", "hours from trip are: " + hours);
            Log.d("TripDetailsAct", "minutes from trip are: " + minutes);
            Log.d("TripDetailsAct", "seconds from trip are: " + seconds);

            /*
            * if the trip duration is equal or less then 12 hours
            * then trip earnings of the driver is ₹ 600
            * and travelling allowance is ₹ 200 because it's a one way trip
            * and total earnings will be ₹ 800
            * if the trip duration is greater then 12 hours
            * then the trip earnings of the driver will be ₹ 50 extra for every hour
            * travelling allowance is ₹ 200 because it's a one way trip
            * Ex: if the trip hours is 15 hours 29 minutes 59 seconds
            * then the driver will get ₹ 25 in trip earnings
            * and if the trip hours is ₹ 15 hours 30 minutes 00 seconds
            * then also the driver will get ₹ 25 in trip earnings
            * and if the minutes go above 30
            * then the driver will get ₹ 50 in trip earnings
            * */
            if (hours <= 12) {
                /// trip duration is less then 12 hours
                txtTripEarning.setText(getResources().getString(R.string.rs_symbol) + " " + "600");
                txtTravellingAllowance.setText(getResources().getString(R.string.rs_symbol) + " " + "50");
                txtTotalEarning.setText(String.valueOf(getResources().getString(R.string.rs_symbol) + " " + "650"));
            } else {
                /// trip duration is more then 12 hours
                int amt, travellingAllowance = 200;

                /// subtracting extra hours from 12
                hours = hours - 12;

                /// multiplying ₹ 50 to extra hours
                amt = 50 * hours;
                Log.d("TripDetailsAct", "hours after 12 hours are: " + hours);
                Log.d("TripDetailsAct", "extra amount earned in hours by driver is: " + amt);

                /*
                * checking how much extra minutes were in trip duration
                * if minutes is less then or equal to 29
                * then add extra ₹ 25 to trip earnings
                * other then the trip earnings calculated above
                * if minutes is equal to 30 and seconds is also equal to 0
                * then add extra ₹ 25 to trip earnings
                * if minutes is greater then 30 and seconds is greater then 1
                * then add extra ₹ 50 to trip earnings
                * */
                if (minutes <= 29) {
                    amt += 25;
                    Log.d("TripDetailsAct", "extra amount earned in minutes & seconds by driver is: " + amt);
                } else if (minutes == 30 && seconds == 0) {
                    amt += 25;
                    Log.d("TripDetailsAct", "extra amount earned in minutes & seconds by driver is: " + amt);
                } else if (minutes >= 30 && seconds >= 1) {
                    amt += 50;
                    Log.d("TripDetailsAct", "extra amount earned in minutes & seconds by driver is: " + amt);
                }

                /// adding extra amounts to basic trip earnings
                int tripEarnings = 600 + amt;
                txtTripEarning.setText(getResources().getString(R.string.rs_symbol) + " " + String.valueOf(tripEarnings));

                /// calculating total earnings of the driver
                /// travelling allowance + total trip earnings
                int totalEarnings = travellingAllowance + tripEarnings;
                txtTravellingAllowance.setText(getResources().getString(R.string.rs_symbol) + " " + String.valueOf(travellingAllowance));
                txtTotalEarning.setText(getResources().getString(R.string.rs_symbol) + " " + String.valueOf(totalEarnings));
            }
        } catch (Exception e) {
            Log.d("Exception", "while calculating driver earnings in trip details activity");
            e.printStackTrace();
        }
    }

    /**
      * Added by Amit Jangid
      * 2017 May 08 - Monday - 11:22 AM
      * Calculate Driver Earnings For Outstation Return Trip
     */
    private void calculateDriverEarningsForOutstationReturnTrip() {
        try {
            /// separating hours, minutes and seconds from trip duration
            /// tripDuration = "15:29:59";
            String[] time = tripDuration.split(":");
            int hours = Integer.parseInt(time[0]);
            int minutes = Integer.parseInt(time[1]);
            int seconds = Integer.parseInt(time[2]);

            Log.d("TripDetailsAct", "hours from trip are: " + hours);
            Log.d("TripDetailsAct", "minutes from trip are: " + minutes);
            Log.d("TripDetailsAct", "seconds from trip are: " + seconds);

            /*
            * if the trip duration is equal or less then 12 hours
            * then trip earnings of the driver is ₹ 600
            * and travelling allowance is ₹ 50 because it's a return trip
            * and total earnings will be ₹ 650
            * if the trip duration is greater then 12 hours
            * then the trip earnings of the driver will be ₹ 50 extra for every hour
            * travelling allowance is ₹ 50 because it's a return trip
            * Ex: if the trip hours is 15 hours 29 minutes 59 seconds
            * then the driver will get ₹ 25 in trip earnings
            * and if the trip hours is ₹ 15 hours 30 minutes 00 seconds
            * then also the driver will get ₹ 25 in trip earnings
            * and if the minutes go above 30
            * then the driver will get ₹ 50 in trip earnings
            * */
            if (hours <= 12) {
                /// trip duration is less then 12 hours
                txtTripEarning.setText(getResources().getString(R.string.rs_symbol) + " " + "600");
                txtTravellingAllowance.setText(getResources().getString(R.string.rs_symbol) + " " + "50");
                txtTotalEarning.setText(String.valueOf(getResources().getString(R.string.rs_symbol) + " " + "650"));
            } else {
                /// trip duration is more then 12 hours
                int amt, travellingAllowance = 50;

                /// subtracting extra hours from 12
                hours = hours - 12;

                /// multiplying ₹ 50 to extra hours
                amt = 50 * hours;
                Log.d("TripDetailsAct", "hours after 12 hours are: " + hours);
                Log.d("TripDetailsAct", "extra amount earned in hours by driver is: " + amt);

                /*
                * checking how much extra minutes were in trip duration
                * if minutes is less then or equal to 29
                * then add extra ₹ 25 to trip earnings
                * other then the trip earnings calculated above
                * if minutes is equal to 30 and seconds is also equal to 0
                * then add extra ₹ 25 to trip earnings
                * if minutes is greater then 30 and seconds is greater then 1
                * then add extra ₹ 50 to trip earnings
                * */
                if (minutes <= 29) {
                    amt += 25;
                    Log.d("TripDetailsAct", "extra amount earned in minutes & seconds by driver is: " + amt);
                } else if (minutes == 30 && seconds == 0) {
                    amt += 25;
                    Log.d("TripDetailsAct", "extra amount earned in minutes & seconds by driver is: " + amt);
                } else if (minutes >= 30 && seconds >= 1) {
                    amt += 50;
                    Log.d("TripDetailsAct", "extra amount earned in minutes & seconds by driver is: " + amt);
                }

                /// adding extra amounts to basic trip earnings
                int tripEarnings = 600 + amt;
                txtTripEarning.setText(getResources().getString(R.string.rs_symbol) + " " + String.valueOf(tripEarnings));

                /// calculating total earnings of the driver
                /// travelling allowance + total trip earnings
                int totalEarnings = travellingAllowance + tripEarnings;
                txtTravellingAllowance.setText(getResources().getString(R.string.rs_symbol) + " " + String.valueOf(travellingAllowance));
                txtTotalEarning.setText(getResources().getString(R.string.rs_symbol) + " " + String.valueOf(totalEarnings));
            }
        } catch (Exception e) {
            Log.d("Exception", "while calculating driver earnings in trip details activity");
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
