package com.zuver.driver.data;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.zuver.driver.R;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

//Class for common variable which used in entire project.
public class CommonData
{
    public static String companykey = "RH7PVsKE18qGe6Y6YOC8kdRntOEyDnD0uW";
    public static String[] CUSTOMERCARE = {"+918067650565", "+912243686868"};

    public static String BasePath = "http://52.77.214.224/mobileapi115/index/dGF4aV9hbGw=/?";

    /// Development or Test URL for testing purpose
    public static String MAIN_URL = "http://52.15.73.173/ApiWebService/";

    /// Production URL
    /// public static String MAIN_URL = "http://54.87.254.193/zuverdash/ApiWebService/";

    //public static String MAIN_URL = "http://54.87.254.193/ApiWebService/";
    //public static String BasePath = "http://54.87.254.193/mobileapi115/index/dGF4aV9hbGw=/?";

    public static String ZooCar_BaseAPI = "http://api.zoomcar.com/public/v1/hd_vendors/";
    public static String ZoomCarCheckList = "checklist_request";
    public static String ZoomCarDriverStatus = "http://api.zoomcar.com/public/v1/hd_vendors/driver_status";

    public static String mDevice_id;
    public static double getlatitude = 0.0;
    public static double getlongitude = 0.0;
    public static double last_getlatitude = 0.0;
    public static double last_getlongitude = 0.0;
    public static double travel_km = 0.0;
    public static String hstravel_km = "";
    public static String current_act;
    public static String currentspeed = "";
    public static boolean iswaitingrunning;
    public static boolean speed_waiting_stop = true;
    public static String below_speed = "";
    public static int km_calc = 0;
    public static Context sContext;

    public static boolean isNotificationDriverDecline = false;
    public static boolean timer1_stop = true;
    public static boolean timer2_stop = true;

    public static void isSetNotificationDriverDecline(boolean value)
    {
        isNotificationDriverDecline = value;
    }

    public static boolean isGetNotificationDriverDecline() {
        return isNotificationDriverDecline;
    }

    public static void Animation1(Activity activity)
    {
        activity.overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    public static String encodeToBase64()
    {
        String live_key = "ntaxi_" + companykey;
        String encodedString = "";

        try
        {
            byte[] byteData = null;

            if (Build.VERSION.SDK_INT >= 8)
            {
                byteData = android.util.Base64.encode(live_key.getBytes(), android.util.Base64.DEFAULT);
            }

            encodedString = new String(byteData);
        }
        catch (Exception e)
        {
            Log.i("ExceptionEncodeBase64", "" + e);
        }

        try
        {
            encodedString = URLEncoder.encode(encodedString, "utf-8");
            encodedString = encodedString.replace("%0A", "");
        }
        catch (UnsupportedEncodingException e)
        {
            Log.i("ConversionNotSupported", "" + e.toString());
        }

        return encodedString;
    }

    /*
     * method to clear the cache. input-context
     */
    public static void trimCache(Context context)
    {
        try
        {
            File dir = context.getCacheDir();

            if (dir != null && dir.isDirectory())
            {
                deleteDir(dir);
            }
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir)
    {
        if (dir != null && dir.isDirectory())
        {
            String[] children = dir.list();

            for (int i = 0; i < children.length; i++)
            {
                boolean success = deleteDir(new File(dir, children[i]));

                if (!success)
                {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }
}