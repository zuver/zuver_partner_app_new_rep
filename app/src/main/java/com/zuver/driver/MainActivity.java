package com.zuver.driver;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.zuver.driver.data.CommonData;
import com.zuver.driver.data.MystatusData;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.route.JSONParser;
import com.zuver.driver.service.APIService_Volley_JSON;
import com.zuver.driver.service.LocationUpdate;
import com.zuver.driver.service.NonActivity;
import com.zuver.driver.utils.FontHelper;
import com.zuver.driver.utils.SessionSave;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class MainActivity extends FragmentActivity
{
    public final String TAG = getClass().getSimpleName();
    public static MystatusData mMyStatus;
    public Typeface tf;
    public static Dialog mDialog, gps_alert, mAlertDialog;
    static Calendar calendar;
    public static String returnString;
    public int _hour;
    public String _ampm;
    Dialog lDialog;
    public static Dialog sDialog;

    public enum ValidateAction
    {
        NONE, isValueNULL, isValidPassword, isValidSalutation, isValidFirstname, isValidLastname, isValidCard, isValidExpiry, isValidMail, isValidConfirmPassword
    }

    /**
     * This is method for set the layout for the child activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mMyStatus = new MystatusData(MainActivity.this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog = new Dialog(MainActivity.this);
        int view = setLayout();
        setLocale();

        if (view != 0)
        {
            setContentView(view);
            Initialize();

            try
            {
                if (mDialog.isShowing())
                    mDialog.dismiss();
            }
            catch (Exception e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
        }
    }


    public static String getSha1Hex(String clearString)
    {
        try
        {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
            messageDigest.update(clearString.getBytes("UTF-8"));
            byte[] bytes = messageDigest.digest();
            StringBuilder buffer = new StringBuilder();

            for (byte b : bytes)
            {
                buffer.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
            }

            String key = Base64.encodeToString(bytes, Base64.DEFAULT);
            Log.e("HashKeyBase64", key);
            Log.e("HashKey", buffer.toString());
            return key;
        }
        catch (Exception ignored)
        {
            ignored.printStackTrace();
            return null;
        }
    }

    public abstract int setLayout();

    public abstract void Initialize();

    /**
     * This is method for show the toast
     */
    public void ShowToast(Context contex, String message)
    {
        Toast toast = Toast.makeText(contex, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public void requestPermission(Activity activity, String permission)
    {
        ActivityCompat.requestPermissions(activity, new String[]{permission}, 1);
    }

    public static boolean checkPermission_phone(Context context, String permission)
    {
        if (ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED)
        {
            return true;
        }

        return false;
    }


    /**
     * This is method for show the Log
     */
    public void showLog(String msg) {
        Log.i(TAG, msg);
    }

    /**
     * This is method for check the mail is valid by the use of regex class.
     */
    public boolean validdmail(String string)
    {
        // TODO Auto-generated method stub
        boolean isValid = false;
        String expression = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@" + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?" + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\." + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?" + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|" + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(string);

        if (matcher.matches())
        {
            isValid = true;
        }

        return isValid;
    }

    public static void alertView_with_activity(final Context mContext, String title, String message, String success_txt)
    {
        if (mAlertDialog != null && mAlertDialog.isShowing())
        {
            mAlertDialog.dismiss();
        }

        try
        {
            final View view = View.inflate(mContext, R.layout.alert_view, null);
            mAlertDialog = new Dialog(mContext, R.style.dialogwinddow);
            mAlertDialog.setContentView(view);
            mAlertDialog.setCancelable(false);
            mAlertDialog.show();

            final TextView title_text = (TextView) mAlertDialog.findViewById(R.id.title_text);
            final TextView message_text = (TextView) mAlertDialog.findViewById(R.id.message_text);
            final Button button_success = (Button) mAlertDialog.findViewById(R.id.button_success);
            final Button button_failure = (Button) mAlertDialog.findViewById(R.id.button_failure);

            button_failure.setVisibility(View.GONE);
            title_text.setText(title);
            message_text.setText(message);
            button_success.setText(success_txt);

            button_success.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(final View v)
                {
                    // TODO Auto-generated method stub
                    ///clearsession(mContext);
                    mAlertDialog.dismiss();
                    mAlertDialog.cancel();
                    Intent intent = new Intent(mContext, SignAct.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mContext.startActivity(intent);
                    CommonData.Animation1((Activity) mContext);
                }
            });

            button_failure.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(final View v)
                {
                    // TODO Auto-generated method stub
                    mAlertDialog.dismiss();
                }
            });
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    /**
     * This is method for convert the InputStream value into StringBuilder
     */
    public StringBuilder inputStreamToString(InputStream is)
    {
        String rLine = "";
        StringBuilder answer = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        try
        {
            while ((rLine = rd.readLine()) != null)
            {
                answer.append(rLine);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return answer;
    }

    /**
     * This is method for check the Internet connection
     */
    public boolean isOnline()
    {
        ConnectivityManager connectivity = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();

            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }
        }

        return false;
    }

    public static boolean isOnlines(Context mContext)
    {
        ConnectivityManager connectivity = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();

            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }
        }
        return false;
    }

    /**
     * This is method for show progress bar
     */
    public void showLoading(Context context)
    {
        try
        {
            View view = View.inflate(context, R.layout.progress_bar, null);
            mDialog = new Dialog(context, R.style.dialogwinddow);
            mDialog.setContentView(view);
            mDialog.setCancelable(false);
            mDialog.show();
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    /**
     * This method closes the dialog
     */
    public void dismissLoading()
    {
        try
        {
            mDialog.dismiss();
        }
        catch (Exception e)
        {
            Log.d("Exception", "while closing the dialog in main activity:\n");
            e.printStackTrace();
        }
    }

    /**
     * This is method for convert the string value into MD5
     */
    public String convertPassMd5(String pass)
    {
        String password = null;
        MessageDigest mdEnc;

        try
        {
            mdEnc = MessageDigest.getInstance("MD5");
            mdEnc.update(pass.getBytes(), 0, pass.length());
            pass = new BigInteger(1, mdEnc.digest()).toString(16);

            while (pass.length() < 32)
            {
                pass = "0" + pass;
            }

            password = pass;
        }
        catch (NoSuchAlgorithmException e1)
        {
            e1.printStackTrace();
        }
        return password;
    }

    /**
     * This is method for logout the user from their current session.
     */
    public void logout(final Context context)
    {
        try
        {
            final View view = View.inflate(context, R.layout.netcon_lay, null);
            mDialog = new Dialog(context, R.style.dialogwinddow);
            mDialog.setContentView(view);
            mDialog.setCancelable(false);
            mDialog.show();

            FontHelper.applyFont(context, mDialog.findViewById(R.id.alert_id), "DroidSans.ttf");
            final TextView title_text = (TextView) mDialog.findViewById(R.id.title_text);
            final TextView message_text = (TextView) mDialog.findViewById(R.id.message_text);
            final Button button_success = (Button) mDialog.findViewById(R.id.button_success);
            final Button button_failure = (Button) mDialog.findViewById(R.id.button_failure);

            button_failure.setVisibility(View.VISIBLE);
            title_text.setText("" + context.getResources().getString(R.string.message));
            message_text.setText("" + context.getResources().getString(R.string.confirmlogout));
            button_success.setText("" + context.getResources().getString(R.string.yes));
            button_failure.setText("" + context.getResources().getString(R.string.no));
            button_success.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(final View v)
                {
                    // TODO
                    // Auto-generated
                    // method stub
                    try
                    {
                        mDialog.dismiss();
                        // TODO Auto-generated method stub
                        JSONObject j = new JSONObject();
                        j.put("driver_id", SessionSave.getSession("Id", context));
                        j.put("latitude", SessionSave.getSession("mLatitude", context));
                        j.put("longitude", SessionSave.getSession("mLongitude", context));
                        NonActivity nonactiityobj = new NonActivity();
                        nonactiityobj.stopServicefromNonActivity(context);
                        String url = "user_logout";
                        new Logout(url, j);
                    }
                    catch (Exception e)
                    {
                        // TODO: handle exception
                        e.printStackTrace();
                    }
                }
            });

            button_failure.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(final View v) {
                    // TODO
                    // Auto-generated
                    // method stub
                    mDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

  /*  *//**
     * This is method for driver_status_select API call to get the driver status.
     *//*
    public void Status_select() {
        try {
            NonActivity nonactiityobj = new NonActivity();
            nonactiityobj.stopServicefromNonActivity(MainActivity.this);
            String url = "driver_status_select&driver_id=" + SessionSave.getSession("Id", MainActivity.this);
            new Driverstatus(url);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }*/

    /**
     * This is method for set the language configuration.
     */
    public void setLocale()
    {
        if (SessionSave.getSession("Lang", MainActivity.this).equals(""))
        {
            SessionSave.saveSession("Lang", "en", MainActivity.this);
            SessionSave.saveSession("Lang_Country", "en_GB", MainActivity.this);
        }

        System.out.println("Lang" + SessionSave.getSession("Lang", MainActivity.this));
        System.out.println("Lang_Country" + SessionSave.getSession("Lang_Country", MainActivity.this));

        Configuration config = new Configuration();
        String langcountry = SessionSave.getSession("Lang_Country", MainActivity.this);
        String arry[] = langcountry.split("_");
        config.locale = new Locale(arry[0], arry[1]);
        Locale.setDefault(new Locale(arry[0], arry[1]));
        MainActivity.this.getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    /**
     * This is class for logout API call and process the response.Clear their current session.
     */
    private class Logout implements APIResult
    {
        private Logout(String url, JSONObject data)
        {
            System.out.println("" + url);
            System.out.println("" + data);

            if (isOnline())
            {
                new APIService_Volley_JSON(MainActivity.this, this, data, false).execute(url);
            }
            else
            {
                alert_view(MainActivity.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_internet), "" + getResources().getString(R.string.ok), "");
            }
        }

        @Override
        public void getResult(boolean isSuccess, String result)
        {
            if (isSuccess)
            {
                try
                {
                    JSONObject json = new JSONObject(result);

                    if (json.getInt("status") == 1)
                    {
                        Intent locationService = new Intent(MainActivity.this, LocationUpdate.class);
                        stopService(new Intent(locationService));
                        clearsession(MainActivity.this);

                        final View view = View.inflate(MainActivity.this, R.layout.alert_view, null);
                        lDialog = new Dialog(MainActivity.this, R.style.NewDialog);
                        lDialog.setContentView(view);
                        lDialog.setCancelable(false);
                        lDialog.show();

                        final TextView title_text = (TextView) lDialog.findViewById(R.id.title_text);
                        final TextView message_text = (TextView) lDialog.findViewById(R.id.message_text);
                        final Button button_success = (Button) lDialog.findViewById(R.id.button_success);

                        title_text.setText("" + getResources().getString(R.string.message));
                        message_text.setText("" + json.getString("message"));
                        button_success.setText("" + getResources().getString(R.string.ok));

                        button_success.setOnClickListener(new OnClickListener()
                        {
                            @Override
                            public void onClick(final View v)
                            {
                                // TODO Auto-generated method stub
                                lDialog.dismiss();
                                Intent intent = new Intent(MainActivity.this, SignAct.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                                CommonData.Animation1(MainActivity.this);
                            }
                        });
                    }
                    else if (json.getInt("status") == -1)
                    {
                        //User has been logged out for any reason
                        askForLogin();
                    }
                    else
                    {
                        alert_view(MainActivity.this, "" + getResources().getString(R.string.message), "" + json.getString("message"), "" + getResources().getString(R.string.ok), "");
                    }
                }
                catch (JSONException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            else
            {
                alert_view(MainActivity.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
            }
        }
    }


    /**
     * clear all driver session variables used except getcoreconfig details
     */
    public static void clearsession(Context ctx)
    {
        try
        {
            SessionSave.saveSession("Id", "", ctx);
            SessionSave.saveSession("Driver_locations", "", ctx);
            SessionSave.saveSession("driver_id", "", ctx);
            SessionSave.saveSession("Name", "", ctx);
            SessionSave.saveSession("company_id", "", ctx);
            SessionSave.saveSession("bookedby", "", ctx);
            SessionSave.saveSession("p_image", "", ctx);
            SessionSave.saveSession("Email", "", ctx);
            SessionSave.saveSession("Phone", "", ctx);
            SessionSave.saveSession("start_auto_logout", "0", ctx);
            SessionSave.saveSession("Auth_Key", "", ctx);
            SessionSave.saveSession("Id", "", ctx);
            SessionSave.saveSession("User_Type", "", ctx);
            SessionSave.saveSession("travel_status", "", ctx);
            SessionSave.saveSession("status", "F", ctx);
            SessionSave.saveSession("trip_id", "", ctx);
            SessionSave.saveSession("longitude", "", ctx);
            SessionSave.saveSession("latitude", "", ctx);
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    /**
     * Call this method if user logged out from backend or for invalid user found
     * Can call from anywhere
     */
    public void askForLogin()
    {
        Intent intent = new Intent(MainActivity.this, UserLoginAct.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
        CommonData.Animation1(MainActivity.this);
    }

    public Dialog alertDialog;

    /**
     * Custom alert dialog used in entire project.can call from anywhere with the following param Context,title,message,success and failure button text.
     */
    public void alert_view(Context mContext, String title, String message, String success_txt, String failure_txt)
    {
        final View view = View.inflate(mContext, R.layout.alert_view, null);
        alertDialog = new Dialog(mContext, R.style.NewDialog);
        alertDialog.setContentView(view);
        alertDialog.setCancelable(true);
        FontHelper.applyFont(mContext, alertDialog.findViewById(R.id.alert_id), "DroidSans.ttf");
        alertDialog.show();

        final TextView title_text = (TextView) alertDialog.findViewById(R.id.title_text);
        final TextView message_text = (TextView) alertDialog.findViewById(R.id.message_text);
        final Button button_success = (Button) alertDialog.findViewById(R.id.button_success);
        title_text.setText(title);
        message_text.setText(message);
        button_success.setText(success_txt);

        button_success.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(final View v)
            {
                alertDialog.dismiss();
            }
        });
    }

    public static class getDropLocation extends AsyncTask<Double, String, String>
    {
        private Context mContext;

        public getDropLocation(Context mCon) {
            mContext = mCon;
        }

        @Override
        protected String doInBackground(final Double... params)
        {
            JSONParser jParser = new JSONParser();
            String result = "";

            try
            {
                String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + params[0] + "," + params[1] + "&sensor=false";
                result = jParser.getJSONFromUrl(googleMapUrl);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            if (!result.equalsIgnoreCase(""))
            {
                return result;
            }
            else
            {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String inputJson)
        {
            super.onPostExecute(inputJson);

           try
           {
                if (!inputJson.equalsIgnoreCase(""))
                {
                    JSONObject object = new JSONObject("" + inputJson);
                    JSONArray array = object.getJSONArray("results");
                    object = array.getJSONObject(0);
                    SessionSave.saveSession("drop_location", object.getString("formatted_address"), mContext);
                    SessionSave.saveSession("Driver_locations_home", object.getString("formatted_address"), mContext);
                    OngoingAct.Address = object.getString("formatted_address");
                }
                else
                {
                    SessionSave.saveSession("drop_location", "", mContext);
                    SessionSave.saveSession("Driver_locations_home", "", mContext);
                    OngoingAct.Address = "";
                }
            }
            catch (Exception e)
            {
                Log.e("Error", e.toString());
            }
        }
    }

    public static void isConnect(final Context mContext, final boolean isconnect)
    {
        try
        {
            if (!isconnect)
            {
                final View view = View.inflate(mContext, R.layout.netcon_lay, null);
                sDialog = new Dialog(mContext, R.style.dialogwinddow);
                sDialog.setContentView(view);
                sDialog.setCancelable(true);
                FontHelper.applyFont(mContext, sDialog.findViewById(R.id.alert_id), "DroidSans.ttf");
                sDialog.show();

                final TextView title_text = (TextView) sDialog.findViewById(R.id.title_text);
                final TextView message_text = (TextView) sDialog.findViewById(R.id.message_text);
                final Button button_success = (Button) sDialog.findViewById(R.id.button_success);
                final Button button_failure = (Button) sDialog.findViewById(R.id.button_failure);

                title_text.setText("" + mContext.getResources().getString(R.string.message));
                message_text.setText("" + mContext.getResources().getString(R.string.check_net_connection));
                button_success.setText("" + mContext.getResources().getString(R.string.c_tryagain));
                button_failure.setText("" + mContext.getResources().getString(R.string.cancell));

                button_success.setOnClickListener(new OnClickListener()
                {
                    @Override
                    public void onClick(final View v)
                    {
                        if (isOnlines(mContext))
                        {
                            sDialog.dismiss();
                            sDialog.cancel();
                            Intent intent = new Intent(mContext, mContext.getClass());
                            Activity activity = (Activity) mContext;
                            activity.finish();
                            mContext.startActivity(intent);
                        }
                        else
                        {
                            isConnect(mContext, false);
                        }
                    }
                });

                button_failure.setOnClickListener(new OnClickListener()
                {
                    @Override
                    public void onClick(final View v)
                    {
                        // TODO Auto-generated method stub
                        sDialog.dismiss();
                        Activity activity = (Activity) mContext;
                        activity.finish();
                        final Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        activity.startActivity(intent);
                    }
                });
            }
            else
            {
                try
                {
                    sDialog.dismiss();

                    if (mContext != null)
                    {
                        Intent intent = new Intent(mContext, mContext.getClass());
                        mContext.startActivity(intent);
                    }
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    public static void gpsalert(final Context mContext, boolean isconnect)
    {
        if (!isconnect)
        {
            final View view = View.inflate(mContext, R.layout.alert_view, null);
            gps_alert = new Dialog(mContext, R.style.NewDialog);
            gps_alert.setContentView(view);
            FontHelper.applyFont(mContext, gps_alert.findViewById(R.id.alert_id), "DroidSans.ttf");
            gps_alert.setCancelable(false);

            if (!gps_alert.isShowing())
                gps_alert.show();

            final TextView title_text = (TextView) gps_alert.findViewById(R.id.title_text);
            final TextView message_text = (TextView) gps_alert.findViewById(R.id.message_text);
            final Button button_success = (Button) gps_alert.findViewById(R.id.button_success);
            final Button button_failure = (Button) gps_alert.findViewById(R.id.button_failure);

            button_failure.setVisibility(View.GONE);
            title_text.setText("" + mContext.getResources().getString(R.string.location_disable));
            message_text.setText("" + mContext.getResources().getString(R.string.location_enable));
            button_success.setText("" + mContext.getResources().getString(R.string.enable));

            button_success.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(final View v)
                {
                    // TODO Auto-generated method stub
                    if (gps_alert != null && gps_alert.isShowing())
                    {
                        gps_alert.dismiss();
                    }

                    Intent mIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    mContext.startActivity(mIntent);
                }
            });

            button_failure.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(final View v)
                {
                    // TODO Auto-generated method stub
                    gps_alert.dismiss();
                }
            });
        }
        else
        {
            try
            {
                if (gps_alert != null && gps_alert.isShowing())
                {
                    gps_alert.dismiss();
                }
            }
            catch (Exception e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
        }
    }

    public static InputFilter filter = new InputFilter()
    {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend)
        {
          /*  if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }*/
            if (source.equals(""))
            { // for backspace
                return source;
            }

            if (source.toString().matches("[a-zA-Z0-9 ]+"))
            {
                return source;
            }

            return "";
        }
    };

    /**
     * This is method to format date(dd-MM-yyyy hh:mm:ss a) eg:(21-MAY-2015 11:15:34 AM)
     */
    public static String date_conversion(String date_time)
    {
        try
        {
            SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            calendar = Calendar.getInstance(Locale.UK);
            java.util.Date yourDate = parser.parse(date_time);
            calendar.setTime(yourDate);
            String month = monthFullName(calendar.get(Calendar.MONTH) + 1);
            String dayName = daytwodigit(calendar.get(Calendar.DAY_OF_MONTH) + 1);
            String timeformat = timetwodigit(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));
            returnString = (dayName + "-" + month + "-" + calendar.get(Calendar.YEAR) + " " + timeformat);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        return returnString;
    }

    /**
     * This is method to format month value (MMM) eg:(MAY)
     */
    private static String monthFullName(int monthnumber)
    {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, monthnumber - 1);
        SimpleDateFormat sf = new SimpleDateFormat("MMM");
        sf.setCalendar(cal);
        String monthName = sf.format(cal.getTime());
        return monthName;
    }

    /**
     * This is method to format month value (dd) eg:(21)
     */
    private static String daytwodigit(int daynumber)
    {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, daynumber - 1);
        SimpleDateFormat sf = new SimpleDateFormat("dd");
        sf.setCalendar(cal);
        String day2digit = sf.format(cal.getTime());
        return day2digit;
    }

    /**
     * This is method to format month value (hh:mm:ss a) eg:(11:15:34 AM)
     */
    private static String timetwodigit(int year, int month, int day, int hr, int min, int sec)
    {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, day, hr, min, sec);
        SimpleDateFormat sf = new SimpleDateFormat("hh:mm:ss a");
        sf.setCalendar(cal);
        String time2digit = sf.format(cal.getTime());
        return time2digit.toUpperCase();
    }

    /**
     * This is method to validate the field like Mail,Password,Name,Salutation etc and show the appropriate alert message.
     */
    public boolean validations(ValidateAction VA, Context con, String stringtovalidate)
    {
        String message = "";
        boolean result = false;

        switch (VA)
        {
            case isValueNULL:

                if (TextUtils.isEmpty(stringtovalidate))
                {
                    message = "" + con.getResources().getString(R.string.enter_mobile_number);
                }
                else
                {
                    result = true;
                }

                break;

            case isValidPassword:

                if (TextUtils.isEmpty(stringtovalidate))
                {
                    message = "" + con.getResources().getString(R.string.enter_the_password);
                }
                else if (stringtovalidate.length() < 5)
                {
                    message = "" + con.getResources().getString(R.string.pwd_min);
                }
                else if (stringtovalidate.length() > 32)
                {
                    message = "" + con.getResources().getString(R.string.s_pass_max);
                }
                else
                {
                    result = true;
                }

                break;

            case isValidFirstname:

                if (TextUtils.isEmpty(stringtovalidate))
                {
                    message = "" + con.getResources().getString(R.string.enter_the_first_name);
                }
                else
                {
                    result = true;
                }

                break;

            case isValidLastname:

                if (TextUtils.isEmpty(stringtovalidate))
                {
                    message = "" + con.getResources().getString(R.string.enter_the_last_name);
                }
                else
                {
                    result = true;
                }

                break;

            case isValidCard:

                if (TextUtils.isEmpty(stringtovalidate))
                    message = "" + con.getResources().getString(R.string.enter_the_card_number);
                else if (stringtovalidate.length() < 9 || stringtovalidate.length() > 16)
                    message = "" + con.getResources().getString(R.string.enter_the_valid_card_number);
                else
                    result = true;
                break;
            case isValidExpiry:
                if (TextUtils.isEmpty(stringtovalidate))
                    message = "" + con.getResources().getString(R.string.enter_the_expiry_date);
                else
                    result = true;
                break;
            case isValidMail:
                if (TextUtils.isEmpty(stringtovalidate))
                    message = "" + con.getResources().getString(R.string.enter_the_email);
                else if (!validdmail(stringtovalidate))
                    message = "" + con.getResources().getString(R.string.enter_the_valid_email);
                else
                    result = true;
                break;
            case isValidConfirmPassword:
                if (TextUtils.isEmpty(stringtovalidate))
                    message = "" + con.getResources().getString(R.string.enter_the_confirmation_password);
                else
                    result = true;
                break;
        }
        if (!message.equals("")) {
            alert_view(con, "" + getResources().getString(R.string.message), "" + message, "" + getResources().getString(R.string.ok), "");
        }
        return result;
    }

    // Slider menu used to move from one activity to another activity.
    public void ClickMethod(View v) {
        Intent i;
        switch (v.getId()) {
            case R.id.menu_me:
                i = new Intent(MainActivity.this, MeAct.class);
                startActivity(i);
                finish();
                break;
            case R.id.menu_home:
                i = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(i);
                finish();
                break;
            case R.id.menu_logout:
                logout(MainActivity.this);
                i = new Intent(MainActivity.this, SignAct.class);
                startActivity(i);
                finish();
                break;
            case R.id.menu_mystatus:
                i = new Intent(MainActivity.this, MyStatus.class);
                startActivity(i);
                finish();
                break;
        }
    }

    public static String date_time(String date)
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
        String r_time = null;

        try
        {
            String tim = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.getDefault()).format(simpleDateFormat.parse(date));
            r_time = tim.toUpperCase();

        }
        catch (ParseException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return r_time;
    }


    public boolean isGpsEnabled(Context context) {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            return true;
        else
            return false;
    }
}
