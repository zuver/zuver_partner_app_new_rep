package com.zuver.driver.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zuver.driver.MainActivity;
import com.zuver.driver.R;
import com.zuver.driver.utils.FontHelper;
import com.zuver.driver.utils.SessionSave;

//This adapter used to show the job list details into list view.
public class JobsListAdapter extends BaseAdapter
{
    private Context mContext;
    private ArrayList<HashMap<String, String>> mList;

    // constructor
    public JobsListAdapter(Context context, ArrayList<HashMap<String, String>> list)
    {
        this.mContext = context;
        this.mList = list;
    }

    // It returns the list item count.
    @Override
    public int getCount() {
        return mList.size();
    }

    // It returns the item detail with select position.
    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    // It returns the item id with select position.
    @Override
    public long getItemId(int position) {
        return 0;
    }

    // Get the view for each row in the list used view holder.
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        final ViewHolder holder;

        if (convertView == null)
        {
            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.list_booking_item, null);
            FontHelper.applyFont(mContext, convertView.findViewById(R.id.list_booking_root), "DroidSans.ttf");
            holder = new ViewHolder();
            holder.txtTime = (TextView) convertView.findViewById(R.id.booking_timeTxt);
            holder.txtPick = (TextView) convertView.findViewById(R.id.booking_picTxt);
            holder.txtDrop = (TextView) convertView.findViewById(R.id.booking_dropsTxt);
            holder.txtName = (TextView) convertView.findViewById(R.id.booking_nameTxt);
            holder.txtLblTime = (TextView) convertView.findViewById(R.id.txttime);
            holder.txtLblPick = (TextView) convertView.findViewById(R.id.txtpick);
            holder.txtLblDrop = (TextView) convertView.findViewById(R.id.txtdrop);
            holder.txtColon1 = (TextView) convertView.findViewById(R.id.txtcolon);
            holder.txtColon2 = (TextView) convertView.findViewById(R.id.txtcolon1);
            holder.txtColon3 = (TextView) convertView.findViewById(R.id.txtcolon2);
            holder.layout = (LinearLayout) convertView.findViewById(R.id.tripchild);
            holder.lay_top = (LinearLayout) convertView.findViewById(R.id.lay_top);
            holder.txtDate = (TextView) convertView.findViewById(R.id.txtDate);
            holder.txtPickupLocation = (TextView) convertView.findViewById(R.id.txtPickupLocation);
            holder.txtBillingAmount = (TextView) convertView.findViewById(R.id.txtBillingAmount);
            holder.lay_list_adapter = (RelativeLayout) convertView.findViewById(R.id.lay_list_adapter);
            holder.txt_history = (TextView) convertView.findViewById(R.id.txt_history);
            holder.txt_date_format = (TextView) convertView.findViewById(R.id.txt_date_format);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        try
        {
            if (!SessionSave.getSession("DriverAdapter", mContext).equalsIgnoreCase("DriveHistory"))
            {
                holder.lay_top.setVisibility(View.GONE);
                holder.lay_list_adapter.setVisibility(View.VISIBLE);
                holder.txt_history.setText(MainActivity.date_conversion(mList.get(position).get("pickup_time")));
                holder.txt_date_format.setText("" + mList.get(position).get("pickup_location"));
            }
            else
            {
                holder.lay_top.setVisibility(View.VISIBLE);
                holder.lay_list_adapter.setVisibility(View.GONE);
                holder.txtName.setText(Character.toUpperCase(mList
                        .get(position).get("passenger_name").charAt(0))
                        + mList.get(position).get("passenger_name")
                        .substring(1));
                holder.txtDate.setText(MainActivity.date_conversion(mList.get(position).get("pickup_time")));
                holder.txtPickupLocation.setText("" + mList.get(position).get("pickup_location"));
                holder.txtBillingAmount.setText("" + mList.get(position).get("bill_amount"));
            }

        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }

        return convertView;
    }

    // View holder class member this contains in every row in list.
    private class ViewHolder
    {
        LinearLayout layout, lay_top;
        RelativeLayout lay_list_adapter;
        TextView txtTime;
        TextView txtPick;
        TextView txtDrop;
        TextView txtName;
        TextView txtLblTime;
        TextView txtLblPick;
        TextView txtLblDrop;
        TextView txtColon1;
        TextView txtColon2;
        TextView txtColon3;
        TextView txt_history;
        TextView txt_date_format;

        TextView txtDate;
        TextView txtPickupLocation;
        TextView txtBillingAmount;
    }
}
