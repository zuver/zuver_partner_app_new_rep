package com.zuver.driver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zuver.driver.data.CommonData;
import com.zuver.driver.data.MystatusData;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.service.APIServiceVolley;
import com.zuver.driver.service.APIService_Volley_JSON;
import com.zuver.driver.utils.FontHelper;
import com.zuver.driver.utils.LocationDb;
import com.zuver.driver.utils.SessionSave;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This is the only class for showing the fare calculation
 * in driver application.
 *
 * @author Raja
 */
public class BillFareAct extends MainActivity
{
    private String otTime = "", promotions = "";
    private String message;
    private String f_tripid;
    LocationDb objLocationDb;
    public static BillFareAct activity;
    public static Activity mFlagger;
    Intent details;
    //RadioButton radiocashButton, radiocardButton, radiouncardButton, radioaccButton;
    private TextView txtBack, txtTitle;
    private TextView txtPackageType, txtStartTime, txtEndTime, txtHourCharges, txtOTCharges, txtPromotion, txtTotalCharge;
    private String startTime, endTime, tripType, passengerdiscount, totaltripcost, Fare, totalFare, hourCharges, displayTime, totalFareAPI;
    private String f_paymodid = "1", f_minutes_traveled, f_minutes_fare;
    private String f_distance, wallet_Payment, promoDiscount, calculateTime, taxPercent, taxAmount, promotioReferral;
    private EditText txtCharges, txtOtherCharges;
    private LinearLayout layFinalizeTrip;
    private Button btn_finalize;

    private String overTimeCharge = "", waitingTimeCost = "", nightFare = "", nightFareApplicable = "", convencCharge = "", waitingTime = "";

    private String parkingCharges = "0.00", OtherCharges = "0.00";

    private TextView txtTaxAmount, txtReferralAmount, txtTax, ModeOffPay;

    private LinearLayout layPromotions, layReferralPromotions;

    private View viewRefferal, viewPromotions;
    private String FareType;

    private double parCharg = 0.00;
    private double otherCharg = 0.00;

    @Override
    public int setLayout()
    {
        // TODO Auto-generated method stub
        setLocale();
        return R.layout.fare_lay;
    }

    @Override
    public void Initialize()
    {
        // TODO Auto-generated method stub

        CommonData.sContext = this;
        CommonData.current_act = "BillFareAct";
        activity = this;
        mFlagger = this;
        objLocationDb = new LocationDb(BillFareAct.this);
        FontHelper.applyFont(this, findViewById(R.id.id_farelay), "DroidSans.ttf");

        txtBack = (TextView) findViewById(R.id.backup);
        txtBack.setVisibility(View.GONE);
        txtTitle = (TextView) findViewById(R.id.headerTxt);
        txtTitle.setText("" + getResources().getString(R.string.bill_summary));
        ModeOffPay = (TextView) findViewById(R.id.modeofpay);
        txtCharges = (EditText) findViewById(R.id.txt_charges);
        txtOtherCharges = (EditText) findViewById(R.id.txt_other_charges);

        btn_finalize = (Button) findViewById(R.id.btn_finalize);

        txtPackageType = (TextView) findViewById(R.id.txt_package_value);
        txtStartTime = (TextView) findViewById(R.id.txt_start_time);
        txtEndTime = (TextView) findViewById(R.id.txt_end_time);
        txtHourCharges = (TextView) findViewById(R.id.txt_base_hours);
        txtOTCharges = (TextView) findViewById(R.id.txt_hours);
        txtPromotion = (TextView) findViewById(R.id.txt_promotion);
        txtTotalCharge = (TextView) findViewById(R.id.txt_total_charge);

        txtTax = (TextView) findViewById(R.id.txt_tax);
        txtTaxAmount = (TextView) findViewById(R.id.txt_tax_amount);
        txtReferralAmount = (TextView) findViewById(R.id.txt_referral_amt);

        layPromotions = (LinearLayout) findViewById(R.id.lay_promotions);
        layReferralPromotions = (LinearLayout) findViewById(R.id.lay_promo_refferal);

        viewRefferal = findViewById(R.id.view_prom_refer);
        viewPromotions = findViewById(R.id.view_promotions);
        details = getIntent();

        try
        {
            // If Directly comes from end trip page(OngoingAct)
            if (details.getStringExtra("from").equalsIgnoreCase("direct"))
            {
                message = details.getStringExtra("message");
                setFareCalculatorScreen();
            }
            // If comes from Pending bookings(JobsAct).
            else
            {
                JSONObject j = new JSONObject();
                j.put("trip_id", SessionSave.getSession("trip_id", this));
                final String Url = "get_trip_detail";
                new getTripDetails(Url, j);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        btn_finalize.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                callurl();
            }
        });

        txtCharges.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                // TODO Auto-generated method stub
                try
                {
                    parkingCharges = txtCharges.getText().toString().trim();
                    double total = 0.00;

                    if (parkingCharges.length() == 0)
                    {
                        parCharg = 0.00;
                        total = Double.parseDouble(totalFare) + otherCharg + parCharg;
                        txtTotalCharge.setText("" + total);
                    }
                    else
                    {
                        parCharg = Double.parseDouble(parkingCharges);
                        total = parCharg + Double.parseDouble(totalFare) + otherCharg;
                        txtTotalCharge.setText("" + total);
                    }
                }
                catch (Exception e)
                {
                    txtTotalCharge.setText("" + totalFare);
                    e.printStackTrace();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                // TODO Auto-generated method stub
            }
        });

        txtOtherCharges.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                // TODO Auto-generated method stub
                try
                {
                    Log.e("Other Charges", "Parking Charges: " + parCharg);
                    OtherCharges = txtOtherCharges.getText().toString().trim();
                    double total = 0.00;

                    if (OtherCharges.length() == 0)
                    {
                        otherCharg = 0.00;
                        total = Double.parseDouble(totalFare) + otherCharg + parCharg;
                        txtTotalCharge.setText("" + total);
                    }
                    else
                    {
                        otherCharg = Double.parseDouble(OtherCharges);
                        total = otherCharg + Double.parseDouble(totalFare) + parCharg;
                        txtTotalCharge.setText("" + total);
                    }
                }
                catch (Exception e)
                {
                    txtTotalCharge.setText("" + totalFare);
                    e.printStackTrace();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                // TODO Auto-generated method stub
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        Toast.makeText(getApplicationContext(), "Please complete the trip", Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("SdCardPath")
    private void setFareCalculatorScreen()
    {
        if (details != null)
        {
            try
            {
                JSONObject obj = new JSONObject(message);
                JSONObject json = obj.getJSONObject("detail");

                f_tripid = json.getString("trip_id");
                f_distance = json.getString("distance");
                startTime = json.getString("pickup_time");
                endTime = json.getString("drop_time");
                tripType = json.getString("trip_type");
                totalFare = json.getString("total_fare");
                Fare = json.getString("fare");
                totaltripcost = json.getString("total_tripcost");
                passengerdiscount = json.getString("passenger_discount");
                totalFareAPI = json.getString("total_fare");
                hourCharges = json.getString("trip_fare");
                displayTime = json.getString("display_time");
                f_minutes_traveled = json.getString("minutes_traveled");
                calculateTime = json.getString("calculation_time");
                f_minutes_fare = json.getString("minutes_fare");
                otTime = json.getString("ot_time");
                promotions = json.getString("promodiscount_amount");
                FareType = json.getString("faretype");
                promotioReferral = json.getString("referdiscount");
                taxAmount = json.getString("tax_amount");
                taxPercent = json.getString("tax_percentage");
                overTimeCharge = json.getString("overtime_cost");
                waitingTimeCost = json.getString("waiting_cost");
                nightFare = json.getString("nightfare");
                nightFareApplicable = json.getString("nightfare_applicable");
                waitingTime = json.getString("waiting_time");
                convencCharge = json.getString("convenience_charge");
                wallet_Payment = json.getString("wallet_payment");
                promoDiscount = json.getString("promo_discount");
                layReferralPromotions.setVisibility(View.GONE);
                viewRefferal.setVisibility(View.GONE);
                viewPromotions.setVisibility(View.GONE);
                layPromotions.setVisibility(View.GONE);

                if (promotions.length() != 0)
                {
                    viewPromotions.setVisibility(View.VISIBLE);
                    layPromotions.setVisibility(View.VISIBLE);
                }
                else if (promotioReferral.length() != 0)
                {
                    viewRefferal.setVisibility(View.VISIBLE);
                    layReferralPromotions.setVisibility(View.VISIBLE);
                }

                txtPackageType.setText("" + tripType);
                txtStartTime.setText("" + startTime);
                txtEndTime.setText("" + endTime);
                txtHourCharges.setText("" + hourCharges);
                txtOTCharges.setText("" + displayTime + " / " + otTime);
                txtPromotion.setText("" + promotions);
                txtTotalCharge.setText("" + totalFare);

                txtTax.setText("" + getResources().getString(R.string.tax) + "" + taxPercent + "" + getResources().getString(R.string.tax_percent));
                txtTaxAmount.setText("" + taxAmount);
                txtReferralAmount.setText("" + promotioReferral);

                if (FareType.equalsIgnoreCase("1"))
                {
                    ModeOffPay.setText("Cash");
                }
                else if (FareType.equalsIgnoreCase("2"))
                {
                    ModeOffPay.setText("Paytm");
                }
                else if (FareType.equalsIgnoreCase("3"))
                {
                    ModeOffPay.setText("Wallet");
                }
                else
                {
                    ModeOffPay.setText("Cash");
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onResume()
    {
        // TODO Auto-generated method stub
        super.onResume();
    }

    @Override
    protected void onDestroy()
    {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    @Override
    protected void onPause()
    {
        // TODO Auto-generated method stub
        super.onPause();
    }

    @Override
    protected void onStop()
    {
        // TODO Auto-generated method stub
        super.onStop();
    }


    private void callurl()
    {
        String url = "tripfare_update";

        String otherCharges;
        String parkingCharges;

        if (txtCharges.getText().toString().equalsIgnoreCase("") || txtCharges.getText().toString().equalsIgnoreCase("0.00"))
        {
            parkingCharges = "0.00";
        }
        else
        {
            parkingCharges = txtCharges.getText().toString().trim();
        }

        if (txtOtherCharges.getText().toString().equalsIgnoreCase("") || txtOtherCharges.getText().toString().equalsIgnoreCase("0.00"))
        {
            otherCharges = "0.00";
        }
        else
        {
            otherCharges = txtOtherCharges.getText().toString().trim();
        }

        try
        {
            JSONObject j = new JSONObject();
            j.put("trip_id", f_tripid);
            j.put("distance", f_distance);
            j.put("actual_amount", "" + totalFareAPI);
            j.put("trip_fare", "" + hourCharges);
            j.put("fare", "" + totalFareAPI);
            j.put("passenger_promo_discount", "" + promotions);
            j.put("tax_amount", "" + taxAmount);
            j.put("faretype", FareType);
            j.put("referdiscount", "" + promotioReferral);
            j.put("wallet_payment", "" + wallet_Payment);
            j.put("promo_discount", "" + promoDiscount);
            j.put("nightfare_applicable", "" + nightFareApplicable);
            j.put("nightfare", "" + nightFare);
            j.put("waiting_time", "" + waitingTime);
            j.put("waiting_cost", "" + waitingTimeCost);
            j.put("minutes_traveled", f_minutes_traveled);
            j.put("minutes_fare", calculateTime);
            j.put("pay_mod_id", f_paymodid);
            j.put("other_fee", otherCharges);
            j.put("parking_fee", parkingCharges);
            j.put("convenience_charge", "" + convencCharge);
            j.put("overtime_charge", "" + overTimeCharge);
            j.put("passenger_discount", "" + promotions);
            j.put("ot_time", "" + otTime);

            Log.e("input", j.toString());
            new FareUpdate(url, j);
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    // This class helps to call the Fare Update API,get the result and parse it.
    private class FareUpdate implements APIResult
    {
        String msg = "";

        private FareUpdate(String url, JSONObject data)
        {
            if (isOnline())
            {
                new APIService_Volley_JSON(BillFareAct.this, this, data, false).execute(url);
            }
            else
            {
                alert_view(BillFareAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_internet), "" + getResources().getString(R.string.ok), "");
            }
        }

        @Override
        public void getResult(boolean isSuccess, String result)
        {
            if (isSuccess)
            {
                try
                {
                    JSONObject json = new JSONObject(result);

                    if (json.has("status"))
                    {
                        if (json.getInt("status") == 1)
                        {
                            SessionSave.saveSession("travel_status", "", BillFareAct.this);
                            SessionSave.saveSession("trip_id", "", BillFareAct.this);
                            SessionSave.saveSession("status", "F", BillFareAct.this);
                            MainActivity.mMyStatus.setdistance("");
                            msg = json.getString("message");
                            MainActivity.mMyStatus.setOnstatus("");
                            MainActivity.mMyStatus.setStatus("F");
                            SessionSave.saveSession("status", "F", BillFareAct.this);
                            MainActivity.mMyStatus.setOnPassengerImage("");
                            MainActivity.mMyStatus.setOnstatus("On");
                            MainActivity.mMyStatus.setOnstatus("Completed");
                            MainActivity.mMyStatus.setOnpassengerName("");
                            MainActivity.mMyStatus.setOndropLocation("");
                            MainActivity.mMyStatus.setOndropLocation("");
                            MainActivity.mMyStatus.setOnpickupLatitude("");
                            MainActivity.mMyStatus.setOnpickupLongitude("");
                            MainActivity.mMyStatus.setOndropLatitude("");
                            MainActivity.mMyStatus.setOndropLongitude("");
                            JSONObject jsonDriver = json.getJSONObject("driver_statistics");
                            SessionSave.saveSession("driver_statistics", "" + jsonDriver, BillFareAct.this);
                            CommonData.hstravel_km = "";
                            SessionSave.saveSession("waitingHr", "", BillFareAct.this);
                            Intent jobintent = new Intent(BillFareAct.this, JobdoneAct.class);
                            Bundle bun = new Bundle();
                            bun.putString("message", result);
                            jobintent.putExtras(bun);
                            startActivity(jobintent);
                            finish();
                        }
                        else if (json.getInt("status") == 11)
                        {
                            SessionSave.saveSession("travel_status", "", BillFareAct.this);
                            SessionSave.saveSession("trip_id", "", BillFareAct.this);
                            SessionSave.saveSession("status", "F", BillFareAct.this);
                            MainActivity.mMyStatus.setdistance("");
                            msg = json.getString("message");
                            MainActivity.mMyStatus.setOnstatus("");
                            MainActivity.mMyStatus.setStatus("F");
                            SessionSave.saveSession("status", "F", BillFareAct.this);
                            MainActivity.mMyStatus.setOnPassengerImage("");
                            MainActivity.mMyStatus.setOnstatus("On");
                            MainActivity.mMyStatus.setOnstatus("Completed");
                            MainActivity.mMyStatus.setOnpassengerName("");
                            MainActivity.mMyStatus.setOndropLocation("");
                            MainActivity.mMyStatus.setOndropLocation("");
                            MainActivity.mMyStatus.setOnpickupLatitude("");
                            MainActivity.mMyStatus.setOnpickupLongitude("");
                            MainActivity.mMyStatus.setOndropLatitude("");
                            MainActivity.mMyStatus.setOndropLongitude("");
                            CommonData.hstravel_km = "";
                            SessionSave.saveSession("waitingHr", "", BillFareAct.this);
                            Intent jobintent = new Intent(BillFareAct.this, JobdoneAct.class);
                            Bundle bun = new Bundle();
                            bun.putString("message", result);
                            jobintent.putExtras(bun);
                            startActivity(jobintent);
                            finish();
                        }
                        else if (json.getInt("status") == -9)
                        {
                            msg = json.getString("message");
                            alert_view(BillFareAct.this, "" + getResources().getString(R.string.message), "" + msg, "" + getResources().getString(R.string.ok), "");
                        }
                        else if (json.getInt("status") == 0)
                        {
                            msg = json.getString("message");
                            alert_view(BillFareAct.this, "" + getResources().getString(R.string.message), "" + msg, "" + getResources().getString(R.string.ok), "");
                        }
                        else if (json.getInt("status") == -1)
                        {
                            msg = json.getString("message");
                            alert_view(BillFareAct.this, "" + getResources().getString(R.string.message), "" + msg, "" + getResources().getString(R.string.ok), "");
                            SessionSave.saveSession("status", "F", BillFareAct.this);
                            SessionSave.saveSession("travel_status", "", BillFareAct.this);

                            MainActivity.mMyStatus.setOnstatus("");
                            MainActivity.mMyStatus.setStatus("F");
                            SessionSave.saveSession("status", "F", BillFareAct.this);
                            MainActivity.mMyStatus.setOnPassengerImage("");
                            MainActivity.mMyStatus.setOnstatus("On");
                            MainActivity.mMyStatus.setOnstatus("Completed");
                            MainActivity.mMyStatus.setOnpassengerName("");
                            MainActivity.mMyStatus.setOndropLocation("");
                            MainActivity.mMyStatus.setOndropLocation("");
                            MainActivity.mMyStatus.setOnpickupLatitude("");
                            MainActivity.mMyStatus.setOnpickupLongitude("");
                            MainActivity.mMyStatus.setOndropLatitude("");
                            MainActivity.mMyStatus.setOndropLongitude("");
                            MainActivity.mMyStatus.setOndriverLatitude("");
                            MainActivity.mMyStatus.setOndriverLongitude("");

                            if (msg.equalsIgnoreCase("Trip Fare Already Updated"))
                            {
                                Intent jobintent = new Intent(BillFareAct.this, JobdoneAct.class);
                                Bundle bun = new Bundle();
                                bun.putString("message", result);
                                jobintent.putExtras(bun);
                                startActivity(jobintent);
                                finish();
                            }
                        }
                        else
                        {
                            msg = json.getString("message");
                            alert_view(BillFareAct.this, "" + getResources().getString(R.string.message), "" + msg, "" + getResources().getString(R.string.ok), "");
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.e("ERROR : ", e.toString());
                    e.printStackTrace();
                }
            }
            else
            {
                alert_view(BillFareAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_internet), "" + getResources().getString(R.string.ok), "");
            }
        }
    }

    // This class is used to request and process the response for both add and edit card API
    private class getTripDetails implements APIResult
    {
        private getTripDetails(String url, JSONObject data)
        {
            // TODO Auto-generated constructor stub
            new APIService_Volley_JSON(BillFareAct.this, this, data, false).execute(url);
        }

        @Override
        public void getResult(boolean isSuccess, String result)
        {
            // TODO Auto-generated method stub
            if (isSuccess)
            {
                try
                {
                    JSONObject jsonData = new JSONObject(result);

                    if (jsonData.getInt("status") == 1)
                    {
                        JSONObject json = jsonData.getJSONObject("detail");

                        if (MystatusData.getIsZoomCar().equalsIgnoreCase("1"))
                        {
                            try
                            {
                                JSONObject js = new JSONObject();
                                js.put("app_type", "driver");
                                js.put("lat", SessionSave.getSession("latitude", BillFareAct.this));
                                js.put("lng", SessionSave.getSession("longitude", BillFareAct.this));
                                JSONObject j = new JSONObject();
                                j.put("auth_key", MystatusData.getmAuthKey());
                                j.put("booking_key", MystatusData.getBookingKey());
                                j.put("vendor_reference_key", MystatusData.gettripId());
                                j.put("trip_info", js);
                                String url = CommonData.ZooCar_BaseAPI + "checklist_request";
                                new ZoomCar(url, j);
                            }
                            catch (Exception e)
                            {
                                Log.e("Error", e.toString());
                                e.printStackTrace();
                            }
                        }

                        f_tripid = json.getString("trip_id");
                        f_distance = json.getString("distance");
                        startTime = json.getString("pickup_time");
                        endTime = json.getString("drop_time");
                        tripType = json.getString("trip_plan");
                        totalFare = json.getString("amt");
                        passengerdiscount = json.getString("passenger_discount");
                        totalFareAPI = json.getString("amt");
                        hourCharges = json.getString("amt");
                        displayTime = json.getString("waiting_time");
                        f_minutes_traveled = json.getString("trip_duration");
                        calculateTime = json.getString("trip_duration");
                        f_minutes_fare = json.getString("trip_duration");
                        otTime = json.getString("ot_hours");
                        promotions = json.getString("promo_discount");
                        FareType = json.getString("faretype");
                        promotioReferral = json.getString("refferal_discount");
                        taxAmount = json.getString("tax_amount");
                        taxPercent = json.getString("tax_percentage");
                        overTimeCharge = json.getString("overtime_charge");
                        waitingTimeCost = json.getString("other_charge");
                        nightFare = json.getString("other_charge");
                        waitingTime = json.getString("waiting_time");
                        convencCharge = json.getString("convenience_charge");
                        wallet_Payment = json.getString("wallet_payment");
                        promoDiscount = json.getString("promo_discount");
                        layReferralPromotions.setVisibility(View.GONE);
                        viewRefferal.setVisibility(View.GONE);
                        viewPromotions.setVisibility(View.GONE);
                        layPromotions.setVisibility(View.GONE);

                        if (promotions.length() != 0)
                        {
                            viewPromotions.setVisibility(View.VISIBLE);
                            layPromotions.setVisibility(View.VISIBLE);
                        }
                        else if (promotioReferral.length() != 0)
                        {
                            viewRefferal.setVisibility(View.VISIBLE);
                            layReferralPromotions.setVisibility(View.VISIBLE);
                        }

                        txtPackageType.setText("" + tripType);
                        txtStartTime.setText("" + startTime);
                        txtEndTime.setText("" + endTime);
                        txtHourCharges.setText("" + hourCharges);
                        txtOTCharges.setText("" + displayTime + " / " + otTime);
                        txtPromotion.setText("" + promotions);
                        txtTotalCharge.setText("" + totalFare);

                        txtTax.setText("" + getResources().getString(R.string.tax) + "" + taxPercent + "" + getResources().getString(R.string.tax_percent));
                        txtTaxAmount.setText("" + taxAmount);
                        txtReferralAmount.setText("" + promotioReferral);

                        if (FareType.equalsIgnoreCase("1"))
                        {
                            ModeOffPay.setText("Cash");
                        }
                        else if (FareType.equalsIgnoreCase("2"))
                        {
                            ModeOffPay.setText("Paytm");
                        }
                        else if (FareType.equalsIgnoreCase("3"))
                        {
                            ModeOffPay.setText("Wallet");
                        }
                        else
                        {
                            ModeOffPay.setText("Cash");
                        }
                    }
                    else if (jsonData.getInt("status") == -1)
                    {
                        //User has been logged out or invalid user
                        askForLogin();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    private class ZoomCar implements APIResult
    {
        ZoomCar(String url, JSONObject data)
        {
            try
            {
                if (isOnline())
                {
                    new APIServiceVolley(BillFareAct.this, this, data, false, url).execute();
                }
                else
                {
                    alert_view(BillFareAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
                }
            }
            catch (Exception e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        @Override
        public void getResult(boolean isSuccess, String result)
        {
            try
            {
                if (isSuccess)
                {
                    if (result != null && result.length() > 0)
                    {
                        JSONObject data = new JSONObject(result);

                        if (data.getString("status").equalsIgnoreCase("success"))
                        {
                            String checklist1 = data.getString("checklist_1");
                            String checklist2 = data.getString("checklist_2");
                            String otp = data.getString("otp");
                            MystatusData.setmCheckList1(checklist1);
                            MystatusData.setmCheckList2(checklist2);
                            MystatusData.setmCheckListOTP(otp);
                            Intent in = new Intent(BillFareAct.this, ZoomCarAct.class);
                            in.putExtra("CheckList1", checklist1);
                            in.putExtra("CheckList2", checklist2);
                            in.putExtra("otp", otp);
                            in.putExtra("status", "2");
                            startActivity(in);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}
