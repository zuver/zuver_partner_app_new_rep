package com.zuver.driver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.zuver.driver.data.CommonData;
import com.zuver.driver.data.MapWrapperLayout;
import com.zuver.driver.data.MenuSlider;
import com.zuver.driver.data.MystatusData;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.route.Route;
import com.zuver.driver.service.APIServiceVolley;
import com.zuver.driver.service.APIService_Volley_JSON;
import com.zuver.driver.service.LocationUpdate;
import com.zuver.driver.service.NonActivity;
import com.zuver.driver.service.Timer1;
import com.zuver.driver.slidemenu.SlidingMenu;
import com.zuver.driver.utils.FontHelper;
import com.zuver.driver.utils.LocationDb;
import com.zuver.driver.utils.SessionSave;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

@SuppressLint("DefaultLocale")
public class OngoingAct extends MainActivity implements LocationListener, ConnectionCallbacks, OnConnectionFailedListener
{
    // Class members declarations.
    String p_name = "";
    private String p_phone = "";
    private LocationRequest mLocationRequest;
    public GoogleMap map;
    private Marker c_marker, p_marker, d_marker;
    private double latitude1 = 0.0;
    private double longitude1 = 0.0;
    private Double p_latitude, p_longtitude;
    private Double d_latitude, d_longtitude;
    private Double driver_latitude, driver_longtitude;
    private String waitingTime = "";
    private TextView nodataTxt;
    public static String Address = "";
    private String p_pickloc = "";
    private TextView backup;
    public static TextView HeadTitle;
    public static TextView CancelTxt;
    private RelativeLayout mapsupport_lay;
    public static Activity mFlagger;
    private final Handler myHandler = new Handler();
    private float waitingHr;
    public static Button butt_onboard;
    Route route = null;
    long timeclear = 0L;
    private LatLng pickupLatLng;
    private LatLng dropLatLng;
    private LatLng currentLatLng;
    public static CharSequence sTimer;
    Context context;
    String str, driverETA, carDetails, keyPickUp, tripPlan, landMark;
    public static OngoingAct activity;
    private MenuSlider menu1;
    private SlidingMenu menu;
    private ImageLoader imageLoader;
    private LinearLayout tripchild1;
    float zoom = 17f;
    NonActivity nonactiityobj = new NonActivity();
    public static TextView speedtext, speed_txt, total_km, total_km_val, txtTime;
    private String totalkm = "";
    private String LowSpeed = "";
    public static String speedlimit = "0";
    LinearLayout km_lay, layTime;
    private Bundle alert_bundle = new Bundle();
    private String alert_msg;
    ImageView butt_navigator;
    RelativeLayout botton_layout;
    LocationDb objLocationDb;
    float bearing;
    int layoutheight;
    ImageView driver_image, imgUp;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;

    int calculateTime = 2;
    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 30000; // 30 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 1; // 10 meters
    private MapWrapperLayout mapWrapperLayout;

    private TextView txt_pickup, txt_drop, txtCancel,
            txtKeyPickUp, txtDropMark;

    private LinearLayout layDriverBottom, layDrivNow;
    String tripType, otHours;
    private int otHoursMinute;

    private View viewPickUp;
    private LinearLayout lay_pickup, lay_drop, lay_eta_drop, layCancel,
            layCall, layDriverInfoDetail, layPick;

    private ImageView img_pickup, img_drop, img_call, imgDown;
    private TextView txt_drive_request, txt_eta_time, txt_current_location,
            txt_profile_name, txt_call_number, txtCurrPickUp;

    private ImageView imgPassengerProfile;
    private DisplayImageOptions options;
    private String currTimer1 = "", currTimer2 = "";
    private LinearLayout layKeyPickUp, layCarDetails, layDropMark;

    // Set the layout to activity.
    @Override
    public int setLayout()
    {
        setLocale();
        return R.layout.accept_lay;
    }

    // Initialize the views on layout
    @SuppressLint("DefaultLocale")
    @Override
    public void Initialize()
    {
        CommonData.current_act = "OngoingAct";
        CommonData.sContext = this;

        try
        {
            objLocationDb = new LocationDb(OngoingAct.this);
            alert_bundle = getIntent().getExtras();

            if (alert_bundle != null)
            {
                alert_msg = alert_bundle.getString("alert_message");
            }

            if (alert_msg != null && alert_msg.length() != 0)
                alert_view(OngoingAct.this,
                        "" + getResources().getString(R.string.message), ""
                                + alert_msg,
                        "" + getResources().getString(R.string.ok), "");
            if (!SessionSave.getSession("trip_id", OngoingAct.this).equals(""))
            {
                Log.e("Init Trip Call ", "Init Trip Call");
                JSONObject j = new JSONObject();
                j.put("trip_id", SessionSave.getSession("trip_id", OngoingAct.this));
                final String Url = "get_trip_detail";
                new Tripdetails(Url, j);
                nonactiityobj.startServicefromNonActivity(OngoingAct.this);
            }
            else
            {
                alert_view(OngoingAct.this, "" + getResources().getString(R.string.message), "You don't have any trip",
                        "" + getResources().getString(R.string.ok), "");
                startActivity(new Intent(OngoingAct.this, DashAct.class));
                CommonData.Animation1(OngoingAct.this);
            }
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }

        if (servicesConnected())
        {
            // Building the GoogleApi client
            buildGoogleApiClient();
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(UPDATE_INTERVAL);
            mLocationRequest.setFastestInterval(FATEST_INTERVAL);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
        }

        FontHelper.applyFont(this, findViewById(R.id.ongoing_lay), "DroidSans.ttf");
        activity = this;
        mFlagger = this;
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        menu1 = new MenuSlider(this, menu);
        menu = menu1.slidemenu(this);
        HeadTitle = (TextView) findViewById(R.id.headerTxt);
        CancelTxt = (TextView) findViewById(R.id.cancelTxt);
        nodataTxt = (TextView) findViewById(R.id.nodataTxt);
        butt_onboard = (Button) findViewById(R.id.butt_onboard);
        HeadTitle.setText(" " + getResources().getString(R.string.app_name));
        txtTime = (TextView) findViewById(R.id.txt_time);

        speedtext = (TextView) findViewById(R.id.km_timer_txt);
        speed_txt = (TextView) findViewById(R.id.km_txt);
        km_lay = (LinearLayout) findViewById(R.id.km_lay);

        layTime = (LinearLayout) findViewById(R.id.lay_time);
        total_km = (TextView) findViewById(R.id.total_km);
        total_km_val = (TextView) findViewById(R.id.total_km_val);
        backup = (TextView) findViewById(R.id.backup);
        backup.setVisibility(View.GONE);
        tripchild1 = (LinearLayout) findViewById(R.id.tripchild1);
        butt_onboard.setVisibility(View.VISIBLE);
        mapsupport_lay = (RelativeLayout) findViewById(R.id.mapsupport_lay);
        butt_navigator = (ImageView) findViewById(R.id.butt_navigator);
        botton_layout = (RelativeLayout) findViewById(R.id.botton_layout);
        layDrivNow = (LinearLayout) findViewById(R.id.driv_now_lay);

        layCancel = (LinearLayout) findViewById(R.id.lay_cancel);
        lay_pickup = (LinearLayout) findViewById(R.id.lay_pickup);
        lay_drop = (LinearLayout) findViewById(R.id.lay_drop);
        lay_eta_drop = (LinearLayout) findViewById(R.id.eta_lay);

        layDriverInfoDetail = (LinearLayout) findViewById(R.id.driver_info_detail);
        layPick = (LinearLayout) findViewById(R.id.lay_pick);
        viewPickUp = findViewById(R.id.view_pickup);

        txt_call_number = (TextView) findViewById(R.id.txt_call_number);
        txt_profile_name = (TextView) findViewById(R.id.txt_profile_name);
        txt_drive_request = (TextView) findViewById(R.id.txt_drive_request);
        txt_eta_time = (TextView) findViewById(R.id.txt_eta_time);
        txt_eta_time.setVisibility(View.GONE);
        txt_current_location = (TextView) findViewById(R.id.txt_current_location);
        txt_pickup = (TextView) findViewById(R.id.txt_pickup);
        txt_drop = (TextView) findViewById(R.id.txt_drop);
        txtCancel = (TextView) findViewById(R.id.txt_cancel);

        txtCurrPickUp = (TextView) findViewById(R.id.txt_curr_pick);

        img_pickup = (ImageView) findViewById(R.id.img_pickup);
        img_drop = (ImageView) findViewById(R.id.img_drop);
        img_call = (ImageView) findViewById(R.id.img_call);
        imgDown = (ImageView) findViewById(R.id.img_down);
        imgUp = (ImageView) findViewById(R.id.img_up);
        txtDropMark = (TextView) findViewById(R.id.txt_landmark);
        txtKeyPickUp = (TextView) findViewById(R.id.txt_key_pickup);
        // driver_image = (ImageView) findViewById(R.id.dash_image);

        layCall = (LinearLayout) findViewById(R.id.lay_call);

        layDriverBottom = (LinearLayout) findViewById(R.id.driver_top_bottom);


        layKeyPickUp = (LinearLayout) findViewById(R.id.lay_key_pickup);
        layCarDetails = (LinearLayout) findViewById(R.id.lay_car_details);
        layDropMark = (LinearLayout) findViewById(R.id.lay_land_mark);
        imgPassengerProfile = (ImageView) findViewById(R.id.img_pass_profile);

        options = new DisplayImageOptions.Builder().showImageOnLoading(null) // resource
                // or
                // drawable
                .showImageForEmptyUri(null) // resource or drawable
                .showImageOnFail(null) // resource or drawable
                .resetViewBeforeLoading(false) // default
                .delayBeforeLoading(1000).cacheInMemory(true) // default
                .cacheOnDisc(true) // default
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED) // default
                .bitmapConfig(Bitmap.Config.RGB_565) // default
                .displayer(new SimpleBitmapDisplayer()) // default
                .handler(new Handler()) // default
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).defaultDisplayImageOptions(options).build();
        ImageLoader.getInstance().init(config);

        String Image_path = SessionSave.getSession("p_image", OngoingAct.this);

        if (Image_path != null && Image_path.length() > 0)
        {
            Log.e("Picasso Profile Image", SessionSave.getSession("p_image", OngoingAct.this));
            ImageLoader.getInstance().displayImage(Image_path, imgPassengerProfile, options);
        }

        txtCancel.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                if (Build.VERSION.SDK_INT >= 23)
                {
                    if (ActivityCompat.checkSelfPermission(OngoingAct.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
                    {
                        requestPermission(OngoingAct.this, android.Manifest.permission.CALL_PHONE);
                    }
                    else
                    {
                        CallCustomerCare();
                    }
                }
                else
                {
                    CallCustomerCare();
                }
            }
        });

        layKeyPickUp.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub

                if (txtKeyPickUp.getText().toString().equalsIgnoreCase(getResources().getString(R.string.no_data)) || txtKeyPickUp.getText().toString().equalsIgnoreCase(""))
                {
                    return;
                }
                else
                {
                    alert_view(OngoingAct.this, getResources().getString(R.string.key_pickup_location), txtKeyPickUp.getText().toString(), getResources().getString(R.string.ok), null);
                }
            }
        });

        layCarDetails.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (txtCurrPickUp.getText().toString().equalsIgnoreCase(getResources().getString(R.string.no_data)) || txtCurrPickUp.getText().toString().equalsIgnoreCase(""))
                {
                    return;
                }
                else
                {
                    alert_view(OngoingAct.this, getResources().getString(R.string.t_pickloc), txtCurrPickUp.getText().toString(), getResources().getString(R.string.ok), null);
                }
            }
        });

        layDropMark.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub

                if (txtDropMark.getText().toString().equalsIgnoreCase(getResources().getString(R.string.no_data)) || txtDropMark.getText().toString().equalsIgnoreCase(""))
                {
                    return;
                }
                else
                {
                    alert_view(OngoingAct.this, getResources().getString(R.string.notes), txtDropMark.getText().toString(), getResources().getString(R.string.ok), null);
                }
            }
        });

        imgDown.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                layDrivNow.setVisibility(View.GONE);
                layDriverBottom.setVisibility(View.GONE);
                imgUp.setVisibility(View.VISIBLE);
                imgDown.setVisibility(View.GONE);
            }
        });

        imgUp.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                layDriverBottom.setVisibility(View.VISIBLE);
                layDrivNow.setVisibility(View.VISIBLE);
                imgUp.setVisibility(View.GONE);
                imgDown.setVisibility(View.VISIBLE);
            }
        });

        // Following set of code to initialize and google map.
        try
        {
            final int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(OngoingAct.this);

            if (resultCode == ConnectionResult.SUCCESS)
            {
                new Getimg().execute();
                final SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                map = mapFrag.getMap();
                System.gc();
                MapsInitializer.initialize(OngoingAct.this);
                mapWrapperLayout = (MapWrapperLayout) findViewById(R.id.map_relative_layout);
                mapWrapperLayout.init(map, getPixelsFromDp(this, 39 + 20));
                map.getUiSettings().setZoomControlsEnabled(false);
                map.getUiSettings().setCompassEnabled(true);
                map.getUiSettings().setMyLocationButtonEnabled(false);
                map.setMyLocationEnabled(true);
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mapsupport_lay.setVisibility(View.VISIBLE);
                nodataTxt.setVisibility(View.GONE);
                System.err.println("animate camera:" + latitude1 + "lng" + longitude1);
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(LocationUpdate.latitude1, LocationUpdate.longitude1), zoom));
            }
            else
            {
                mapsupport_lay.setVisibility(View.GONE);
                nodataTxt.setVisibility(View.VISIBLE);
                nodataTxt.setText(""
                        + ""
                        + getResources().getString(
                        R.string.device_not_support_map));
            }
        }
        catch (final Exception e)
        {
            e.printStackTrace();
        }

        layCall.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                try
                {
                    if (MainActivity.mMyStatus.getpassengerphone().length() == 0)
                        alert_view(OngoingAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.invalid_mobile_number), "" + getResources().getString(R.string.ok), "");
                    else
                    {
                        if (Build.VERSION.SDK_INT >= 23)
                        {
                            if (ActivityCompat.checkSelfPermission(OngoingAct.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
                            {
                                requestPermission(OngoingAct.this, android.Manifest.permission.CALL_PHONE);
                            }
                            else
                            {
                                Intent callIntent = new Intent(Intent.ACTION_CALL);
                                callIntent.setData(Uri.parse("tel:" + MainActivity.mMyStatus.getpassengerphone()));
                                startActivity(callIntent);
                            }
                        }
                        else
                        {
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:" + MainActivity.mMyStatus.getpassengerphone()));
                            startActivity(callIntent);
                        }
                    }
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        });

        // This onclick method used to move from this activity to home activity.
        backup.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(final View v)
            {
                backup.setEnabled(false);
                Intent jobintent = new Intent(OngoingAct.this, HomeActivity.class);
                startActivity(jobintent);
            }
        });

        // This onclick method used to move navigator application with pickup
        // and drop place lat/lng.
        butt_navigator.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                try
                {
                    if (MainActivity.mMyStatus.getOnpickupLatitude() != null
                            && MainActivity.mMyStatus.getOnpickupLongitude() != null
                            && MainActivity.mMyStatus.getOndriverLatitude() != null
                            && MainActivity.mMyStatus.getOndriverLongitude() != null)
                    {
                        if (MainActivity.mMyStatus.getOnpickupLatitude() != "0.0"
                                && MainActivity.mMyStatus.getOnpickupLongitude() != "0.0"
                                && MainActivity.mMyStatus.getOndriverLatitude() != "0.0"
                                && MainActivity.mMyStatus.getOndriverLongitude() != "0.0")
                        {

                            final String locationurl = "http://maps.google.com/maps?saddr="
                                    + latitude1
                                    + ","
                                    + longitude1
                                    + "&daddr="
                                    + MainActivity.mMyStatus
                                    .getOnpickupLatitude()
                                    + ","
                                    + MainActivity.mMyStatus
                                    .getOnpickupLongitude() + "";
                            final Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(locationurl));
                            startActivity(intent);
                        }
                    }
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        });

        // This onclick method used to handle the three state in ongoing trip
        // page(Arrived,Start and End).In each phase will use different API.
        butt_onboard.setOnClickListener(new OnClickListener()
        {
            @SuppressLint("DefaultLocale")
            @Override
            public void onClick(final View v)
            {
                try
                {
//					butt_onboard.setClickable(false);
                    // If the trip in accepted not get arrived
                    if (MainActivity.mMyStatus.getOnstatus().equalsIgnoreCase("On"))
                    {
                        nonactiityobj.stopServicefromNonActivity(OngoingAct.this);
                        JSONObject jDriverArrived = new JSONObject();
                        jDriverArrived.put("trip_id", SessionSave.getSession("trip_id", OngoingAct.this));
                        final String arrived_url = "driver_arrived";
                        new DriverArrived(arrived_url, jDriverArrived);
                    }
                    // If trip in arrived state and going to start the trip
                    else if (MainActivity.mMyStatus.getOnstatus().equalsIgnoreCase("Arrivd"))
                    {
                        // ShowToast(OngoingAct.this, "ARRVD");
                        nonactiityobj.stopServicefromNonActivity(OngoingAct.this);

                        if (latitude1 != 0.0 && longitude1 != 0.0)
                        {
                            JSONObject jstart = new JSONObject();
                            jstart.put("driver_id", SessionSave.getSession("Id", OngoingAct.this));
                            jstart.put("latitude", latitude1);
                            jstart.put("longitude", longitude1);
                            jstart.put("status", "A");
                            jstart.put("trip_id", SessionSave.getSession("trip_id", OngoingAct.this));
                            final String start_trip_url = "driver_start_trip";
                            SessionSave.saveSession("slat", "" + latitude1, OngoingAct.this);
                            SessionSave.saveSession("slng", "" + longitude1, OngoingAct.this);
                            CommonData.last_getlatitude = latitude1;
                            CommonData.last_getlongitude = longitude1;
                            new Onboard(start_trip_url, jstart);
                        }
                        else
                        {
                            JSONObject jstart = new JSONObject();
                            jstart.put("driver_id", SessionSave.getSession("Id", OngoingAct.this));
                            jstart.put("latitude", "");
                            jstart.put("longitude", "");
                            jstart.put("status", "A");
                            jstart.put("trip_id", SessionSave.getSession("trip_id", OngoingAct.this));
                            final String start_trip_url = "driver_start_trip";
                            new Onboard(start_trip_url, jstart);
                        }
                    }
                    // If trip in progress and going to end the trip
                    else if (MainActivity.mMyStatus.getOnstatus().equalsIgnoreCase("Complete"))
                    {
                        try
                        {
                            // ShowToast(OngoingAct.this, "Complete");
                            nonactiityobj.stopServicefromNonActivity(OngoingAct.this);
                            MainActivity.mMyStatus.setOnstatus("Complete");
//							stopService(new Intent(OngoingAct.this, WaitingTimerRun.class));
//							myHandler.removeCallbacks(r);
                            float h = 0.0f;

                            if (waitingTime.equals(""))
                            {
                                waitingTime = "00:00:00";
                            }

                            String[] split = waitingTime.split(":");
                            int hr = Integer.parseInt(split[0]);
                            int min = Integer.parseInt(split[1]);

                            h = (float) min / 60;
                            String str = String.format(Locale.UK, "%.02f", h);
                            Float f = Float.valueOf(str);
                            waitingHr = f + hr;

                            MainActivity.mMyStatus.setDriverWaitingHr(Float.toString(waitingHr));
                            SessionSave.saveSession("waitingHr", Float.toString(waitingHr), OngoingAct.this);
                            SessionSave.saveSession("drop_location", SessionSave.getSession("Driver_locations_home", OngoingAct.this), OngoingAct.this);
                            alertCompleteTrip(OngoingAct.this, getResources().getString(R.string.message), getResources().getString(R.string.end_trip), getResources().getString(R.string.ok), "");

//							final String completeUrl = "complete_trip";
//							new CompleteTrip(completeUrl, latitude1, longitude1);
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                            // TODO: handle exception
                        }
                    }
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        });
    }

    // Get the google map pixels from xml density independent pixel.
    public static int getPixelsFromDp(final Context context, final float dp)
    {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }


    public void CallCustomerCare()
    {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View alert_view = inflater.inflate(R.layout.alertview_call, null);
        //FontHelper.applyFont(OngoingAct.this,alert_view.findViewById(R.id.alert_view), "DroidSans.ttf");
        AlertDialog.Builder builder = new AlertDialog.Builder(OngoingAct.this);
        builder.setView(alert_view);

        final AlertDialog al = builder.create();
        al.setCancelable(true);
        al.show();

        Button call = (Button) alert_view.findViewById(R.id.yes);
        Button cancel = (Button) alert_view.findViewById(R.id.no);

        call.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    al.dismiss();

                    if (Build.VERSION.SDK_INT >= 23)
                    {
                        if (ActivityCompat.checkSelfPermission(OngoingAct.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
                        {
                            requestPermission(OngoingAct.this, android.Manifest.permission.CALL_PHONE);
                        }
                        else
                        {
                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + CommonData.CUSTOMERCARE[1]));
                            startActivity(intent);
                        }
                    }
                    else
                    {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + CommonData.CUSTOMERCARE[1]));
                        startActivity(intent);
                    }
                }
                catch (ActivityNotFoundException e)
                {
                    e.printStackTrace();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                al.dismiss();

            }
        });
    }

    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /// On Start method by default it called when activity is open.
    @Override
    public void onStart()
    {
        super.onStart();

        try
        {
            if (mGoogleApiClient != null)
            {
                mGoogleApiClient.connect();
                /*JSONObject j = new JSONObject();
                j.put("driver_id", SessionSave.getSession("Id", OngoingAct.this));
                Log.d("driver_id", "driver id from session is: " + SessionSave.getSession("Id", OngoingAct.this));
                String url = "driver_current_status";
                new getDriverCurrentStatus(url, j);*/
            }
        }
        catch (Exception e)
        {
            Log.d("Exception", "while getting drivers current status:\n");
            e.printStackTrace();
        }
    }

    //region Added By Amit Jangid

    /**
     * Added by Amit Jangid
     * Getting Current Shift Status of the driver
     * This method checks with the server the login status
     * and shift status using this API : driver_current_status
     **/
    private class getDriverCurrentStatus implements APIResult
    {
        private getDriverCurrentStatus(String url, JSONObject data)
        {
            try
            {
                /// checking is user is connected to internet or not
                if (isOnline())
                {
                    /// user is connected to internet
                    /// making api call using volley
                    new  APIService_Volley_JSON(OngoingAct.this, this, data, false).execute(url);
                }
                else
                {
                    /// user is not connected to internet
                    /// show message to check internet connection
                    alert_view(
                            OngoingAct.this,
                            "" + getResources().getString(R.string.message),
                            ""
                                    + getResources().getString(
                                    R.string.check_net_connection), ""
                                    + getResources().getString(R.string.ok), "");
                }
            }
            catch (Exception e)
            {
                Log.d("Exception", "while getting profile data in dashboard activity:\n");
                e.printStackTrace();
            }
        }

        @Override
        public void getResult(boolean isSuccess, String result)
        {
            try
            {
                if (isSuccess)
                {
                    JSONObject json = new JSONObject(result);

                    if (json.getInt("status") == 1)
                    {
                        JSONObject details = json.getJSONObject("detail");
                        String shift_status = details.getString("dutyStatus");
                        String loginStatus = details.getString("loginStatus");

                        ///Log.d("loginStatus", "login status from server is: " + loginStatus);
                        ///Log.d("shift_status", "shift status from server is: " + shift_status);

                        if (loginStatus.equalsIgnoreCase("1"))
                        {
                            if (shift_status.equalsIgnoreCase("1"))
                            {
                                shift_status = "IN";
                            }
                            else
                            {
                                shift_status = "OUT";
                            }

                            SessionSave.saveSession("shift_status", shift_status, OngoingAct.this);

                            /**
                             * edited by Amit Jangid
                             * checking if  the status of the driver is 'IN'
                             * if shift is 'IN' then show "ON DUTY" on switch
                             * else show 'OFF DUTY'
                             * to update the location of the user
                             * first stopping the update location service
                             * then starting it again to change the time interval
                             **/
                            /*if (SessionSave.getSession("shift_status", DashAct.this).equalsIgnoreCase("IN"))
                            {
                                tvCancel.setText(getResources().getString(R.string.onduty));
                            }
                            else if (SessionSave.getSession("shift_status", DashAct.this).equalsIgnoreCase("OUT"))
                            {
                                tvCancel.setText(getResources().getString(R.string.off_duty));
                            }

                            if (SessionSave.getSession("shift_status", DashAct.this).equals("IN"))
                            {
                                chkShift.setChecked(true);
                                nonactiityobj.stopServicefromNonActivity(DashAct.this);
                                nonactiityobj.startServicefromNonActivity(DashAct.this);
                            }
                            else
                            {
                                chkShift.setChecked(false);
                                nonactiityobj.stopServicefromNonActivity(DashAct.this);
                                nonactiityobj.startServicefromNonActivity(DashAct.this);
                            }*/
                        }
                        else
                        {
                            askForLogin();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.d("Exception", "while getting profile data in dashboard activity:\n");
                e.printStackTrace();
            }
        }
    }

    //endregion Added by Amit Jangid

    /**
     * Starting the location updates
     */
    protected void startLocationUpdates()
    {
        try
        {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates()
    {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    protected void onDestroy()
    {
        // TODO Auto-generated method stub
        super.onDestroy();
        stopLocationUpdates();

        myHandler.removeCallbacks(rTimer1);
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        try
        {
            GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
            final int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(OngoingAct.this);

            if (resultCode == ConnectionResult.SUCCESS)
            {
                // init();
                new Getimg().execute();
                mapsupport_lay.setVisibility(View.VISIBLE);
                nodataTxt.setVisibility(View.GONE);
            }
            else
            {
                mapsupport_lay.setVisibility(View.GONE);
                nodataTxt.setVisibility(View.VISIBLE);
                nodataTxt.setText(getResources().getString(
                        R.string.device_not_support_map));
            }
        }
        catch (final Exception e)
        {
            e.printStackTrace();
        }
    }

    // To get current location as address.
    private void location()
    {
        final Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;

        try
        {
            if (mLastLocation != null)
            {
                if (Geocoder.isPresent())
                {
                    addresses = geocoder.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 1);

                    if (addresses != null && addresses.size() > 0)
                    {
                        final Address address = addresses.get(0);
                        Address = getString(R.string.address_output_string, address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "", address.getLocality(), address.getCountryName());
                        SessionSave.saveSession("drop_location", Address, OngoingAct.this);
                    }
                    else
                        new getDropLocation(OngoingAct.this).execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                }
                else
                {
                    new getDropLocation(OngoingAct.this).execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                }
            }
        }
        catch (final IOException e)
        {
            e.printStackTrace();
        }
        catch (final IllegalArgumentException e)
        {
            e.printStackTrace();
        }

    }


    // To get pickup/drip location as address and place the pickup/drop markers
    // on map.
    private class GetPickdropLoc extends AsyncTask<Void, Void, Void>
    {
        private LatLng HAMBURG;

        @Override
        protected Void doInBackground(final Void... params)
        {
            // TODO Auto-generated method stub
            try
            {
                location();
            }
            catch (final Exception e)
            {
                // TODO: handle exception
            }

            return null;
        }

        @Override
        protected void onPostExecute(final Void result)
        {
            super.onPostExecute(result);
            try
            {
                map.clear();
                System.err.println("mLastLocation" + mLastLocation);

                if (mLastLocation != null)
                {
                    latitude1 = mLastLocation.getLatitude();
                    longitude1 = mLastLocation.getLongitude();
                    bearing = mLastLocation.getBearing();
                    currentLatLng = new LatLng(latitude1, longitude1);
                }

                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                HAMBURG = new LatLng(latitude1, longitude1);

                if (c_marker != null)
                    c_marker.remove();

                c_marker = map.addMarker(new MarkerOptions()
                        .position(HAMBURG)
                        .title(Address)
                        .flat(true)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.driver_img2))
                        .draggable(true));

                bearing = 0;
                route = new Route();

                if (p_latitude != null && p_latitude != 0.0 && p_longtitude != null && p_longtitude != 0.0)
                {
                    p_marker = map.addMarker(new MarkerOptions()
                            .position(new LatLng(p_latitude, p_longtitude))
                            .title("" + getResources().getString(
                                    R.string.pickuploc))
                            .flat(true)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.o_marker_pick))
                            .draggable(true));
                    pickupLatLng = new LatLng(p_latitude, p_longtitude);
                }
                if (d_latitude != null && d_latitude != 0.0 && d_longtitude != null && d_longtitude != 0.0)
                {
                    d_marker = map.addMarker(new MarkerOptions()
                            .position(new LatLng(d_latitude, d_longtitude))
                            .flat(true)
                            .title("" + getResources()
                                    .getString(R.string.droploc))
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.o_marker_drop))
                            .draggable(true));
                    dropLatLng = new LatLng(d_latitude, d_longtitude);
                }

                if (driver_latitude != null && driver_latitude != 0.0
                        && driver_longtitude != null
                        && driver_longtitude != 0.0)
                {
                    currentLatLng = new LatLng(driver_latitude, driver_longtitude);
                }

                mHandler.sendEmptyMessage(1);

                if (!Address.equals(""))
                {
                    SessionSave.saveSession("drop_location", Address, OngoingAct.this);
                    MainActivity.mMyStatus.setOndropLocation(Address);
                }
            }
            catch (final Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    // Timer for total time calculation
    private final Runnable rTimer1 = new Runnable()
    {
        @Override
        public void run()
        {
            Log.e("Timer 1", Timer1.sTimer1 + " : " + tripType);

            if (!CommonData.timer1_stop)
            {
                txtTime.setText(Timer1.sTimer1);
            }

            myHandler.postDelayed(rTimer1, 1000);
        }
    };


    private class ZoomCar implements APIResult
    {
        ZoomCar(String url, JSONObject data)
        {
            try
            {
                if (isOnline())
                {
                    new APIServiceVolley(OngoingAct.this, this, data, false, url).execute();
                }
                else
                {
                    alert_view(OngoingAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
                }
            }
            catch (Exception e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        @Override
        public void getResult(boolean isSuccess, String result)
        {
            try
            {
                if (isSuccess)
                {
                    if (result != null && result.length() > 0)
                    {
                        JSONObject data = new JSONObject(result);

                        if (data.getString("status").equalsIgnoreCase("success"))
                        {
                            String checklist1 = data.getString("checklist_1");
                            String checklist2 = data.getString("checklist_2");
                            String otp = data.getString("otp");
                            MystatusData.setmCheckList1(checklist1);
                            MystatusData.setmCheckList2(checklist2);
                            MystatusData.setmCheckListOTP(otp);
                            Intent in = new Intent(OngoingAct.this, ZoomCarAct.class);
                            in.putExtra("CheckList1", checklist1);
                            in.putExtra("CheckList2", checklist2);
                            in.putExtra("otp", otp);
                            in.putExtra("status", "1");
                            startActivity(in);
                        }
                        else
                        {
                            alert_view(OngoingAct.this, "Zoom Car", data.getString("message"), "OK", "");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * Used to call the driver arrived Api and parse the response
     */
    private class DriverArrived implements APIResult
    {
        private DriverArrived(final String url, JSONObject data)
        {
            try
            {
                if (isOnline())
                {
                    new APIService_Volley_JSON(OngoingAct.this, this, data, false).execute(url);
                }
                else
                {
                    alert_view(OngoingAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(
                            R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
                }
            }
            catch (Exception e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        /**
         * Parse the response and update the UI.
         */
        @Override
        public void getResult(final boolean isSuccess, final String result)
        {
            try
            {
                if (isSuccess)
                {
                    butt_onboard.setClickable(true);
                    final JSONObject json = new JSONObject(result);

                    if (json.getInt("status") == 1)
                    {
                        if (MystatusData.getIsZoomCar().equalsIgnoreCase("1"))
                        {
                            try
                            {
                                JSONObject js = new JSONObject();
                                js.put("app_type", "driver");
                                js.put("lat", SessionSave.getSession("latitude", OngoingAct.this));
                                js.put("lng", SessionSave.getSession("longitude", OngoingAct.this));

                                JSONObject j = new JSONObject();
                                j.put("auth_key", MystatusData.getmAuthKey());
                                j.put("booking_key", MystatusData.getBookingKey());
                                j.put("vendor_reference_key", MystatusData.gettripId());
                                j.put("trip_info", js);
                                String url = CommonData.ZooCar_BaseAPI + "checklist_request";
                                new ZoomCar(url, j);
                            }
                            catch (Exception e)
                            {
                                Log.e("Error", "in DriverArrived class:\n");
                                e.printStackTrace();
                            }
                        }

                        butt_navigator.setVisibility(View.GONE);
                        layDriverInfoDetail.setVisibility(View.VISIBLE);
                        viewPickUp.setVisibility(View.VISIBLE);
                        layPick.setVisibility(View.VISIBLE);
                        layCancel.setVisibility(View.GONE);
                        SessionSave.saveSession("Ongoing", "ongoing", OngoingAct.this);
                        MainActivity.mMyStatus.setOnstatus("Arrivd");
                        HeadTitle.setText("" + getResources().getString(R.string.arrived));
                        HeadTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        butt_onboard.setText("" + getResources().getString(R.string.pass_onboard).toUpperCase());
                        txt_eta_time.setVisibility(View.GONE);
                        SessionSave.saveSession("status", "B", OngoingAct.this);
                        nonactiityobj.startServicefromNonActivity(OngoingAct.this);
                        CancelTxt.setText("00:00:00");
                    }
                    else if (json.getInt("status") == -1)
                    {
                        SessionSave.saveSession("status", "F", OngoingAct.this);
                        SessionSave.saveSession("travel_status", "", OngoingAct.this);
                        MainActivity.mMyStatus.settripId("");
                        SessionSave.saveSession("trip_id", "", OngoingAct.this);
                        MainActivity.mMyStatus.setOnstatus("On");
                        MainActivity.mMyStatus.setOnPassengerImage("");
                        MainActivity.mMyStatus.setOnpassengerName("");
                        MainActivity.mMyStatus.setOndropLocation("");
                        MainActivity.mMyStatus.setOnpickupLatitude("");
                        MainActivity.mMyStatus.setOnpickupLongitude("");
                        MainActivity.mMyStatus.setOndropLatitude("");
                        MainActivity.mMyStatus.setOndropLongitude("");
                        nonactiityobj.startServicefromNonActivity(OngoingAct.this);
                        SessionSave.saveSession("Ongoing", "farecal", OngoingAct.this);
                        final Intent jobintent = new Intent(OngoingAct.this, HomeActivity.class);
                        Bundle extras = new Bundle();
                        extras.putString("alert_message", json.getString("message"));
                        jobintent.putExtras(extras);
                        startActivity(jobintent);
                        finish();
                    }
                    else
                    {
                        CancelTxt.setText("00:00:00");
                        nonactiityobj.startServicefromNonActivity(OngoingAct.this);
                    }
                }
                else
                {
                    butt_onboard.setClickable(true);
                    alert_view(OngoingAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(
                            R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
                }
            }
            catch (final Exception e)
            {
                e.printStackTrace();
                butt_onboard.setClickable(true);
            }
        }
    }

    /**
     * Used to call the driver_start the trip  API and parse the response.
     */
    private class Onboard implements APIResult
    {
        private Onboard(final String url, JSONObject data)
        {
            try
            {
                if (isOnline())
                {
                    new APIService_Volley_JSON(OngoingAct.this, this, data, false).execute(url);
                }
                else
                {
                    alert_view(OngoingAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(
                            R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
                }
            }
            catch (Exception e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        /**
         * Parse the response and update the UI.
         */
        @Override
        public void getResult(final boolean isSuccess, final String result)
        {
            try
            {
                if (isSuccess)
                {
                    butt_onboard.setClickable(true);
                    final JSONObject json = new JSONObject(result);

                    if (json.getInt("status") == 1)
                    {

                        txt_drive_request.setVisibility(View.GONE);
                        txt_eta_time.setVisibility(View.GONE);
                        img_pickup.setVisibility(View.VISIBLE);
                        lay_pickup.setVisibility(View.VISIBLE);
                        txt_current_location.setVisibility(View.GONE);
                        imgDown.setVisibility(View.VISIBLE);
                        img_drop.setVisibility(View.GONE);
                        lay_drop.setVisibility(View.GONE);
                        txt_pickup.setVisibility(View.VISIBLE);
                        txt_drop.setVisibility(View.VISIBLE);
                        layCancel.setVisibility(View.GONE);
                        layDriverInfoDetail.setVisibility(View.GONE);
                        viewPickUp.setVisibility(View.GONE);
                        layPick.setVisibility(View.GONE);
                        HeadTitle.setText(getResources().getString(R.string.ongoing_journey));
                        HeadTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        butt_onboard.setText("" + getResources().getString(R.string.arvd_destination).toUpperCase());
                        layTime.setVisibility(View.VISIBLE);
                        speed_txt.setVisibility(View.VISIBLE);
                        speedtext.setVisibility(View.VISIBLE);
                        backup.setVisibility(View.GONE);
                        SessionSave.saveSession("speedwaiting", "", OngoingAct.this);
                        Intent i1 = new Intent(OngoingAct.this, Timer1.class);
                        startService(i1);
                        myHandler.postDelayed(rTimer1, 0); //started timer 1
                        SessionSave.saveSession("travel_status", "2", OngoingAct.this);
                        MainActivity.mMyStatus.setOnstatus("Complete");
                        MainActivity.mMyStatus.setdistance("");
                        SessionSave.saveSession("status", "A", OngoingAct.this);
                        nonactiityobj.startServicefromNonActivity(OngoingAct.this);
                        new GetPickdropLoc().execute();
                    }
                    else if (json.getInt("status") == 2)
                    {
                        alert_view(OngoingAct.this, "" + getResources().getString(R.string.message), "" + json.getString("message"), "" + getResources().getString(R.string.ok), "");
                        txt_drive_request.setVisibility(View.GONE);
                        txt_eta_time.setVisibility(View.GONE);
                        img_pickup.setVisibility(View.VISIBLE);
                        lay_pickup.setVisibility(View.VISIBLE);
                        txt_current_location.setVisibility(View.GONE);
                        imgDown.setVisibility(View.VISIBLE);
                        img_drop.setVisibility(View.GONE);
                        lay_drop.setVisibility(View.GONE);
                        txt_pickup.setVisibility(View.VISIBLE);
                        txt_drop.setVisibility(View.VISIBLE);
                        layCancel.setVisibility(View.GONE);
                        layDriverInfoDetail.setVisibility(View.GONE);
                        viewPickUp.setVisibility(View.GONE);
                        layPick.setVisibility(View.GONE);
                        HeadTitle.setText(getResources().getString(R.string.ongoing_journey));
                        HeadTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        butt_onboard.setText("" + getResources().getString(R.string.arvd_destination).toUpperCase());
                        layTime.setVisibility(View.VISIBLE);
                        speed_txt.setVisibility(View.VISIBLE);
                        speedtext.setVisibility(View.VISIBLE);
                        backup.setVisibility(View.GONE);
                        SessionSave.saveSession("speedwaiting", "", OngoingAct.this);
                        Intent i1 = new Intent(OngoingAct.this, Timer1.class);
                        startService(i1);
                        myHandler.postDelayed(rTimer1, 0); //started timer 1
                        SessionSave.saveSession("travel_status", "2", OngoingAct.this);
                        MainActivity.mMyStatus.setOnstatus("Complete");
                        MainActivity.mMyStatus.setdistance("");
                        SessionSave.saveSession("status", "A", OngoingAct.this);
                        nonactiityobj.startServicefromNonActivity(OngoingAct.this);
                        new GetPickdropLoc().execute();
                    }
                    else if (json.getInt("status") == -1)
                    {
                        final Intent jobintent = new Intent(OngoingAct.this, HomeActivity.class);
                        SessionSave.saveSession("status", "F", OngoingAct.this);
                        SessionSave.saveSession("trip_id", "", OngoingAct.this);
                        SessionSave.saveSession("travel_status", "", OngoingAct.this);
                        Bundle extras = new Bundle();
                        extras.putString("alert_message", json.getString("message"));
                        jobintent.putExtras(extras);
                        startActivity(jobintent);
                        finish();
                    }
                }
                else
                {
                    butt_onboard.setClickable(true);
                    alert_view(OngoingAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(
                            R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                butt_onboard.setClickable(true);
            }
        }
    }

    /**
     * Used to call the driver_fare_update API and parse the response.
     */
    private class FreeUpdate implements APIResult
    {
        private FreeUpdate(final String url)
        {
            try
            {
                if (isOnline())
                {
                    new APIService_Volley_JSON(OngoingAct.this, this, null, true).execute(url);
                }
                else
                {
                    alert_view(
                            OngoingAct.this,
                            "" + getResources().getString(R.string.message),
                            ""
                                    + getResources().getString(
                                    R.string.check_net_connection), ""
                                    + getResources().getString(R.string.ok), "");
                }
            } catch (Exception e) {
                // TODO: hande exception
                e.printStackTrace();
            }
        }

        /**
         * Parse the response and update the UI.
         */
        @Override
        public void getResult(final boolean isSuccess, final String result)
        {
            try
            {
                if (isSuccess)
                {
                    SessionSave.saveSession("Ongoing", "farecal", OngoingAct.this);
                }
                else
                {
                    alert_view(
                            OngoingAct.this,
                            "" + getResources().getString(R.string.message),
                            ""
                                    + getResources().getString(
                                    R.string.check_net_connection), ""
                                    + getResources().getString(R.string.ok), "");
                }
            }
            catch (final Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * Used to call the complete_trip API and parse the response.
     */
    private class CompleteTrip implements APIResult
    {
        private CompleteTrip(final String url, final Double latitude, final Double longitude)
        {
            try
            {
                JSONObject j = new JSONObject();
                j.put("trip_id", SessionSave.getSession("trip_id", OngoingAct.this));
                j.put("drop_latitude", Double.toString(latitude1));
                j.put("drop_longitude", Double.toString(longitude1));
                j.put("drop_location", SessionSave.getSession("drop_location", OngoingAct.this).replaceAll("\n", " "));
                j.put("distance", "");
                j.put("actual_distance", "");
                j.put("plan_type", tripType);
                j.put("waiting_hour", SessionSave.getSession("waitingHr", OngoingAct.this));
                Log.e("Complete trip Input", j.toString());

                if (isOnline())
                {
                    new APIService_Volley_JSON(OngoingAct.this, this, j, false).execute(url);
                }
                else
                {
                    final String completeUrl = "complete_trip";
                    new CompleteTrip(completeUrl, latitude1, longitude1);
                }
            }
            catch (Exception e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        /**
         * Parse the response and update the UI.
         */
        @Override
        public void getResult(final boolean isSuccess, final String result)
        {
            try
            {
                final JSONObject json = new JSONObject(result);

                if (isSuccess)
                {
                    butt_onboard.setClickable(true);

                    if (json.getInt("status") == 4)
                    {
                        MainActivity.mMyStatus.setOnstatus("");
                        MainActivity.mMyStatus.setOnPassengerImage("");
                        MainActivity.mMyStatus.setOnstatus("Completed");
                        MainActivity.mMyStatus.setOnpassengerName("");
                        MainActivity.mMyStatus.setOnpickupLatitude("");
                        MainActivity.mMyStatus.setOnpickupLongitude("");
                        MainActivity.mMyStatus.setOndropLatitude("");
                        MainActivity.mMyStatus.setOndropLongitude("");
                        SessionSave.saveSession("Ongoing", "farecal", OngoingAct.this);
                        SessionSave.saveSession("travel_status", "5", OngoingAct.this);
                        CommonData.currentspeed = "";
                        speedtext.setText("" + getResources().getString(R.string.m_timer));
                        SessionSave.saveSession("speedwaiting", "", OngoingAct.this);
                        stopService(new Intent(OngoingAct.this, Timer1.class));
                        myHandler.removeCallbacks(rTimer1);
                        MainActivity.mMyStatus.setsaveTime(timeclear);
                        final Intent farecal = new Intent(OngoingAct.this, BillFareAct.class);
                        farecal.putExtra("from", "direct");
                        farecal.putExtra("message", result);
                        startActivity(farecal);
                        finish();
                    }
                    else if (json.getInt("status") == 2)
                    {
                        MainActivity.mMyStatus.setOnstatus("");
                        MainActivity.mMyStatus.setOnPassengerImage("");
                        MainActivity.mMyStatus.setOnstatus("Completed");
                        MainActivity.mMyStatus.setOnpassengerName("");
                        MainActivity.mMyStatus.setOndropLocation("");
                        MainActivity.mMyStatus.setOnpickupLatitude("");
                        MainActivity.mMyStatus.setOnpickupLongitude("");
                        MainActivity.mMyStatus.setOndropLatitude("");
                        MainActivity.mMyStatus.setOndropLongitude("");
                        MainActivity.mMyStatus.setOndriverLatitude("");
                        MainActivity.mMyStatus.setOndriverLongitude("");
                        SessionSave.saveSession("status", "F", OngoingAct.this);
                        SessionSave.saveSession("trip_id", "", OngoingAct.this);
                        SessionSave.saveSession("travel_status", "", OngoingAct.this);
                        stopService(new Intent(OngoingAct.this, Timer1.class));
                        myHandler.removeCallbacks(rTimer1);
                        SessionSave.saveSession("trip_id", "", OngoingAct.this);
                        final String status_update = "driver_start_trip&driver_id=" + SessionSave.getSession("Id", OngoingAct.this)
                                + "&latitude="
                                + latitude1
                                + "&longitude="
                                + longitude1 + "&status=" + "F" + "&trip_id=";
                        SessionSave.saveSession("Ongoing", "flagger", OngoingAct.this);
                        new FreeUpdate(status_update);
                        final Intent jobintent = new Intent(OngoingAct.this, HomeActivity.class);
                        Bundle extras = new Bundle();
                        extras.putString("alert_message", json.getString("message"));
                        jobintent.putExtras(extras);
                        startActivity(jobintent);
                        finish();
                    }
                    else if (json.getInt("status") == -1)
                    {
                        MainActivity.mMyStatus.setOnstatus("");
                        MainActivity.mMyStatus.setOnPassengerImage("");
                        MainActivity.mMyStatus.setOnstatus("Completed");
                        MainActivity.mMyStatus.setOnpassengerName("");
                        MainActivity.mMyStatus.setOndropLocation("");
                        MainActivity.mMyStatus.setOnpickupLatitude("");
                        MainActivity.mMyStatus.setOnpickupLongitude("");
                        MainActivity.mMyStatus.setOndropLatitude("");
                        MainActivity.mMyStatus.setOndropLongitude("");
                        MainActivity.mMyStatus.setOndriverLatitude("");
                        MainActivity.mMyStatus.setOndriverLongitude("");
                        SessionSave.saveSession("status", "F", OngoingAct.this);
                        SessionSave.saveSession("trip_id", "", OngoingAct.this);
                        SessionSave.saveSession("travel_status", "", OngoingAct.this);
                        stopService(new Intent(OngoingAct.this, Timer1.class));
                        myHandler.removeCallbacks(rTimer1);
                        SessionSave.saveSession("trip_id", "", OngoingAct.this);

                        final String status_update = "driver_start_trip&driver_id=" + SessionSave.getSession("Id", OngoingAct.this)
                                + "&latitude="
                                + latitude1
                                + "&longitude="
                                + longitude1 + "&status=" + "F" + "&trip_id=";
                        SessionSave.saveSession("Ongoing", "flagger", OngoingAct.this);
                        new FreeUpdate(status_update);
                        final Intent jobintent = new Intent(OngoingAct.this, HomeActivity.class);
                        Bundle extras = new Bundle();
                        extras.putString("alert_message", json.getString("message"));
                        jobintent.putExtras(extras);
                        startActivity(jobintent);
                        finish();
                    }
                    else
                    {
                        alert_view(OngoingAct.this, "" + getResources().getString(R.string.message), "" + json.getString("message"), "" + getResources().getString(R.string.ok), "");
                    }
                }
                else
                {
                    butt_onboard.setClickable(true);
                    alert_view(OngoingAct.this, "" + getResources().getString(R.string.message), "" + json.getString("message"), "" + getResources().getString(R.string.ok), "");
                    final String completeUrl = "complete_trip";
                    new CompleteTrip(completeUrl, latitude1, longitude1);
                }
            }
            catch (final Exception e)
            {
                e.printStackTrace();
                butt_onboard.setClickable(true);
            }
        }
    }

    // Initially update the trip details based on get_trip_detail response.
    private void init()
    {
        if (MainActivity.mMyStatus.getOnstatus().equalsIgnoreCase("on"))
        {
            butt_navigator.setVisibility(View.VISIBLE);
            layDriverInfoDetail.setVisibility(View.VISIBLE);
            viewPickUp.setVisibility(View.VISIBLE);
            layPick.setVisibility(View.VISIBLE);
            HeadTitle.setText("" + getResources().getString(R.string.driver_enroute));
            HeadTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

            //I have arrived
            butt_onboard.setText("" + getResources().getString(R.string.ive_arrived).toUpperCase());
            butt_onboard.setVisibility(View.VISIBLE);
            layCancel.setVisibility(View.VISIBLE);

            txt_profile_name.setText(MainActivity.mMyStatus.getOnpassengerName());
            txt_call_number.setText(" " + getResources().getString(R.string.call));

            final String pickup = " " + MainActivity.mMyStatus.getOnpickupLocation();
            txt_pickup.setText(pickup);

            txt_eta_time.setText("ETA " + driverETA + " "
                    + getResources().getString(R.string.eta_time));
            txtCurrPickUp.setText(p_pickloc);

            txtKeyPickUp.setText("" + keyPickUp);
            txt_drive_request.setText("" + tripPlan);

            if (!MainActivity.mMyStatus.getPassengerOndropLocation().equals(""))
            {
                txt_drop.setText(MainActivity.mMyStatus.getPassengerOndropLocation());
            }
            else
            {
                lay_eta_drop.setVisibility(View.GONE);
            }

            if (MainActivity.mMyStatus.getOnpickupLatitude().length() != 0)
                p_latitude = Double.parseDouble(MainActivity.mMyStatus
                        .getOnpickupLatitude());

            if (MainActivity.mMyStatus.getOnpickupLongitude().length() != 0)
                p_longtitude = Double.parseDouble(MainActivity.mMyStatus
                        .getOnpickupLongitude());

            if (MainActivity.mMyStatus.getOndropLatitude().length() != 0)
                d_latitude = Double.parseDouble(MainActivity.mMyStatus
                        .getOndropLatitude());

            if (MainActivity.mMyStatus.getOndropLongitude().length() != 0)
                d_longtitude = Double.parseDouble(MainActivity.mMyStatus
                        .getOndropLongitude());

            if (MainActivity.mMyStatus.getOndriverLatitude().length() != 0)
                driver_latitude = Double.parseDouble(MainActivity.mMyStatus
                        .getOndriverLatitude());

            if (MainActivity.mMyStatus.getOndriverLongitude().length() != 0)
                driver_longtitude = Double.parseDouble(MainActivity.mMyStatus
                        .getOndriverLongitude());

            new GetPickdropLoc().execute();
        }
        else if (MainActivity.mMyStatus.getOnstatus().equalsIgnoreCase("Arrivd"))
        {
            butt_navigator.setVisibility(View.GONE);
            layCancel.setVisibility(View.GONE);
            txt_profile_name.setText(MainActivity.mMyStatus.getOnpassengerName());
            txt_call_number.setText(" " + getResources().getString(R.string.call));
            layDriverInfoDetail.setVisibility(View.VISIBLE);
            viewPickUp.setVisibility(View.VISIBLE);
            layPick.setVisibility(View.VISIBLE);

            HeadTitle.setText("" + getResources().getString(R.string.ongoing_journey));
            HeadTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            //Begin Trip
            butt_onboard.setText("" + getResources().getString(R.string.pass_onboard).toUpperCase());
            butt_onboard.setVisibility(View.VISIBLE);
            final String pickup = "" + MainActivity.mMyStatus.getOnpickupLocation();
            txt_pickup.setText(pickup);
            txt_eta_time.setVisibility(View.GONE);
            txtCurrPickUp.setText(p_pickloc);
            txtKeyPickUp.setText("" + keyPickUp);
            txt_drive_request.setText("" + tripPlan);

            if (!MainActivity.mMyStatus.getPassengerOndropLocation().equals(""))
            {
                txt_drop.setText(MainActivity.mMyStatus.getPassengerOndropLocation());
            }
            else
            {
                lay_eta_drop.setVisibility(View.GONE);
            }

            if (MainActivity.mMyStatus.getOnpickupLatitude().length() != 0)
                p_latitude = Double.parseDouble(MainActivity.mMyStatus.getOnpickupLatitude());

            if (MainActivity.mMyStatus.getOnpickupLongitude().length() != 0)
                p_longtitude = Double.parseDouble(MainActivity.mMyStatus.getOnpickupLongitude());

            if (MainActivity.mMyStatus.getOndropLatitude().length() != 0)
                d_latitude = Double.parseDouble(MainActivity.mMyStatus.getOndropLatitude());

            if (MainActivity.mMyStatus.getOndropLongitude().length() != 0)
                d_longtitude = Double.parseDouble(MainActivity.mMyStatus.getOndropLongitude());

            if (MainActivity.mMyStatus.getOndriverLatitude().length() != 0)
                driver_latitude = Double.parseDouble(MainActivity.mMyStatus.getOndriverLatitude());

            if (MainActivity.mMyStatus.getOndriverLongitude().length() != 0)
                driver_longtitude = Double.parseDouble(MainActivity.mMyStatus.getOndriverLongitude());

            new GetPickdropLoc().execute();
        }
        else if (MainActivity.mMyStatus.getOnstatus().equalsIgnoreCase("Complete"))
        {
            butt_navigator.setVisibility(View.GONE);
            txt_drive_request.setVisibility(View.GONE);
            txt_eta_time.setVisibility(View.GONE);
            img_pickup.setVisibility(View.VISIBLE);
            img_drop.setVisibility(View.GONE);
            imgDown.setVisibility(View.VISIBLE);
            lay_pickup.setVisibility(View.VISIBLE);
            lay_drop.setVisibility(View.GONE);
            txt_pickup.setVisibility(View.VISIBLE);
            txt_drop.setVisibility(View.VISIBLE);
            txt_current_location.setVisibility(View.GONE);
            layCancel.setVisibility(View.GONE);

            layDriverInfoDetail.setVisibility(View.GONE);
            viewPickUp.setVisibility(View.GONE);
            layPick.setVisibility(View.GONE);

            // ShowToast(OngoingAct.this, "COMPLETE INIT");

            CancelTxt.setText("00:00:00");
            HeadTitle.setText("Trip In-Progress");
            HeadTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            //End Trip
            butt_onboard.setText("" + getResources().getString(R.string.arvd_destination).toUpperCase());

            txt_profile_name.setText(MainActivity.mMyStatus
                    .getOnpassengerName());
            txt_call_number.setText(" " + getResources().getString(R.string.call));

            final String pickup = ""
                    + MainActivity.mMyStatus.getOnpickupLocation();
            txt_pickup.setText("" + pickup);

            txt_eta_time.setText("ETA " + driverETA + " " + getResources().getString(R.string.eta_time));
            txtCurrPickUp.setText(p_pickloc);
            txtKeyPickUp.setText("" + keyPickUp);
            txt_drive_request.setText("" + tripPlan);

            if (!MainActivity.mMyStatus.getPassengerOndropLocation().equals(""))
            {
                txt_drop.setText(MainActivity.mMyStatus.getPassengerOndropLocation());
            }
            else
            {
                txt_drop.setVisibility(View.GONE);
            }

            if (MainActivity.mMyStatus.getOnpickupLatitude().length() != 0)
                p_latitude = Double.parseDouble(MainActivity.mMyStatus.getOnpickupLatitude());

            if (MainActivity.mMyStatus.getOnpickupLongitude().length() != 0)
                p_longtitude = Double.parseDouble(MainActivity.mMyStatus.getOnpickupLongitude());

            if (MainActivity.mMyStatus.getOndropLatitude().length() != 0)
                d_latitude = Double.parseDouble(MainActivity.mMyStatus.getOndropLatitude());

            if (MainActivity.mMyStatus.getOndropLongitude().length() != 0)
                d_longtitude = Double.parseDouble(MainActivity.mMyStatus.getOndropLongitude());

            if (MainActivity.mMyStatus.getOndriverLatitude().length() != 0)
                driver_latitude = Double.parseDouble(MainActivity.mMyStatus.getOndriverLatitude());

            if (MainActivity.mMyStatus.getOndriverLongitude().length() != 0)
                driver_longtitude = Double.parseDouble(MainActivity.mMyStatus.getOndriverLongitude());

            Log.e("Drop Latitude", "Drop Latitude" + d_latitude + " : " + d_longtitude);

            new GetPickdropLoc().execute();
            layTime.setVisibility(View.VISIBLE);
            backup.setVisibility(View.GONE);
            Intent i1 = new Intent(OngoingAct.this, Timer1.class);
            startService(i1);
            myHandler.postDelayed(rTimer1, 0);
            butt_onboard.setVisibility(View.VISIBLE);
        }
        else
        {
            new GetPickdropLoc().execute();
            txt_profile_name.setText("" + getResources().getString(R.string.you));
            txt_call_number.setText(" " + getResources().getString(R.string.call));
        }
    }

    // This handler helps to draw the route between driver place to pickup place
    // and pickup place to drop place.
    Handler mHandler = new Handler()
    {
        private CountDownTimer countDownTimer;
        private Dialog mProgressdialog;

        @Override
        public void handleMessage(final android.os.Message msg)
        {
            switch (msg.what)
            {
                case 0:
                    break;

                case 1:

                    try
                    {
                        Log.e("Complete1111111111", "Complete111111111111: " + pickupLatLng + " : " + dropLatLng);

                        if (MainActivity.mMyStatus.getOnstatus().equalsIgnoreCase("Complete"))
                        {
                            route.drawRoute(map, OngoingAct.this, pickupLatLng, dropLatLng, "en", Color.BLUE);
                        }
                        else
                        {
                            route.drawRoute(map, OngoingAct.this, currentLatLng, pickupLatLng, "en", Color.BLUE);
                        }
                    }
                    catch (final Exception e)
                    {
                        e.printStackTrace();
                    }

                    break;

                case 2:

                    final View view = View.inflate(OngoingAct.this, R.layout.progress_bar, null);
                    mProgressdialog = new Dialog(OngoingAct.this, R.style.NewDialog);
                    mProgressdialog.setContentView(view);
                    mProgressdialog.setCancelable(false);
                    mProgressdialog.show();
                    mHandler.sendEmptyMessage(1);

                    break;

                case 3:

                    showLog("dismiss handler");
                    mProgressdialog.dismiss();

                    break;

                case 4:

                    countDownTimer.cancel();

                    break;
            }
        }
    };

    // This asynchronous task to update the passenger in UI.
    public class Getimg extends AsyncTask<Void, Void, Void>
    {
        String imagepath = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(final Void... params)
        {
            try
            {
                String Image_path = SessionSave.getSession("p_image", OngoingAct.this);

                if (Image_path != null && Image_path.length() > 0)
                {
                    Log.e("Profile Image", "" + SessionSave.getSession("p_image", OngoingAct.this));
                    ImageLoader.getInstance().displayImage(Image_path, imgPassengerProfile, options);
                }
            }
            catch (final Exception e)
            {
                Log.d("Exception", "while getting image");
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(final Void result)
        {
            Log.i("Imagepath", imagepath);
            super.onPostExecute(result);
        }
    }

    // This method helps to update the driver current lat/lng in map view.
    private void MyLocationListener1()
    {
        if (mLastLocation != null)
        {
            latitude1 = mLastLocation.getLatitude();
            longitude1 = mLastLocation.getLongitude();
            bearing = mLastLocation.getBearing();
        }
//		if (bearing >= 0)
//			bearing = bearing + 90;
//		else
//			bearing = bearing - 90;
        if (c_marker != null)
        {
            c_marker.remove();
        }

        currentLatLng = new LatLng(latitude1, longitude1);
        c_marker = map.addMarker(new MarkerOptions()
                .position(new LatLng(latitude1, longitude1))
//				.rotation(bearing)
                .title(Address)
                .flat(true)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.driver1)));

        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude1, longitude1), zoom));
        bearing = 0;
    }

    @Override
    public void ClickMethod(final View v)
    {
        Intent i;
        switch (v.getId())
        {
            case R.id.menu_me:

                i = new Intent(OngoingAct.this, MeAct.class);
                startActivity(i);
                menu.toggle();
                finish();

                break;

            case R.id.menu_home:

                i = new Intent(OngoingAct.this, HomeActivity.class);
                startActivity(i);
                menu.toggle();
                finish();

                break;

            case R.id.menu_ongoing:

                menu.toggle();

                break;

            case R.id.menu_logout:

                logout(OngoingAct.this);

                break;

            case R.id.menu_mystatus:

                i = new Intent(OngoingAct.this, MyStatus.class);
                startActivity(i);
                menu.toggle();
                finish();

                break;
        }
    }

    // This method helps to update the driver current lat/lng in map view when
    // activity get starts.
    @Override
    public void onConnected(final Bundle connectionHint)
    {
        try
        {
            startLocationUpdates();
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (mLastLocation != null)
            {
                latitude1 = mLastLocation.getLatitude();
                longitude1 = mLastLocation.getLongitude();
                SessionSave.saveSession("mLatitude", String.valueOf(mLastLocation.getLatitude()), this);
                SessionSave.saveSession("mLongitude", String.valueOf(mLastLocation.getLongitude()), this);
                currentLatLng = new LatLng(latitude1, longitude1);
                final LatLng coordinate = new LatLng(latitude1, longitude1);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinate, zoom));
            }
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    // This method helps to update the driver current lat/lng in map view by
    // using MyLocationListener() function.
    @Override
    public void onLocationChanged(final Location location)
    {
        mLastLocation = location;
        SessionSave.saveSession("mLatitude", String.valueOf(location.getLatitude()), this);
        SessionSave.saveSession("mLongitude", String.valueOf(location.getLongitude()), this);
        zoom = map.getCameraPosition().zoom;
        MyLocationListener1();
    }

    @Override
    public void onBackPressed()
    {
//		 super.onBackPressed();
//		Intent intent = new Intent(OngoingAct.this, DashAct.class);
//		startActivity(intent);
    }

    // This class is used to get the trip details when activity in opened, It
    // calls the API and parse the response.
    private class Tripdetails implements APIResult
    {
        String taxiNumber, keyPickUpLocation;
        String p_logid = "";

        String p_droploc = "";
        String p_picklat = "";
        String p_picklng = "";
        String p_droplat = "";
        String p_droplng = "";
        String p_driverlat = "";
        String p_driverlng = "";
        String p_travelstatus = "";
        private String p_image = "";

        private String p_notes = "";
        private String p_driverstatus = "", p_taxi_speed = "";

        private Tripdetails(final String url, JSONObject data)
        {
            try
            {
                if (isOnline())
                {
                    new APIService_Volley_JSON(OngoingAct.this, this, data, false).execute(url);
                }
                else
                {
                    alert_view(OngoingAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(
                            R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
                }
            }
            catch (Exception e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        @Override
        public void getResult(final boolean isSuccess, final String result)
        {
            try
            {
                Log.e("Get Trip Detail", "Get Trip Detail" + result.toString());

                if (isSuccess)
                {
                    final JSONObject json = new JSONObject(result);

                    if (json.getInt("status") == 1)
                    {
                        final JSONObject detail = json.getJSONObject("detail");
                        p_logid = detail.getString("trip_id");
                        p_name = detail.getString("passenger_name");
                        p_pickloc = detail.getString("current_location");
                        p_droploc = detail.getString("drop_location");
                        p_picklat = detail.getString("pickup_latitude");
                        p_picklng = detail.getString("pickup_longitude");
                        p_droplat = detail.getString("drop_latitude");
                        p_droplng = detail.getString("drop_longitude");
                        p_driverlat = detail.getString("driver_latitute");
                        p_driverlng = detail.getString("driver_longtitute");
                        p_travelstatus = detail.getString("travel_status");
                        p_driverstatus = detail.getString("driver_status");
                        p_notes = detail.getString("notes");
                        p_phone = detail.getString("passenger_phone");
                        p_image = detail.getString("passenger_image");

                        p_taxi_speed = detail.getString("taxi_min_speed");
                        taxiNumber = detail.getString("taxi_number");
                        driverETA = detail.getString("time_to_reach_passen");
                        tripType = detail.getString("trip_type");
                        otHours = detail.getString("ot_hours");

                        tripPlan = detail.getString("trip_plan");
                        keyPickUp = detail.getString("key_pickup");
                        carDetails = detail.getString("car_details");
                        landMark = detail.getString("land_mark");

                        Log.e("Trip Plan: ", "Trip Plan:" + detail.getString("trip_type"));

                        txtKeyPickUp.setText(keyPickUp);
                        txtDropMark.setText(p_droploc);
                        txt_drive_request.setText(tripPlan);

                        SessionSave.saveSession("passenger_name", "" + p_name, OngoingAct.this);
                        SessionSave.saveSession("TripType", "" + tripType, OngoingAct.this);

                        Log.e("PickupLoc & DriverLoc", "" + p_driverlat + ":" + p_driverlng + "  " + p_picklat + " : " + p_picklng + "  " + p_droplat + ":" + p_droplng + " : " + p_pickloc + ":" + p_droploc);

                        if (!p_driverlat.equals("0") && !p_driverlng.equals("0") && !p_driverlng.equals("") && p_driverlng.length() != 0)
                        {
                            botton_layout.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            botton_layout.setVisibility(View.GONE);
                        }

                        SessionSave.saveSession("p_image", p_image, OngoingAct.this);
                        SessionSave.saveSession("travel_status", p_travelstatus, OngoingAct.this);

                        if ((p_travelstatus.equals("2") || (p_travelstatus.equals("9") || p_travelstatus.equals("3"))))
                        {
                            if (p_driverstatus.equalsIgnoreCase("F") || p_driverstatus.equalsIgnoreCase("B") || (p_driverstatus.equalsIgnoreCase("N") || p_travelstatus.equalsIgnoreCase("2")))
                            {
                                if (p_travelstatus.equalsIgnoreCase("3"))
                                {
                                    MainActivity.mMyStatus.setOnstatus("Arrivd");
                                }
                                else if (p_travelstatus.equalsIgnoreCase("2"))
                                {
                                    MainActivity.mMyStatus.setOnstatus("Complete");
                                }
                                else if (p_travelstatus.equalsIgnoreCase("9"))
                                {
                                    MainActivity.mMyStatus.setOnstatus("On");
                                }

                                p_pickloc = p_pickloc.trim();

                                if (p_pickloc.length() > 0)
                                {
                                    p_pickloc = Character.toUpperCase(p_pickloc.charAt(0)) + p_pickloc.substring(1);
                                    p_droploc = p_droploc.trim();
                                }

                                if (p_droploc.length() > 0)
                                {
                                    p_droploc = Character.toUpperCase(p_droploc.charAt(0)) + p_droploc.substring(1);
                                }

                                if (p_name.length() > 0)
                                {
                                    p_name = Character.toUpperCase(p_name.charAt(0)) + p_name.substring(1);
                                }
                                if (p_taxi_speed.length() > 0 && !p_taxi_speed.equals(null))
                                {
                                    SessionSave.saveSession("taxi_speed", p_taxi_speed, OngoingAct.this);
                                }

                                if (p_notes.length() > 0)
                                {
                                    p_notes = Character.toUpperCase(p_notes.charAt(0)) + p_notes.substring(1);
                                }

                                MainActivity.mMyStatus.setOnpickupLocation(p_pickloc);
                                MainActivity.mMyStatus.setOndropLocation(p_droploc);
                                MainActivity.mMyStatus.setPassengerOndropLocation(p_droploc);
                                MainActivity.mMyStatus.setOnpickupLatitude(p_picklat);
                                MainActivity.mMyStatus.setOnpickupLongitude(p_picklng);
                                MainActivity.mMyStatus.setOndriverLatitude(p_driverlat);
                                MainActivity.mMyStatus.setOndriverLongitude(p_driverlng);
                                MainActivity.mMyStatus.setOnpassengerName(p_name);
                                MainActivity.mMyStatus.settripId(p_logid);
                                SessionSave.saveSession("trip_id", p_logid, OngoingAct.this);
                                MainActivity.mMyStatus.setpickupLoc(p_pickloc);
                                MainActivity.mMyStatus.setOndropLatitude(p_droplat);
                                MainActivity.mMyStatus.setOndropLongitude(p_droplng);
                                MainActivity.mMyStatus.setdropLoc(p_droploc);
                                MainActivity.mMyStatus.setpassengerId(p_logid);
                                MainActivity.mMyStatus.setphoneNo(p_phone);
                                MainActivity.mMyStatus.setOnPassengerImage(p_image);
                                MainActivity.mMyStatus.setpassengerNotes(p_notes);
                                MainActivity.mMyStatus.setpassengerphone(p_phone);
                                init();
                                new Getimg().execute();
                            }
                            else
                            {
                                ShowToast(OngoingAct.this, "" + getResources().getString(
                                        R.string.you_are_in_trip));
                                Intent i = new Intent(OngoingAct.this, JobsAct.class);
                                startActivity(i);
                                finish();
                            }
                        }
                        else if ((p_driverstatus.equalsIgnoreCase("A") || p_driverstatus.equalsIgnoreCase("B")) && p_travelstatus.equalsIgnoreCase("5"))
                        {
                            ShowToast(OngoingAct.this, "OnGoing");
                            Intent i = new Intent(OngoingAct.this, BillFareAct.class);
                            i.putExtra("from", "pending");
                            i.putExtra("lat", detail.getString("drop_latitude"));
                            i.putExtra("lon", detail.getString("drop_longitude"));
                            i.putExtra("distance", detail.getString("distance"));
                            i.putExtra("waitingHr", detail.getString("waiting_time"));
                            i.putExtra("drop_location", detail.getString("drop_location"));
                            startActivity(i);
                            finish();
                        }
                        else
                        {
                            Intent i = new Intent(OngoingAct.this, HomeActivity.class);
                            SessionSave.saveSession("trip_id", "", OngoingAct.this);
                            startActivity(i);
                            finish();
                        }

                        tripchild1.post(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                layoutheight = tripchild1.getHeight() - 20;
                                map.setPadding(0, layoutheight, 0, 0);
                            }
                        });
                    }
                    else if (json.getInt("status") == -1)
                    {
                        /// User has been logged out or invalid user
                        Log.d("Error here", "user is logged in but status from server is: " + json.getInt("status"));
                        askForLogin();
                    }
                    else
                    {
                        Intent i = new Intent(OngoingAct.this, JobsAct.class);
                        startActivity(i);
                        finish();
                    }
                }
                else
                {
                    Intent i = new Intent(OngoingAct.this, JobsAct.class);
                    startActivity(i);
                    finish();
                }
            }
            catch (final Exception e)
            {
                // TODO: handle exception
                e.printStackTrace();
                Intent i = new Intent(OngoingAct.this, JobsAct.class);
                startActivity(i);
                finish();
            }
        }
    }

    private boolean servicesConnected()
    {
        final int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (ConnectionResult.SUCCESS == resultCode)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public void onConnectionSuspended(int arg0)
    {
        // TODO Auto-generated method stub
        mGoogleApiClient.connect();
    }


    @Override
    protected void onStop()
    {
        // TODO Auto-generated method stub
        super.onStop();
        //handlerSetThread.removeCallbacks(k);
        if (MainActivity.mMyStatus.getOnstatus().equalsIgnoreCase("Complete"))
        {

        }
    }


    @Override
    public void onConnectionFailed(ConnectionResult arg0)
    {
        // TODO Auto-generated method stub
    }

    //This is the method for showing confirmation alert to the user for completing trip.
    public void alertCompleteTrip(Context mContext, String title, String message, String success_txt, String failure_txt)
    {
        final View view = View.inflate(mContext, R.layout.alert_view, null);
        alertDialog = new Dialog(mContext, R.style.NewDialog);
        alertDialog.setContentView(view);
        alertDialog.setCancelable(true);

        FontHelper.applyFont(mContext, alertDialog.findViewById(R.id.alert_id), "DroidSans.ttf");
//		if(alertDialog.isShowing() || alertDialog!=null) {
//			alertDialog = null;
//		}

        alertDialog.show();
        final TextView title_text = (TextView) alertDialog.findViewById(R.id.title_text);
        final TextView message_text = (TextView) alertDialog.findViewById(R.id.message_text);
        final Button button_success = (Button) alertDialog.findViewById(R.id.button_success);
        title_text.setText(title);
        message_text.setText(message);
        button_success.setText(success_txt);

        button_success.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(final View v)
            {
                // TODO Auto-generated method stub
                alertDialog.dismiss();

                final String completeUrl = "complete_trip";
                new CompleteTrip(completeUrl, latitude1, longitude1);
            }
        });
    }
}