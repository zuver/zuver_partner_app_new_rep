package com.zuver.driver;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.zuver.driver.data.CommonData;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.service.APIService_Volley_JSON;
import com.zuver.driver.utils.FontHelper;
import com.zuver.driver.utils.LocationDb;
import com.zuver.driver.utils.SessionSave;

public class FarecalcAct extends MainActivity implements OnClickListener
{
	// Class members declarations.
	private String message;
	private String f_tripid;
	private String f_distance;
	private String f_metric;
	private String f_totalfare;
	private String f_nightfareapplicable;
	private String f_nightfare;
	private String f_pickup = "", drop_location = "";
	private String f_waitingtime;
	private String f_waitingcost;
	private String f_taxamount;
	private String f_tripfare;
	private String f_creditcard;
	private String f_farediscount="";
	private String promotax = "";
	private String promoamt = "";
	private String f_paymodid = "";
	private String p_dis = "";
	private double m_distance;
	private double f_fare;
	private double f_tips;
	private double f_total;
	private EditText farecalTxt;
	private EditText tipsTxt;
	private TextView HeadTitle;
	private TextView paycashTxt;
	private TextView paycardTxt;
	private TextView payuncardTxt;
	private TextView accountTxt;
	private TextView totalamountTxt;
	private TextView actdistanceTxt;
	private TextView metricTxt;
	private TextView promopercentTxt;
	private TextView b_farecalCurrency;
	private TextView b_tipsCurrency;
	private TextView b_pickuplocation;
	private TextView b_droplocation;
	private TextView b_total_amt_curency;
	private TextView b_waitingcost, b_tax, b_discount, b_roundtrip,v_trip_fare;
	private TextView remarks;
	private Dialog mDialog;
	private LinearLayout lay_fare;
	private boolean istipsTxt_focus = false;
	private String cmpTax = "";
	private LinearLayout promoLayout;
	private TextView txtCmp, slideImg, backup;
	ArrayList<String> paymentModeId = new ArrayList<String>();
	public static Activity mFlagger;
	public static FarecalcAct activity;
	InputMethodManager imm;
	Intent details;
	public String gid;
	public String aid;
	public String gcompany_id;
	public String limit;
	public String department;
	private String promodiscount_amount;
	RadioButton radiocashButton, radiocardButton, radiouncardButton, radioaccButton;
	private String f_minutes_traveled;
	private String f_minutes_fare;
	private String Cvv;
	View vid_discount;
	Uri URI = null;
	LocationDb objLocationDb;

	// Set the layout to activity.
	@Override
	public int setLayout()
	{
		setLocale();
		return R.layout.farecalc_lay;
	}

	// Initialize the views on layout
	@Override
	public void Initialize()
	{
		CommonData.sContext = this;
		CommonData.current_act = "FarecalcAct";
		activity = this;
		mFlagger = this;
		objLocationDb = new LocationDb(FarecalcAct.this);
		FontHelper.applyFont(this, findViewById(R.id.id_farelay), "DroidSans.ttf");
		b_pickuplocation = (TextView) findViewById(R.id.pickuplocTxt);
		b_droplocation = (TextView) findViewById(R.id.droplocTxt);
		b_farecalCurrency = (TextView) findViewById(R.id.farecalCurrency);
		actdistanceTxt = (TextView) findViewById(R.id.actdistanceTxt);
		metricTxt = (TextView) findViewById(R.id.metricTxt);
		b_tipsCurrency = (TextView) findViewById(R.id.tipsCurrency);
		farecalTxt = (EditText) findViewById(R.id.farecalTxt);
		tipsTxt = (EditText) findViewById(R.id.tipsTxt);
		HeadTitle = (TextView) findViewById(R.id.headerTxt);
		paycashTxt = (TextView) findViewById(R.id.paycash);
		paycardTxt = (TextView) findViewById(R.id.paycard);
		payuncardTxt = (TextView) findViewById(R.id.payuncard);
		accountTxt = (TextView) findViewById(R.id.payaccount);
		totalamountTxt = (TextView) findViewById(R.id.totalamountTxt);
		promopercentTxt = (TextView) findViewById(R.id.promopercentage);
		txtCmp = (TextView) findViewById(R.id.txtcmpTax);
		slideImg = (TextView) findViewById(R.id.slideImg);
		backup = (TextView) findViewById(R.id.backup);
		slideImg.setVisibility(View.GONE);
		backup.setVisibility(View.GONE);
		promoLayout = (LinearLayout) findViewById(R.id.discountlayout);
		lay_fare = (LinearLayout) findViewById(R.id.lay_fare);
		remarks = (TextView) findViewById(R.id.remarks);
		b_total_amt_curency = (TextView) findViewById(R.id.toatalamtCurrency);
		b_waitingcost = (TextView) findViewById(R.id.waitingcost);
		b_tax = (TextView) findViewById(R.id.tax);
		b_discount = (TextView) findViewById(R.id.discount);
		b_roundtrip = (TextView) findViewById(R.id.roundtrip);
		v_trip_fare = (TextView) findViewById(R.id.v_trip_fare);
		vid_discount = (View) findViewById(R.id.vid_discount);
		radiocashButton = (RadioButton) findViewById(R.id.rbtn_cash);
		radiocardButton = (RadioButton) findViewById(R.id.rbtn_card);
		radiouncardButton = (RadioButton) findViewById(R.id.rbtn_uncard);
		radioaccButton = (RadioButton) findViewById(R.id.rbtn_account);
		HeadTitle.setText("" + getResources().getString(R.string.bill_summary));
		details = getIntent();
		try
		{
			// If Directly comes from end trip page(OngoingAct)
			if (details.getStringExtra("from").equalsIgnoreCase("direct"))
			{
				message = details.getStringExtra("message");
				setFareCalculatorScreen();
			}
			// If comes from Pending bookings(JobsAct).
			else
			{
				String lat = details.getStringExtra("lat");
				String lon = details.getStringExtra("lon");
				String distance = details.getStringExtra("distance");
				String waitingHr = details.getStringExtra("waitingHr");
				String drop_location = details.getStringExtra("drop_location");
				String url = "complete_trip";
				new CompleteTrip(url, lat, lon, distance, waitingHr, drop_location);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	protected void onResume()
	{
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy()
	{
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected void onPause()
	{
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onStop()
	{
		// TODO Auto-generated method stub
		super.onStop();
	}

	// This for update the fare calculator page with API result.
	@SuppressLint("SdCardPath")
	private void setFareCalculatorScreen()
	{
		if (details != null)
		{
			try
			{
				JSONObject obj = new JSONObject(message);
				JSONObject json = obj.getJSONObject("detail");
				f_tripid = json.getString("trip_id");
				f_distance = json.getString("distance");
				f_metric = json.getString("metric");
				f_totalfare = json.getString("total_fare");
				f_nightfareapplicable = json.getString("nightfare_applicable");
				f_nightfare = json.getString("nightfare");
				f_pickup = json.getString("pickup");
				drop_location = json.getString("drop");
				f_waitingtime = json.getString("waiting_time");
				f_waitingcost = json.getString("waiting_cost");
				f_taxamount = json.getString("tax_amount");
				f_tripfare = json.getString("trip_fare");
				f_minutes_traveled = json.getString("minutes_traveled");
				f_minutes_fare = json.getString("minutes_fare");
				f_creditcard = json.getString("credit_card_status");
				f_farediscount = json.getString("promodiscount_amount");
				cmpTax = json.getString("company_tax");
				if (f_distance.length() != 0)
					m_distance = Double.parseDouble(f_distance);
				f_distance = String.format("%.2f", m_distance);
				txtCmp.setText("" + getResources().getString(R.string.tax) + cmpTax + "" + getResources().getString(R.string.tax_percent));
				farecalTxt.setText(f_totalfare);
				v_trip_fare.setText("" + SessionSave.getSession("site_currency", FarecalcAct.this) + " " + f_tripfare);
				p_dis = String.valueOf(json.getInt("promo_discount_per"));
				promodiscount_amount = json.getString("promo_discount_per");
				if (!p_dis.equals("0"))
				{
					promopercentTxt.setText("" + getResources().getString(R.string.discount) + promodiscount_amount + "" + getResources().getString(R.string.tax_percent));
					b_discount.setText("" + SessionSave.getSession("site_currency", FarecalcAct.this) + " " + f_farediscount);
					promoLayout.setVisibility(View.VISIBLE);
					vid_discount.setVisibility(View.VISIBLE);
				}
				else
				{
					promoLayout.setVisibility(View.GONE);
					vid_discount.setVisibility(View.GONE);
				}
				if (promoamt.equals("0"))
				{
					promoLayout.setVisibility(View.GONE);
				}
				metricTxt.setText(f_metric);
				actdistanceTxt.setText(f_distance);
				b_pickuplocation.setText(f_pickup);
				if (SessionSave.getSession("drop_location", FarecalcAct.this).length() != 0)
					b_droplocation.setText(SessionSave.getSession("drop_location", FarecalcAct.this));
				else
					b_droplocation.setText(drop_location);
				b_total_amt_curency.setText("" + SessionSave.getSession("site_currency", FarecalcAct.this));
				b_waitingcost.setText("" + SessionSave.getSession("site_currency", FarecalcAct.this) + " " + f_waitingcost);
				b_tax.setText("" + SessionSave.getSession("site_currency", FarecalcAct.this) + " " + f_taxamount);
				b_roundtrip.setText("" + json.getString("roundtrip"));
				tipsTxt.setHint("0");
				remarks.setText("" + objLocationDb.getdistance(f_tripid));
				if (SessionSave.getSession("site_currency", FarecalcAct.this) != null)
				{
					b_farecalCurrency.setText(SessionSave.getSession("site_currency", FarecalcAct.this));
					b_tipsCurrency.setText(SessionSave.getSession("site_currency", FarecalcAct.this));
				}
				if (farecalTxt.length() != 0)
				{
					f_fare = Double.parseDouble(farecalTxt.getText().toString());
				}
				if (tipsTxt.length() != 0)
				{
					f_tips = Double.parseDouble(tipsTxt.getText().toString());
				}
				f_total = f_fare + f_tips;
				totalamountTxt.setText("" + f_totalfare);
				JSONArray ary = new JSONArray(json.getString("gateway_details"));
				// the following code for handle the payment mode dynamically.
				int length = ary.length();
				for (int i = 0; i < length; i++)
				{
					String paymentModeDefault = ary.getJSONObject(i).getString("pay_mod_default");
					String paymentMode_Id = ary.getJSONObject(i).getString("pay_mod_id");
					if (paymentMode_Id.equalsIgnoreCase("1"))
					{
						radiocashButton.setVisibility(View.VISIBLE);
						if (paymentModeDefault.equals("1"))
						{
							radiocashButton.setTextColor(Color.DKGRAY);
						}
					}
					else if (paymentMode_Id.equalsIgnoreCase("2"))
					{
						TextView cardtxt = (TextView) findViewById(R.id.txtline1);
						cardtxt.setVisibility(View.VISIBLE);
						radiocardButton.setVisibility(View.VISIBLE);
						if (paymentModeDefault.equals("1"))
						{
							radiocardButton.setTextColor(Color.DKGRAY);
						}
					}
					else if (paymentMode_Id.equalsIgnoreCase("3"))
					{
						TextView cardtxt2 = (TextView) findViewById(R.id.txtline2);
						cardtxt2.setVisibility(View.VISIBLE);
						radiouncardButton.setVisibility(View.VISIBLE);
						if (paymentModeDefault.equals("1"))
						{
							radiouncardButton.setTextColor(Color.DKGRAY);
						}
					}
					else if (paymentMode_Id.equalsIgnoreCase("4"))
					{
						TextView cardtxt3 = (TextView) findViewById(R.id.txtline3);
						cardtxt3.setVisibility(View.VISIBLE);
						radioaccButton.setVisibility(View.VISIBLE);
						if (paymentModeDefault.equals("1"))
						{
							radioaccButton.setTextColor(Color.DKGRAY);
						}
					}
				}
			}
			catch (JSONException e)
			{
				e.printStackTrace();
			}
		}
		// The following process will done while select the payment mode as cash.
		radiocashButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				radiocashButton.setTextColor(Color.DKGRAY);
				radiocardButton.setTextColor(Color.LTGRAY);
				radiouncardButton.setTextColor(Color.LTGRAY);
				radioaccButton.setTextColor(Color.LTGRAY);
				if (farecalTxt.length() != 0)
				{
					f_fare = Double.parseDouble(farecalTxt.getText().toString());
				}
				if (tipsTxt.length() != 0)
				{
					f_tips = Double.parseDouble(tipsTxt.getText().toString());
				}
				f_total = f_fare + f_tips;
				// totalamountTxt.setText("" + Double.toString(f_total));
				f_paymodid = "1";
				callurl();
			}
		});
		// The following process will done while select the payment mode as card. And it shows the dialog to get the CVV number.
		radiocardButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				radiocashButton.setTextColor(Color.LTGRAY);
				radiocardButton.setTextColor(Color.DKGRAY);
				radiouncardButton.setTextColor(Color.LTGRAY);
				radioaccButton.setTextColor(Color.LTGRAY);
				if (farecalTxt.length() != 0)
				{
					f_fare = Double.parseDouble(farecalTxt.getText().toString());
				}
				if (tipsTxt.length() != 0)
				{
					f_tips = Double.parseDouble(tipsTxt.getText().toString());
				}
				f_total = f_fare + f_tips;
				// totalamountTxt.setText("" + Double.toString(f_total));
				f_paymodid = "2";
				if (f_creditcard.equalsIgnoreCase("1"))
				{
					try
					{
						final View view = View.inflate(FarecalcAct.this, R.layout.forgot_popup, null);
						mDialog = new Dialog(FarecalcAct.this, R.style.dialogwinddow);
						FontHelper.applyFont(FarecalcAct.this, mDialog.findViewById(R.id.inner_content), "DroidSans.ttf");
						mDialog.setContentView(view);
						mDialog.setCancelable(true);
						mDialog.show();
						final TextView t = (TextView) mDialog.findViewById(R.id.message);
						final EditText mail = (EditText) mDialog.findViewById(R.id.forgotmail);
						final Button OK = (Button) mDialog.findViewById(R.id.okbtn);
						final Button Cancel = (Button) mDialog.findViewById(R.id.cancelbtn);
						t.setText("" + getResources().getString(R.string.ent_cvv));
						mail.setHint("" + getResources().getString(R.string.ent_cvv));
						mail.setInputType(InputType.TYPE_CLASS_NUMBER);
						int maxLength = 3;
						InputFilter[] FilterArray = new InputFilter[1];
						FilterArray[0] = new InputFilter.LengthFilter(maxLength);
						mail.setFilters(FilterArray);
						OK.setOnClickListener(new OnClickListener()
						{
							@Override
							public void onClick(final View v)
							{
								// TODO Auto-generated method stub
								Cvv = mail.getText().toString().trim();
								if (Cvv.length() == 0)
									alert_view(FarecalcAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.ent_cvv), "" + getResources().getString(R.string.ok), "");
								else if (Cvv.length() < 3)
									alert_view(FarecalcAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.ent_chk_cvv), "" + getResources().getString(R.string.ok), "");
								else
								{
									mDialog.dismiss();
									callurl();
								}
							}
						});
						Cancel.setOnClickListener(new OnClickListener()
						{
							@Override
							public void onClick(final View v)
							{
								// TODO Auto-generated method stub
								mDialog.dismiss();
							}
						});
					}
					catch (Exception e)
					{
						// TODO: handle exception
						e.printStackTrace();
					}
				}
				else
				{
					callurl();
				}
			}
		});
		// The following process will done while select the payment mode as uncard.
		radiouncardButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
			/*	radiocashButton.setTextColor(Color.LTGRAY);
				radiocardButton.setTextColor(Color.LTGRAY);
				radiouncardButton.setTextColor(Color.DKGRAY);
				radioaccButton.setTextColor(Color.LTGRAY);
				Intent payintent = new Intent(FarecalcAct.this, PayuncardAct.class);
				Bundle bun = new Bundle();
				bun.putString("info", "Uncard");
				bun.putString("message", message);
				bun.putString("f_fare", Double.toString(f_fare));
				bun.putString("f_tips", Double.toString(f_tips));
				bun.putString("f_total", Double.toString(f_total));
				payintent.putExtras(bun);
				startActivity(payintent);*/
			}
		});
		
		setonclickListener();
	}

	private void setonclickListener()
	{
		paycashTxt.setOnClickListener(this);
		paycardTxt.setOnClickListener(this);
		payuncardTxt.setOnClickListener(this);
		accountTxt.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		// TODO Auto-generated method stub
		try
		{
			if (v == paycashTxt)
			{
				if (farecalTxt.length() > 1)
				{
					if (farecalTxt.length() != 0)
					{
						f_fare = Double.parseDouble(farecalTxt.getText().toString());
					}
					if (tipsTxt.length() != 0)
					{
						f_tips = Double.parseDouble(tipsTxt.getText().toString());
					}
					f_total = f_fare + f_tips;
					totalamountTxt.setText("" + Double.toString(f_total));
					f_paymodid = "1";
					callurl();
				}
				else
				{
					alert_view(FarecalcAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.ent_valid_amt), "" + getResources().getString(R.string.ok), "");
				}
			}
			else if (v == paycardTxt)
			{
				if (farecalTxt.length() > 1)
				{
					if (farecalTxt.length() != 0)
					{
						f_fare = Double.parseDouble(farecalTxt.getText().toString());
					}
					if (tipsTxt.length() != 0)
					{
						f_tips = Double.parseDouble(tipsTxt.getText().toString());
					}
					f_total = f_fare + f_tips;
					totalamountTxt.setText("" + Double.toString(f_total));
					f_paymodid = "2";
					callurl();
				}
				else
				{
					alert_view(FarecalcAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.ent_valid_amt), "" + getResources().getString(R.string.ok), "");
				}
			}
			else if (v == payuncardTxt)
			{
				if (farecalTxt.length() > 1)
				{
				/*	Intent payintent = new Intent(FarecalcAct.this, PayuncardAct.class);
					Bundle bun = new Bundle();
					bun.putString("info", "Uncard");
					bun.putString("message", message);
					bun.putString("f_fare", Double.toString(f_fare));
					bun.putString("f_tips", Double.toString(f_tips));
					bun.putString("f_total", Double.toString(f_total));
					payintent.putExtras(bun);
					startActivity(payintent);*/
				}
				else
				{
					alert_view(FarecalcAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.ent_valid_amt), "" + getResources().getString(R.string.ok), "");
				}
				if (istipsTxt_focus && tipsTxt.length() > 0)
				{
					int end_tipsTxt = tipsTxt.getSelectionEnd();
					if (end_tipsTxt != 0)
					{
						String s_tipsTxt = tipsTxt.getText().toString();
						s_tipsTxt = s_tipsTxt.substring(0, end_tipsTxt - 1) + s_tipsTxt.substring(end_tipsTxt);
						tipsTxt.setText(s_tipsTxt);
						tipsTxt.setSelection(tipsTxt.length());
					}
				}
			}
			else if (v == accountTxt)
			{
				if (farecalTxt.length() != 0)
				{
					f_fare = Double.parseDouble(farecalTxt.getText().toString());
				}
				if (tipsTxt.length() != 0)
				{
					f_tips = Double.parseDouble(tipsTxt.getText().toString());
				}
				f_total = f_fare + f_tips;
				totalamountTxt.setText("" + Double.toString(f_total));
				f_paymodid = "4";
			}
		}
		catch (Exception e)
		{
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	// Common API for fareupdate the following method for arrange the inputs and calls the API.
	private void callurl()
	{
		String url = "tripfare_update";
		try
		{
			JSONObject j = new JSONObject();
			j.put("trip_id", f_tripid);
			j.put("distance", f_distance);
			j.put("actual_distance", MainActivity.mMyStatus.getdistance());
			j.put("actual_amount", "" + f_total);
			j.put("trip_fare", f_tripfare);
			j.put("fare", "" + totalamountTxt.getText().toString());
			j.put("tips", "" + tipsTxt.getText().toString());
			j.put("passenger_promo_discount", promotax);
			j.put("tax_amount", f_taxamount);
			j.put("remarks", "");
			j.put("nightfare_applicable", f_nightfareapplicable);
			j.put("nightfare", f_nightfare);
			j.put("waiting_time", f_waitingtime);
			j.put("waiting_cost", f_waitingcost);
			j.put("creditcard_no", "");
			j.put("creditcard_cvv", Cvv);
			j.put("expmonth", "");
			j.put("expyear", "");
			j.put("pay_mod_id", f_paymodid);
			j.put("passenger_discount", p_dis);
			j.put("minutes_traveled", f_minutes_traveled);
			j.put("minutes_fare", f_minutes_fare);
			new FareUpdate(url, j);
		}
		catch (Exception e)
		{
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	// This class helps to call the Fare Update API,get the result and parse it.
	private class FareUpdate implements APIResult
	{
		String msg = "";

		public FareUpdate(String url, JSONObject data)
		{
			if (isOnline())
			{
				new APIService_Volley_JSON(FarecalcAct.this, this, data, false).execute(url);
			}
			else
			{
				alert_view(FarecalcAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_internet), "" + getResources().getString(R.string.ok), "");
			}
		}

		@Override
		public void getResult(boolean isSuccess, String result)
		{
			if (isSuccess)
			{
				try
				{
					JSONObject json = new JSONObject(result);
					if (json.getInt("status") == 1)
					{
						SessionSave.saveSession("travel_status", "", FarecalcAct.this);
						SessionSave.saveSession("trip_id", "", FarecalcAct.this);
						SessionSave.saveSession("status", "F", FarecalcAct.this);
						MainActivity.mMyStatus.setdistance("");
						msg = json.getString("message");
						MainActivity.mMyStatus.setOnstatus("");
						MainActivity.mMyStatus.setStatus("F");
						SessionSave.saveSession("status", "F", FarecalcAct.this);
						MainActivity.mMyStatus.setOnPassengerImage("");
						MainActivity.mMyStatus.setOnstatus("On");
						MainActivity.mMyStatus.setOnstatus("Completed");
						MainActivity.mMyStatus.setOnpassengerName("");
						MainActivity.mMyStatus.setOndropLocation("");
						MainActivity.mMyStatus.setOndropLocation("");
						MainActivity.mMyStatus.setOnpickupLatitude("");
						MainActivity.mMyStatus.setOnpickupLongitude("");
						MainActivity.mMyStatus.setOndropLatitude("");
						MainActivity.mMyStatus.setOndropLongitude("");
						JSONObject jsonDriver = json.getJSONObject("driver_statistics");
						SessionSave.saveSession("driver_statistics", "" + jsonDriver, FarecalcAct.this);
						CommonData.hstravel_km = "";
						SessionSave.saveSession("waitingHr", "", FarecalcAct.this);
						Intent jobintent = new Intent(FarecalcAct.this, JobdoneAct.class);
						Bundle bun = new Bundle();
						bun.putString("message", result);
						jobintent.putExtras(bun);
						startActivity(jobintent);
						finish();
					}
					else if (json.getInt("status") == -9)
					{
						msg = json.getString("message");
						lay_fare.setVisibility(View.VISIBLE);
						alert_view(FarecalcAct.this, "" + getResources().getString(R.string.message), "" + msg, "" + getResources().getString(R.string.ok), "");
					}
					else if (json.getInt("status") == 0)
					{
						msg = json.getString("message");
						lay_fare.setVisibility(View.VISIBLE);
						alert_view(FarecalcAct.this, "" + getResources().getString(R.string.message), "" + msg, "" + getResources().getString(R.string.ok), "");
					}
					else if (json.getInt("status") == -1)
					{
						msg = json.getString("message");
						alert_view(FarecalcAct.this, "" + getResources().getString(R.string.message), "" + msg, "" + getResources().getString(R.string.ok), "");
						SessionSave.saveSession("trip_id", "", FarecalcAct.this);
						SessionSave.saveSession("status", "F", FarecalcAct.this);
						JSONObject jsonDriver = json.getJSONObject("driver_statistics");
						SessionSave.saveSession("driver_statistics", "" + jsonDriver, FarecalcAct.this);
						MainActivity.mMyStatus.setOnstatus("");
						MainActivity.mMyStatus.setStatus("F");
						SessionSave.saveSession("status", "F", FarecalcAct.this);
						MainActivity.mMyStatus.setOnPassengerImage("");
						MainActivity.mMyStatus.setOnstatus("On");
						MainActivity.mMyStatus.setOnstatus("Completed");
						MainActivity.mMyStatus.setOnpassengerName("");
						MainActivity.mMyStatus.setOndropLocation("");
						MainActivity.mMyStatus.setOndropLocation("");
						MainActivity.mMyStatus.setOnpickupLatitude("");
						MainActivity.mMyStatus.setOnpickupLongitude("");
						MainActivity.mMyStatus.setOndropLatitude("");
						MainActivity.mMyStatus.setOndropLongitude("");
						MainActivity.mMyStatus.setOndriverLatitude("");
						MainActivity.mMyStatus.setOndriverLongitude("");
						Intent intent = new Intent(FarecalcAct.this, HomeActivity.class);
						startActivity(intent);
						finish();
					}
					else
					{
						msg = json.getString("message");
						lay_fare.setVisibility(View.VISIBLE);
						alert_view(FarecalcAct.this, "" + getResources().getString(R.string.message), "" + msg, "" + getResources().getString(R.string.ok), "");
					}
				}
				catch (Exception e)
				{
				}
			}
			else
			{
				alert_view(FarecalcAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_internet), "" + getResources().getString(R.string.ok), "");
				lay_fare.setVisibility(View.VISIBLE);
			}
		}
	}

	// This class is used to get the fare details.
	private class CompleteTrip implements APIResult
	{
		public CompleteTrip(String url, String latitude, String longitude, String distance, String waitingHr, String drop_location)
		{
			try
			{
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(7);
				nameValuePairs.add(new BasicNameValuePair("trip_id", SessionSave.getSession("trip_id", FarecalcAct.this)));
				nameValuePairs.add(new BasicNameValuePair("drop_latitude", latitude));
				nameValuePairs.add(new BasicNameValuePair("drop_longitude", longitude));
				nameValuePairs.add(new BasicNameValuePair("drop_location", drop_location));
				nameValuePairs.add(new BasicNameValuePair("distance", distance));
				nameValuePairs.add(new BasicNameValuePair("actual_distance", ""));
				nameValuePairs.add(new BasicNameValuePair("waiting_hour", waitingHr));
				JSONObject j = new JSONObject();
				j.put("trip_id", SessionSave.getSession("trip_id", FarecalcAct.this));
				j.put("drop_latitude", latitude);
				j.put("drop_longitude", longitude);
				j.put("drop_location", drop_location);
				j.put("distance", distance);
				j.put("actual_distance", "");
				j.put("waiting_hour", waitingHr);
				new APIService_Volley_JSON(FarecalcAct.this, this, j, false).execute(url);
			}
			catch (Exception e)
			{
				// TODO: handle exception
			}
		}

		@Override
		public void getResult(boolean isSuccess, String result)
		{
			if (isSuccess)
			{
				try
				{
					message = result;
					setFareCalculatorScreen();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	}
}
