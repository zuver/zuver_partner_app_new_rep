package com.zuver.driver;

import android.app.Dialog;
import android.content.Intent;
import android.provider.Settings.Secure;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.zuver.driver.data.CommonData;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.service.APIService_Volley_JSON;
import com.zuver.driver.service.LocationUpdate;
import com.zuver.driver.utils.FontHelper;
import com.zuver.driver.utils.SessionSave;

import org.json.JSONException;
import org.json.JSONObject;

public class UserLoginAct extends MainActivity
{
    // Class members declarations.
    private EditText PhoneEdt;
    private EditText PasswordEdt;
    private TextView ForgotTxt;
    private TextView CancelBtn;
    private TextView DoneBtn;
    private TextView HeadTitle;
    private String phone;
    private String password;
    private SplashAct mSplash;
    private ImageView driver_image;
    private String mobile, email;

    // Set the layout to activity.
    @Override
    public int setLayout()
    {
        setLocale();
        return R.layout.signin;
    }

    // Initialize the views on layout
    @Override
    public void Initialize()
    {
        mSplash = new SplashAct();
        FontHelper.applyFont(this, findViewById(R.id.signinlayout), "DroidSans.ttf");
        CommonData.current_act = "SplashAct";
        CancelBtn = (TextView) findViewById(R.id.cancelBtn);
        CancelBtn.setVisibility(View.GONE);
        DoneBtn = (TextView) findViewById(R.id.doneBtn);
        HeadTitle = (TextView) findViewById(R.id.signup_title);
        DoneBtn.setText("" + getResources().getString(R.string.next));
        HeadTitle.setText("" + getResources().getString(R.string.s_signin));
        PhoneEdt = (EditText) findViewById(R.id.phoneEdt);
        PasswordEdt = (EditText) findViewById(R.id.passwordEdt);

        driver_image = (ImageView) findViewById(R.id.dash_image);

        if (!SessionSave.getSession("phone_number", UserLoginAct.this).equals("") && !SessionSave.getSession("driver_password", UserLoginAct.this).equals(""))
        {
            PhoneEdt.setText(SessionSave.getSession("phone_number", UserLoginAct.this));
            PasswordEdt.setText(SessionSave.getSession("driver_password", UserLoginAct.this));
        }

        ForgotTxt = (TextView) findViewById(R.id.forgotpswdTxt);
        CommonData.mDevice_id = Secure.getString(getContentResolver(), Secure.ANDROID_ID);

        ForgotTxt.setOnClickListener(new OnClickListener()
        {
            private Dialog mDialog;

            @Override
            public void onClick(final View v)
            {
                final View view = View.inflate(UserLoginAct.this, R.layout.forgot_popup, null);
                mDialog = new Dialog(UserLoginAct.this, R.style.NewDialog);
                mDialog.setContentView(view);
                FontHelper.applyFont(UserLoginAct.this, mDialog.findViewById(R.id.inner_content), "DroidSans.ttf");
                mDialog.setCancelable(true);
                mDialog.show();

                final EditText mail = (EditText) mDialog.findViewById(R.id.forgotmail);
                final Button OK = (Button) mDialog.findViewById(R.id.okbtn);
                final Button Cancel = (Button) mDialog.findViewById(R.id.cancelbtn);

                OK.setOnClickListener(new OnClickListener()
                {
                    private String Email;

                    @Override
                    public void onClick(final View v)
                    {
                        try
                        {
                            Email = mail.getText().toString();

                            if (validations(ValidateAction.isValueNULL, UserLoginAct.this, Email))
                            {
                                JSONObject j = new JSONObject();
                                j.put("phone_no", Email);
                                j.put("user_type", "D");
                                final String url = "forgot_password";
                                new ForgotPassword(url, j);
                                mail.setText("");
                                mDialog.dismiss();
                            }
                        }
                        catch (Exception e)
                        {
                            // TODO: handle exception
                            e.printStackTrace();
                        }
                    }
                });

                Cancel.setOnClickListener(new OnClickListener()
                {
                    @Override
                    public void onClick(final View v) {
                        mDialog.dismiss();
                    }
                });
            }
        });

        DoneBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                mobile = "";
                email = "";
                phone = PhoneEdt.getText().toString().trim();

                if (isNumeric(phone))
                {
                    mobile = phone;
                }
                else
                {
                    email = phone;
                }

                if (validations(ValidateAction.isValueNULL, UserLoginAct.this, phone))
                {
                    if (validations(ValidateAction.isValidPassword, UserLoginAct.this, PasswordEdt.getText().toString().trim()))
                    {
                        SessionSave.saveSession("phone_number", phone, UserLoginAct.this);
                        SessionSave.saveSession("driver_password", PasswordEdt.getText().toString().trim(), UserLoginAct.this);
                        password = PasswordEdt.getText().toString().trim();
                        final String url = "driver_login";
                        new SignIn(url);
                    }
                }
            }
        });
        CancelBtn.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(final View v)
            {
                final Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    /**
     * ForgotPassword API response parsing.
     */
    private class ForgotPassword implements APIResult
    {
        public ForgotPassword(String url, JSONObject data) {
            if (isOnline()) {
                new APIService_Volley_JSON(UserLoginAct.this, this, data, false).execute(url);
            } else {
                alert_view(UserLoginAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
            }
        }

        @Override
        public void getResult(final boolean isSuccess, final String result) {
            if (isSuccess) {
                try {
                    final JSONObject json = new JSONObject(result);
                    if (json.getInt("status") == 1)
                        alert_view(UserLoginAct.this, "" + getResources().getString(R.string.message), "" + json.getString("message"), "" + getResources().getString(R.string.ok), "");
                    else
                        alert_view(UserLoginAct.this, "" + getResources().getString(R.string.message), "" + json.getString("message"), "" + getResources().getString(R.string.ok), "");
                } catch (final JSONException e) {
                    e.printStackTrace();
                }
            } else {
                alert_view(UserLoginAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
            }
        }
    }

    /**
     * Signin post method API call and response parsing.
     */
    private class SignIn implements APIResult
    {
        public SignIn(final String url)
        {
            try
            {
                JSONObject j = new JSONObject();
                j.put("email", email);
                j.put("phone", mobile);
                j.put("password", password);
                j.put("device_id", CommonData.mDevice_id);
                j.put("device_token", "");
                j.put("device_type", "1");
                /*j.put("latitude", SessionSave.getSession("mLatitude", context));
                j.put("longitude", SessionSave.getSession("mLongitude", context));*/
                new APIService_Volley_JSON(UserLoginAct.this, this, j, false).execute(url);
            }
            catch (Exception e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        @Override
        public void getResult(final boolean isSuccess, final String result)
        {
//			Log.e("", "");
            try
            {
                if (isSuccess)
                {
                    final JSONObject json = new JSONObject(result);

                    if (json.getInt("status") == 1)
                    {
                        json.getJSONObject("detail").getJSONObject("driver_details").getString("shift_status");
                        JSONObject data = json.getJSONObject("detail");
                        JSONObject driver_details = data.getJSONObject("driver_details");
                        JSONObject auth_details = driver_details.getJSONObject("auth_details");

                        SessionSave.saveSession("Auth_Key", auth_details.getString("authKey"), UserLoginAct.this);
                        SessionSave.saveSession("User_Type", auth_details.getString("userType"), UserLoginAct.this);

                        SessionSave.saveSession("Email", driver_details.getString("email"), UserLoginAct.this);
                        SessionSave.saveSession("Id", driver_details.getString("userid"), UserLoginAct.this);
                        SessionSave.saveSession("Lastname", driver_details.getString("lastname"), UserLoginAct.this);
                        SessionSave.saveSession("Phone", driver_details.getString("phone"), UserLoginAct.this);
                        SessionSave.saveSession("Name", driver_details.getString("name"), UserLoginAct.this);
                        SessionSave.saveSession("Bankname", driver_details.getString("bankname"), UserLoginAct.this);
                        SessionSave.saveSession("Bankaccount_No", driver_details.getString("bankaccount_no"), UserLoginAct.this);
                        SessionSave.saveSession("Salutation", "", UserLoginAct.this);
                        SessionSave.saveSession("taxi_id", driver_details.getString("taxi_id"), UserLoginAct.this);
                        SessionSave.saveSession("company_id", driver_details.getString("company_id"), UserLoginAct.this);

                        if (driver_details.getString("driver_status").equalsIgnoreCase("") || driver_details.getString("driver_status").equalsIgnoreCase("N"))
                        {
                            SessionSave.saveSession("status", "F", UserLoginAct.this);
                        }
                        else
                        {
                            SessionSave.saveSession("status", driver_details.getString("driver_status"), UserLoginAct.this);
                        }

                        SessionSave.saveSession("start_auto_logout", "0", UserLoginAct.this);
                        SessionSave.saveSession("Shiftupdate_Id", driver_details.getString("shiftupdate_id"), UserLoginAct.this);

                        if (!driver_details.getString("shiftupdate_id").equals(""))
                            SessionSave.saveSession("driver_shift", "IN", UserLoginAct.this);

                        SessionSave.saveSession("driver_profileimage", driver_details.getString("profile_picture"), UserLoginAct.this);
                        SessionSave.saveSession("Register", "", UserLoginAct.this);

                        if (!driver_details.getString("trip_id").equals(""))
                        {
                            SessionSave.saveSession("trip_id", driver_details.getString("trip_id"), UserLoginAct.this);
                            MainActivity.mMyStatus.settripId(driver_details.getString("trip_id"));
                            SessionSave.saveSession("status", driver_details.getString("driver_status"), UserLoginAct.this);
                            SessionSave.saveSession("travel_status", driver_details.getString("travel_status"), UserLoginAct.this);
                        }

                        JSONObject driver_statistics = driver_details.getJSONObject("driver_statistics");
                        SessionSave.saveSession("driver_statistics", "" + driver_statistics, UserLoginAct.this);
                        SessionSave.saveSession("Version_Update", "0", UserLoginAct.this);
                        SessionSave.saveSession("shift_status", driver_statistics.getString("shift_status"), UserLoginAct.this);

                        pop_up(driver_statistics);
                    }
                    else if (json.getInt("status") == -5)
                        alert_view(UserLoginAct.this, "" + getResources().getString(R.string.message), "" + json.getString("message"), "" + getResources().getString(R.string.ok), "");
                    else
                        alert_view(UserLoginAct.this, "" + getResources().getString(R.string.message), "" + json.getString("message"), "" + getResources().getString(R.string.ok), "");
                }
                else
                    alert_view(UserLoginAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
            }
            catch (final JSONException e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        final Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    Dialog mDialog;

    /**
     * Function: To show the success popup dialog and move to dashboard
     */
    public void pop_up(final JSONObject jsonDriverObject)
    {
        final View view = View.inflate(UserLoginAct.this, R.layout.forgot_popup, null);
        mDialog = new Dialog(UserLoginAct.this, R.style.NewDialog);
        mDialog.setContentView(view);
        FontHelper.applyFont(UserLoginAct.this, mDialog.findViewById(R.id.inner_content), "DroidSans.ttf");
        mDialog.setCancelable(false);
        mDialog.show();
        final EditText mail = (EditText) mDialog.findViewById(R.id.forgotmail);
        mail.setInputType(InputType.TYPE_CLASS_NUMBER);
        mail.setVisibility(View.GONE);
        final Button OK = (Button) mDialog.findViewById(R.id.okbtn);
        final Button Cancel = (Button) mDialog.findViewById(R.id.cancelbtn);
        Cancel.setText("" + getResources().getString(R.string.t_ok));
        Cancel.setBackgroundResource(R.drawable.draw_decline);
        OK.setVisibility(View.GONE);
        final TextView heading = (TextView) mDialog.findViewById(R.id.message);
        heading.setText("" + getResources().getString(R.string.t_success));
        final TextView message = (TextView) mDialog.findViewById(R.id.timetoleave);
        message.setVisibility(View.VISIBLE);
        message.setText("" + getResources().getString(R.string.login_success));
        Cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (!SessionSave.getSession("trip_id", UserLoginAct.this).equals("")) {
                    if (SessionSave.getSession("travel_status", UserLoginAct.this).equals("5")) {
                        SessionSave.saveSession("status", "A", UserLoginAct.this);
                        Intent in = new Intent(UserLoginAct.this, OngoingAct.class);
                        startActivity(in);
                        startService(new Intent(UserLoginAct.this, LocationUpdate.class));
                        finish();
                        dismissLoading();
                        mDialog.dismiss();
                    } else if (SessionSave.getSession("travel_status", UserLoginAct.this).equals("2")) {
                        SessionSave.saveSession("status", "A", UserLoginAct.this);
                        Intent in = new Intent(UserLoginAct.this, OngoingAct.class);
                        startActivity(in);
                        startService(new Intent(UserLoginAct.this, LocationUpdate.class));
                        finish();
                        dismissLoading();
                        mDialog.dismiss();
                    } else {
                        showLoading(UserLoginAct.this);
                        final Intent i = new Intent(UserLoginAct.this, DashAct.class);
                        startService(new Intent(UserLoginAct.this, LocationUpdate.class));
                        startActivity(i);
                        finish();
                        dismissLoading();
                        mDialog.dismiss();
                    }
                } else {
                    try {
                        showLoading(UserLoginAct.this);
                        final Intent i = new Intent(UserLoginAct.this, DashAct.class);
                        startService(new Intent(UserLoginAct.this, LocationUpdate.class));
                        startActivity(i);
                        finish();
                        dismissLoading();
                        mDialog.dismiss();
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public boolean isAlpha(String name) {
        return name.matches("[a-zA-Z0-9]+");
    }

    public boolean isNumeric(String name) {
        return name.matches("[0-9]+");
    }
}
