package com.zuver.driver;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;

import com.zuver.driver.data.CommonData;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.service.APIService_Volley_JSON;
import com.zuver.driver.service.NonActivity;
import com.zuver.driver.utils.FontHelper;
import com.zuver.driver.utils.SessionSave;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This is the common class for this application is used
 * for cancelling the trip in two different places.
 *
 * @author developer
 */
public class CancellationAct extends MainActivity
{
    private TextView txtHeaderBack, slideImg, headerTxt;

    private EditText edtComments;

    private CheckBox chckReason1, chckReason2, chckReason3,
            chckReason4;
    private String message = "", edtMessage;

    private String driverReply;

    private TextView btnCancel;

    boolean bookCommentFlag = false;
    NonActivity nonactiityobj = new NonActivity();

    @Override
    public int setLayout()
    {
        // TODO Auto-generated method stub
        setLocale();
        return R.layout.cancellation_lay;
    }

    @Override
    public void Initialize()
    {
        // TODO Auto-generated method stub

        btnCancel = (TextView) findViewById(R.id.btn_cancel);
        txtHeaderBack = (TextView) findViewById(R.id.backup);
        txtHeaderBack.setVisibility(View.GONE);

        txtHeaderBack.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                Intent jobintent = new Intent(CancellationAct.this, HomeActivity.class);
                startActivity(jobintent);
            }
        });

        nonactiityobj.startServicefromNonActivity(CancellationAct.this);
        chckReason1 = (CheckBox) findViewById(R.id.chk_reason1);
        chckReason2 = (CheckBox) findViewById(R.id.chk_reason2);
        chckReason3 = (CheckBox) findViewById(R.id.chk_reason3);
        chckReason4 = (CheckBox) findViewById(R.id.chk_others);
        edtComments = (EditText) findViewById(R.id.edt_comments);

        slideImg = (TextView) findViewById(R.id.backup);
        headerTxt = (TextView) findViewById(R.id.headerTxt);
        headerTxt.setText(getResources().getString(R.string.cancel_alert));
        slideImg.setVisibility(View.GONE);


        try
        {
            if (getIntent().getStringExtra("DriverReply") != null)
            {
                driverReply = getIntent().getStringExtra("DriverReply");
//				Log.e("", "");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        if (SessionSave.getSession("BackCancelTrip", CancellationAct.this).equalsIgnoreCase("BackCancelTrip"))
        {
            slideImg.setVisibility(View.VISIBLE);
        }

        slideImg.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                finish();
            }
        });


        chckReason1.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                    {
                        edtComments.setText("");
                        // TODO Auto-generated method stub
                    }
                });

        chckReason2.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                    {
                        edtComments.setText("");
                        // TODO Auto-generated method stub
                    }
                });

        chckReason3.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                    {
                        edtComments.setText("");
                        // TODO Auto-generated method stub
                    }
                });

        chckReason4.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                    {
                        // TODO Auto-generated method stub
                        message = getResources().getString(R.string.missed_notifiy4);

                        if (isChecked)
                        {
                            bookCommentFlag = true;
                        }
                        else
                        {
                            bookCommentFlag = false;
                        }
                    }
                });

        btnCancel.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                if (chckReason1.isChecked())
                {
                    message = getResources().getString(R.string.cancel_reason1);
                }
                else if (chckReason2.isChecked())
                {
                    message = getResources().getString(R.string.cancel_reason2);
                }
                else if (chckReason4.isChecked())
                {
                    message = edtComments.getText().toString();
                }

                edtMessage = edtComments.getText().toString();
                Log.e("asdf", "sadf" + edtMessage);

                if (bookCommentFlag)
                {
                    if (edtMessage.equals(""))
                    {
//						message = "";
                        alert_view(CancellationAct.this, getResources().getString(R.string.message), getResources().getString(R.string.comment_fields), getResources().getString(R.string.ok), null);
                        return;
                    }

                }
                else
                {
                    if (message == null || message.equals(""))
                    {
                        alert_view(CancellationAct.this, getResources().getString(R.string.message), "Please Pick the reason from the list", "OK", "");
                        return;
                    }
                }

                Log.e("Trip ID", "Trip ID" + SessionSave.getSession("trip_id", CancellationAct.this));

                if (CommonData.isGetNotificationDriverDecline())
                {
                    sendTripRejectReason();
                }
                else
                {
                    cancelTripAPI();
                }
//				cancelTripAPI();
//				sendTripRejectReason();
            }
        });
    }


    public void sendTripRejectReason()
    {
        if (message == null || message.equals(""))
        {
            alert_view(CancellationAct.this, getResources().getString(R.string.message), "Please Pick the reason from the list", "OK", "");
            return;
        }

        JSONObject j = new JSONObject();

        try
        {
            String trip_id = MainActivity.mMyStatus.gettripId();
            j.put("trip_id", trip_id);
            j.put("driver_id", SessionSave.getSession("Id", CancellationAct.this));
            j.put("reason", "" + message);
            j.put("reject_type", "1");
            final String Url = "reject_trip_reason";
            new TripReject(Url, j);
        }
        catch (JSONException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private class TripReject implements APIResult
    {
        String msg;

        public TripReject(final String url, JSONObject data)
        {
            // TODO Auto-generated constructor stub
            new APIService_Volley_JSON(CancellationAct.this, this, data, false).execute(url);
        }

        @Override
        public void getResult(final boolean isSuccess, final String result)
        {
            // TODO Auto-generated method stub
            showLog("Reject:" + result);

            try
            {
                if (isSuccess)
                {
                    nonactiityobj.startServicefromNonActivity(CancellationAct.this);
                    final JSONObject json = new JSONObject(result);

                    if (json.getInt("status") == 1)
                    {
                        CommonData.isSetNotificationDriverDecline(false);
                        alert_view_pickup(CancellationAct.this, ""
                                        + getResources().getString(R.string.message),
                                "" + json.getString("message"),
                                "" + getResources().getString(R.string.ok), "");

                        SessionSave.saveSession("trip_id", "", CancellationAct.this);
                        JSONObject jsonDriver = json.getJSONObject("driver_statistics");
                        SessionSave.saveSession("driver_statistics", "" + jsonDriver, CancellationAct.this);
                    }
                    else if (json.getInt("status") == -1)
                    {
                        //User has been logged out for any reason
                        askForLogin();
                    }
                }
                else
                    alert_view(
                            CancellationAct.this,
                            "" + getResources().getString(R.string.message),
                            ""
                                    + getResources().getString(
                                    R.string.check_net_connection), ""
                                    + getResources().getString(R.string.ok), "");
            }
            catch (final JSONException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void alert_view_pickup(Context mContext, String title, String message, String success_txt, String failure_txt)
    {
        final View view = View.inflate(mContext, R.layout.alert_view, null);
        alertDialog = new Dialog(mContext, R.style.NewDialog);
        alertDialog.setContentView(view);
        alertDialog.setCancelable(true);
        FontHelper.applyFont(mContext, alertDialog.findViewById(R.id.alert_id), "DroidSans.ttf");
        alertDialog.show();

        final TextView title_text = (TextView) alertDialog.findViewById(R.id.title_text);
        final TextView message_text = (TextView) alertDialog.findViewById(R.id.message_text);
        final Button button_success = (Button) alertDialog.findViewById(R.id.button_success);

        title_text.setText(title);
        message_text.setText(message);
        button_success.setText(success_txt);

        button_success.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(final View v)
            {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
                showLoading(CancellationAct.this);
                Intent intent = new Intent(CancellationAct.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    /*
     * This is the method for cancelling the trip with user enter
     * details.
     */
    public void cancelTripAPI()
    {
        try
        {
            System.out.println("TRIP_ID session:" + SessionSave.getSession("trip_id", CancellationAct.this));

            // TODO Auto-generated method stub
            if (SessionSave.getSession("status", CancellationAct.this).equalsIgnoreCase("A"))
                alert_view(CancellationAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.you_are_in_trip), "" + getResources().getString(R.string.ok), "");
            else if (SessionSave.getSession("trip_id", CancellationAct.this).length() == 0)
                finish();
            else
            {
                nonactiityobj.stopServicefromNonActivity(CancellationAct.this);
                JSONObject j = new JSONObject();
                j.put("pass_logid", SessionSave.getSession("trip_id", CancellationAct.this));
                j.put("driver_id", SessionSave.getSession("Id", CancellationAct.this));
                j.put("taxi_id", SessionSave.getSession("taxi_id", CancellationAct.this));
                j.put("company_id", SessionSave.getSession("company_id", CancellationAct.this));
                j.put("driver_reply", driverReply);
                j.put("field", "" + message);
                j.put("flag", "1");
                Log.e("Cancellation", "Cancellation" + j.toString());
                final String canceltrip_url = "driver_reply";
                new CancelTrip(canceltrip_url, j);
            }
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    // This is the API call cancelling the trip with user provided datas. those datas are perserved for our maintainenance purpose.
    private class CancelTrip implements APIResult
    {
        private String msg;

        public CancelTrip(final String url, JSONObject data)
        {
            try
            {
                if (isOnline())
                {
                    new APIService_Volley_JSON(CancellationAct.this, this, data, false).execute(url);
                }
                else
                {
                    alert_view(CancellationAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
                }
            }
            catch (Exception e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        /**
         * Parse the response and update the UI.
         */
        @Override
        public void getResult(final boolean isSuccess, final String result)
        {
            try
            {
                if (isSuccess)
                {
                    final JSONObject json = new JSONObject(result);

                    if (json.getInt("status") == 3)
                    {
                        Log.e("Cancellation Act", "Cancellation Act" + json.toString());

                        msg = json.getString("message");
                        JSONObject jsonDriver = json.getJSONObject("driver_statistics");
                        SessionSave.saveSession("driver_statistics", "" + jsonDriver, CancellationAct.this);
                        SessionSave.saveSession("BackCancelTrip", "", CancellationAct.this);
                        SessionSave.saveSession("status", "F", CancellationAct.this);
                        MainActivity.mMyStatus.settripId("");
                        SessionSave.saveSession("trip_id", "", CancellationAct.this);
                        MainActivity.mMyStatus.setOnstatus("On");
                        MainActivity.mMyStatus.setOnPassengerImage("");
                        MainActivity.mMyStatus.setOnpassengerName("");
                        MainActivity.mMyStatus.setOndropLocation("");
                        MainActivity.mMyStatus.setPassengerOndropLocation("");
                        MainActivity.mMyStatus.setOnpickupLatitude("");
                        MainActivity.mMyStatus.setOnpickupLongitude("");
                        MainActivity.mMyStatus.setOndropLatitude("");
                        MainActivity.mMyStatus.setOndropLongitude("");
                        MainActivity.mMyStatus.setOndriverLatitude("");
                        MainActivity.mMyStatus.setOndriverLongitude("");
                        SessionSave.saveSession("Ongoing", "farecal", CancellationAct.this);

                        Intent intent = new Intent();
                        Bundle extras = new Bundle();
                        extras.putString("alert_message", json.getString("message"));
                        intent.putExtras(extras);

                        intent.setAction(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
                                | Intent.FLAG_ACTIVITY_NO_ANIMATION
                                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        ComponentName cn = new ComponentName(CancellationAct.this, HomeActivity.class);
                        intent.setComponent(cn);
                        startActivity(intent);
                        finish();
                    }
                    else if (json.getInt("status") == 2)
                    {
                        Log.e("Cancellation Act", "Cancellation Act" + json.toString());

                        msg = json.getString("message");
                        JSONObject jsonDriver = json.getJSONObject("driver_statistics");
                        SessionSave.saveSession("driver_statistics", "" + jsonDriver, CancellationAct.this);
                        SessionSave.saveSession("BackCancelTrip", "", CancellationAct.this);
                        SessionSave.saveSession("status", "F", CancellationAct.this);
                        MainActivity.mMyStatus.settripId("");
                        SessionSave.saveSession("trip_id", "", CancellationAct.this);
                        MainActivity.mMyStatus.setOnstatus("On");
                        MainActivity.mMyStatus.setOnPassengerImage("");
                        MainActivity.mMyStatus.setOnpassengerName("");
                        MainActivity.mMyStatus.setOndropLocation("");
                        MainActivity.mMyStatus.setPassengerOndropLocation("");
                        MainActivity.mMyStatus.setOnpickupLatitude("");
                        MainActivity.mMyStatus.setOnpickupLongitude("");
                        MainActivity.mMyStatus.setOndropLatitude("");
                        MainActivity.mMyStatus.setOndropLongitude("");
                        MainActivity.mMyStatus.setOndriverLatitude("");
                        MainActivity.mMyStatus.setOndriverLongitude("");
                        SessionSave.saveSession("Ongoing", "farecal", CancellationAct.this);

                        Intent intent = new Intent();
                        Bundle extras = new Bundle();
                        extras.putString("alert_message", json.getString("message"));
                        intent.putExtras(extras);

                        intent.setAction(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
                                | Intent.FLAG_ACTIVITY_NO_ANIMATION
                                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        ComponentName cn = new ComponentName(CancellationAct.this, HomeActivity.class);
                        intent.setComponent(cn);
                        startActivity(intent);
                        finish();
                    }
                    else if (json.getInt("status") == -1)
                    {
                        //User has been logged out for any reason
                        askForLogin();
                    }
                    else
                    {
                        msg = json.getString("message");
                        alert_view(CancellationAct.this, "" + getResources().getString(R.string.message), "" + msg, "" + getResources().getString(R.string.ok), "");
                    }
                }
                else
                {
                    alert_view(CancellationAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
                }
            }
            catch (final Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    public void OnChecked(View v)
    {
        switch (v.getId())
        {
            case R.id.chk_reason1:

                chckReason1.setChecked(true);
                chckReason2.setChecked(false);
                chckReason3.setChecked(false);
                chckReason4.setChecked(false);
                hide_keyboard(CancellationAct.this);
                edtComments.setEnabled(false);

                break;

            case R.id.chk_reason2:

                chckReason1.setChecked(false);
                chckReason2.setChecked(true);
                chckReason3.setChecked(false);
                chckReason4.setChecked(false);
                hide_keyboard(CancellationAct.this);
                edtComments.setEnabled(false);

                break;

            case R.id.chk_reason3:

                chckReason1.setChecked(false);
                chckReason2.setChecked(false);
                chckReason3.setChecked(true);
                chckReason4.setChecked(false);
                hide_keyboard(CancellationAct.this);
                edtComments.setEnabled(false);

                break;

            case R.id.chk_others:

                chckReason1.setChecked(false);
                chckReason2.setChecked(false);
                chckReason3.setChecked(false);
                chckReason4.setChecked(true);
                edtComments.setEnabled(true);

                break;

            default:
                break;
        }
    }

    /*
     * Hide the keyboard for required places.
     */
    private void hide_keyboard(Activity activity)
    {
        try
        {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            // Find the currently focused view, so we can grab the correct window
            // token from it.
            View view = activity.getCurrentFocus();

            // If no view currently has focus, create a new one, just so we can grab
            // a window token from it
            if (view == null)
            {
                view = new View(activity);
            }

            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed()
    {
        // TODO Auto-generated method stub
//		super.onBackPressed();
    }
}
