package com.zuver.driver.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.zuver.driver.MainActivity;
import com.zuver.driver.R;
import com.zuver.driver.SignAct;
import com.zuver.driver.data.CommonData;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.service.APIService_Volley_JSON;
import com.zuver.driver.service.LocationUpdate;
import com.zuver.driver.service.NonActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

/**
 * This BroadcastReceiver to update network connections is Available/Not.
 */
public class NetworkStatus extends BroadcastReceiver
{
    public Context mContext;
    private String message;
    private int INTERVAL_TIME = 15;
    private int INTERVAL_TIME_SEC = 10000;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        mContext = context;

        try
        {
            if (getConnectivityStatus(mContext))
            {
                message = " Network connection is Enable!!";

                if (!CommonData.current_act.equals("SplashAct"))
                {
                    generatNotification(message);
                }

                if (CommonData.sContext != null)
                {
                    MainActivity.isConnect(CommonData.sContext, true);
                }

                if (SessionSave.getSession("start_auto_logout", mContext).equals("1"))
                {
                    if (SessionSave.getSession("trip_id", mContext).equals(""))
                    {
                        try
                        {
                            JSONObject j = new JSONObject();
                            j.put("driver_id", SessionSave.getSession("Id", mContext));
                            j.put("shiftupdate_id", SessionSave.getSession("Shiftupdate_Id", mContext));
                            j.put("shift_end", SessionSave.getSession("date_time", mContext));
                            NonActivity nonactiityobj = new NonActivity();
                            nonactiityobj.stopServicefromNonActivity(mContext);
                            String url = "type=user_logout";
                            new Logout(url, j);
                        }
                        catch (Exception e)
                        {
                            // TODO: handle exception
                            e.printStackTrace();
                        }
                    }
                }
            }
            else
            {
                message = " Network connection is Disable!!";

                if (!CommonData.current_act.equals("SplashAct"))
                    generatNotification(message);

                if (CommonData.sContext != null)
                    MainActivity.isConnect(CommonData.sContext, false);

                new Thread(mytask).start();
            }
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    private Runnable mytask = new Runnable()
    {
        @Override
        public void run()
        {
            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();
            SessionSave.saveSession("date_time", ts, mContext);

            try
            {
                TimeUnit.MINUTES.sleep(INTERVAL_TIME);
                //Thread.sleep(INTERVAL_TIME);
                //Thread.sleep(15 * 60 * 1000);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            finally
            {
                if (!getConnectivityStatus(mContext))
                {
                    SessionSave.saveSession("start_auto_logout", "1", mContext);
                }
                else
                {
                    SessionSave.saveSession("start_auto_logout", "0", mContext);
                }
            }
        }
    };

    public static boolean isOnline(Context mContext2)
    {
        ConnectivityManager connectivity = (ConnectivityManager) mContext2.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();

            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }
        }
        return false;
    }

    public static boolean getConnectivityStatus(Context context)
    {
        boolean conn = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (null != activeNetwork)
        {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
            {
                if (activeNetwork.isConnected())
                {
                    conn = true;
                }
                else
                {
                    conn = false;
                }
            }
        }
        else
        {
            conn = false;
        }

        return conn;
    }

    @SuppressWarnings("deprecation")
    private void generatNotification(String message2)
    {
        // TODO Auto-generated method stub
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(R.drawable.app_icon, message, System.currentTimeMillis());
        String title = mContext.getString(R.string.app_name);
        Intent intent = new Intent(mContext, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(mContext, (int) System.currentTimeMillis(), intent, 0);
        Notification n = new Notification.Builder(mContext)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.drawable.app_icon)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .build();

        notificationManager.notify(0, n);
    }

    private class Logout implements APIResult
    {
        private Logout(String url, JSONObject data)
        {
            new APIService_Volley_JSON(mContext, this, data, false).execute(url);
        }

        @Override
        public void getResult(boolean isSuccess, String result)
        {
            if (isSuccess)
            {
                try
                {
                    JSONObject json = new JSONObject(result);

                    if (json.getInt("status") == 1)
                    {
                        Intent locationService = new Intent(mContext, LocationUpdate.class);
                        mContext.stopService(new Intent(locationService));
                        ///MainActivity.clearsession(mContext);
                        Intent intent = new Intent(mContext, SignAct.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        mContext.startActivity(intent);
                    }

                }
                catch (JSONException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }
}
