package com.zuver.driver;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.text.InputFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.zuver.driver.data.CommonData;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.service.APIService_Volley_JSON;
import com.zuver.driver.utils.SessionSave;

import org.json.JSONException;
import org.json.JSONObject;


/*
 * This class is used for change the driver profile password.
 */
public class ChangepassAct extends MainActivity implements OnClickListener
{
    private TextView CancelBtn, headTitle;
    private TextView DoneBtn;
    private EditText editoldpwd;
    private EditText editnewpwd;
    private EditText editconfirmpwd;
    private Button passchange_btn;
    private String oldpwd;
    private String newpwd;
    private String confirmpwd;
    private String md5oldpwd;
    private String md5newpwd;
    private String md5confirmpwd;

    @Override
    public int setLayout()
    {
        setLocale();
        return R.layout.changepass_lay;
    }

    @SuppressLint("NewApi")
    @Override
    public void Initialize()
    {
        // TODO Auto-generated method stub
        CommonData.sContext = this;
        CancelBtn = (TextView) findViewById(R.id.cancelBtn);
        headTitle = (TextView) findViewById(R.id.signup_title);
        DoneBtn = (TextView) findViewById(R.id.doneBtn);
        DoneBtn.setBackground(getResources().getDrawable(R.drawable.back_green_to_white));
        DoneBtn.setText(getResources().getString(R.string.save));
//		DoneBtn.setVisibility(View.GONE);

        CancelBtn.setVisibility(View.VISIBLE);
        CancelBtn.setBackground(getResources().getDrawable(R.drawable.back_green_to_white));
        CancelBtn.setText(getResources().getString(R.string.back));
        CancelBtn.setTextColor(getResources().getColor(R.color.txt_color));

        editoldpwd = (EditText) findViewById(R.id.editoldpwd);
        editnewpwd = (EditText) findViewById(R.id.editnewpwd);
        editconfirmpwd = (EditText) findViewById(R.id.editconfirmpwd);
        passchange_btn = (Button) findViewById(R.id.pro_change_btn);

        editoldpwd.setFilters(new InputFilter[]{filter});
        editnewpwd.setFilters(new InputFilter[]{filter});
        editconfirmpwd.setFilters(new InputFilter[]{filter});
        headTitle.setText("Change Password");
        headTitle.setTextColor(getResources().getColor(R.color.txt_color));
//		DoneBtn.setVisibility(View.GONE);
        setonclickListener();
    }

    private void setonclickListener()
    {
        passchange_btn.setOnClickListener(this);
        CancelBtn.setOnClickListener(this);
        DoneBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        // TODO Auto-generated method stub
        try
        {
            if (v == DoneBtn)
            {
                oldpwd = editoldpwd.getText().toString().trim();
                newpwd = editnewpwd.getText().toString().trim();
                confirmpwd = editconfirmpwd.getText().toString().trim();
                showLog("oldpwd" + SessionSave.getSession("Org_Password", ChangepassAct.this));

                if (oldpwd.trim().length() == 0)
                {
                    ShowToast(ChangepassAct.this, "" + getResources().getString(R.string.enter_the_old_password));
                }
                else if (newpwd.trim().length() == 0)
                {
                    ShowToast(ChangepassAct.this, "" + getResources().getString(R.string.enter_the_new_password));
                }
                else if (newpwd.trim().length() < 6)
                {
                    ShowToast(ChangepassAct.this, "" + getResources().getString(R.string.password_min_character));
                }
                else if (confirmpwd.trim().length() == 0)
                {
                    ShowToast(ChangepassAct.this, "" + getResources().getString(R.string.enter_the_confirmation_password));
                }
                else if (!confirmpwd.equals(newpwd))
                {
                    ShowToast(ChangepassAct.this, "" + getResources().getString(R.string.confirmation_password_mismatch_with_password));
                }
                else if (oldpwd.equals(confirmpwd))
                {
                    ShowToast(ChangepassAct.this, "" + getResources().getString(R.string.old_password_and_newpassword_are_same));
                }
                else
                {
                    md5oldpwd = convertPassMd5(oldpwd);
                    md5newpwd = convertPassMd5(newpwd);
                    md5confirmpwd = convertPassMd5(confirmpwd);
                    JSONObject j = new JSONObject();
                    j.put("id", SessionSave.getSession("Id", ChangepassAct.this));
                    j.put("old_password", md5oldpwd);
                    j.put("new_password", md5newpwd);
                    j.put("confirm_password", md5confirmpwd);
                    j.put("org_new_password", newpwd);
                    j.put("user_type", "D");
                    String url = "chg_password_driver";
                    new ChangePassword(url, j);
                }
            }
            else if (v == CancelBtn)
            {
                Intent intent = new Intent(ChangepassAct.this, MeAct.class);
                startActivity(intent);
                finish();
                CommonData.Animation1(ChangepassAct.this);
            }
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    // This API is used for requesting parameter to change the password.
    private class ChangePassword implements APIResult
    {
        String msg = "";

        private ChangePassword(String url, JSONObject data)
        {
            // TODO Auto-generated constructor stub
            new APIService_Volley_JSON(ChangepassAct.this, this, data, false).execute(url);
        }

        @Override
        public void getResult(boolean isSuccess, String result)
        {
            // TODO Auto-generated method stub
            showLog("ChangePassword : " + result);

            if (isSuccess)
            {
                try
                {
                    JSONObject json = new JSONObject(result);

                    if (json.getInt("status") == 1)
                    {
                        msg = json.getString("message");
                        SessionSave.saveSession("Org_Password", newpwd, ChangepassAct.this);
                        ShowToast(ChangepassAct.this, msg);
                        startActivity(new Intent(ChangepassAct.this, MeAct.class));
                        finish();
                    }
                    else if (json.getInt("status") == -1)
                    {
                        //User has been logged out for any reason
                        askForLogin();
                    }
                    else
                    {
                        msg = json.getString("message");
                        ShowToast(ChangepassAct.this, msg);
                    }
                }
                catch (JSONException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            else
            {
                ShowToast(getApplicationContext(), "" + getResources().getString(R.string.check_net_connection));
            }
        }
    }
}
