package com.zuver.driver;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.zuver.driver.data.CommonData;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.service.APIService_Volley_JSON;
import com.zuver.driver.service.NonActivity;
import com.zuver.driver.utils.FontHelper;
import com.zuver.driver.utils.SessionSave;

import org.json.JSONObject;

public class HomeActivity extends MainActivity implements OnClickListener
{
    // Class members declarations.
    private CheckBox chkShift;
    private TextView txtTrackTrip, txtProfile, txtMyJobs;
    private TextView txtTotalJobs, txtRejected, txtTimeDriven, txtCancelled,
            txtTotalEarnings, txtCurrentLocation;
    private boolean doubleBackToExitPressedOnce = false;
    private String strTotalJobs = "", strRejected = "", strTimeDriven = "", strCancelled = "",
            strTotalEarnings = "";
    NonActivity nonactiityobj = new NonActivity();
    private static final int WHAT = 1;
    private static final int TIME_TO_WAIT = 4000;
    public Handler regularHandler;
    String checked = "OUT";
    private String alert_msg;
    private Bundle alert_bundle = new Bundle();

    LinearLayout layHome;
    TextView txtHome;
    ImageView imgHome;

    LinearLayout driver_profile, driver_ongoing, my_drive, dashboard, parentSlideLay;
    ImageView driver_image, ongoing_img, mydriv_img, dash_img, img_edit;
    //			LinearLayout menu_img;
    TextView txt_driver_profile_name, txt_driver_profile_number, txt_ongoing, txt_mydrive, txt_dash;
    View dashLay;

    private DisplayImageOptions options;

    // Set the layout to activity.
    @Override
    public int setLayout()
    {
        setLocale();
        return R.layout.home_layout;
    }

    // Initialize the views on layout
    @Override
    public void Initialize()
    {
        try
        {
            alert_bundle = getIntent().getExtras();

            if (alert_bundle != null)
            {
                alert_msg = alert_bundle.getString("alert_message");
            }

            if (alert_msg != null && alert_msg.length() != 0)
            {
                alert_view(HomeActivity.this, "" + getResources().getString(R.string.message), "" + alert_msg, "" + getResources().getString(R.string.ok), "");
            }

            CommonData.sContext = this;
            CommonData.current_act = "HomeActivity";
            nonactiityobj.stopServicefromNonActivity(HomeActivity.this);
            FontHelper.applyFont(this, findViewById(R.id.id_home), "DroidSans.ttf");
            chkShift = (CheckBox) findViewById(R.id.chkshift);
            txtTrackTrip = (TextView) findViewById(R.id.txttracktrip);
            txtProfile = (TextView) findViewById(R.id.txtprofile);
            txtMyJobs = (TextView) findViewById(R.id.txtmyjob);
            txtTotalJobs = (TextView) findViewById(R.id.txtjobs);
            txtRejected = (TextView) findViewById(R.id.txtrejected);
            txtCancelled = (TextView) findViewById(R.id.txt_cancelled);
            txtTimeDriven = (TextView) findViewById(R.id.txttimedriven);
            txtTotalEarnings = (TextView) findViewById(R.id.totalearnings);
            txtCurrentLocation = (TextView) findViewById(R.id.currentlocation);
            driver_profile = (LinearLayout) findViewById(R.id.driver_profile_lay);
            driver_ongoing = (LinearLayout) findViewById(R.id.ongoing_lay);
            my_drive = (LinearLayout) findViewById(R.id.mydrive_lay);
            dashboard = (LinearLayout) findViewById(R.id.dashboard_lay);
            parentSlideLay = (LinearLayout) findViewById(R.id.driver_profile_parent);

            driver_image = (ImageView) findViewById(R.id.dash_image);
            ongoing_img = (ImageView) findViewById(R.id.img_ongoing);
            mydriv_img = (ImageView) findViewById(R.id.img_mydrive);
            dash_img = (ImageView) findViewById(R.id.dash_img);

            if (!SessionSave.getSession("trip_id", HomeActivity.this).equals(""))
            {
                driver_ongoing.setVisibility(View.VISIBLE);
            }
            else
            {
                driver_ongoing.setVisibility(View.GONE);
            }

            options = new DisplayImageOptions.Builder().showImageOnLoading(null) // resource
                    // or
                    // drawable
                    .showImageForEmptyUri(null) // resource or drawable
                    .showImageOnFail(null) // resource or drawable
                    .resetViewBeforeLoading(false) // default
                    .delayBeforeLoading(1000).cacheInMemory(true) // default
                    .cacheOnDisc(true) // default
                    .imageScaleType(ImageScaleType.EXACTLY_STRETCHED) // default
                    .bitmapConfig(Bitmap.Config.RGB_565) // default
                    .displayer(new SimpleBitmapDisplayer()) // default
                    .handler(new Handler()) // default
                    // .showImageOnLoading(R.drawable.ic_stub)
                    .build();

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).defaultDisplayImageOptions(options).build();
            ImageLoader.getInstance().init(config);
            String Image_path = SessionSave.getSession("driver_profileimage", HomeActivity.this);

            if (Image_path != null && Image_path.length() > 0)
            {
                Log.e("Profile Image", "" + SessionSave.getSession("driver_profileimage", HomeActivity.this));
                ImageLoader.getInstance().displayImage(Image_path, driver_image, options);
            }

            txt_driver_profile_name = (TextView) findViewById(R.id.txt_driver_profile_name);
            txt_driver_profile_number = (TextView) findViewById(R.id.txt_driver_profile_number);
            txt_ongoing = (TextView) findViewById(R.id.txt_ongoing);
            txt_mydrive = (TextView) findViewById(R.id.txt_mydrive);
            txt_dash = (TextView) findViewById(R.id.txt_dash);

            dashLay = findViewById(R.id.include_lay);
            txt_driver_profile_name.setText(SessionSave.getSession("Name", HomeActivity.this));
            txt_driver_profile_number.setText(SessionSave.getSession("Phone", HomeActivity.this));

            layHome = (LinearLayout) findViewById(R.id.home_lay);
            imgHome = (ImageView) findViewById(R.id.home_img);
            txtHome = (TextView) findViewById(R.id.txt_home);

			/* Get notification messages from bundle and show */
            txtTrackTrip.setOnClickListener(this);
            txtProfile.setOnClickListener(this);
            txtMyJobs.setOnClickListener(this);
            chkShift.setOnClickListener(this);

            new requestStatisticsApi();
            /*try {
                *//*
                 * Update the UI about driver statistics if any changes is
				 * there.
				 *//*
                if (!SessionSave.getSession("driver_statistics", HomeActivity.this).equals("")) {
                    JSONObject jsonDriverObject = new JSONObject(SessionSave.getSession("driver_statistics", HomeActivity.this));
                    strTotalJobs = jsonDriverObject.getString("total_trip");
                    strTotalEarnings = jsonDriverObject.getString("total_earnings");
                    strRejected = jsonDriverObject.getString("overall_rejected_trips");
                    strCancelled = jsonDriverObject.getString("cancel_trips");
                    strTimeDriven = jsonDriverObject.getString("time_driven");
                    settingValues();
                }
            } catch (Exception e) {
            }*/
            /* Timer to update current location from location update */
            regularHandler = new Handler(new Handler.Callback()
            {
                public boolean handleMessage(Message msg)
                {
                    // Do stuff
                    regularHandler.sendEmptyMessageDelayed(WHAT, TIME_TO_WAIT);

                    if (SessionSave.getSession("Driver_locations_home", HomeActivity.this).equals(""))
                    {
                        txtCurrentLocation.setText("" + getResources().getString(R.string.locating_you));
                    }
                    else
                    {
                        txtCurrentLocation.setText("" + SessionSave.getSession("Driver_locations_home", HomeActivity.this));
                    }

                    return true;
                }
            });
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume()
    {
        try
        {
            super.onResume();
            SessionSave.saveSession("speedwaiting", "", HomeActivity.this);
            regularHandler.sendEmptyMessageDelayed(WHAT, TIME_TO_WAIT);
          /*  if (!SessionSave.getSession("driver_statistics", HomeActivity.this).equals("")) {
                JSONObject jsonDriverObject = new JSONObject(SessionSave.getSession("driver_statistics", HomeActivity.this));
                strTotalJobs = jsonDriverObject.getString("total_trip");
                strTotalEarnings = jsonDriverObject.getString("total_earnings");
                strRejected = jsonDriverObject.getString("overall_rejected_trips");
                strCancelled = jsonDriverObject.getString("cancel_trips");
                strTimeDriven = jsonDriverObject.getString("time_driven");
                settingValues();
            }
            */
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy()
    {
        // TODO Auto-generated method stub
        super.onDestroy();

        try
        {
            regularHandler.removeMessages(WHAT);
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop()
    {
        // TODO Auto-generated method stubdriver_profile = (LinearLayout)
        // findViewById(R.id.driver_profile_lay);
        driver_ongoing = (LinearLayout) findViewById(R.id.ongoing_lay);
        my_drive = (LinearLayout) findViewById(R.id.mydrive_lay);
        dashboard = (LinearLayout) findViewById(R.id.dashboard_lay);
        parentSlideLay = (LinearLayout) findViewById(R.id.driver_profile_parent);

        driver_image = (ImageView) findViewById(R.id.dash_image);
        ongoing_img = (ImageView) findViewById(R.id.img_ongoing);
        mydriv_img = (ImageView) findViewById(R.id.img_mydrive);
        dash_img = (ImageView) findViewById(R.id.dash_img);
        super.onStop();

        try
        {
            regularHandler.removeMessages(WHAT);
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(HomeActivity.this, DashAct.class));
        CommonData.Animation1(HomeActivity.this);
    }

    // Click method used to move from one activity to another activity.
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.txttracktrip:

                showLoading(HomeActivity.this);

                if (SessionSave.getSession("status", HomeActivity.this).equals("A"))
                {
                    dismissLoading();
                    Intent intent = new Intent(HomeActivity.this, OngoingAct.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    dismissLoading();
                    Intent intent = new Intent(HomeActivity.this, MyStatus.class);
                    startActivity(intent);
                    finish();
                }

                break;

            case R.id.txtprofile:

                showLoading(HomeActivity.this);
                Intent intent = new Intent(HomeActivity.this, MeAct.class);
                startActivity(intent);
                finish();
                dismissLoading();

                break;

            case R.id.txtmyjob:

                showLoading(HomeActivity.this);
                Intent intentJobsAct = new Intent(HomeActivity.this, JobsAct.class);
                startActivity(intentJobsAct);
                finish();
                dismissLoading();

                break;

            case R.id.chkshift:

                chkShift.setClickable(false);
                new RequestingCheckBox();

                break;

            default:
                break;
        }
    }

    // call the DriverHistory Activity
    public void search_driver_history(View v)
    {
        showLoading(HomeActivity.this);
        Intent intentDriverHstyAct = new Intent(HomeActivity.this, DriveHistoryAct.class);
        startActivity(intentDriverHstyAct);
        dismissLoading();
    }

    // Click method used to change the language and update the UI based on
    // selected language.
    public void language_settings(View v)
    {
        final View view = View.inflate(HomeActivity.this, R.layout.lang_list, null);
        mDialog = new Dialog(HomeActivity.this, R.style.dialogwinddow);
        mDialog.setContentView(view);
        FontHelper.applyFont(HomeActivity.this, mDialog.findViewById(R.id.id_lang), "DroidSans.ttf");
        mDialog.setCancelable(true);
        mDialog.show();

        final LinearLayout lay_fav_res1 = (LinearLayout) mDialog.findViewById(R.id.lay_fav_res1);
        final LinearLayout lay_fav_res7 = (LinearLayout) mDialog.findViewById(R.id.lay_fav_res7);
        getIntent().removeExtra("alert_message");

        lay_fav_res1.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                SessionSave.saveSession("Lang", "en", HomeActivity.this);
                SessionSave.saveSession("Lang_Country", "en_GB", HomeActivity.this);
                // TODO Auto-generated method stub
                mDialog.dismiss();
                RefreshAct();
            }
        });

        lay_fav_res7.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                SessionSave.saveSession("Lang", "hi", HomeActivity.this);
                SessionSave.saveSession("Lang_Country", "hi_IN", HomeActivity.this);
                // TODO Auto-generated method stub
                mDialog.dismiss();
                RefreshAct();
            }
        });
    }

    /*
     * This click method is used for accessing the class from the
     * slide menu bar.
     */
    public void clickMethod(View v)
    {
        Intent i;

        switch (v.getId())
        {
            case R.id.driver_profile_lay:

                showLoading(HomeActivity.this);
                driver_profile.setBackgroundResource(R.color.lightgrey);
                i = new Intent(this, MeAct.class);
                startActivity(i);
                finish();
                dismissLoading();

                break;

            case R.id.ongoing_lay:

                showLoading(HomeActivity.this);
                driver_ongoing.setBackgroundResource(R.color.gray);
                txt_ongoing.setTextColor(getResources().getColor(R.color.white));
                ongoing_img.setImageResource(R.drawable.ongoing_drive_focus);

                if (SessionSave.getSession("status", HomeActivity.this).equals("A") || SessionSave.getSession("status", HomeActivity.this).equals("B"))
                {
                    Intent intentOnGoing = new Intent(HomeActivity.this, OngoingAct.class);
                    startActivity(intentOnGoing);
                    finish();
                    dismissLoading();
                }

                break;

            case R.id.mydrive_lay:

                showLoading(HomeActivity.this);
                my_drive.setBackgroundResource(R.color.gray);
                txt_mydrive.setTextColor(getResources().getColor(R.color.white));
                mydriv_img.setImageResource(R.drawable.mydriver_focus);

                i = new Intent(this, JobsAct.class);
                startActivity(i);
                finish();

                // logout(DashAct.this);

                break;
            case R.id.dashboard_lay:

                parentSlideLay.setVisibility(View.GONE);

                break;

            case R.id.home_lay:

                showLoading(HomeActivity.this);
                layHome.setBackgroundResource(R.color.gray);
                txtHome.setTextColor(getResources().getColor(R.color.white));
                imgHome.setImageResource(R.drawable.home_focus);
                // dashboard.setBackgroundColor(getResources().getColor(R.id.li))
                dismissLoading();
                i = new Intent(this, DashAct.class);
                startActivity(i);
                finish();

                break;

            case R.id.lin_menu:

                if (parentSlideLay.isShown())
                {
                    parentSlideLay.setVisibility(View.GONE);
                    dashLay.setVisibility(View.GONE);
                }
                else
                {
                    parentSlideLay.setVisibility(View.VISIBLE);
                    dashLay.setVisibility(View.VISIBLE);
                }

                break;

            default:
                break;
        }
    }

    /*
     * This class for API call to change the driver_statistics changes based on
     * API result
     */
    private class requestStatisticsApi implements APIResult
    {
        private requestStatisticsApi()
        {
            try
            {
                String driver_number = SessionSave.getSession("Id", HomeActivity.this);
                JSONObject j = new JSONObject();
                j.put("driver_id", driver_number);
                String requestingUrl = "driver_statistics";

                if (isOnline())
                {
                    new APIService_Volley_JSON(HomeActivity.this, this, j, false).execute(requestingUrl);
                }
                else
                {
                    alert_view(HomeActivity.this, "" + getResources().getString(R.string.message), "" + getResources().getString(
                            R.string.check_internet), "" + getResources().getString(R.string.ok), "");
                }
            }
            catch (Exception e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        @Override
        public void getResult(boolean isSuccess, String result)
        {
            // TODO Auto-generated method stub
            try
            {
                if (isSuccess)
                {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("status") == 1)
                    {
                        jsonObject = jsonObject.getJSONObject("detail");
                        strRejected = jsonObject.getString("overall_rejected_trips");
                        strCancelled = jsonObject.getString("cancel_trips");
                        strTimeDriven = jsonObject.getString("time_driven");
                        strTotalEarnings = jsonObject.getString("total_earnings");
                        strTotalJobs = jsonObject.getString("total_trip");
                        SessionSave.saveSession("shift_status", jsonObject.getString("shift_status"), HomeActivity.this);
                        settingValues();
                    }
                    else if (jsonObject.getInt("status") == -1)
                    {
                        //User has been logged out for any reason
                        askForLogin();
                    }
                    else
                    {
                        alert_view(HomeActivity.this, "" + getResources().getString(R.string.message), "" + jsonObject.getString("message"), "" + getResources().getString(R.string.ok), "");
                    }

                    nonactiityobj.startServicefromNonActivity(HomeActivity.this);
                }
                else
                {
                    alert_view(HomeActivity.this, "" + getResources().getString(R.string.message), "" + getResources().getString(
                            R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }

    /// Getting Current Shift Status of the driver
    private class getDriverCurrentStatus implements APIResult
    {
        private getDriverCurrentStatus(String url, JSONObject data)
        {
            try
            {
                if (isOnline())
                {
                    new  APIService_Volley_JSON(HomeActivity.this, this, data, false).execute(url);
                }
                else
                {
                    alert_view(
                            HomeActivity.this,
                            "" + getResources().getString(R.string.message),
                            ""
                                    + getResources().getString(
                                    R.string.check_net_connection), ""
                                    + getResources().getString(R.string.ok), "");
                }
            }
            catch (Exception e)
            {
                Log.d("Exception", "while getting profile data in dashboard activity:\n");
                e.printStackTrace();
            }
        }

        @Override
        public void getResult(boolean isSuccess, String result)
        {
            try
            {
                if (isSuccess)
                {
                    JSONObject json = new JSONObject(result);

                    if (json.getInt("status") == 1)
                    {
                        JSONObject details = json.getJSONObject("detail");
                        String shift_status = details.getString("dutyStatus");
                        String loginStatus = details.getString("loginStatus");

                        Log.d("loginStatus", "login status from server is: " + loginStatus);
                        Log.d("shift_status", "shift status from server is: " + shift_status);

                        if (loginStatus.equalsIgnoreCase("1"))
                        {
                            if (shift_status.equalsIgnoreCase("1"))
                            {
                                shift_status = "IN";
                            }
                            else
                            {
                                shift_status = "OUT";
                            }

                            SessionSave.saveSession("shift_status", shift_status, HomeActivity.this);

                            // checking if  the status of the driver is 'IN'
                            // if shift is 'IN' then show "ON DUTY" switch
                            // else show 'OFF DUTY'
                            if (SessionSave.getSession("shift_status", HomeActivity.this).equals("IN"))
                            {
                                chkShift.setChecked(true);
                                nonactiityobj.startServicefromNonActivity(HomeActivity.this);
                            }
                            else
                            {
                                chkShift.setChecked(false);
                                nonactiityobj.stopServicefromNonActivity(HomeActivity.this);
                            }
                        }
                        else
                        {
                            askForLogin();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.d("Exception", "while getting profile data in dashboard activity:\n");
                e.printStackTrace();
            }
        }
    }

    private void RefreshAct()
    {
        Intent intent = getIntent();
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        startActivity(intent);
    }

    /* Update the UI about driver statistics. */
    private void settingValues()
    {
        txtTotalJobs.setText("" + strTotalJobs);
        txtRejected.setText("" + strRejected);
        txtCancelled.setText("" + strCancelled);
        txtTimeDriven.setText("" + strTimeDriven);
        txtTotalEarnings.setText("" + SessionSave.getSession("site_currency", HomeActivity.this) + " " + strTotalEarnings);
        txtCurrentLocation.setText(SessionSave.getSession("Driver_locations_home", HomeActivity.this));

        if (SessionSave.getSession("shift_status", HomeActivity.this).equals("IN"))
        {
            chkShift.setChecked(true);
            nonactiityobj.startServicefromNonActivity(HomeActivity.this);
        }
        else
        {
            chkShift.setChecked(false);
            nonactiityobj.stopServicefromNonActivity(HomeActivity.this);
        }
    }

    /*
     * This class for API call to change the driver shift changes and Check Box
     * value based on API result
     */
    private class RequestingCheckBox implements APIResult
    {
        private RequestingCheckBox()
        {
            try
            {
                if (chkShift.isChecked())
                {
                    checked = "IN";
                }
                else
                {
                    checked = "OUT";
                }

                JSONObject j = new JSONObject();
                j.put("driver_id", SessionSave.getSession("Id", HomeActivity.this));
                j.put("shiftstatus", checked);
                String requestingCheckBox = "driver_shift";

                if (isOnline())
                {
                    new APIService_Volley_JSON(HomeActivity.this, this, j, false).execute(requestingCheckBox);
                }
                else
                {
                    chkShift.setClickable(true);
                    alert_view(HomeActivity.this, "" + getResources().getString(R.string.message), "" + getResources().getString(
                            R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");

                    if (checked.equals("IN"))
                    {
                        chkShift.setChecked(false);
                    }
                    else
                    {
                        chkShift.setChecked(true);
                    }
                }
            }
            catch (Exception e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        @Override
        public void getResult(boolean isSuccess, String result)
        {
            try
            {
                if (isSuccess)
                {
                    chkShift.setClickable(true);
                    JSONObject object = new JSONObject(result);

                    if (object.getInt("status") == 1)
                    {
                        alert_view(HomeActivity.this, "" + getResources().getString(R.string.message), "" + object.getString("message"), "" + getResources().getString(R.string.ok), "");
                        chkShift.setChecked(true);
                        SessionSave.saveSession("shift_status", "IN", HomeActivity.this);
                        nonactiityobj.startServicefromNonActivity(HomeActivity.this);
                    }
                    else if (object.getInt("status") == -1)
                    {
                        //User has been logged out for any reason
                        askForLogin();
                    }
                    else if (object.getInt("status") == 2)
                    {
                        alert_view(HomeActivity.this, "" + getResources().getString(R.string.message), "" + object.getString("message"), "" + getResources().getString(R.string.ok), "");
                        chkShift.setChecked(false);
                        SessionSave.saveSession("shift_status", "OUT", HomeActivity.this);
                        nonactiityobj.stopServicefromNonActivity(HomeActivity.this);
                    }
                    else if (object.getInt("status") == -4)
                    {
                        alert_view(HomeActivity.this, "" + getResources().getString(R.string.message), "" + object.getString("message"), "" + getResources().getString(R.string.ok), "");
                        chkShift.setChecked(true);
                    }
                    else
                    {
                        alert_view(HomeActivity.this, "" + getResources().getString(R.string.message), "" + object.getString("message"), "" + getResources().getString(R.string.ok), "");

                        if (checked.equals("IN"))
                        {
                            chkShift.setChecked(true);
                        }
                        else
                        {
                            chkShift.setChecked(false);
                        }
                    }
                }
                else
                {
                    chkShift.setClickable(true);
                    alert_view(HomeActivity.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");

                    if (checked.equals("IN"))
                    {
                        chkShift.setChecked(true);
                    }
                    else
                    {
                        chkShift.setChecked(false);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                chkShift.setClickable(true);
                alert_view(HomeActivity.this, "" + getResources().getString(R.string.message), "" + getResources().getString(
                        R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");

                if (checked.equals("IN"))
                {
                    chkShift.setChecked(true);
                }
                else
                {
                    chkShift.setChecked(false);
                }
            }
        }
    }
}