package com.zuver.driver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.TextView;

import com.zuver.driver.data.CommonData;
import com.zuver.driver.data.MystatusData;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.service.APIService_Volley_JSON;
import com.zuver.driver.service.CurrentLocationUpdateClass;
import com.zuver.driver.service.LocationUpdate;
import com.zuver.driver.service.NonActivity;
import com.zuver.driver.utils.FontHelper;
import com.zuver.driver.utils.SessionSave;

import org.json.JSONException;
import org.json.JSONObject;

public class NotificationAct extends MainActivity
{
    // Class members declarations.
    public String message;
    int time_out; //  timer;
    CountDownTimer countDownTimer;
    Ringtone r;
    private String trip_id = "";
    private String pickup;
    public String passenger_id;
    private String bookedby;
    private String passenger_phone;
    public String pickup_time;
    public String pickUpTime, tripType;
    NonActivity nonactiityobj = new NonActivity();
    Bundle bun;
    Button butt_accept, butt_decline;
    Activity nActivity;
    TextView idsec, idmin, minTxt, slideImg;
    private TextView notifCity, mTripTypeTxt, secTxt, PickTime;
    private String mBookingKey, mSecretKey;

    private int FLAG = 0;

    // Set the layout to activity.
    @Override
    public int setLayout()
    {
        setLocale();
        return R.layout.notification_lay;
    }

    // Initialize the views on layout
    @Override
    public void Initialize()
    {
        try
        {
            bun = getIntent().getExtras();
            nActivity = this;
            CommonData.current_act = "NotificationAct";

            if (bun != null)
            {
                unlockScreen();
                FontHelper.applyFont(this, findViewById(R.id.noti_font), "DroidSans.ttf");
                nonactiityobj.stopServicefromNonActivity(NotificationAct.this);
                stopService(new Intent(this, LocationUpdate.class));
                butt_accept = (Button) findViewById(R.id.butt_accept);
                butt_decline = (Button) findViewById(R.id.butt_decline);
                slideImg = (TextView) findViewById(R.id.slideImg);
                slideImg.setVisibility(View.GONE);
                secTxt = (TextView) findViewById(R.id.secTxt);
                idsec = (TextView) findViewById(R.id.idsec);
                idmin = (TextView) findViewById(R.id.idmin);
                minTxt = (TextView) findViewById(R.id.minTxt);
                notifCity = (TextView) findViewById(R.id.notif_city);
                PickTime = (TextView) findViewById(R.id.pick_time);
                mTripTypeTxt = (TextView) findViewById(R.id.trip_typeR);
                message = bun.getString("message");
                AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION,
                        audioManager.getStreamMaxVolume(AudioManager.STREAM_RING),
                        AudioManager.FLAG_ALLOW_RINGER_MODES | AudioManager.FLAG_VIBRATE);
                Uri sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.alert_tone);
                r = RingtoneManager.getRingtone(NotificationAct.this, sound);

                FLAG++;

                try
                {
                    final JSONObject json = new JSONObject(message);
                    final JSONObject tripdetails = json.getJSONObject("trip_details");
                    time_out = tripdetails.getInt("notification_time");

                    MystatusData.setIsZoomCar(tripdetails.getString("is_zoomcar_trip"));
                    MystatusData.setSecretSalt(tripdetails.getString("zoomcar_secret_salt"));
                    MystatusData.setmZoomCarAuthKey(tripdetails.getString("zoomcar_access_token"));
                    SessionSave.saveSession("ZoomcarAuthKey", tripdetails.getString("zoomcar_access_token"), this);
                    mSecretKey = tripdetails.getString("zoomcar_secret_salt");

                    trip_id = tripdetails.getString("passengers_log_id");
                    SessionSave.saveSession("trip_id", "" + trip_id, NotificationAct.this);
                    MainActivity.mMyStatus.settripId(trip_id);
                    final JSONObject details = tripdetails.getJSONObject("booking_details");
                    pickup = details.getString("pickupplace");
                    pickup = pickup.trim();

                    if (pickup.length() != 0)
                    {
                        pickup = Character.toUpperCase(pickup.charAt(0)) + pickup.substring(1);
                    }

                    pickup_time = date_conversion(details.getString("pickup_time"));
                    passenger_phone = details.getString("passenger_phone");
                    passenger_id = details.getString("passenger_id");
                    passenger_phone = details.getString("passenger_phone");

                    SessionSave.saveSession("company_id", details.getString("company_id"), this);

                    if (details.getString("bookedby").length() != 0)
                    {
                        bookedby = details.getString("bookedby");
                    }

                    MystatusData.setBookingKey(details.getString("booking_key"));
                    MainActivity.mMyStatus.setpassengerphone(passenger_phone);
                    tripType = details.getString("trip_plan");
                    pickUpTime = details.getString("pickup_time");

                    mBookingKey = details.getString("booking_key");
                    String key = getSha1Hex(mBookingKey + "|" + mSecretKey);
                    MystatusData.setmAuthKey(key);
                    mTripTypeTxt.setText(tripType);
                    notifCity.setText(pickup);
                    PickTime.setText(pickup_time);

                }
                catch (final JSONException e)
                {
                    e.printStackTrace();
                }

                if (FLAG == 1)
                {
                    countDownTimer();
                }
            }

            // If driver accept the trip,following actions will perform.
            butt_accept.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(final View v)
                {
                    try
                    {
                        if (r != null && r.isPlaying())
                        {
                            r.stop();
                        }

                        countDownTimer.cancel();
                        MainActivity.mMyStatus.settripId(trip_id);

                        MainActivity.mMyStatus.setpassengerId(trip_id);
                        JSONObject j = new JSONObject();
                        j.put("pass_logid", trip_id);
                        j.put("driver_id", SessionSave.getSession("Id", NotificationAct.this));
                        j.put("taxi_id", SessionSave.getSession("taxi_id", NotificationAct.this));
                        j.put("company_id", SessionSave.getSession("company_id", NotificationAct.this));
                        j.put("driver_reply", "A");
                        j.put("field", "rejection");
                        j.put("flag", "0");
                        final String Url = "driver_reply";
                        new TripAccept(Url, j);
                    }
                    catch (Exception e)
                    {
                        // TODO: handle exception
                        e.printStackTrace();
                    }
                }
            });

            // If driver decline the trip,following actions will perform.
            butt_decline.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(final View v)
                {
                    try
                    {
                        // TODO Auto-generated method stub
                        if (r != null && r.isPlaying())
                        {
                            r.stop();
                        }

                        countDownTimer.cancel();

                        JSONObject j = new JSONObject();
                        j.put("trip_id", trip_id);
                        j.put("driver_id", SessionSave.getSession("Id", NotificationAct.this));
                        j.put("taxi_id", SessionSave.getSession("taxi_id", NotificationAct.this));
                        j.put("company_id", SessionSave.getSession("company_id", NotificationAct.this));
                        j.put("reason", "");
                        j.put("reject_type", "1");
                        final String Url = "reject_trip";
                        new TripReject(Url, j);
                    }
                    catch (Exception e)
                    {
                        // TODO: handle exception
                        e.printStackTrace();
                    }
                }
            });
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }


    void countDownTimer()
    {
        // Timer function runs based on server response and once it finished, Onfinish() method calls the reject _trip API to update the driver timeout status to server.
        countDownTimer = new CountDownTimer((time_out + 1) * 1000, 1000)
        {
            int time = 1;

            @Override
            public void onTick(final long millisUntilFinished_)
            {
                r.play();
                final long sec = millisUntilFinished_ / 1000;
                secTxt.setText(sec + "");
                time++;
            }

            @Override
            public void onFinish()
            {
                try
                {
                    if (r != null && r.isPlaying())
                    {
                        r.stop();
                    }

                    countDownTimer.cancel();

                    JSONObject j = new JSONObject();
                    j.put("trip_id", trip_id);
                    j.put("driver_id", SessionSave.getSession("Id", NotificationAct.this));
                    j.put("taxi_id", SessionSave.getSession("taxi_id", NotificationAct.this));
                    j.put("company_id", SessionSave.getSession("company_id", NotificationAct.this));
                    j.put("reason", "");
                    j.put("reject_type", "0");
                    final String Url = "reject_trip";
                    new TripReject(Url, j);
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        }.start();
    }

    @Override
    protected void onPause()
    {
        // TODO Auto-generated method stub
        super.onPause();
    }

    @Override
    protected void onStop()
    {
        // TODO Auto-generated method stub
        super.onStop();
        bun.clear();
    }

    @Override
    protected void onDestroy()
    {
        // TODO Auto-generated method stub
        super.onDestroy();
        bun.clear();
    }

    /**
     * Used to call the trip accept API and parse the response
     */
    private class TripAccept implements APIResult
    {
        String msg;

        private TripAccept(final String url, JSONObject data)
        {
            new APIService_Volley_JSON(NotificationAct.this, this, data, false).execute(url);
        }

        @Override
        public void getResult(final boolean isSuccess, final String result)
        {
            try
            {
                Log.e("Notification Act", "Notification Act: " + result.toString());

                if (isSuccess)
                {
                    final JSONObject json = new JSONObject(result);

                    if (json.getInt("status") == 1)
                    {
                        msg = json.getString("message");

                        if (bookedby.equals("0"))
                        {
                            SessionSave.saveSession("trip_id", "" + trip_id, NotificationAct.this);
                            SessionSave.saveSession("speedwaiting", "", NotificationAct.this);
                            SessionSave.saveSession("speedwaitingTime1", "", NotificationAct.this);
                            SessionSave.saveSession("speedwaitingTime2", "", NotificationAct.this);
                            MainActivity.mMyStatus.settripId(trip_id);
                            SessionSave.saveSession("trip_id", "" + trip_id, NotificationAct.this);
                            SessionSave.saveSession("bookedby", "" + bookedby, NotificationAct.this);

                            SessionSave.saveSession("status", "B", NotificationAct.this);
                            SessionSave.saveSession("BackCancelTrip", "BackCancelTrip", NotificationAct.this);

                            showLoading(NotificationAct.this);
                            final Intent intent = new Intent(NotificationAct.this, OngoingAct.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            Bundle extras = new Bundle();
                            extras.putString("alert_message", msg);
                            intent.putExtras(extras);
                            startActivity(intent);
                            finish();
                        }
                    }
                    else if (json.getInt("status") == 5)
                    {
                        msg = json.getString("message");
                        Intent i = new Intent(getBaseContext(), OngoingAct.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        Bundle extras = new Bundle();
                        extras.putString("alert_message", msg);
                        i.putExtras(extras);
                        getApplication().startActivity(i);
                        nActivity.finish();
                    }
                    else if (json.getInt("status") == 7)
                    {

                        msg = json.getString("message");
                        SessionSave.saveSession("trip_id", "", NotificationAct.this);
                        Intent i = new Intent(getBaseContext(), HomeActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        Bundle extras = new Bundle();
                        extras.putString("alert_message", msg);
                        i.putExtras(extras);
                        getApplication().startActivity(i);
                        nActivity.finish();
                    }
                    else if (json.getInt("status") == 6)
                    {
                        msg = json.getString("message");
                        SessionSave.saveSession("trip_id", "", NotificationAct.this);
                        Intent i = new Intent(getBaseContext(), HomeActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        Bundle extras = new Bundle();
                        extras.putString("alert_message", msg);
                        i.putExtras(extras);
                        getApplication().startActivity(i);
                        nActivity.finish();
                    }
                    else if (json.getInt("status") == -1)
                    {
                        //User has been logged out for any reason
                        askForLogin();
                    }
                    else
                    {
                        msg = json.getString("message");
                        alert_view(NotificationAct.this, "" + getResources().getString(R.string.message), "" + msg, "" + getResources().getString(R.string.ok), "");
                    }
                }
                else
                {
                    alert_view(NotificationAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
                }
            }
            catch (final JSONException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    /**
     * Used to call the trip reject API and parse the response
     */
    private class TripReject implements APIResult
    {
        String msg;

        private TripReject(final String url, JSONObject data)
        {
            // TODO Auto-generated constructor stub
            new APIService_Volley_JSON(NotificationAct.this, this, data, false).execute(url);
        }

        @Override
        public void getResult(final boolean isSuccess, final String result)
        {
            // TODO Auto-generated method stub
            showLog("Reject:" + result);

            try
            {
                if (isSuccess)
                {
                    nonactiityobj.startServicefromNonActivity(NotificationAct.this);
                    startService(new Intent(NotificationAct.this, LocationUpdate.class));
                    final JSONObject json = new JSONObject(result);

                    if (json.getInt("status") == 6)
                    {
                        CommonData.isSetNotificationDriverDecline(true);
                        Log.e("Status 6", "Status 6");
                        msg = json.getString("message");

                        new RequestingCheckBox();
                        Intent intent = new Intent(NotificationAct.this, CancellationAct.class);
                        intent.putExtra("DriverReply", "R");
                        startActivity(intent);
                        finish();
                    }
                    else if (json.getInt("status") == 7)
                    {
                        Log.e("Status 7", "Status 7");
                        msg = json.getString("message");
                        Intent intent = new Intent(NotificationAct.this, PickupMissedNotification.class);
                        intent.putExtra("trip_type", tripType);
                        intent.putExtra("pickup_time", pickUpTime);
                        startActivity(intent);
                        finish();
                    }
                    else if (json.getInt("status") == 8)
                    {
                        msg = json.getString("message");
                        SessionSave.saveSession("trip_id", "", NotificationAct.this);
                        Intent i = new Intent(getBaseContext(), HomeActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        Bundle extras = new Bundle();
                        extras.putString("alert_message", msg);
                        i.putExtras(extras);
                        getApplication().startActivity(i);
                        nActivity.finish();
                    }
                    else if (json.getInt("status") == -1)
                    {
                        //User has been logged out for any reason
                        askForLogin();
                    }
                    else if (json.getInt("status") != 6 || json.getInt("status") != 8 || json.getInt("status") != 3 || json.getInt("status") != 2 || json.getInt("status") != -1)
                    {
                        msg = "Trip has been is rejected";
                    }
                    else
                    {
                        msg = "Trip has been already cancelled";
                    }

                    showLoading(NotificationAct.this);

                }
                else
                {
                    alert_view(NotificationAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
                }
            }
            catch (final JSONException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    /*
     *  This API request is to manage the shifts for the driver.
     */
    private class RequestingCheckBox implements APIResult
    {
        private RequestingCheckBox()
        {
            try
            {
                Log.d("Notification", "Off duty api was called");
                ///String checked = "OUT";
                JSONObject j = new JSONObject();
                j.put("driver_id", SessionSave.getSession("Id", NotificationAct.this));
                j.put("shiftstatus", "OUT");
                j.put("latitude", SessionSave.getSession("mLatitude", NotificationAct.this));
                j.put("longitude", SessionSave.getSession("mLongitude", NotificationAct.this));
                String requestingCheckBox = "driver_shift";

                if (isOnline())
                {
                    new APIService_Volley_JSON(NotificationAct.this, this, j, false).execute(requestingCheckBox);
                }
                else
                {
                    /// edited by Amit Jangid
                    /// No internet connectivity
                    /// chkShift.setClickable(true);
                    alert_view(NotificationAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
                }
            }
            catch (Exception e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        @Override
        public void getResult(boolean isSuccess, String result)
        {
            try
            {
                if (isSuccess)
                {
                    /// chkShift.setClickable(true);
                    JSONObject object = new JSONObject(result);

                    if (object.getInt("status") == 1)
                    {
                        alert_view(NotificationAct.this, "" + getResources().getString(R.string.message), "" + object.getString("message"), "" + getResources().getString(R.string.ok), "");
                        ///chkShift.setChecked(true);
                        SessionSave.saveSession("shift_status", "IN", NotificationAct.this);
                        Log.d("Notification", "Off duty api was called and status was changed");

                        /*if (SessionSave.getSession("shift_status", NotificationAct.this).equalsIgnoreCase("IN"))
                        {
                            tvCancel.setText(getResources().getString(R.string.onduty));
                        }
                        else if (SessionSave.getSession("shift_status", DashAct.this).equalsIgnoreCase("OUT"))
                        {
                            chkShift.setClickable(false);
                            tvCancel.setText(getResources().getString(R.string.off_duty));
                        }*/

                        Log.e("Shift Status", "" + SessionSave.getSession("shift_status", NotificationAct.this));
                        stopService(new Intent(NotificationAct.this, CurrentLocationUpdateClass.class));
                        nonactiityobj.stopServicefromNonActivity(NotificationAct.this);
                        nonactiityobj.startServicefromNonActivity(NotificationAct.this);
                    }
                    else if (object.getInt("status") == -1)
                    {
                        /// user is on trip should not to allowed to change the shift from on to off
                        alert_view(NotificationAct.this, "" + "Message", "" + object.get("message"), "" + getResources().getString(R.string.ok), "");

                        /*if (chkShift.isChecked())
                        {
                            chkShift.setChecked(false);
                        }
                        else
                        {
                            chkShift.setChecked(true);
                        }*/
                    }
                    else if (object.getInt("status") == 2)
                    {
                        alert_view(NotificationAct.this, "" + getResources().getString(R.string.message), "" + object.getString("message"), "" + getResources().getString(R.string.ok), "");
                        /// chkShift.setChecked(false);
                        SessionSave.saveSession("shift_status", "OUT", NotificationAct.this);
                        Log.e("Shift Status", "" + SessionSave.getSession("shift_status", NotificationAct.this));

                        /*if (SessionSave.getSession("shift_status", DashAct.this).equalsIgnoreCase("IN"))
                        {
                            tvCancel.setText(getResources().getString(R.string.onduty));
                        }
                        else if (SessionSave.getSession("shift_status", DashAct.this).equalsIgnoreCase("OUT"))
                        {
                            tvCancel.setText(getResources().getString(R.string.off_duty));
                        }*/

                        nonactiityobj.stopServicefromNonActivity(NotificationAct.this);
                        //startService(new Intent(DashAct.this, CurrentLocationUpdateClass.class));
                        stopService(new Intent(NotificationAct.this, CurrentLocationUpdateClass.class));
                        nonactiityobj.startServicefromNonActivity(NotificationAct.this);
                    }
                    else if (object.getInt("status") == -4)
                    {
                        alert_view(NotificationAct.this, "" + getResources().getString(R.string.message), "" + object.getString("message"), "" + getResources().getString(R.string.ok), "");
                        /// chkShift.setChecked(true);
                    }
                    else
                    {
                        alert_view(NotificationAct.this, "" + getResources().getString(R.string.message), "" + object.getString("message"), "" + getResources().getString(R.string.ok), "");

                        /*if (checked.equals("IN"))
                        {
                            chkShift.setChecked(false);
                        }
                        else
                        {
                            chkShift.setChecked(true);
                        }*/
                    }
                }
                else
                {
                    /// error going here for check for internet connection
                    ///chkShift.setClickable(true);
                    alert_view(NotificationAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(
                            R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");

                    /*if (checked.equals("IN"))
                    {
                        chkShift.setChecked(false);
                    }
                    else
                    {
                        chkShift.setChecked(true);
                    }*/
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                //chkShift.setClickable(true);
                alert_view(NotificationAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");

                /*if (checked.equals("IN"))
                {
                    chkShift.setChecked(false);
                }
                else
                {
                    chkShift.setChecked(true);
                }*/
            }
        }
    }

    // This method is to check and open the notification view in front even the mobile screen off.
    private void unlockScreen()
    {
        Window window = this.getWindow();
        window.addFlags(LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.addFlags(LayoutParams.FLAG_TURN_SCREEN_ON);
    }

    @Override
    public void onBackPressed()
    {
        // TODO Auto-generated method stub
        // super.onBackPressed();
    }
}
