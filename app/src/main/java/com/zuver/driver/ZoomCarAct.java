package com.zuver.driver;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.zuver.driver.data.CommonData;
import com.zuver.driver.data.MystatusData;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.service.APIServiceVolley;

import org.json.JSONObject;

public class ZoomCarAct extends MainActivity {


    private WebView mWebview;
    private ProgressBar mProgressBar;
    private String checkListURL1, checkListURL2, mOTP = "";
    private String mStatus;
    private String mWebviewURL = "";
    private Button mRefresh;

    @Override
    public int setLayout() {
        return R.layout.activity_zoom_car;
    }

    @Override
    public void Initialize() {


        if (getIntent() != null && getIntent().hasExtra("status")) {
            mStatus = getIntent().getStringExtra("status");

            if (mStatus.equalsIgnoreCase("1")) {
                checkListURL1 = getIntent().getStringExtra("CheckList1");
                mWebviewURL = checkListURL1;
            } else if (mStatus.equalsIgnoreCase("2")) {
                checkListURL2 = getIntent().getStringExtra("CheckList2");
                mWebviewURL = checkListURL2;
            }
        }
        mWebview = (WebView) findViewById(R.id.zoomcarView);
        mRefresh = (Button) findViewById(R.id.Refresh);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.setWebViewClient(new myWebClient());
        mWebview.loadUrl(mWebviewURL);


        mRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(ZoomCarAct.this, ZoomCarAct.class);
                in.putExtra("CheckList1", MystatusData.getmCheckList1());
                in.putExtra("CheckList2", MystatusData.getmCheckList2());
                in.putExtra("status", "1");
                startActivity(in);
                finish();
            }
        });
    }


    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            mProgressBar.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub

            mProgressBar.setVisibility(View.VISIBLE);
            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // do your stuff here
            mProgressBar.setVisibility(View.GONE);
        }
    }


    @Override
    public void onBackPressed() {
        BackPressAlert(ZoomCarAct.this);
    }

    private class ZoomCarDriverStatus implements APIResult {


        public ZoomCarDriverStatus(String url, JSONObject data) {
            try {
                if (isOnline()) {
                    new APIServiceVolley(ZoomCarAct.this, this, data, false, url).execute();
                } else {
                    alert_view(ZoomCarAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
                }
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        @Override
        public void getResult(boolean isSuccess, String result) {
            try {
                if (isSuccess) {
                    if (result != null && result.length() > 0) {
                        JSONObject data = new JSONObject(result);
                        if (data.getString("status").equalsIgnoreCase("success")) {
                            Toast.makeText(getApplicationContext(), "Status Updated", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            } catch (Exception e) {
                Log.e("Error", e.toString());
            }
        }
    }


    public void BackPressAlert(Context mContext) {
        try {
            final View view = View.inflate(mContext, R.layout.back_alert, null);
            final Dialog mAlertDialog = new Dialog(mContext, R.style.dialogwinddow);
            mAlertDialog.setContentView(view);
            mAlertDialog.setCancelable(false);
            mAlertDialog.show();
            final Button button_success = (Button) mAlertDialog.findViewById(R.id.cancelBtnTxt);
            final Button button_failure = (Button) mAlertDialog.findViewById(R.id.okBtnTxt);
            button_success.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    // TODO Auto-generated method stub
                    try {
                        int ss;
                        if (mStatus.equalsIgnoreCase("1")) {
                            ss = 1;
                        } else {
                            ss = 2;
                        }
                        JSONObject j = new JSONObject();
                        j.put("booking_key", MystatusData.getBookingKey());
                        j.put("verndor_reference_key", MystatusData.gettripId());
                        j.put("status", ss);
                        final String start_trip_url = CommonData.ZooCar_BaseAPI + "driver_status";
                        new ZoomCarDriverStatus(start_trip_url, j);
                    } catch (Exception e) {
                        Log.e("Error", e.toString());
                    }

                }
            });
            button_failure.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    // TODO Auto-generated method stub
                }
            });
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }


}
