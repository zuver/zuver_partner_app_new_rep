package com.zuver.driver;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.zuver.driver.adapter.JobsListAdapter;
import com.zuver.driver.data.CommonData;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.service.APIService_Volley_JSON;
import com.zuver.driver.service.NonActivity;
import com.zuver.driver.utils.SessionSave;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;


/**
 * This class is used to show complete details about
 * the driver , and total trips of the driver.
 *
 * @author developer
 */
public class DriveHistoryAct extends MainActivity implements OnClickListener
{
    private TextView headTitle;
    // private TextView menu_img, backup;
//	private ImageView menu_img;
    private ListView list;
    private int limit = 50;
    private JSONArray pastJsonArray;
    private TextView edtFromDate, edtToDate;
    private String startDt = "", endDt = "";

    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;

    private SimpleDateFormat dateFormatter;
    private Button btnGo;
    Date today, fromDate, toDate;

    private String totalAmount, totalTrip, totalHours;
    NonActivity nonactiityobj = new NonActivity();

    LinearLayout driver_profile, driver_ongoing, my_drive, dashboard,
            parentSlideLay;

    ImageView driver_image, ongoing_img, mydriv_img, dash_img, img_edit;
    TextView txt_driver_profile_name, txt_driver_profile_number, txt_ongoing,
            txt_mydrive, txt_dash;

    View dashLay;
    private DisplayImageOptions options;

    TextView txtRides, txtTotalHrs, txtTotalBilling;

    LinearLayout layHome;
    TextView txtHome;
    ImageView imgHome;

    @Override
    public int setLayout()
    {
        // TODO Auto-generated method stub
        setLocale();
        return R.layout.drive_history_lay;
    }

    @Override
    public void Initialize()
    {
        // TODO Auto-generated method stub
        headTitle = (TextView) findViewById(R.id.title_header);

        txtRides = (TextView) findViewById(R.id.txt_rides);
        txtTotalHrs = (TextView) findViewById(R.id.txt_total_hr);
        txtTotalBilling = (TextView) findViewById(R.id.txt_total_billing);

//		menu_img = (ImageView) findViewById(R.id.menu_img);

        headTitle.setText(getResources().getString(
                R.string.driver_history_title));

        btnGo = (Button) findViewById(R.id.btn_go);
        edtFromDate = (TextView) findViewById(R.id.edt_fromdate);
        edtToDate = (TextView) findViewById(R.id.edt_todate);
        today = new Date();
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        startDt = dateFormatter.format(today);
        endDt = dateFormatter.format(today);

        edtFromDate.setText(dateFormatter.format(today));
        edtToDate.setText(dateFormatter.format(today));

        setDateTimeField();
        SessionSave.saveSession("DriverAdapter", "DriveHistory", DriveHistoryAct.this);
        list = (ListView) findViewById(R.id.history_listView);

        driver_profile = (LinearLayout) findViewById(R.id.driver_profile_lay);
        driver_ongoing = (LinearLayout) findViewById(R.id.ongoing_lay);
        my_drive = (LinearLayout) findViewById(R.id.mydrive_lay);
        dashboard = (LinearLayout) findViewById(R.id.dashboard_lay);
        parentSlideLay = (LinearLayout) findViewById(R.id.driver_profile_parent);

        driver_image = (ImageView) findViewById(R.id.dash_image);
        ongoing_img = (ImageView) findViewById(R.id.img_ongoing);
        mydriv_img = (ImageView) findViewById(R.id.img_mydrive);
        dash_img = (ImageView) findViewById(R.id.dash_img);

        options = new DisplayImageOptions.Builder().showImageOnLoading(null) // resource
                // or
                // drawable
                .showImageForEmptyUri(null) // resource or drawable
                .showImageOnFail(null) // resource or drawable
                .resetViewBeforeLoading(false) // default
                .delayBeforeLoading(1000).cacheInMemory(true) // default
                .cacheOnDisc(true) // default
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED) // default
                .bitmapConfig(Bitmap.Config.RGB_565) // default
                .displayer(new SimpleBitmapDisplayer()) // default
                .handler(new Handler()) // default
                // .showImageOnLoading(R.drawable.ic_stub)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                this).defaultDisplayImageOptions(options).build();
        ImageLoader.getInstance().init(config);

        if (SessionSave.getSession("driver_profileimage", DriveHistoryAct.this) != null && SessionSave.getSession("driver_profileimage", DriveHistoryAct.this).equals(""))
        {
            ImageLoader.getInstance().displayImage(SessionSave.getSession("driver_profileimage", DriveHistoryAct.this), driver_image, options);
        }

        txt_driver_profile_name = (TextView) findViewById(R.id.txt_driver_profile_name);
        txt_driver_profile_number = (TextView) findViewById(R.id.txt_driver_profile_number);
        txt_ongoing = (TextView) findViewById(R.id.txt_ongoing);
        txt_mydrive = (TextView) findViewById(R.id.txt_mydrive);
        txt_dash = (TextView) findViewById(R.id.txt_dash);

        dashLay = findViewById(R.id.include_lay);
        layHome = (LinearLayout) findViewById(R.id.home_lay);
        imgHome = (ImageView) findViewById(R.id.home_img);
        txtHome = (TextView) findViewById(R.id.txt_home);

        new requestingApiDate(false);
        edtFromDate.setOnClickListener(this);
        edtToDate.setOnClickListener(this);
        btnGo.setOnClickListener(this);

        if (!SessionSave.getSession("trip_id", DriveHistoryAct.this).equals(""))
        {
            driver_ongoing.setVisibility(View.VISIBLE);
        }
        else
        {
            driver_ongoing.setVisibility(View.GONE);
        }

        txt_driver_profile_name.setText(SessionSave.getSession("Name", DriveHistoryAct.this));
        txt_driver_profile_number.setText(SessionSave.getSession("Phone", DriveHistoryAct.this));

        list.setOnItemClickListener(new OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
            {
                try
                {
                    // if (!isShowingPendingTrip)
                    // {
                    Intent intent = new Intent(DriveHistoryAct.this, TripDetails.class);
                    Bundle bundle = new Bundle();

                    //bundle.putString("FromAct","DriveHistoryAct");
                    bundle.putString("passenger_name", "" + pastJsonArray.getJSONObject(position).getString("passenger_name"));
                    bundle.putString("passengers_log_id", "" + pastJsonArray.getJSONObject(position).getString("passengers_log_id"));
                    bundle.putString("pickup_location", "" + pastJsonArray.getJSONObject(position).getString("pickup_location"));
                    bundle.putString("pickup_time", "" + pastJsonArray.getJSONObject(position).getString("pickup_time"));
                    bundle.putString("drop_location", "" + pastJsonArray.getJSONObject(position).getString("drop_location"));
                    bundle.putString("drop_time", "" + pastJsonArray.getJSONObject(position).getString("drop_time"));
                    bundle.putString("distance", "" + pastJsonArray.getJSONObject(position).getString("distance"));
                    bundle.putString("trip_duration", "" + pastJsonArray.getJSONObject(position).getString("trip_duration"));
                    bundle.putString("travel_status", "" + pastJsonArray.getJSONObject(position).getString("travel_status"));
                    bundle.putString("trip_amt", "" + pastJsonArray.getJSONObject(position).getString("amt"));
                    bundle.putString("pimage",
                            ""
                                    + pastJsonArray.getJSONObject(position)
                                    .getString("profile_image"));
                    bundle.putString("paymenttype",
                            ""
                                    + pastJsonArray.getJSONObject(position)
                                    .getString("payment_type"));


                    // for Bill Summary
                    bundle.putString("driver_name", ""
                            + pastJsonArray.getJSONObject(position)
                            .getString("driver_name"));
                    bundle.putString("pickup_time", ""
                            + pastJsonArray.getJSONObject(position)
                            .getString("pickup_time"));
                    bundle.putString("drop_time", ""
                            + pastJsonArray.getJSONObject(position)
                            .getString("drop_time"));
                    bundle.putString("amt", ""
                            + pastJsonArray.getJSONObject(position)
                            .getString("amt"));

                    bundle.putString("trip_plan", ""
                            + pastJsonArray.getJSONObject(position)
                            .getString("trip_plan"));

                    bundle.putString("payment_mode", ""
                            + pastJsonArray.getJSONObject(position)
                            .getString("payment_mode"));

                    bundle.putString("convenience_charge", ""
                            + pastJsonArray.getJSONObject(position)
                            .getString("convenience_charge"));

                    bundle.putString("overtime_charge", ""
                            + pastJsonArray.getJSONObject(position)
                            .getString("overtime_charge"));
                    bundle.putString("tax", ""
                            + pastJsonArray.getJSONObject(position)
                            .getString("tax"));
                    bundle.putString("passenger_discount", ""
                            + pastJsonArray.getJSONObject(position)
                            .getString("passenger_discount"));
                    bundle.putString("tripfare", ""
                            + pastJsonArray.getJSONObject(position)
                            .getString("tripfare"));

                    bundle.putString("payment_type", ""
                            + pastJsonArray.getJSONObject(position)
                            .getString("payment_type"));
                    bundle.putString("parking_charge", ""
                            + pastJsonArray.getJSONObject(position)
                            .getString("parking_charge"));
                    bundle.putString("other_charge", ""
                            + pastJsonArray.getJSONObject(position)
                            .getString("other_charge"));
                    bundle.putString("tax_percentage", ""
                            + pastJsonArray.getJSONObject(position)
                            .getString("tax_percentage"));

                    intent.putExtras(bundle);
                    startActivity(intent);

                }
                catch (Exception e2)
                {
                    e2.printStackTrace();
                }
            }
        });

        btnGo.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                Calendar c = Calendar.getInstance();

                if (!edtFromDate.getText().toString().trim().equals("") || !edtToDate.getText().toString().trim().equals(""))
                {
                    if (fromDate != null && toDate != null)
                    {
                        if (fromDate.after(toDate))
                        {
                            alert_view(DriveHistoryAct.this, getResources().getString(R.string.message), getResources().getString(R.string.date_comp), getResources().getString(R.string.ok), "");
                        }
                        else if (toDate.after(c.getTime()))
                        {
                            alert_view(DriveHistoryAct.this, getResources().getString(R.string.message), getResources().getString(R.string.todate_comp), getResources().getString(R.string.ok), "");
                        }
                        else
                        {
                            //startDt = edtFromDate.getText().toString().trim() + "00:00:00";
                            new requestingApiDate(true);
                        }
                    }
                    else
                    {
                        new requestingApiDate(true);
                    }
                }
                else
                {
                    alert_view(DriveHistoryAct.this, getResources().getString(R.string.message), getResources().getString(R.string.date_valid), getResources().getString(R.string.ok), "");
                }
            }
        });
    }

    private void setDateTimeField()
    {
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new OnDateSetListener()
        {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                    {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        // edtFromDate.setText(dateFormatter.format(newDate.getTime()));
                        fromDate = new Date(year - 1900, monthOfYear, dayOfMonth);
                        // startDt = dateFormatter.format(fromDate) + "00:00:00";
                        edtFromDate.setText(dateFormatter.format(fromDate));
                    }

                }, newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(this, new OnDateSetListener()
        {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                    {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        toDate = new Date(year - 1900, monthOfYear, dayOfMonth);
                        //endDt = dateformat.format(toDate);
                        edtToDate.setText(dateFormatter.format(toDate));
                    }

                }, newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    /*
     *
     */
    public class requestingApiDate implements APIResult
    {
        public requestingApiDate(boolean isClick)
        {
            if (isClick)
            {
                startDt = edtFromDate.getText().toString().trim();
                endDt = edtToDate.getText().toString().trim();
            }
            else
            {
                startDt = "";
                endDt = "";
            }
            try
            {
                JSONObject j = new JSONObject();
                j.put("driver_id", SessionSave.getSession("Id", DriveHistoryAct.this));
                j.put("start", "0");
                j.put("limit", limit);
                j.put("device_type", "1");
                j.put("start_date", startDt);
                j.put("end_date", endDt);
                String driverTripRequesting = "driver_booking_list";

                Log.e("Requesting date ", "Data: " + j.toString());

                if (isOnline())
                {
                    new APIService_Volley_JSON(DriveHistoryAct.this, this, j, false).execute(driverTripRequesting);
                }
                else
                {
                    alert_view(DriveHistoryAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(
                            R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
                }
            }
            catch (Exception e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        @Override
        public void getResult(boolean isSuccess, String result)
        {
            // TODO Auto-generated method stub
            Log.i("DriveHistory", result.toString());

            try
            {
                if (isSuccess)
                {
                    JSONObject object = new JSONObject(result);

                    if (object.getInt("status") == 1)
                    {
                        object = object.getJSONObject("detail");

                        if (object.has("count_details"))
                        {
                            JSONObject countDetails = object.getJSONObject("count_details");
                            totalAmount = countDetails.getString("total_amount");
                            totalTrip = countDetails.getString("total_trips");
                            totalHours = countDetails.getString("total_hours");
                            Log.e("Count Details: ", "Count Details: " + countDetails);
                            txtRides.setText(totalTrip);
                            txtTotalBilling.setText(totalAmount);
                            txtTotalHrs.setText(totalHours);
                        }

                        pastJsonArray = object.getJSONArray("past_booking");

                        Log.e("PastJsonArray :", "Length: " + pastJsonArray.length());

                        if (pastJsonArray.length() == 0)
                        {
                            alert_view(DriveHistoryAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(R.string.no_data_found), "" + getResources().getString(R.string.ok), "");
                        }

                        pastBooking(pastJsonArray);
                    }
                    else if (object.getInt("status") == -1)
                    {
                        //User has been logged out for any reason
                        askForLogin();
                    }
                    else
                    {
                        alert_view(DriveHistoryAct.this, ""
                                        + getResources().getString(R.string.message),
                                "" + object.getString("message"),
                                "" + getResources().getString(R.string.ok), "");
                    }
                    nonactiityobj.startServicefromNonActivity(DriveHistoryAct.this);
                }
                else
                {
                    alert_view(
                            DriveHistoryAct.this,
                            "" + getResources().getString(R.string.message),
                            ""
                                    + getResources().getString(
                                    R.string.check_net_connection), ""
                                    + getResources().getString(R.string.ok), "");
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }

    /*
     * This click method is used for accessing the
     * activities using menus.
     */
    public void clickMethod(View v)
    {
        Intent i;

        switch (v.getId())
        {
            case R.id.driver_profile_lay:

                showLoading(DriveHistoryAct.this);
                driver_profile.setBackgroundResource(R.color.lightgrey);
//			txt_driver_profile_name.setTextColor(getResources().getColor(
//					R.color.lightgrey));
                // txt_driver_profile_name.setTextColor()

                i = new Intent(this, MeAct.class);
                startActivity(i);
                finish();

                break;

            case R.id.ongoing_lay:

                showLoading(DriveHistoryAct.this);
                driver_ongoing.setBackgroundResource(R.color.gray);
                txt_ongoing.setTextColor(getResources().getColor(R.color.white));
                ongoing_img.setImageResource(R.drawable.ongoing_drive_focus);

                i = new Intent(this, MyStatus.class);
                startActivity(i);
                finish();

                break;

            case R.id.mydrive_lay:

                showLoading(DriveHistoryAct.this);
                my_drive.setBackgroundResource(R.color.gray);
                txt_mydrive.setTextColor(getResources().getColor(R.color.white));
                mydriv_img.setImageResource(R.drawable.mydriver_focus);

                i = new Intent(this, JobsAct.class);
                startActivity(i);
                finish();

                // logout(DashAct.this);

                break;

            case R.id.dashboard_lay:

                showLoading(DriveHistoryAct.this);
                dashboard.setBackgroundResource(R.color.gray);
                txt_dash.setTextColor(getResources().getColor(R.color.white));
                dash_img.setImageResource(R.drawable.dashboard_focus);
                // dashboard.setBackgroundColor(getResources().getColor(R.id.li))
                i = new Intent(this, HomeActivity.class);
                startActivity(i);
                finish();

                break;

            case R.id.home_lay:

                showLoading(DriveHistoryAct.this);
                layHome.setBackgroundResource(R.color.gray);
                txtHome.setTextColor(getResources().getColor(R.color.white));
                imgHome.setImageResource(R.drawable.home_focus);
                // dashboard.setBackgroundColor(getResources().getColor(R.id.li))
                i = new Intent(this, HomeActivity.class);
                startActivity(i);
                finish();
                break;

            case R.id.menu_img:

                if (parentSlideLay.isShown())
                {
                    parentSlideLay.setVisibility(View.GONE);
                    dashLay.setVisibility(View.GONE);
                }
                else
                {
                    parentSlideLay.setVisibility(View.VISIBLE);
                    dashLay.setVisibility(View.VISIBLE);
                }

                break;

            default:
                break;
        }
    }

    /*
     * This method is used to store all the details about driver
     * past booking for future reference.
     */
    private void pastBooking(JSONArray pastArray)
    {
        try
        {
            int arrayLength = pastArray.length();
            nonactiityobj.startServicefromNonActivity(DriveHistoryAct.this);

            if (arrayLength > 0)
            {
                ArrayList<HashMap<String, String>> pastTripList = new ArrayList<HashMap<String, String>>(arrayLength);

                for (int iterator = 0; iterator < arrayLength; iterator++)
                {
                    HashMap<String, String> hashMap = new HashMap<String, String>();
                    hashMap.put("passenger_name", "" + pastArray.getJSONObject(iterator).getString("passenger_name"));
                    hashMap.put("passengers_log_id", "" + pastArray.getJSONObject(iterator).getString("passengers_log_id"));
                    hashMap.put("pickup_location", "" + pastArray.getJSONObject(iterator).getString("pickup_location"));
                    hashMap.put("pickup_time", "" + pastArray.getJSONObject(iterator).getString("pickup_time"));
                    hashMap.put("drop_location", "" + pastArray.getJSONObject(iterator).getString("drop_location"));
                    hashMap.put("travel_status", "" + pastArray.getJSONObject(iterator).getString("travel_status"));
                    hashMap.put("waiting_hour", "" + pastArray.getJSONObject(iterator).getString("waiting_hour"));
                    hashMap.put("distance", "" + pastArray.getJSONObject(iterator).getString("distance"));
                    hashMap.put("drop_latitude", "" + pastArray.getJSONObject(iterator).getString("drop_latitude"));
                    hashMap.put("drop_longitude", "" + pastArray.getJSONObject(iterator).getString("drop_longitude"));
                    hashMap.put("bill_amount", "" + pastArray.getJSONObject(iterator).getString("amt"));
                    pastTripList.add(hashMap);
                }

                list.setVisibility(View.VISIBLE);
                SessionSave.saveSession("DriverAdapter", "DriveHistory", DriveHistoryAct.this);
                JobsListAdapter adapter = new JobsListAdapter(DriveHistoryAct.this, pastTripList);
                adapter.notifyDataSetChanged();
                list.setAdapter(adapter);
            }
            else
            {
                list.setVisibility(View.GONE);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            nonactiityobj.startServicefromNonActivity(DriveHistoryAct.this);
        }
    }

    // This the click method for both from date and end date views.
    @Override
    public void onClick(View view)
    {
        // TODO Auto-generated method stub

        if (view == edtFromDate)
        {
            fromDatePickerDialog.show();
        }
        else if (view == edtToDate)
        {
            toDatePickerDialog.show();
        }
    }

    @Override
    public void onBackPressed()
    {
//		super.onBackPressed();
        startActivity(new Intent(DriveHistoryAct.this, DashAct.class));
        CommonData.Animation1(DriveHistoryAct.this);
    }
}
