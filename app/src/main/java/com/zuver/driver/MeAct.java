package com.zuver.driver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.zuver.driver.data.CommonData;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.service.APIService_Volley_JSON;
import com.zuver.driver.utils.FontHelper;
import com.zuver.driver.utils.RoundedImageView;
import com.zuver.driver.utils.SessionSave;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;


/**
 * This is driver profile of the application, maintaining the
 * more details of the driver.
 *
 * @author developer
 */

public class MeAct extends MainActivity implements OnClickListener
{
    // Class members declarations.
    private TextView DoneBtn;
    private TextView HeadTitle;
    private TextView forgotpswdTxt;
    private EditText emailEdt;
    private EditText mobileEdt;
    private EditText passwordEdt;
    private EditText confirmpswdEdt;
    private EditText bankEdt;
    private EditText bankaccnoEdt;
    private EditText firstTxt;
    private EditText lastTxt;
    private String phone, name, company, age, Language, experience, license,
            address;
    private String newpwd;
    private String confirmpwd;
    private String bankname;
    private String bankaccNo;
    public static Activity mFlagger;
    private ImageView profileImage;
    private String base64 = "", file_base64 = "";
    private LinearLayout slider;
    public static MeAct profileAct;
    Dialog fileDialog;
    private TextView btnLogout, btnupload;
    public ImageView fileimg;
    int imageSelect = 0, imageSelected = 0;
    private ImageLoader imageLoader;
    private Bitmap mBitmap;
    private Uri imageUri;
    private Bitmap downImage;
    private RelativeLayout layProfileDetails, layCall;
    Date oldDate, today;
    private SimpleDateFormat dateFormatter;
    private RoundedImageView roundProfileImage;


    private TextView nameValue, licenseValue, ageValue, expValue, ratingValue,
            addressValue, langValue, emailValue, companyValue, txtEditImage, txtCall;

    private boolean edtFlag = false;

    LinearLayout driver_profile, driver_ongoing, my_drive, dashboard,
            parentSlideLay;
    ImageView driver_image, ongoing_img, mydriv_img, dash_img, img_edit,
            menu_img;
    TextView txt_driver_profile_name, txt_driver_profile_number, txt_ongoing,
            txt_mydrive, txt_dash;

    View dashLay;

    LinearLayout layHome;
    TextView txtHome, txtChangePassword;
    ImageView imgHome;
    private RatingBar ratingBar;

    private DatePickerDialog toDatePickerDialog;

    private DisplayImageOptions options;

    // Set the layout to activity.
    @Override
    public int setLayout()
    {
        setLocale();
        return R.layout.me_lay;
    }

    // Initialize the views on layout
    @Override
    public void Initialize() {
        try {

            System.out.println("get trip id MeAct:" + SessionSave.getSession("trip_id", MeAct.this));

            CommonData.sContext = this;
            CommonData.current_act = "MeAct";
            FontHelper.applyFont(this, findViewById(R.id.me_layout), "DroidSans.ttf");
            profileAct = this;
            slider = (LinearLayout) findViewById(R.id.slideImg);
            DoneBtn = (TextView) findViewById(R.id.donebtn);
            HeadTitle = (TextView) findViewById(R.id.headerTxt);
            firstTxt = (EditText) findViewById(R.id.firstTxt);
            lastTxt = (EditText) findViewById(R.id.lastTxt);
            emailEdt = (EditText) findViewById(R.id.emailEdt);
            emailEdt.setEnabled(false);
            mobileEdt = (EditText) findViewById(R.id.mobileEdt);
            mobileEdt.setEnabled(false);
            btnupload = (TextView) findViewById(R.id.btnupload);
            passwordEdt = (EditText) findViewById(R.id.passwordEdt);
            confirmpswdEdt = (EditText) findViewById(R.id.confirmpswdEdt);
            bankEdt = (EditText) findViewById(R.id.bankEdt);
            bankaccnoEdt = (EditText) findViewById(R.id.bankaccnoEdt);
            profileImage = (ImageView) findViewById(R.id.profileimage);
            ratingBar = (RatingBar) findViewById(R.id.ratingBar);
            // roundProfileImage = (RoundedImageView)
            // findViewById(R.id.dash_image);

            nameValue = (EditText) findViewById(R.id.txt_name_value);
            licenseValue = (EditText) findViewById(R.id.txt_license_value);
            ageValue = (TextView) findViewById(R.id.txt_age_value);
            companyValue = (EditText) findViewById(R.id.txt_company_value);
            langValue = (EditText) findViewById(R.id.txt_lang_value);
            expValue = (EditText) findViewById(R.id.txt_exp_value);
            ratingValue = (EditText) findViewById(R.id.txt_rate_value);
            addressValue = (EditText) findViewById(R.id.txt_address_value);
            emailValue = (TextView) findViewById(R.id.txt_email_value);

            txtEditImage = (TextView) findViewById(R.id.txt_edit);
            txtChangePassword = (TextView) findViewById(R.id.txt_change_password);
            HeadTitle.setText(profileAct.getResources()
                    .getString(R.string.m_me));
            DoneBtn.setVisibility(View.GONE);
            DoneBtn.setText(profileAct.getResources()
                    .getString(R.string.save));
            btnLogout = (TextView) findViewById(R.id.btnlogout);
            layProfileDetails = (RelativeLayout) findViewById(R.id.details_lay);
            layCall = (RelativeLayout) findViewById(R.id.lay_call);

            btnLogout.setOnClickListener(this);

            txtChangePassword.setText("*********\n" + getResources().getString(R.string.c_chnpwd));
            final View view = View.inflate(MeAct.this,
                    R.layout.fileupload_popup, null);
            fileDialog = new Dialog(MeAct.this, R.style.dialogwinddow);
            fileDialog.setContentView(view);

            FontHelper.applyFont(MeAct.this, fileDialog.findViewById(R.id.topid_fileup), "DroidSans.ttf");
            fileDialog.setCancelable(true);
            imageLoader = ImageLoader.getInstance();
            imageLoader.init(ImageLoaderConfiguration.createDefault(this));
            clickEditableFieldFalse();

            driver_profile = (LinearLayout) findViewById(R.id.driver_profile_lay);
            driver_ongoing = (LinearLayout) findViewById(R.id.ongoing_lay);
            my_drive = (LinearLayout) findViewById(R.id.mydrive_lay);
            dashboard = (LinearLayout) findViewById(R.id.dashboard_lay);
            parentSlideLay = (LinearLayout) findViewById(R.id.driver_profile_parent);

            driver_image = (ImageView) findViewById(R.id.dash_image);
            ongoing_img = (ImageView) findViewById(R.id.img_ongoing);
            mydriv_img = (ImageView) findViewById(R.id.img_mydrive);
            dash_img = (ImageView) findViewById(R.id.dash_img);

            txt_driver_profile_name = (TextView) findViewById(R.id.txt_driver_profile_name);
            txt_driver_profile_number = (TextView) findViewById(R.id.txt_driver_profile_number);
            txt_ongoing = (TextView) findViewById(R.id.txt_ongoing);
            txt_mydrive = (TextView) findViewById(R.id.txt_mydrive);
            txt_dash = (TextView) findViewById(R.id.txt_dash);

            txtCall = (TextView) findViewById(R.id.txt_call);

            dashLay = findViewById(R.id.include_lay);


            layHome = (LinearLayout) findViewById(R.id.home_lay);
            imgHome = (ImageView) findViewById(R.id.home_img);
            txtHome = (TextView) findViewById(R.id.txt_home);
            if (!SessionSave.getSession("trip_id", MeAct.this).equals("")) {
                driver_ongoing.setVisibility(View.VISIBLE);
            } else {
                driver_ongoing.setVisibility(View.GONE);
            }
            options = new DisplayImageOptions.Builder().showImageOnLoading(null) // resource
                    // or
                    // drawable
                    .showImageForEmptyUri(null) // resource or drawable
                    .showImageOnFail(null) // resource or drawable
                    .resetViewBeforeLoading(false) // default
                    .delayBeforeLoading(1000).cacheInMemory(true) // default
                    .cacheOnDisc(true) // default
                    .imageScaleType(ImageScaleType.EXACTLY_STRETCHED) // default
                    .bitmapConfig(Bitmap.Config.RGB_565) // default
                    .displayer(new SimpleBitmapDisplayer()) // default
                    .handler(new Handler()) // default
                    // .showImageOnLoading(R.drawable.ic_stub)
                    .build();
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                    this).defaultDisplayImageOptions(options).build();
            ImageLoader.getInstance().init(config);

            oldDate = new Date();
            dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

            today = new Date();
            dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

            // edtFromDate.setText(dateFormatter.format(today));
            Calendar newCalendar = Calendar.getInstance();
            toDatePickerDialog = new DatePickerDialog(this,
                    new OnDateSetListener() {


                        @SuppressLint("NewApi")
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            Calendar newDate = Calendar.getInstance();
                            newDate.set(year, monthOfYear, dayOfMonth);

                            toDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime());

                            oldDate = new Date(year - 1900, monthOfYear,
                                    dayOfMonth);
                            int yr = getYearsBetweenDates(today, oldDate);
                            yr = (yr < 0 ? (-(yr) - 1) : yr - 1);
                            ageValue.setText("" + yr);
//							Log.e("Year", "Year: "+yr);
                            Log.e("Date Picker:", "" + dateFormatter.format(oldDate));
//							}
//							Log.e("Date :", "Date: "+oldDate.getYear() +":"+today.getYear());
                        }

                    }, newCalendar.get(Calendar.YEAR),
                    newCalendar.get(Calendar.MONTH),
                    newCalendar.get(Calendar.DAY_OF_MONTH));

            JSONObject j = new JSONObject();
            j.put("userid", SessionSave.getSession("Id", MeAct.this));
            String pro_url = "driver_profile";
            new GetProfileData(pro_url, j);

            txt_driver_profile_name.setText(SessionSave.getSession("Name", MeAct.this));
            txt_driver_profile_number.setText(SessionSave.getSession("Phone", MeAct.this));
            setOnclicklistener();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    /*
     * This method is used for calculating the years between two dates.
     */
    public int getYearsBetweenDates(Date first, Date second) {
        Calendar firstCal = GregorianCalendar.getInstance();
        Calendar secondCal = GregorianCalendar.getInstance();

        firstCal.setTime(first);
        secondCal.setTime(second);

        secondCal.add(Calendar.DAY_OF_YEAR, -firstCal.get(Calendar.DAY_OF_YEAR));

        return secondCal.get(Calendar.YEAR) - firstCal.get(Calendar.YEAR);
    }


    private void clickEditableFieldFalse() {
        nameValue.setEnabled(false);
        licenseValue.setEnabled(false);
        companyValue.setEnabled(false);
        langValue.setEnabled(false);
        expValue.setEnabled(false);
        addressValue.setEnabled(false);
    }

    private void clickEditableFieldTrue() {
        nameValue.setEnabled(true);
        licenseValue.setEnabled(true);
        langValue.setEnabled(true);
        expValue.setEnabled(true);
        addressValue.setEnabled(true);
    }

    // Adding click listener to required views.
    private void setOnclicklistener() {

        DoneBtn.setOnClickListener(this);
        profileImage.setOnClickListener(this);
        btnupload.setOnClickListener(this);
        layProfileDetails.setOnClickListener(this);
        txtEditImage.setOnClickListener(this);
        ageValue.setOnClickListener(this);
        layCall.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        //super.onBackPressed();
        startActivity(new Intent(MeAct.this, DashAct.class));
        CommonData.Animation1(MeAct.this);
    }

    // Actions to be performed Onclick.
    @Override
    public void onClick(View v) {
        try {
            // If logout view clicked the following process runs.
            if (v == btnLogout) {
                logout(MeAct.this);

            } else if (v == layProfileDetails) {
                Intent intent = new Intent(MeAct.this, ChangepassAct.class);
                startActivity(intent);
                // finish();
            } else if (v == forgotpswdTxt) {
                Intent intent = new Intent(MeAct.this, ChangepassAct.class);
                startActivity(intent);
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    /**
     * @API call(get method) to get the driver profile data and parsing the
     * response
     */
    private class GetProfileData implements APIResult {
        public GetProfileData(String url, JSONObject data) {
            try {
                if (isOnline()) {
                    new APIService_Volley_JSON(MeAct.this, this, data, false)
                            .execute(url);
                } else {
                    alert_view(
                            MeAct.this,
                            "" + getResources().getString(R.string.message),
                            ""
                                    + getResources().getString(
                                    R.string.check_net_connection), ""
                                    + getResources().getString(R.string.ok), "");
                }
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        @Override
        public void getResult(boolean isSuccess, String result) {
            try {
                if (isSuccess) {
                    JSONObject json = new JSONObject(result);
                    if (json.getInt("status") == 1) {
                        JSONObject details = json.getJSONObject("detail");

                        txtCall.setText(details.getString("phone"));
                        emailValue.setText(details.getString("email"));
                        nameValue.setText(details.getString("name"));
                        companyValue.setText(details.getString("company_name"));
                        ageValue.setText(details.getString("age"));
                        langValue.setText(details.getString("language"));
                        expValue.setText(details.getString("experience"));
                        ratingValue.setText(details.getString("name"));
                        licenseValue.setText(details
                                .getString("driver_license_id"));
                        addressValue.setText(details.getString("address"));
                        String rating = "0.00";
                        if (details.getString("driver_rating") != null) {
                            rating = details.getString("driver_rating");
                        } else {
                            rating = "0.00";
                        }

                        ratingBar.setRating(Float.parseFloat(rating)); // for static content

                        String imgPath = details.getString("main_image_path");
                        try {
                            if (imgPath != null && imgPath.length() > 0) {
                                // imageLoader.displayImage(imgPath,
                                // profileImage, roundedImageoptions);

                                new DownloadImageTask(profileImage)
                                        .execute(imgPath);
                            }
                        } catch (Exception e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }
                        SessionSave.saveSession("Bankname", details.getString("bankname"), MeAct.this);
                        SessionSave.saveSession("Bankaccount_No", details.getString("bankaccount_no"), MeAct.this);
                        SessionSave.saveSession("Org_Password", confirmpwd, MeAct.this);
                        SessionSave.saveSession("Phone", details.getString("phone"), MeAct.this);
                        SessionSave.saveSession("Name", details.getString("name"), MeAct.this);
                        SessionSave.saveSession("Lastname", details.getString("lastname"), MeAct.this);
                        SessionSave.saveSession("Email", details.getString("email"), MeAct.this);
                    }
                    else if (json.getInt("status") == -1){
                        //User has been logged out for any reason
                        askForLogin();
                    }

                } else {
                    alert_view(MeAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(
                            R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(final ImageView bmImage) {
            this.bmImage = bmImage;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(final String... urls) {
            final String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                final InputStream in = new java.net.URL(urldisplay)
                        .openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (final Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            bmImage.setImageBitmap(result);
            downImage = result;

            if (SessionSave.getSession("driver_profileimage", MeAct.this) != null) {

                ImageLoader.getInstance().displayImage(SessionSave.getSession("driver_profileimage",
                        MeAct.this), driver_image, options);
            }
        }
    }

    private class ImageCompressionAsyncTask extends
            AsyncTask<String, Void, Bitmap> {
        private Dialog mDialog;
        private String result;
        private int orientation;
        private int requestcode;

        public ImageCompressionAsyncTask(int reqcode) {
            // TODO Auto-generated constructor stub
            requestcode = reqcode;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            final View view = View.inflate(MeAct.this, R.layout.progress_bar,
                    null);
            mDialog = new Dialog(MeAct.this, R.style.NewDialog);
            mDialog.setContentView(view);
            mDialog.setCancelable(false);
            mDialog.show();
        }

        @Override
        protected Bitmap doInBackground(final String... params) {
            try {
                result = getRealPathFromURI(params[0]);
                final File file = new File(result);
                mBitmap = decodeImageFile(file);
                if (mBitmap != null) {
                    final Matrix matrix = new Matrix();
                    try {
                        final ExifInterface exif = new ExifInterface(
                                file.getAbsolutePath());
                        orientation = exif.getAttributeInt(
                                ExifInterface.TAG_ORIENTATION, 1);
                        if (orientation == 3) {
                            matrix.postRotate(180);
                            mBitmap = Bitmap.createBitmap(mBitmap, 0, 0,
                                    mBitmap.getWidth(), mBitmap.getHeight(),
                                    matrix, true);
                        } else if (orientation == 6) {
                            matrix.postRotate(90);
                            mBitmap = Bitmap.createBitmap(mBitmap, 0, 0,
                                    mBitmap.getWidth(), mBitmap.getHeight(),
                                    matrix, true);
                        } else if (orientation == 8) {
                            matrix.postRotate(270);
                            mBitmap = Bitmap.createBitmap(mBitmap, 0, 0,
                                    mBitmap.getWidth(), mBitmap.getHeight(),
                                    matrix, true);
                        }
                    } catch (final IOException e) {
                    }
                }
                final ByteArrayOutputStream stream = new ByteArrayOutputStream();
                mBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            } catch (final Exception e) {
                // TODO: handle exception
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mDialog.isShowing())
                            mDialog.dismiss();
                        alert_view(
                                MeAct.this,
                                "" + getResources().getString(R.string.message),
                                ""
                                        + getResources().getString(
                                        R.string.image_failed),
                                "" + getResources().getString(R.string.ok), "");
                        if (requestcode == 0 || requestcode == 1) {
                            profileImage.setBackgroundDrawable(getResources()
                                    .getDrawable(R.drawable.profile_img));
                        } else {
                            imageSelected = 0;
                            fileimg.setBackgroundDrawable(getResources()
                                    .getDrawable(R.drawable.no_file));
                        }
                    }
                });
            }
            return mBitmap;
        }

        @Override
        protected void onPostExecute(final Bitmap result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (mDialog.isShowing())
                mDialog.dismiss();
            if (requestcode == 0 || requestcode == 1)
                profileImage.setImageBitmap(result);
            else {
                imageSelected = 1;
                fileimg.setImageBitmap(result);
            }
        }
    }

    public static Bitmap decodeImageFile(final File f) {
        try {
            final BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
            final int REQUIRED_SIZE = 100;
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE
                    && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;
            final BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (final FileNotFoundException e) {
        }
        return null;
    }

    private String getRealPathFromURI(final String contentURI) {
        final Uri contentUri = Uri.parse(contentURI);
        final Cursor cursor = getContentResolver().query(contentUri, null,
                null, null, null);
        if (cursor == null)
            return contentUri.getPath();
        else {
            cursor.moveToFirst();
            final int idx = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }


    @Override
    protected void onActivityResult(int requestcode, int resultcode, Intent data) {
        super.onActivityResult(requestcode, resultcode, data);
        try {
            if (resultcode == RESULT_OK) {
                System.gc();
                switch (requestcode) {
                    case 0:
                        try {
                            new ImageCompressionAsyncTask(requestcode).execute(data
                                    .getDataString());
                        } catch (final Exception e) {
                        }
                        break;
                    case 1:
                        try {
                            new ImageCompressionAsyncTask(requestcode).execute(
                                    imageUri.toString()).get();
                        } catch (final Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case 2:
                        try {
                            new ImageCompressionAsyncTask(requestcode).execute(data
                                    .getDataString());
                        } catch (final Exception e) {
                        }
                        break;
                    case 3:
                        try {
                            new ImageCompressionAsyncTask(requestcode).execute(
                                    imageUri.toString()).get();
                        } catch (final Exception e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }


    // Accessing the activities from the slide menu bar.
    public void clickMethod(View v) {
        Intent i;
        switch (v.getId()) {

            case R.id.driver_profile_lay:
                driver_profile.setBackgroundResource(R.color.lightgrey);
//			txt_driver_profile_name.setTextColor(getResources().getColor(
//					R.color.lightgrey));
                // txt_driver_profile_name.setTextColor()
                showLoading(MeAct.this);
                i = new Intent(this, MeAct.class);
                startActivity(i);
                finish();
                break;

            case R.id.ongoing_lay:
                showLoading(MeAct.this);
                driver_ongoing.setBackgroundResource(R.color.gray);
                txt_ongoing.setTextColor(getResources().getColor(R.color.white));
                ongoing_img.setImageResource(R.drawable.ongoing_drive_focus);
                if (SessionSave.getSession("status", MeAct.this).equals("A") || SessionSave.getSession("status", MeAct.this).equals("B")) {
                    Intent intentOnGoing = new Intent(MeAct.this, OngoingAct.class);
                    startActivity(intentOnGoing);
                    finish();
                }
                break;

            case R.id.mydrive_lay:
                my_drive.setBackgroundResource(R.color.gray);
                txt_mydrive.setTextColor(getResources().getColor(R.color.white));
                mydriv_img.setImageResource(R.drawable.mydriver_focus);

                showLoading(MeAct.this);
                i = new Intent(this, JobsAct.class);
                startActivity(i);
                finish();

                // logout(DashAct.this);
                break;
            case R.id.dashboard_lay:
                dashboard.setBackgroundResource(R.color.gray);
                txt_dash.setTextColor(getResources().getColor(R.color.white));
                dash_img.setImageResource(R.drawable.dashboard_focus);
                // dashboard.setBackgroundColor(getResources().getColor(R.id.li))
                showLoading(MeAct.this);
                i = new Intent(this, HomeActivity.class);
                startActivity(i);
                finish();
                break;

            case R.id.home_lay:
                layHome.setBackgroundResource(R.color.gray);
                txtHome.setTextColor(getResources().getColor(R.color.white));
                imgHome.setImageResource(R.drawable.home_focus);
                // dashboard.setBackgroundColor(getResources().getColor(R.id.li))
//			showLoading(DashAct.this);
                i = new Intent(this, DashAct.class);
                startActivity(i);
                finish();
                break;

            case R.id.slideImg:

                if (parentSlideLay.isShown()) {
                    parentSlideLay.setVisibility(View.GONE);
                    dashLay.setVisibility(View.GONE);
                } else {
                    parentSlideLay.setVisibility(View.VISIBLE);
                    dashLay.setVisibility(View.VISIBLE);
                }
                break;

            default:
                break;
        }
    }


}
