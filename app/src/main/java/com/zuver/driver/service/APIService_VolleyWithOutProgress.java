package com.zuver.driver.service;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.zuver.driver.R;
import com.zuver.driver.data.CommonData;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.utils.NetworkStatus;
import com.zuver.driver.utils.SessionSave;
import com.zuver.volley.AppController;

/**
 * @author developer This AsyncTask used to communicate the application with server through Volley framework. Here the response completely in JSON format. Constructor get the input details List<NameValuePair>,POST or GET then url. In Background,Connect and get the response. In Post execute, Return the result with interface. This class call the API without any progress on UI.
 */
public class APIService_VolleyWithOutProgress extends AsyncTask<String, String, String>
{
    HashMap<String, String> map;
    final String check_internet = "Check your Internet Connection";
    final String please_try_again = "Please try again later!";
    StringRequest ObjReq;
    public Context mContext;
    private boolean isSuccess = true;
    private boolean GetMethod = true;
    private final List<NameValuePair> data;
    private APIResult response;
    private InputStream in;
    int requestingMethod = 0;
    String result = "";

    public APIService_VolleyWithOutProgress(Context ctx, APIResult res, List<NameValuePair> data, boolean getmethod)
    {
        mContext = ctx;
        response = res;
        this.data = data;
        GetMethod = getmethod;
    }

    @Override
    protected void onPreExecute()
    {
        // TODO Auto-generated method stub
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params)
    {
        // TODO Auto-generated method stub
        if (!NetworkStatus.isOnline(mContext))
        {
            isSuccess = false;
            result = "" + mContext.getResources().getString(R.string.check_net_connection);
            return result;
        }

        params[0] = params[0].replace(" ", "%20");
        String url = CommonData.BasePath + "lang=" + SessionSave.getSession("Lang", mContext) + "&" + params[0];
        Log.d("Driver Api request", "" + url + "\n data" + data);

        if (GetMethod)
        {
            requestingMethod = 0;
        }
        else
        {
            requestingMethod = 1;

            if (data != null)
            {
                map = new HashMap<String, String>();

                for (int i = 0; i < data.size(); i++)
                {
                    String values = "";
                    String dataset = data.get(i).toString();
                    String[] datasetary = dataset.split("=");
                    String key = datasetary[0];

                    if (datasetary.length > 1)
                    {
                        values = datasetary[1];
                    }

                    map.put(key, values);
                }
            }
        }

        ObjReq = new StringRequest(requestingMethod, url, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String res)
            {
                ObjReq.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                response.getResult(isSuccess, res);
                result = res;
                System.out.println("Driver Api response" + result);
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                ObjReq.setRetryPolicy(new DefaultRetryPolicy(15000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                String errorMessage = "Please Try Again, ";

                if (error instanceof NetworkError)
                {
                    errorMessage += "Network Connection Error ";
                }
                else if (error instanceof ClientError)
                {
                    errorMessage += "Client Error ";
                }
                else if (error instanceof ServerError)
                {
                    errorMessage += "Server connection Error ";
                }
                else if (error instanceof AuthFailureError)
                {
                    errorMessage += "AuthFailureError ";
                }
                else if (error instanceof ParseError)
                {
                    errorMessage += "ParseError ";
                }
                else if (error instanceof TimeoutError)
                {
                    errorMessage += "Request Timeout";
                }
                else if (error instanceof NoConnectionError)
                {
                    errorMessage += "Network Connection Error ";
                }

                response.getResult(false, errorMessage);
            }
        }) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> param = new HashMap<String, String>();

                if (!GetMethod)
                {
                    param.putAll(map);
                    return param;
                }

                return param;
            }
        };

        ObjReq.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(ObjReq);
        return result;
    }
}