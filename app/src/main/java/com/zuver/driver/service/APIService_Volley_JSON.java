package com.zuver.driver.service;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.zuver.driver.R;
import com.zuver.driver.data.CommonData;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.utils.NetworkStatus;
import com.zuver.driver.utils.SessionSave;
import com.zuver.volley.AppController;

/**
 * @author developer This AsyncTask used to communicate the application with server through Volley framework. Here the response completely in JSON format. Constructor get the input details List<NameValuePair>,POST or GET then url. In pre execute, Show the progress dialog. In Background,Connect and get the response. In Post execute, Return the result with interface. This class call the API without any progress on UI.
 */
public class APIService_Volley_JSON extends AsyncTask<String, String, String>
{
    HashMap<String, String> map;
    final String check_internet = "Check your Internet Connection";
    final String please_try_again = "Please try again later!";
    public Dialog mProgressdialog;
    StringRequest ObjReq;
    public Context mContext;
    private boolean isSuccess = true;
    private boolean GetMethod = true;
    private Dialog mDialog;
    private JSONObject data;
    private APIResult response;
    private InputStream in;
    int requestingMethod = 0;
    String result = "";
    JsonObjectRequest JOR;

    public APIService_Volley_JSON(Context ctx, APIResult res, JSONObject j, boolean getmethod)
    {
        mContext = ctx;
        response = res;
        this.data = j;
        GetMethod = getmethod;
    }

    public APIService_Volley_JSON(Context ctx, APIResult res, boolean getmethod)
    {
        mContext = ctx;
        response = res;
        GetMethod = getmethod;
    }

    @Override
    protected void onPreExecute()
    {
        // TODO Auto-generated method stub
        super.onPreExecute();

        if (NetworkStatus.isOnline(mContext))
        {
            View view = View.inflate(mContext, R.layout.progress_bar, null);
            mDialog = new Dialog(mContext, R.style.dialogwinddow);
            mDialog.setContentView(view);
            mDialog.setCancelable(false);
            mDialog.show();
        }
    }

    @Override
    protected String doInBackground(String... params)
    {
        // TODO Auto-generated method stub
        if (!NetworkStatus.isOnline(mContext))
        {
            isSuccess = false;
            result = "" + mContext.getResources().getString(R.string.check_net_connection);
            /// return result;
        }

        // params[0] = params[0].replace(" ", "%20");
        String url = CommonData.MAIN_URL + params[0];
        //String url = CommonData.BasePath + "lang=" + SessionSave.getSession("Lang", mContext) + "&" + params[0];
        Log.d("Driver Api request", "" + url + "\ndata" + data);

        if (GetMethod)
        {
            requestingMethod = 0;
        }
        else
        {
            requestingMethod = 1;
        }

        JOR = new JsonObjectRequest(Request.Method.POST, url, data, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject jresponse)
            {
                try
                {
                    if (mDialog != null || mDialog.isShowing())
                    {
                        mDialog.dismiss();
                        mDialog = null;
                    }

                    if (jresponse.has("status"))
                    {
                        Log.d("Status", "Status value from server is: " + jresponse.getString("status"));

                        if (jresponse.getString("status").equalsIgnoreCase("401"))
                        {
                            Log.d("Status", "401 Error occurred.");
                            System.out.println("Driver Api response" + result);

                            if (!((Activity) mContext).isFinishing())
                            {
                                ((Activity) mContext).runOnUiThread(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        /// MainActivity.alertView_with_activity(mContext, "Authentication Failed", "Please contact ZUVER customer care for more details", "OK");
                                        new AlertDialog.Builder(mContext)
                                                .setTitle("Authentication Failed")
                                                .setMessage("Please contact ZUVER customer care for more details")
                                                .setCancelable(false)
                                                .setPositiveButton("OK", new DialogInterface.OnClickListener()
                                                {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which)
                                                    {

                                                    }
                                                }).show();
                                    }
                                });
                            }
                        }
                        else
                        {
                            response.getResult(isSuccess, jresponse.toString());
                            result = jresponse.toString();
                            System.out.println("Driver Api response" + result);
                        }
                    }
                    else
                    {
                        Log.d("Status", "response does not have status");
                    }
                }
                catch (Exception e)
                {
                    Log.d("APIServer Volley", "exception occurred response does not have status");
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                try
                {
                    if (mDialog != null || mDialog.isShowing())
                    {
                        mDialog.dismiss();
                        mDialog = null;
                    }

                    String errorMessage = "Please Try Again, ";

                    if (error instanceof NetworkError)
                    {
                        errorMessage += " Network Connection Error ";
                    }
                    else if (error instanceof ClientError)
                    {
                        errorMessage += " Client Error ";
                    }
                    else if (error instanceof ServerError)
                    {
                        errorMessage += " Server connection Error ";
                    }
                    else if (error instanceof AuthFailureError)
                    {
                        errorMessage += " AuthFailureError ";
                    }
                    else if (error instanceof ParseError)
                    {
                        errorMessage += " ParseError ";
                    }
                    else if (error instanceof NoConnectionError)
                    {
                        errorMessage += " Network Connection Error ";
                    }
                    else if (error instanceof TimeoutError)
                    {
                        errorMessage += " Request Timeout";
                    }

                    response.getResult(false, errorMessage);
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("authKey", SessionSave.getSession("Auth_Key", mContext));
                params.put("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
                params.put("userId", SessionSave.getSession("Id", mContext));
                params.put("userType", SessionSave.getSession("User_Type", mContext));
                return params;

            }
        };

        JOR.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(JOR);
        return result;
    }
}