package com.zuver.driver.service;

/**
 * Created by lenovo on 01-Mar-16.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.zuver.driver.R;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.utils.NetworkStatus;
import com.zuver.driver.utils.SessionSave;
import com.zuver.volley.AppController;

import org.json.JSONObject;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;


public class APIServiceVolley extends AsyncTask<String, String, String>
{
    public Context mContext;
    private boolean isSuccess = true;
    private boolean GetMethod = true;
    private JSONObject data;
    private APIResult response;
    private String mUrl;
    private InputStream in;
    int requestingMethod = 0;
    String result = "";
    JsonObjectRequest JOR;

    public APIServiceVolley(Context ctx, APIResult res, JSONObject j, boolean getmethod, String url)
    {
        mContext = ctx;
        response = res;
        this.data = j;
        GetMethod = getmethod;
        mUrl = url;
    }

    @Override
    protected void onPreExecute()
    {
        // TODO Auto-generated method stub
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params)
    {
        // TODO Auto-generated method stub
        if (!NetworkStatus.isOnline(mContext))
        {
            isSuccess = false;
            result = "" + mContext.getResources().getString(R.string.check_net_connection);
            return result;
        }

        JOR = new JsonObjectRequest(Request.Method.POST, mUrl, data, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject jresponse)
            {
                try
                {
                    response.getResult(isSuccess, jresponse.toString());
                    result = jresponse.toString();
                    System.out.println("ZoomCar Api response" + result);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                try
                {
                    String errorMessage = "Please Try Again, ";

                    if (error instanceof NetworkError)
                    {
                        errorMessage += "Network Connection Error ";
                    }
                    else if (error instanceof ClientError)
                    {
                        errorMessage += "Client Error ";
                    }
                    else if (error instanceof ServerError)
                    {
                        errorMessage += "Server connection Error ";
                    }
                    else if (error instanceof AuthFailureError)
                    {
                        errorMessage += "AuthFailureError ";
                    }
                    else if (error instanceof ParseError)
                    {
                        errorMessage += "ParseError ";
                    }
                    else if (error instanceof NoConnectionError)
                    {
                        errorMessage += "Network Connection Error ";
                    }
                    else if (error instanceof TimeoutError)
                    {
                        errorMessage += "Request Timeout";
                    }

                    response.getResult(false, errorMessage);
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token token=" + SessionSave.getSession("ZoomcarAuthKey", mContext));
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        JOR.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(JOR);
        return result;
    }
}