package com.zuver.driver.service;

import com.zuver.driver.data.CommonData;
import com.zuver.driver.utils.SessionSave;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;

public class Timer1 extends Service
{
    public static String sTimer1 = "00:00:00";
    private long startTime = 0L;
    private final Handler myHandler = new Handler();
    long timeInMillies = 0L;
    long timeSwap = 0L;
    public static long finalTime1 = 0L;
    private String Tag;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        startTime();
    }

    private void startTime()
    {
        CommonData.timer1_stop = false;
        startTime = SystemClock.uptimeMillis();

        if (SessionSave.getSession("speedwaitingTime1", Timer1.this).length() != 0)
        {
            timeSwap = Long.parseLong(SessionSave.getSession("speedwaitingTime1", Timer1.this));
        }
        else
        {
            timeSwap = 0L;
        }

        myHandler.postDelayed(updateTimerMethod, 0);
    }

    private final Runnable updateTimerMethod = new Runnable()
    {
        @Override
        public void run()
        {
            timeInMillies = SystemClock.uptimeMillis() - startTime;
            finalTime1 = timeSwap + timeInMillies;
            int actual_total_second = (int) (finalTime1 / 1000);
            int total_hour = actual_total_second / 3600; // get hours
            int remaining_total_minutes = actual_total_second % 3600;
            int total_minutes = remaining_total_minutes / 60; // get minutes
            int total_seconds = remaining_total_minutes % 60; // get seconds
            sTimer1 = String.format("%02d", total_hour) + ":" + String.format("%02d", total_minutes) + ":" + String.format("%02d", total_seconds);

            myHandler.postDelayed(this, 0);
        }
    };

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        SessionSave.saveSession("speedwaitingTime1", "" + finalTime1, Timer1.this);
        stoptime();
    }


    private void stoptime()
    {
        CommonData.timer1_stop = true;
        timeSwap += timeInMillies;
        myHandler.removeCallbacks(updateTimerMethod);
    }
}
