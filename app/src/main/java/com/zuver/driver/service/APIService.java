package com.zuver.driver.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.view.View;

import com.zuver.driver.R;
import com.zuver.driver.data.CommonData;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.utils.NetworkStatus;
import com.zuver.driver.utils.SessionSave;

/**
 * @author developer This AsyncTask used to communicate the application with server over HTTP request/response. Constructor get the input details List<NameValuePair>,POST or GET then url. In pre execute,Show the progress dialog. In Background,Connect and get the response. In Post execute, Return the result with interface. This class call the API without any progress on UI.
 */
public class APIService extends AsyncTask<String, String, String>
{
    private final Context mContext;
    private boolean isSuccess = true;
    private boolean isGetMethod = true;
    private InputStream in;
    public Dialog mProgressdialog;
    private final APIResult response;
    private List<NameValuePair> data;
    // messages
    final String check_internet = "Check your Internet Connection";
    final String please_try_again = "Please try again later!";
    NonActivity nonactiityobj = new NonActivity();

    public APIService(Context context, APIResult response, boolean isGetMethod)
    {
        mContext = context;
        this.response = response;
        this.isGetMethod = isGetMethod;
    }

    public APIService(Context context, APIResult response, List<NameValuePair> data, boolean isGetMethod)
    {
        mContext = context;
        this.response = response;
        this.data = data;
        this.isGetMethod = isGetMethod;
    }

    @Override
    protected void onPreExecute()
    {
        super.onPreExecute();

        try
        {
            // nonactiityobj.stopServicefromNonActivity(mContext);
            View view = View.inflate(mContext, R.layout.progress_bar, null);
            mProgressdialog = new Dialog(mContext, R.style.dialogwinddow);
            mProgressdialog.setContentView(view);
            mProgressdialog.setCancelable(false);
            mProgressdialog.show();
        }
        catch (Exception e)
        {
            // mProgressdialog.show();
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(String... params)
    {
        String result = "";

        if (!NetworkStatus.isOnline(mContext))
        {
            isSuccess = false;
            result = check_internet;
            return result;
        }

        params[0] = params[0].replace(" ", "%20");
        String url = CommonData.BasePath + "lang=" + SessionSave.getSession("Lang", mContext) + "&" + "" + params[0];
        Log.d("Driver Api request", "" + url + "\ndata" + data);

        try
        {
            if (isGetMethod)
            {
                URL u = new URL(url);
                URLConnection c = u.openConnection();
                c.setConnectTimeout(100000);
                c.setReadTimeout(100000);
                c.connect();
                in = c.getInputStream();
                // PostMethod
            }
            else
            {
                in = postData(url, data);
            }
        }
        catch (SocketTimeoutException ex)
        {
            if (mProgressdialog.isShowing())
                mProgressdialog.dismiss();
            return "server connection timout";
        }
        catch (ClientProtocolException ex)
        {
            if (mProgressdialog.isShowing())
                mProgressdialog.dismiss();
            Log.i("ClientProtocolException", "" + ex.toString());
            return "Client Protocol Exception";
        }
        catch (IOException ioe)
        {
            if (mProgressdialog.isShowing())
                mProgressdialog.dismiss();
            Log.i("IOException", "" + ioe.toString());
            ioe.printStackTrace();
            return "IOException";
        }
        catch (Exception e)
        {
            if (mProgressdialog.isShowing())
                mProgressdialog.dismiss();
            e.printStackTrace();
            return "Exception";
        }

        // convert response to string
        if (in == null)
        {
            isSuccess = false;
            return please_try_again;
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder builder = new StringBuilder();
        String temp = "";

        try
        {
            while ((temp = reader.readLine()) != null)
            {
                builder.append(temp);
                result = builder.toString();
            }
        }
        catch (IOException e)
        {
            isSuccess = false;
            result = please_try_again;

            if (mProgressdialog.isShowing())
                mProgressdialog.dismiss();
        }

        System.out.println("Driver Api response" + result);
        return result;
    }

    @Override
    protected void onPostExecute(String result)
    {
        super.onPostExecute(result);
        try
        {
            if (mProgressdialog.isShowing())
                mProgressdialog.dismiss();

            response.getResult(isSuccess, Html.fromHtml(result).toString());
            // nonactiityobj.startServicefromNonActivity(mContext);
        }
        catch (Exception e)
        {
            if (mProgressdialog.isShowing())
                mProgressdialog.dismiss();
            Log.i("Exception", e.toString());
            e.printStackTrace();
        }
    }

    private InputStream postData(String Url, List<NameValuePair> data)
    {
        HttpParams httpParams = new BasicHttpParams();
        ConnManagerParams.setTimeout(httpParams, 100000);
        HttpConnectionParams.setConnectionTimeout(httpParams, 150000);
        HttpConnectionParams.setSoTimeout(httpParams, 150000);
        HttpClient httpclient = new DefaultHttpClient(httpParams);
        HttpPost httppost = new HttpPost(Url);

        try
        {
            httppost.setEntity(new UrlEncodedFormEntity(data));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            return entity.getContent();
        }
        catch (ClientProtocolException e)
        {
            if (mProgressdialog.isShowing())
                mProgressdialog.dismiss();
            e.printStackTrace();
        }
        catch (IOException e)
        {
            if (mProgressdialog.isShowing())
                mProgressdialog.dismiss();
            e.printStackTrace();
        }

        return null;
    }
}