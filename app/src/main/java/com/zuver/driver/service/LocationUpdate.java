package com.zuver.driver.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONObject;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.zuver.driver.CanceltripAct;
import com.zuver.driver.HomeActivity;
import com.zuver.driver.MainActivity;
import com.zuver.driver.NotificationAct;
import com.zuver.driver.R;
import com.zuver.driver.data.CommonData;
import com.zuver.driver.utils.LocationDb;
import com.zuver.driver.utils.NetworkStatus;
import com.zuver.driver.utils.SessionSave;

import javax.net.ssl.HttpsURLConnection;

/**
 * This class helps to get the driver current location using location client. It
 * Keep on updating driver location to server with certain time interval (Every
 * 5sec). In this class,Driver gets the new request notification and trip cancel
 * notifications.
 */
@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class LocationUpdate extends Service implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener
{
    private final Timer mTimer = new Timer();
    public static double latitude1 = 0.0;
    public static double longitude1 = 0.0;

    String c_lat = "", c_lon = "";
    private String sLocation = "", cLoctions = "";
    int locationUpdate = 60000;
    private Handler mhandler;
    private Context mContext;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private static int Notification_ID = 1;
    LocationDb objLocationDb;
    float _speed;
    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 10000; // 5 sec
    private static int FATEST_INTERVAL = 7000; // 5 sec
    private static float DISPLACEMENT = 0f; // 10 meters
    private Location mLastLocation;


    @Override
    public void onStart(final Intent intent, final int startId)
    {
        super.onStart(intent, startId);

        if (mGoogleApiClient != null)
        {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        mContext = this;
        mhandler = new Handler(Looper.getMainLooper());
        objLocationDb = new LocationDb(mContext);

        //region Edited by Amit Jangid

        /**
         * Edited by Amit Jangid
         * Getting current shift status & trip id from session
        **/
        String tripID = SessionSave.getSession("trip_id", mContext);
        String currentShiftStatus = SessionSave.getSession("shift_status", mContext);
        /// Log.d("OnCreate LocationUpdate", "Trip id in location update class is: " + tripID);
        /// Log.d("OnCreate LocationUpdate", "Current shift status in location update class is: " + currentShiftStatus);

        /**
         * checking the current shift status
         * if current shift status is 'IN' then update the location in 1 min
         * else update the location in every one hour
        **/
        if (currentShiftStatus.equalsIgnoreCase("IN"))
        {
            /**
             * the driver is 'on duty' so the update interval for location ping is 1 min
             * the location of the driver should be updated in every 1 min
             * checking if the trip id from session is empty or not
             * if the trip id is not empty that means driver is assigned with a trip
             * when the driver accepts the trip the location update should be in every 15 seconds
             */
            if (!tripID.equalsIgnoreCase(""))
            {
                /// driver is on trip
                /// update the location in every 15 seconds
                locationUpdate = 15 * 1000; /// 15 seconds
                //Log.d("OnCreate LocationUpdate", "location update ping from on trip");
                mTimer.scheduleAtFixedRate(new getRideUpdates(), 0, locationUpdate);
            }
            else
            {
                /// driver is not on trip
                /// but he is on duty
                /// update the location in every 1 min
                locationUpdate = 60 * 1000; /// 60 seconds
                //Log.d("OnCreate LocationUpdate", "location update ping from on duty");
                mTimer.scheduleAtFixedRate(new getRideUpdates(), 0, locationUpdate);
            }
        }
        else
        {
            /**
             * this locationUpdate time is when the driver is off duty
             * the location of the driver has to be updated in one hour
             */
            locationUpdate = 3600 * 1000; /// 1 hour
            //Log.d("OnCreate LocationUpdate", "location update ping from off duty");
            mTimer.scheduleAtFixedRate(new getRideUpdates(), 0, locationUpdate);
        }

        //endregion Edited by Amit Jangid

        if (servicesConnected())
        {
            buildGoogleApiClient();
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(UPDATE_INTERVAL);
            mLocationRequest.setFastestInterval(FATEST_INTERVAL);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
        }
    }

    private class getRideUpdates extends TimerTask
    {
        @Override
        public void run()
        {
            try
            {
                Location loc = getLastKnownLoaction(true);

                if (loc != null)
                {
                    latitude1 = loc.getLatitude();
                    longitude1 = loc.getLongitude();
                    c_lat = String.valueOf(loc.getLatitude());
                    c_lon = String.valueOf(loc.getLongitude());
                    CommonData.getlatitude = latitude1;
                    CommonData.getlongitude = longitude1;
                    sLocation = "" + latitude1 + "," + longitude1 + "|";
                }

                sLocation = "" + latitude1 + "," + longitude1 + "|";
                SessionSave.saveSession("latitude", c_lat, mContext);
                SessionSave.saveSession("longitude", c_lon, mContext);
                cLoctions = sLocation;
                SessionSave.saveSession("cLoctions", cLoctions, mContext);
                sLocation = "";

                try
                {
                    JSONObject j = new JSONObject();
                    j.put("driver_id", SessionSave.getSession("Id", mContext));
                    j.put("trip_id", SessionSave.getSession("trip_id", mContext));
                    j.put("longitude", SessionSave.getSession("longitude", mContext));
                    j.put("latitude", SessionSave.getSession("latitude", mContext));
                    j.put("status", SessionSave.getSession("status", mContext));
                    j.put("travel_status", SessionSave.getSession("travel_status", mContext));
                    String url = CommonData.MAIN_URL + "driver_location_history";
                    new getRideUpdate(url, j).execute();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    private class getRideUpdate extends AsyncTask<String, Void, String>
    {
        private JSONObject mInputData;
        private String mUrl;

        private getRideUpdate(String url, JSONObject data)
        {
            this.mInputData = data;
            this.mUrl = url;
        }

        protected String doInBackground(String... arg0)
        {
            try
            {
                URL url = new URL(mUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);//milliseconds
                conn.setConnectTimeout(15000);//milliseconds
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
                conn.setRequestProperty("authKey", "" + SessionSave.getSession("Auth_Key", mContext));
                conn.setRequestProperty("userType", "" + SessionSave.getSession("User_Type", mContext));
                conn.setRequestProperty("userId", "" + SessionSave.getSession("Id", mContext));
                conn.setDoInput(true);
                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(this.mInputData));
                writer.flush();
                writer.close();
                os.close();
                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK)
                {
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(
                                    conn.getInputStream()));
                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            if (result != null && !result.equalsIgnoreCase(""))
            {
                try
                {
                    Log.e("UpdateHistory", result);
                    final JSONObject json = new JSONObject(result);

                    if (json.getInt("status") == 401)
                    {
                        stopService(new Intent(mContext, LocationUpdate.class));
                        MainActivity.alertView_with_activity(mContext, "Authentication Failed", "Please contact ZUVER customer care for more details", "OK");
                    }
                    else
                    {
                        if (json.getInt("status") == 5)
                        {
                            Intent intent = new Intent(mContext, NotificationAct.class);
                            intent.putExtra("message", result);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
                                    | Intent.FLAG_ACTIVITY_NO_ANIMATION
                                    | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                            startActivity(intent);
                        }
                        else if (json.getInt("status") == 7 || json.getInt("status") == 10)
                        {
                            String cancelmsg = "";
                            generateNotification(mContext, json.getString("message"), HomeActivity.class);
                            cancelmsg = json.getString("message");
                            MainActivity.mMyStatus.setStatus("F");
                            SessionSave.saveSession("status", "F", mContext);
                            MainActivity.mMyStatus.settripId("");
                            SessionSave.saveSession("trip_id", "", mContext);
                            SessionSave.saveSession("travel_status", "", mContext);
                            MainActivity.mMyStatus.setOnstatus("");
                            MainActivity.mMyStatus.setOnPassengerImage("");
                            MainActivity.mMyStatus.setOnpassengerName("");
                            MainActivity.mMyStatus.setOndropLocation("");
                            MainActivity.mMyStatus.setOnpickupLatitude("");
                            MainActivity.mMyStatus.setOnpickupLongitude("");
                            MainActivity.mMyStatus.setOndropLatitude("");
                            MainActivity.mMyStatus.setOndropLongitude("");
                            MainActivity.mMyStatus.setOndriverLatitude("");
                            MainActivity.mMyStatus.setOndriverLongitude("");
                            Intent cancelIntent = new Intent();
                            Bundle bun = new Bundle();
                            bun.putString("message", cancelmsg);
                            cancelIntent.putExtras(bun);
                            cancelIntent.setAction(Intent.ACTION_MAIN);
                            cancelIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                            cancelIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                    | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET
                                    | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
                                    | Intent.FLAG_ACTIVITY_NO_ANIMATION
                                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                            ComponentName cn = new ComponentName(mContext, CanceltripAct.class);
                            cancelIntent.setComponent(cn);
                            startActivity(cancelIntent);

                        }
                        else if (json.getInt("status") == 1)
                        {
                            cLoctions = "";
                            SessionSave.saveSession("cLoctions", cLoctions, mContext);
                            SessionSave.saveSession("latitude", "", mContext);
                            SessionSave.saveSession("longitude", "", mContext);

                            if (SessionSave.getSession("status", mContext).equals("A"))
                            {
                                if (json.has("distance"))
                                {
                                    CommonData.travel_km = json.getDouble("distance");
                                }
                            }
                            else
                            {
                                CommonData.travel_km = 0;
                            }
                            if (NetworkStatus.isOnline(mContext))
                            {
                                getCurrentAddress(CommonData.getlatitude, CommonData.getlongitude);
                            }
                        }
                        else
                        {
                            sLocation = SessionSave.getSession("cLoctions", mContext) + sLocation;
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.e("Error", e.toString());
                }
            }
        }
    }

    public String getPostDataString(JSONObject params) throws Exception
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext())
        {
            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));
        }

        return result.toString();
    }


    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    @Override
    public IBinder onBind(final Intent intent) {
        return null;
    }


    /**
     * Starting the location updates
     */
    protected void startLocationUpdates()
    {
        try
        {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates()
    {
        try
        {
            if (mGoogleApiClient != null)
            {
                if (mGoogleApiClient.isConnected())
                    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void onDestroy()
    {
        stopLocationUpdates();
        mTimer.cancel();
        super.onDestroy();
    }

    @Override
    public void onConnectionFailed(final ConnectionResult arg0)
    {

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

    public void getCurrentAddress(double lat, double lon)
    {
        Geocoder geocoder;
        List<Address> addresses = null;
        String address = "";
        String city = "";
        String country = "";
        geocoder = new Geocoder(this, Locale.getDefault());

        try
        {
            if (Geocoder.isPresent())
            {
                addresses = geocoder.getFromLocation(lat, lon, 1);
                address = addresses.get(0).getAddressLine(0);
                city = addresses.get(0).getAddressLine(1);
                country = addresses.get(0).getAddressLine(2);
                SessionSave.saveSession("Driver_locations_home", "" + address + "\n" + city + "\n" + country, mContext);
            }
            else
            {
                new MainActivity.getDropLocation(this).execute(lat, lon);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnected(final Bundle connectionHint)
    {
        try
        {
            startLocationUpdates();
            mLastLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);

            if (mLastLocation != null)
            {
                latitude1 = mLastLocation.getLatitude();
                longitude1 = mLastLocation.getLongitude();
                c_lat = String.valueOf(latitude1);
                c_lon = String.valueOf(longitude1);
                CommonData.getlatitude = latitude1;
                CommonData.getlongitude = longitude1;
            }
            else
            {
                mhandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Location loc = getLastKnownLoaction(true);

                        if (loc != null)
                        {
                            latitude1 = loc.getLatitude();
                            longitude1 = loc.getLongitude();
                            c_lat = String.valueOf(latitude1);
                            c_lon = String.valueOf(longitude1);
                            CommonData.getlatitude = latitude1;
                            CommonData.getlongitude = longitude1;
                        }

                        Toast.makeText(mContext, "GETTING GPS FAILED", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
        catch (final Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(final Location location)
    {
        if (location != null)
        {
            // for showing current location name
            CommonData.getlatitude = location.getLatitude();
            CommonData.getlongitude = location.getLongitude();
            latitude1 = location.getLatitude();
            longitude1 = location.getLongitude();
            c_lat = String.valueOf(latitude1);
            c_lon = String.valueOf(longitude1);

            SessionSave.saveSession("latitude", c_lat, mContext);
            SessionSave.saveSession("longitude", c_lon, mContext);
            sLocation += String.valueOf(latitude1) + "," + String.valueOf(longitude1) + "|";
            String latlng = "" + latitude1 + "," + longitude1 + "|";
            _speed = location.getSpeed();
            CommonData.currentspeed = String.valueOf(roundDecimal(convertSpeed(_speed), 2));

            if (SessionSave.getSession("travel_status", mContext).equals("2") && _speed > 0)
            {
                objLocationDb.insert_locations(SessionSave.getSession("trip_id", mContext), latlng, "");
            }
        }
    }

    private boolean servicesConnected()
    {
        final int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);

        if (ConnectionResult.SUCCESS == resultCode)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void generateNotification(Context context, String message, Class<?> class1)
    {
        // Prepare intent which is triggered if the
        // notification is selected
        Intent intent = new Intent(this, class1);
        PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);

        String title = context.getString(R.string.app_name);
        Notification noti = null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
        {
            noti = new Notification.Builder(this)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setContentIntent(pIntent)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .build();
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        noti.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(Notification_ID, noti);
        Uri notification1 = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        try
        {
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification1);
            r.play();
        }
        catch (NullPointerException ex)
        {
            ex.printStackTrace();
        }
    }


    private double convertSpeed(double speed) {
        return ((speed * 3600) * 0.001);
    }

    private double roundDecimal(double value, final int decimalPlace)
    {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(decimalPlace, RoundingMode.HALF_UP);
        value = bd.doubleValue();
        return value;
    }

    // This method for creating txt file and store the location in locally.
    @SuppressLint("SdCardPath")
    public void write(String tripid, String locations)
    {
        try
        {
            String fpath = "/sdcard/trip_" + tripid + ".txt";
            File file = new File(fpath);

            // If file does not exists, then create it
            if (!file.exists())
            {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.append(locations);
            bw.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    // check last known location from location provider
    private Location getLastKnownLoaction(boolean enabledProvidersOnly)
    {
        LocationManager manager = (LocationManager) getApplicationContext()
                .getSystemService(Context.LOCATION_SERVICE);
        Location location = null;
        List<String> providers = manager.getProviders(enabledProvidersOnly);

        for (String provider : providers)
        {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {

            }

            location = manager.getLastKnownLocation(provider);
            // may be try adding some Criteria here
            if (location != null)
            {
                return location;
            }
        }

        // at this point we've done all we can and no location is returned
        return null;
    }
}