package com.zuver.driver.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.view.View;

import com.zuver.driver.R;
import com.zuver.driver.interfaces.APIResult;

/**
 * @author developer This AsyncTask used to communicate the application with server through HTTP connection. Here the request and response completely in JSON format. Constructor get the input details JSONObject,POST or GET then url. In pre execute,Show the progress dialog. In Background,Connect the and get the response. In Post execute, Return the result with interface.
 */
public class APIService_HTTP_JSON extends AsyncTask<String, String, String>
{
	private final Context mContext;
	private boolean isSuccess = true;
	private boolean isGetMethod = true;
	public Dialog mProgressdialog;
	private final APIResult response;
	InputStream inputStream = null;
	private JSONObject j;
	private String uri;

	public APIService_HTTP_JSON(Context context, APIResult response, JSONObject j, boolean isGetMethod, String uri)
	{
		mContext = context;
		this.response = response;
		this.j = j;
		this.isGetMethod = isGetMethod;
		this.uri = uri;
	}

	@Override
	protected void onPreExecute()
	{
		super.onPreExecute();
		View view = View.inflate(mContext, R.layout.progress_bar, null);
		mProgressdialog = new Dialog(mContext, R.style.dialogwinddow);
		mProgressdialog.setContentView(view);
		mProgressdialog.setCancelable(false);
		mProgressdialog.show();
	}

	@Override
	protected String doInBackground(String... params)
	{
		String result = "";
		String jsonString = "";
		Log.d("Driver Api request",""+uri+"\n data"+j);
		try
		{
			if (isGetMethod)
			{
				HttpResponse response;
				HttpClient myClient = new DefaultHttpClient();
				HttpPost myConnection = new HttpPost(uri);
				try
				{
					response = myClient.execute(myConnection);
					result = EntityUtils.toString(response.getEntity(), "UTF-8");
				}
				catch (ClientProtocolException e)
				{
					e.printStackTrace();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost(uri);
				jsonString = j.toString();
				StringEntity se = new StringEntity(jsonString);
				httpPost.setEntity(se);
				httpPost.setHeader("Content-type", "application/x-www-form-urlencoded;charset=UTF-8");
				HttpResponse httpResponse = httpclient.execute(httpPost);
				inputStream = httpResponse.getEntity().getContent();
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";
			}
			System.out.println("Driver Api response"+result);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}

	@Override
	protected void onPostExecute(String result)
	{
		super.onPostExecute(result);
		try
		{
			if (mProgressdialog.isShowing())
				mProgressdialog.dismiss();
			response.getResult(isSuccess, Html.fromHtml(result).toString());
		}
		catch (Exception e)
		{
			if (mProgressdialog.isShowing())
				mProgressdialog.dismiss();
			Log.i("Exception", e.toString());
		}
	}

	private static String convertInputStreamToString(InputStream inputStream) throws IOException
	{
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;
		inputStream.close();
		return result;
	}
}