package com.zuver.driver.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONObject;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.zuver.driver.MainActivity;
import com.zuver.driver.data.CommonData;
import com.zuver.driver.utils.LocationDb;
import com.zuver.driver.utils.SessionSave;

import javax.net.ssl.HttpsURLConnection;

/**
 * This class helps to get the driver current location using location client. It
 * Keep on updating driver location to server with certain time interval (Every
 * 5sec). In this class,Driver gets the new request notification and trip cancel
 * notifications.
 */
@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class CurrentLocationUpdateClass extends Service implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener
{
    private final Timer mTimer = new Timer();
    public static double latitude1 = 0.0;
    public static double longitude1 = 0.0;

    String c_lat = "", c_lon = "";
    private String sLocation = "", cLoctions = "";
    int locationUpdateOUT = 720000;
    private Context mContext;
    private Handler mhandler;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private static int Notification_ID = 1;
    LocationDb objLocationDb;
    float _speed;
    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 50000; // 5 min
    private static int FATEST_INTERVAL = 10000; // 10 min
    private static float DISPLACEMENT = 0f; // 10 meters
    private Location mLastLocation;

    // private static Activity actContext;
    @Override
    public void onStart(final Intent intent, final int startId)
    {
        super.onStart(intent, startId);

        if (mGoogleApiClient != null)
        {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        mContext = this;
        mhandler = new Handler(Looper.getMainLooper());
        objLocationDb = new LocationDb(CurrentLocationUpdateClass.this);
        mTimer.scheduleAtFixedRate(new getRideUpdates(), 0, locationUpdateOUT);

        if (servicesConnected())
        {
            buildGoogleApiClient();
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(UPDATE_INTERVAL);
            mLocationRequest.setFastestInterval(FATEST_INTERVAL);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
        }
    }

    private class getRideUpdates extends TimerTask
    {
        @Override
        public void run()
        {
            try
            {
                Location loc = getLastKnownLoaction(true);

                if (loc != null)
                {
                    latitude1 = loc.getLatitude();
                    longitude1 = loc.getLongitude();
                    c_lat = String.valueOf(loc.getLatitude());
                    c_lon = String.valueOf(loc.getLongitude());
                    CommonData.getlatitude = latitude1;
                    CommonData.getlongitude = longitude1;
                    sLocation = "" + latitude1 + "," + longitude1 + "|";
                }

                sLocation = "" + latitude1 + "," + longitude1 + "|";
                SessionSave.saveSession("latitude", c_lat, mContext);
                SessionSave.saveSession("longitude", c_lon, mContext);
                cLoctions = sLocation;
                SessionSave.saveSession("cLoctions", cLoctions, mContext);
                sLocation = "";

                try
                {
                    JSONObject j = new JSONObject();
                    j.put("driver_id", SessionSave.getSession("Id", mContext));
                    j.put("trip_id", SessionSave.getSession("trip_id", mContext));
                    j.put("longitude", SessionSave.getSession("longitude", mContext));
                    j.put("latitude", SessionSave.getSession("latitude", mContext));
                    j.put("status", SessionSave.getSession("status", mContext));
                    j.put("travel_status", SessionSave.getSession("travel_status", mContext));
                    String url = CommonData.MAIN_URL + "driver_location_history";
                    new getRideUpdate(url, j).execute();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    private class getRideUpdate extends AsyncTask<String, Void, String>
    {
        private JSONObject mInputData;
        private String mUrl;

        private getRideUpdate(String url, JSONObject data)
        {
            this.mInputData = data;
            this.mUrl = url;
        }

        protected String doInBackground(String... arg0)
        {
            try
            {
                URL url = new URL(mUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);//milliseconds
                conn.setConnectTimeout(15000);//milliseconds
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
                conn.setRequestProperty("authKey", "" + SessionSave.getSession("Auth_Key", mContext));
                conn.setRequestProperty("userType", "" + SessionSave.getSession("User_Type", mContext));
                conn.setRequestProperty("userId", "" + SessionSave.getSession("Id", mContext));
                conn.setDoInput(true);
                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(this.mInputData));
                writer.flush();
                writer.close();
                os.close();
                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK)
                {
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(
                                    conn.getInputStream()));
                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null)
                    {
                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            if (result != null && !result.equalsIgnoreCase(""))
            {
                try
                {
                    JSONObject json = new JSONObject(result);
                    Log.e("OfflineMode", result);

                    if (json.getInt("status") == 401)
                    {
                        stopService(new Intent(CurrentLocationUpdateClass.this, CurrentLocationUpdateClass.class));
                        MainActivity.alertView_with_activity(CurrentLocationUpdateClass.this, "Authentication Failed", "Please contact ZUVER customer care for more details", "OK");
                    }
                }
                catch (Exception e)
                {
                    Log.e("Error", e.toString());
                    e.printStackTrace();
                }
            }
        }
    }

    public String getPostDataString(JSONObject params) throws Exception
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext())
        {
            String key = itr.next();
            Object value = params.get(key);

            if (first)
            {
                first = false;
            }
            else
            {
                result.append("&");
            }

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));
        }

        return result.toString();
    }

    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    @Override
    public IBinder onBind(final Intent intent) {
        return null;
    }

    /**
     * Starting the location updates
     */
    protected void startLocationUpdates()
    {
        try
        {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates()
    {
        try
        {
            if (mGoogleApiClient != null)
            {
                if (mGoogleApiClient.isConnected())
                {
                    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy()
    {
        stopLocationUpdates();
        mTimer.cancel();
        super.onDestroy();
    }

    @Override
    public void onConnectionFailed(final ConnectionResult arg0)
    {

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(final Bundle connectionHint)
    {
        try
        {
            startLocationUpdates();
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (mLastLocation != null)
            {
                latitude1 = mLastLocation.getLatitude();
                longitude1 = mLastLocation.getLongitude();
                c_lat = String.valueOf(latitude1);
                c_lon = String.valueOf(longitude1);
                // for showing current location name
                CommonData.getlatitude = latitude1;
                CommonData.getlongitude = longitude1;
            }
            else
            {
                mhandler.post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Location loc = getLastKnownLoaction(true);

                        if (loc != null)
                        {
                            latitude1 = loc.getLatitude();
                            longitude1 = loc.getLongitude();
                            c_lat = String.valueOf(latitude1);
                            c_lon = String.valueOf(longitude1);
                            CommonData.getlatitude = latitude1;
                            CommonData.getlongitude = longitude1;
                        }

                        Toast.makeText(CurrentLocationUpdateClass.this, "GETTING GPS FAILED", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
        catch (final Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(final Location location)
    {
        if (location != null)
        {
            // for showing current location name
            CommonData.getlatitude = location.getLatitude();
            CommonData.getlongitude = location.getLongitude();
            latitude1 = location.getLatitude();
            longitude1 = location.getLongitude();
            c_lat = String.valueOf(latitude1);
            c_lon = String.valueOf(longitude1);

            SessionSave.saveSession("latitude", c_lat, CurrentLocationUpdateClass.this);
            SessionSave.saveSession("longitude", c_lon, CurrentLocationUpdateClass.this);
            sLocation += String.valueOf(latitude1) + "," + String.valueOf(longitude1) + "|";
            String latlng = "" + latitude1 + "," + longitude1 + "|";
            _speed = location.getSpeed();
            CommonData.currentspeed = String.valueOf(roundDecimal(convertSpeed(_speed), 2));

            if (SessionSave.getSession("travel_status", CurrentLocationUpdateClass.this).equals("2") && _speed > 0)
            {
                objLocationDb.insert_locations(SessionSave.getSession("trip_id", CurrentLocationUpdateClass.this), latlng, "");
            }
        }
    }

    private boolean servicesConnected()
    {
        final int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (ConnectionResult.SUCCESS == resultCode)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private double convertSpeed(double speed) {
        return ((speed * 3600) * 0.001);
    }

    private double roundDecimal(double value, final int decimalPlace)
    {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(decimalPlace, RoundingMode.HALF_UP);
        value = bd.doubleValue();
        return value;
    }

    // This method for creating txt file and store the location in locally.
    @SuppressLint("SdCardPath")
    public void write(String tripid, String locations)
    {
        try
        {
            String fpath = "/sdcard/trip_" + tripid + ".txt";
            File file = new File(fpath);

            // If file does not exists, then create it
            if (!file.exists())
            {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.append(locations);
            bw.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    // check last known location from location provider
    private Location getLastKnownLoaction(boolean enabledProvidersOnly)
    {
        LocationManager manager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        Location location = null;
        List<String> providers = manager.getProviders(enabledProvidersOnly);

        for (String provider : providers)
        {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {

            }

            location = manager.getLastKnownLocation(provider);

            if (location != null)
            {
                return location;
            }
        }

        return null;
    }
}