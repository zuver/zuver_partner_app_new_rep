package com.zuver.driver;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class SignAct extends MainActivity {


    private TextView txtSignIn;

    @Override
    public int setLayout() {
        // TODO Auto-generated method stub
        return R.layout.login;
    }

    @Override
    public void Initialize() {
        // TODO Auto-generated method stub


        txtSignIn = (TextView) findViewById(R.id.txt_sign_in);

        txtSignIn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(SignAct.this, UserLoginAct.class);
                startActivity(intent);
                finish();
            }
        });

    }

}
