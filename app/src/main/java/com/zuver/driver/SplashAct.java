package com.zuver.driver;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings.Secure;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.zuver.driver.data.CommonData;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.service.APIService_Volley_JSON;
import com.zuver.driver.utils.FontHelper;
import com.zuver.driver.utils.LocationDb;
import com.zuver.driver.utils.SessionSave;

import org.json.JSONObject;

public class SplashAct extends MainActivity
{
    private String mDeviceid;
    Button langEn, langRu;
    private LocationManager mLocationManager;
    private boolean isGPSEnabled;
    public static Dialog mProgressdialog;
    private boolean isLocationEnabled = false;
    LocationDb objLocationDb;
    private TextView buildnoTxt;
    private String VersionNo = "";
    private AlertDialog al;
    private String currentVersion = "", latestVersion = "";
    private static final int PERMISSION_REQUEST_CODE = 1;

    @Override
    public int setLayout()
    {
        // TODO Auto-generated method stub
        return R.layout.splash_lay;
    }

    @SuppressLint("NewApi")
    @Override
    public void Initialize()
    {
        // TODO Auto-generated method stub
        FontHelper.applyFont(this, findViewById(R.id.rootlay), "DroidSans.ttf");
        objLocationDb = new LocationDb(SplashAct.this);
        buildnoTxt = (TextView) findViewById(R.id.buildnoTxt);
        mDeviceid = Secure.getString(SplashAct.this.getContentResolver(), Secure.ANDROID_ID);

        CommonData.current_act = "SplashAct";
        langEn = (Button) findViewById(R.id.english);
        langRu = (Button) findViewById(R.id.russian);
        CommonData.mDevice_id = mDeviceid;

        if (Build.VERSION.SDK_INT >= 23)
        {
            if (isOnline())
            {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                {
                    ActivityCompat.requestPermissions(SplashAct.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
                }
                else
                {
                    isLocationEnabled = true;
                }
            }
        }
        if (SessionSave.getSession("Lang", SplashAct.this).equals(""))
        {
            SessionSave.saveSession("Lang", "en", SplashAct.this);
            SessionSave.saveSession("Lang_Country", "en_GB", SplashAct.this);
        }
        try
        {
            VersionNo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName;
            currentVersion = VersionNo;
            buildnoTxt.setText(getResources().getString(R.string.app_buildversion) + ":" + VersionNo);
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        if (Build.VERSION.SDK_INT >= 23)
        {
            if (isOnline())
            {
                if (isLocationEnabled)
                {
                    if (isGpsEnabled(SplashAct.this))
                    {
                        new CoreConfigCall();
                    }
                    else
                    {
                        gpsalert(SplashAct.this, false);
                    }
                }
            }
            else
            {
                isConnect(SplashAct.this, false);
            }
        }
        else
        {
            if (isOnline())
            {
                if (isGpsEnabled(SplashAct.this))
                {
                    new CoreConfigCall();
                }
                else
                {
                    gpsalert(SplashAct.this, false);
                }
            }
            else
            {
                MainActivity.isConnect(SplashAct.this, false);
            }
        }
    }

    private void showUpdateDialog()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("A New Update is Available");

        builder.setPositiveButton("Update", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.zuver.driver")));
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Skip", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                LaunchApp();
                dialog.dismiss();
            }
        });

        builder.setCancelable(false);
        al = builder.create();
        al.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        switch (requestCode)
        {
            case PERMISSION_REQUEST_CODE:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    isLocationEnabled = true;
                }
                else
                {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) &&
                            (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)))
                    {
                        showDialogOK("Location Services Permission required for this app",
                                new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which)
                                    {
                                        switch (which)
                                        {
                                            case DialogInterface.BUTTON_POSITIVE:
                                                ActivityCompat.requestPermissions(SplashAct.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
                                                break;
                                        }
                                    }
                                });
                    }
                    else
                    {
                        Toast.makeText(this, "Go to settings and enable permissions", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener)
    {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .create()
                .show();
    }

    private class CoreConfigCall implements APIResult
    {
        private CoreConfigCall()
        {
            // TODO Auto-generated constructor stub
            final String url = "getcoreconfig";
            new APIService_Volley_JSON(SplashAct.this, this, null, false).execute(url);
        }

        @Override
        public void getResult(final boolean isSuccess, final String result)
        {
            // TODO Auto-generated method stub
            if (isSuccess)
            {
                try
                {
                    final JSONObject json = new JSONObject(result);

                    if (json.getInt("status") == 1)
                    {
                        JSONObject data = new JSONObject(json.getString("detail"));
                        SessionSave.saveSession("noimage_base", data.getString("noimage_base"), getApplicationContext());
                        SessionSave.saveSession("site_currency", data.getString("site_currency"), getApplicationContext());
                        SessionSave.saveSession("invite_txt", data.getString("aboutpage_description"), getApplicationContext());
                        SessionSave.saveSession("Metric", data.getString("metric"), SplashAct.this);

                        if (data.has("driver_app_ver"))
                        {
                            latestVersion = data.getString("driver_app_ver");
                        }

                        if (currentVersion != null && !currentVersion.equalsIgnoreCase("")
                                && latestVersion != null && !latestVersion.equalsIgnoreCase(""))
                        {
                            Log.e("CV and NV", currentVersion + " " + latestVersion);

                            if (!currentVersion.equalsIgnoreCase(latestVersion))
                            {
                                if (al != null && al.isShowing())
                                {
                                    al.dismiss();
                                }

                                showUpdateDialog();
                            }
                            else
                            {
                                Log.d("SplashActivity", "going to right place");
                                LaunchApp();
                            }
                        }
                        else
                        {
                            Log.d("SplashActivity", "going to right place");
                            LaunchApp();
                        }
                    }
                    else if (json.getInt("status") == -1)
                    {
                        Log.d("SplashActivity", "Status from server is: " + json.getInt("status"));
                        //User has been logged out
                        askForLogin();
                    }
                    else
                    {
                        Log.e("WebService Status : ", json.getString("status"));
                        ShowToast(SplashAct.this, "Web Service Response Issue");
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    Log.e("WebService Exception : ", e.toString());
                    ShowToast(SplashAct.this, "Web Service : " + e.toString());
                    //LaunchApp();
                }
            }
            else
            {
                Log.e("WebService Status : ", result);
                ShowToast(SplashAct.this, result);
            }
        }
    }

    void LaunchApp()
    {
        Intent i;

        if (SessionSave.getSession("Phone", SplashAct.this).equals(""))
        {
            Log.d("Splash Activity", "Phone from local storage is: " + SessionSave.getSession("Phone", SplashAct.this));
            i = new Intent(SplashAct.this, SignAct.class);
            startActivity(i);
            finish();
            CommonData.Animation1(SplashAct.this);
        }
        else
        {
            if (SessionSave.getSession("trip_id", SplashAct.this).equals(""))
            {
                Log.d("Splash Activity", "trip id from session is: " + SessionSave.getSession("trip_id", SplashAct.this));
                showLoading(SplashAct.this);
                i = new Intent(SplashAct.this, DashAct.class);
                startActivity(i);
                finish();
                dismissLoading();
                CommonData.Animation1(SplashAct.this);
            }
            else
            {
                Log.d("Splash Activity", "trip id from session is: " + SessionSave.getSession("trip_id", SplashAct.this));
                i = new Intent(SplashAct.this, OngoingAct.class);
                startActivity(i);
                finish();
                CommonData.Animation1(SplashAct.this);
            }
        }
    }

    @Override
    protected void onStart()
    {
        // TODO Auto-generated method stub
        super.onStart();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            isLocationEnabled = true;
        }
        else
        {
            isLocationEnabled = false;
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }
}
