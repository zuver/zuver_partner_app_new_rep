package com.zuver.driver;

import java.util.List;
import java.util.Locale;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.zuver.driver.data.CommonData;
import com.zuver.driver.data.MapWrapperLayout;
import com.zuver.driver.interfaces.APIResult;
import com.zuver.driver.service.APIService_Volley_JSON;
import com.zuver.driver.service.CurrentLocationUpdateClass;
import com.zuver.driver.service.NonActivity;
import com.zuver.driver.utils.FontHelper;
import com.zuver.driver.utils.SessionSave;


/**
 * This the main class of the driver applications.
 * Here we have more options in driver like menu, changing
 * shifts and the statistics of the driver.
 *
 * @author developer
 */
public class DashAct extends MainActivity implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener
{
    private CheckBox chkShift;
    public static GoogleMap googleMap;
    private int mapstatus;
    private MapWrapperLayout mapWrapperLayout;
    private Marker c_marker;
    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 30000; // 30 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 0; // 10 meters
    private Location mLastLocation;
    private LocationRequest mLocationRequest;
    private String mLatitude = "", mLongitude = "";
    private GoogleApiClient mGoogleApiClient;

    private boolean doubleBackToExitPressedOnce = false;

    LatLng coordinate;
    String checked = "OUT";
    LinearLayout driver_profile, driver_ongoing, my_drive, dashboard, parentSlideLay;
    ImageView driver_image, ongoing_img, mydriv_img, dash_img, img_edit;
    LinearLayout menu_img;
    TextView txt_driver_profile_name, txt_driver_profile_number, txt_ongoing, txt_mydrive, txt_dash;
    NonActivity nonactiityobj = new NonActivity();

    LinearLayout layHome;
    TextView txtHome, tvCancel;
    ImageView imgHome;

    private DisplayImageOptions options;

    @Override
    public int setLayout()
    {
        // TODO Auto-generated method stub
        mapstatus = 0;
        setLocale();
        return R.layout.dashact_lay;
    }

    @Override
    public void Initialize()
    {
        // TODO Auto-generated method stub
        CommonData.sContext = this;
        mapstatus = 0;
        CommonData.current_act = "MyStatus";
        FontHelper.applyFont(this, findViewById(R.id.mystatus_layout), "DroidSans.ttf");

        if (servicesConnected())
        {
            // Building the GoogleApi client
            buildGoogleApiClient();
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(UPDATE_INTERVAL);
            mLocationRequest.setFastestInterval(FATEST_INTERVAL);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
        }

        try
        {
            GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
            final SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            googleMap = mapFrag.getMap();
            MapsInitializer.initialize(DashAct.this);
            mapWrapperLayout = (MapWrapperLayout) findViewById(R.id.map_relative_layout);
            mapWrapperLayout.init(googleMap, getPixelsFromDp(DashAct.this, 39 + 20));
            // System.gc();
            googleMap.setMyLocationEnabled(true);
            // Find myLocationButton view
            View myLocationButton = mapFrag.getView().findViewById(0x2);

            if (myLocationButton != null && myLocationButton.getLayoutParams() instanceof RelativeLayout.LayoutParams)
            {
                // ZoomControl is inside of RelativeLayout
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) myLocationButton.getLayoutParams();
                // Align it to - parent BOTTOM|LEFT
                params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
                params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);

                // Update margins, set to 10dp
                final int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());
                params.setMargins(margin, margin, margin, margin);
                myLocationButton.setLayoutParams(params);
            }
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }

        driver_profile = (LinearLayout) findViewById(R.id.driver_profile_lay);
        driver_ongoing = (LinearLayout) findViewById(R.id.ongoing_lay);
        my_drive = (LinearLayout) findViewById(R.id.mydrive_lay);
        dashboard = (LinearLayout) findViewById(R.id.dashboard_lay);
        parentSlideLay = (LinearLayout) findViewById(R.id.driver_profile_parent);

        driver_image = (ImageView) findViewById(R.id.dash_image);
        ongoing_img = (ImageView) findViewById(R.id.img_ongoing);
        mydriv_img = (ImageView) findViewById(R.id.img_mydrive);
        dash_img = (ImageView) findViewById(R.id.dash_img);
        tvCancel = (TextView) findViewById(R.id.tvCancel);

        options = new DisplayImageOptions.Builder().showImageOnLoading(null) // resource
                // or
                // drawable
                .showImageForEmptyUri(null) // resource or drawable
                .showImageOnFail(null) // resource or drawable
                .resetViewBeforeLoading(false) // default
                .delayBeforeLoading(1000).cacheInMemory(true) // default
                .cacheOnDisc(true) // default
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED) // default
                .bitmapConfig(Bitmap.Config.RGB_565) // default
                .displayer(new SimpleBitmapDisplayer()) // default
                .handler(new Handler()) // default
                // .showImageOnLoading(R.drawable.ic_stub)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).defaultDisplayImageOptions(options).build();
        ImageLoader.getInstance().init(config);
        String Image_path = SessionSave.getSession("driver_profileimage", DashAct.this);

        if (Image_path != null && Image_path.length() > 0)
        {
            ImageLoader.getInstance().displayImage(Image_path, driver_image, options);
        }

        menu_img = (LinearLayout) findViewById(R.id.menu_img);
        txt_driver_profile_name = (TextView) findViewById(R.id.txt_driver_profile_name);
        txt_driver_profile_number = (TextView) findViewById(R.id.txt_driver_profile_number);
        txt_ongoing = (TextView) findViewById(R.id.txt_ongoing);
        txt_mydrive = (TextView) findViewById(R.id.txt_mydrive);
        txt_dash = (TextView) findViewById(R.id.txt_dash);

        layHome = (LinearLayout) findViewById(R.id.home_lay);
        imgHome = (ImageView) findViewById(R.id.home_img);
        txtHome = (TextView) findViewById(R.id.txt_home);

        nonactiityobj.stopServicefromNonActivity(DashAct.this);
        chkShift = (CheckBox) findViewById(R.id.chkshift);

        txt_driver_profile_name.setText(SessionSave.getSession("Name", DashAct.this));
        txt_driver_profile_number.setText(SessionSave.getSession("Phone", DashAct.this));
        Log.e("DashAct: ", "DashAct: " + SessionSave.getSession("trip_id", DashAct.this) + " : " + SessionSave.getSession("status", DashAct.this));

        if (!SessionSave.getSession("trip_id", DashAct.this).equals(""))
        {
            driver_ongoing.setVisibility(View.VISIBLE);
        }
        else
        {
            driver_ongoing.setVisibility(View.GONE);
        }

        chkShift.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                chkShift.setClickable(false);
                new RequestingCheckBox();
            }
        });
    }

    // Method for check the google service available or not.
    private boolean servicesConnected()
    {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (ConnectionResult.SUCCESS == resultCode)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // This click method is used for accessing the menu from all the activity.
    public void clickMethod(View v)
    {
        Intent i;

        switch (v.getId())
        {
            case R.id.driver_profile_lay:

                showLoading(DashAct.this);
                driver_profile.setBackgroundResource(R.color.lightgrey);

                i = new Intent(this, MeAct.class);
                startActivity(i);
                finish();

                break;

            case R.id.ongoing_lay:

                Log.e("OnGOing ", "OnGOing " + SessionSave.getSession("status", DashAct.this));
                showLoading(DashAct.this);
                driver_ongoing.setBackgroundResource(R.color.gray);
                txt_ongoing.setTextColor(getResources().getColor(R.color.white));
                ongoing_img.setImageResource(R.drawable.ongoing_drive_focus);

                if (SessionSave.getSession("status", DashAct.this).equals("A") || SessionSave.getSession("status", DashAct.this).equals("B"))
                {
                    Intent intentOnGoing = new Intent(DashAct.this, OngoingAct.class);
                    startActivity(intentOnGoing);
                    finish();
                }

                break;

            case R.id.mydrive_lay:

                showLoading(DashAct.this);
                my_drive.setBackgroundResource(R.color.gray);
                txt_mydrive.setTextColor(getResources().getColor(R.color.white));
                mydriv_img.setImageResource(R.drawable.mydriver_focus);

                dismissLoading();
                i = new Intent(this, JobsAct.class);
                startActivity(i);
                finish();
                //logout(DashAct.this);

                break;

            case R.id.dashboard_lay:

                //showLoading(DashAct.this);
                dashboard.setBackgroundResource(R.color.gray);
                txt_dash.setTextColor(getResources().getColor(R.color.white));
                dash_img.setImageResource(R.drawable.dashboard_focus);
                // dashboard.setBackgroundColor(getResources().getColor(R.id.li))
                i = new Intent(this, HomeActivity.class);
                startActivity(i);
                finish();

                break;

            case R.id.home_lay:

                parentSlideLay.setVisibility(View.GONE);

                break;

            case R.id.menu_img:

                if (parentSlideLay.getVisibility() == View.VISIBLE)
                {
                    parentSlideLay.setVisibility(View.GONE);
                }
                else
                {
                    parentSlideLay.setVisibility(View.VISIBLE);
                }

                break;

            default:
                break;
        }
    }

    /**
     * Starting the location updates
     */
    protected void startLocationUpdates()
    {
        try
        {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates()
    {
        try
        {
            if (mGoogleApiClient != null)
            {
                if (mGoogleApiClient.isConnected())
                {
                    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                    mGoogleApiClient.disconnect();
                }
            }
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    // Get the google map pixels from xml density independent pixel.
    public static int getPixelsFromDp(final Context context, final float dp)
    {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    @Override
    public void onStart()
    {
        super.onStart();

        try
        {
            if (mGoogleApiClient != null)
            {
                mGoogleApiClient.connect();
                JSONObject j = new JSONObject();
                j.put("driver_id", SessionSave.getSession("Id", DashAct.this));
                Log.d("driver_id", "driver id from session is: " + SessionSave.getSession("Id", DashAct.this));
                String url = "driver_current_status";
                new getDriverCurrentStatus(url, j);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    //region Added By Amit Jangid

    /**
     * Added by Amit Jangid
     * Getting Current Shift Status of the driver
     * This method checks with the server the login status
     * and shift status using this API : driver_current_status
    **/
    private class getDriverCurrentStatus implements APIResult
    {
        private getDriverCurrentStatus(String url, JSONObject data)
        {
            try
            {
                /// checking is user is connected to internet or not
                if (isOnline())
                {
                    /// user is connected to internet
                    /// making api call using volley
                    new  APIService_Volley_JSON(DashAct.this, this, data, false).execute(url);
                }
                else
                {
                    /// user is not connected to internet
                    /// show message to check internet connection
                    alert_view(
                            DashAct.this,
                            "" + getResources().getString(R.string.message),
                            ""
                                    + getResources().getString(
                                    R.string.check_net_connection), ""
                                    + getResources().getString(R.string.ok), "");

                    /// setting the check box for shift status of user
                    if (chkShift.isChecked())
                    {
                        chkShift.setChecked(true);
                    }
                    else
                    {
                        chkShift.setChecked(false);
                    }
                }
            }
            catch (Exception e)
            {
                Log.d("Exception", "while getting profile data in dashboard activity:\n");
                e.printStackTrace();
            }
        }

        @Override
        public void getResult(boolean isSuccess, String result)
        {
            try
            {
                if (isSuccess)
                {
                    JSONObject json = new JSONObject(result);

                    if (json.getInt("status") == 1)
                    {
                        JSONObject details = json.getJSONObject("detail");
                        String shift_status = details.getString("dutyStatus");
                        String loginStatus = details.getString("loginStatus");

                        ///Log.d("loginStatus", "login status from server is: " + loginStatus);
                        ///Log.d("shift_status", "shift status from server is: " + shift_status);

                        if (loginStatus.equalsIgnoreCase("1"))
                        {
                            if (shift_status.equalsIgnoreCase("1"))
                            {
                                shift_status = "IN";
                            }
                            else
                            {
                                shift_status = "OUT";
                            }

                            SessionSave.saveSession("shift_status", shift_status, DashAct.this);

                            /**
                             * edited by Amit Jangid
                             * checking if  the status of the driver is 'IN'
                             * if shift is 'IN' then show "ON DUTY" on switch
                             * else show 'OFF DUTY'
                             * to update the location of the user
                             * first stopping the update location service
                             * then starting it again to change the time interval
                             **/
                            if (SessionSave.getSession("shift_status", DashAct.this).equalsIgnoreCase("IN"))
                            {
                                tvCancel.setText(getResources().getString(R.string.onduty));
                            }
                            else if (SessionSave.getSession("shift_status", DashAct.this).equalsIgnoreCase("OUT"))
                            {
                                tvCancel.setText(getResources().getString(R.string.off_duty));
                            }

                            if (SessionSave.getSession("shift_status", DashAct.this).equals("IN"))
                            {
                                chkShift.setChecked(true);
                                nonactiityobj.stopServicefromNonActivity(DashAct.this);
                                nonactiityobj.startServicefromNonActivity(DashAct.this);
                            }
                            else
                            {
                                chkShift.setChecked(false);
                                nonactiityobj.stopServicefromNonActivity(DashAct.this);
                                nonactiityobj.startServicefromNonActivity(DashAct.this);
                            }
                        }
                        else
                        {
                            askForLogin();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.d("Exception", "while getting profile data in dashboard activity:\n");
                e.printStackTrace();
            }
        }
    }

    //endregion Added by Amit Jangid

    /*
     *  This method is used to get the location
     *  corresponding latitude and longitude.
     */
    private void getCurrentAddress(double lat, double lon)
    {
        Geocoder geocoder;
        List<Address> addresses = null;
        String address = "";
        String city = "";
        String country = "";
        geocoder = new Geocoder(this, Locale.UK);

        try
        {
            addresses = geocoder.getFromLocation(lat, lon, 1);
            address = addresses.get(0).getAddressLine(0);
            city = addresses.get(0).getAddressLine(1);
            country = addresses.get(0).getAddressLine(2);
            LatLng coordinate = new LatLng(lat, lon);

            if (c_marker != null)
            {
                c_marker.remove();
            }

            c_marker = googleMap.addMarker(new MarkerOptions()
                    .position(coordinate)
                    .title("" + getResources().getString(
                            R.string.you_are_here))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.mystatus)));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location)
    {
        // TODO Auto-generated method stub
        if (location != null)
        {
            mLatitude = String.valueOf(location.getLatitude());
            mLongitude = String.valueOf(location.getLongitude());
            SessionSave.saveSession("mLatitude", mLatitude, this);
            SessionSave.saveSession("mLongitude", mLongitude, this);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult arg0)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void onConnected(Bundle arg0) {
        // TODO Auto-generated method stub

        try {
            startLocationUpdates();
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (servicesConnected()) {
                if (isOnline()) {
                    if (mLastLocation != null) {
                        coordinate = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                        mLatitude = String.valueOf(mLastLocation.getLatitude());
                        mLongitude = String.valueOf(mLastLocation.getLongitude());
                        SessionSave.saveSession("mLatitude", mLatitude, this);
                        SessionSave.saveSession("mLongitude", mLongitude, this);
                        getCurrentAddress(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 16));
                    }

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }


    @Override
    protected void onResume() {
        try {
            super.onResume();
            if (SessionSave.getSession("shift_status", DashAct.this).equalsIgnoreCase("IN"))
            {
                tvCancel.setText("" + getResources().getString(R.string.onduty));

            }
            else if (SessionSave.getSession("shift_status", DashAct.this).equalsIgnoreCase("OUT"))
            {
                tvCancel.setText("" + getResources().getString(R.string.off_duty));
            }

            if (SessionSave.getSession("shift_status", DashAct.this).equals("IN"))
            {
                chkShift.setChecked(true);
                nonactiityobj.stopServicefromNonActivity(DashAct.this);
                nonactiityobj.startServicefromNonActivity(DashAct.this);
            }
            else
            {
                chkShift.setChecked(false);
                nonactiityobj.stopServicefromNonActivity(DashAct.this);
                nonactiityobj.startServicefromNonActivity(DashAct.this);
            }

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    @Override
    protected void onDestroy() {
        try {
            stopLocationUpdates();
            googleMap = null;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        super.onDestroy();
    }

    /*
     *  This API request is to manage the shifts for the driver.
     */
    private class RequestingCheckBox implements APIResult
    {
        private RequestingCheckBox()
        {
            try
            {
                if (chkShift.isChecked())
                    checked = "IN";
                else
                    checked = "OUT";
                JSONObject j = new JSONObject();
                j.put("driver_id", SessionSave.getSession("Id", DashAct.this));
                j.put("shiftstatus", checked);
                j.put("latitude", mLatitude);
                j.put("longitude", mLongitude);
                String requestingCheckBox = "driver_shift";

                if (isOnline())
                    new APIService_Volley_JSON(DashAct.this, this, j, false).execute(requestingCheckBox);
                else
                {
                    /// edited by Amit Jangid
                    /// No internet connectivity
                    chkShift.setClickable(true);
                    alert_view(DashAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(
                            R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");

                    // revert to previous state
                    // if already on duty stay on duty
                    // else stay off duty
                    if (checked.equals("IN"))
                        chkShift.setChecked(false);
                    else {
                        chkShift.setChecked(true);
                    }
                }
            } catch (Exception e)
            {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        @Override
        public void getResult(boolean isSuccess, String result)
        {
            try
            {
                if (isSuccess)
                {
                    chkShift.setClickable(true);
                    JSONObject object = new JSONObject(result);

                    if (object.getInt("status") == 1)
                    {
                        alert_view(DashAct.this, "" + getResources().getString(R.string.message), "" + object.getString("message"), "" + getResources().getString(R.string.ok), "");
                        chkShift.setChecked(true);
                        SessionSave.saveSession("shift_status", "IN", DashAct.this);

                        if (SessionSave.getSession("shift_status", DashAct.this).equalsIgnoreCase("IN"))
                        {
                            tvCancel.setText(getResources().getString(R.string.onduty));
                        }
                        else if (SessionSave.getSession("shift_status", DashAct.this).equalsIgnoreCase("OUT"))
                        {
                            chkShift.setClickable(false);
                            tvCancel.setText(getResources().getString(R.string.off_duty));
                        }

                        Log.e("Shift Status", "" + SessionSave.getSession("shift_status", DashAct.this));

                        stopService(new Intent(DashAct.this, CurrentLocationUpdateClass.class));
                        nonactiityobj.stopServicefromNonActivity(DashAct.this);
                        nonactiityobj.startServicefromNonActivity(DashAct.this);
                    }
                    else if (object.getInt("status") == -1)
                    {
                        /// user is on trip should not to allowed to change the shift from on to off
                        alert_view(DashAct.this, "" + "Message", "" + object.get("message"), "" + getResources().getString(R.string.ok), "");

                        if (chkShift.isChecked())
                        {
                            chkShift.setChecked(false);
                        }
                        else
                        {
                            chkShift.setChecked(true);
                        }
                    }
                    else if (object.getInt("status") == 2)
                    {
                        alert_view(DashAct.this, "" + getResources().getString(R.string.message), "" + object.getString("message"), "" + getResources().getString(R.string.ok), "");
                        chkShift.setChecked(false);
                        SessionSave.saveSession("shift_status", "OUT", DashAct.this);
                        Log.e("Shift Status", "" + SessionSave.getSession("shift_status", DashAct.this));

                        if (SessionSave.getSession("shift_status", DashAct.this).equalsIgnoreCase("IN"))
                        {
                            tvCancel.setText(getResources().getString(R.string.onduty));
                        }
                        else if (SessionSave.getSession("shift_status", DashAct.this).equalsIgnoreCase("OUT"))
                        {
                            tvCancel.setText(getResources().getString(R.string.off_duty));
                        }

                        nonactiityobj.stopServicefromNonActivity(DashAct.this);
                        //startService(new Intent(DashAct.this, CurrentLocationUpdateClass.class));
                        stopService(new Intent(DashAct.this, CurrentLocationUpdateClass.class));
                        nonactiityobj.startServicefromNonActivity(DashAct.this);
                    }
                    else if (object.getInt("status") == -4)
                    {
                        alert_view(DashAct.this, "" + getResources().getString(R.string.message), "" + object.getString("message"), "" + getResources().getString(R.string.ok), "");
                        chkShift.setChecked(true);
                    }
                    else
                    {
                        alert_view(DashAct.this, "" + getResources().getString(R.string.message), "" + object.getString("message"), "" + getResources().getString(R.string.ok), "");

                        if (checked.equals("IN"))
                        {
                            chkShift.setChecked(false);
                        }
                        else
                        {
                            chkShift.setChecked(true);
                        }
                    }
                }
                else
                {
                    // error going here for check for internet connection
                    chkShift.setClickable(true);
                    alert_view(DashAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(
                            R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");

                    if (checked.equals("IN"))
                    {
                        chkShift.setChecked(false);
                    }
                    else
                    {
                        chkShift.setChecked(true);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                chkShift.setClickable(true);
                alert_view(DashAct.this, "" + getResources().getString(R.string.message), "" + getResources().getString(
                        R.string.check_net_connection), "" + getResources().getString(R.string.ok), "");

                if (checked.equals("IN"))
                {
                    chkShift.setChecked(false);
                }
                else
                {
                    chkShift.setChecked(true);
                }
            }
        }
    }

    @SuppressLint("InlinedApi")
    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "" + getResources().getString(
                R.string.please_click_back_again_exit), Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }
}
